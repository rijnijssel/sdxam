# Single sign-on

SDXam supports two external authentication providers, GitLab and SurfConext.

## Configuration

Both the GitLab and SurfConext integrations are configured through the `.env` file.

### GitLab

| Environment variable   | Description                                                                         |
| ---------------------- | ----------------------------------------------------------------------------------- |
| `GITLAB_ENABLED`       | Enable or disable GitLab login button                                               |
| `GITLAB_CLIENT_ID`     | Client ID from GitLab OAuth2 application                                            |
| `GITLAB_CLIENT_SECRET` | Client secret from GitLab OAuth2 application                                        |
| `GITLAB_INSTANCE_URI`  | Base URL of GitLab instance (example: `https://gitlab.com/`, note the trailing `/`) |

Example configuration:

```properties
GITLAB_ENABLED=true
GITLAB_CLIENT_ID=cfd3f82aa9d039865b3af6c58e212e9901d9b17645bff6ba21bce3403ac1417e
GITLAB_CLIENT_SECRET=d64e873a0633c871dad89823d50fb1892cac3de30e2a24fd903357e51668b59f
GITLAB_REDIRECT_URI="${APP_URL}/sso/gitlab/callback"
GITLAB_INSTANCE_URI=https://gitlab.com/
```

### SurfConext

| Environment variable       | Description                                                            |
| -------------------------- | ---------------------------------------------------------------------- |
| `SURFCONEXT_ENABLED`       | Enable or disable SurfConext login button                              |
| `SURFCONEXT_CLIENT_ID`     | Client ID from SurfConext OAuth2 service                               |
| `SURFCONEXT_CLIENT_SECRET` | Client secret from SurfConext OAuth2 service                           |
| `SURFCONEXT_TEST`          | Use SurfConext text environment instead of production ([users][users]) |

Example configuration:

```properties
SURFCONEXT_ENABLED=true
SURFCONEXT_CLIENT_ID=cfd3f82aa9d039865b3af6c58e212e9901d9b17645bff6ba21bce3403ac1417e
SURFCONEXT_CLIENT_SECRET=d64e873a0633c871dad89823d50fb1892cac3de30e2a24fd903357e51668b59f
SURFCONEXT_REDIRECT_URI="${APP_URL}/sso/surfconext/callback"
SURFCONEXT_TEST=false
```

[users]: https://idp.diy.surfconext.nl/showusers.php "SurfConext IDP users"
