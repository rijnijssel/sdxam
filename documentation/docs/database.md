# Database

SDXam supports only one database engine, MySQL/MariaDB.

## Configuration

The database connection is configured through the `.env` file.

| Environment variable | Description                                  |
| -------------------- | -------------------------------------------- |
| `DB_HOST`            | Host of MySQL/MariaDB                        |
| `DB_PORT`            | Port of MySQL/MariaDB                        |
| `DB_DATABASE`        | Database name in MySQL/MariaDB               |
| `DB_USERNAME`        | Username for authentication to MySQL/MariaDB |
| `DB_PASSWORD`        | Password for authentication to MySQL/MariaDB |

Example configuration

```properties
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=sdxam
DB_USERNAME=sdxam
DB_PASSWORD=password
```
