# SDXam

Deze documentatie is bedoeld om een overzicht te geven van alle functionaliteit, die beschikbaar is in SDXam.

- [Handleiding](guide.md) - korte handleiding met screenshots
- [Berekeningen](calculations.md) - overzicht van alle berekeningen die gedaan worden in de applicatie
