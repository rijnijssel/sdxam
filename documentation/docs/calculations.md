# Calculations

## Calculate score (`calculate_score`)

### Function

```sql
-- calculate the score based on points and max points
create
function calculate_score(points double, max_points double)
    returns decimal(3, 1)
    reads sql data
begin declare half, result double;
set half = max_points / 2;

if points < half then
        set result = 4.5 / half * points + 1;
else
        set result = 4.5 / (max_points - half) * (points - half) + 5.5;
end if;

return CEILING(result * 10) / 10;
end;
```

### Usage

```sql
-- calculate score for 15 out of 19 points
select calculate_score(15, 19) as score;
```

## Get max points for process (`calculate_max_points`)

### Function

```sql
-- calculate the max points of a process
create
function calculate_max_points(p_process_id int)
    returns int
    reads sql data
begin return (
        select sum(points.max_points)
        from (select MAX(rti.points) as max_points
              from rubric_task_items rti
                       join rubric_tasks rt on rt.id = rti.rubric_task_id
                       join rubrics r on r.id = rt.rubric_id
                       join processes p on p.id = r.process_id
              where p.id = p_process_id
              group by rt.id
             ) as points
    );
end;
```

### Usage

```sql
-- calculate max points for process 3
select calculate_max_points(3) as max_points;
```

## Calculate the points achieved for process (`calculate_process_points`)

### Function

```sql
-- calculate the points achieved for a given process,
-- taking into account the required criteria for a rubric, and not fully-graded rubrics.
create
function calculate_process_points(p_execution_student_id int, p_process_id int) returns int
                reads sql data
begin set @filled_points = (
    select count(*)
    from scores s
             join rubric_tasks t on t.id = s.rubric_task_id
             join rubrics r on r.id = t.rubric_id
             join execution_student_tasks est on est.id = s.execution_student_task_id
             join execution_students es on es.id = est.execution_student_id
    where es.id = p_execution_student_id
      and r.process_id = p_process_id
      and s.score_id is not null
);

set @missing = (
    select (count(*) - @filled_points) as missing
    from rubric_tasks rt
             join rubrics r on rt.rubric_id = r.id
             join processes p on p.id = r.process_id
    where p.id = p_process_id
);

if @missing > 0 then
                    return null;
end if;

set @required_points = (
    select count(*)
    from processes p
             join rubrics r on p.id = r.process_id
             join rubric_tasks rt on r.id = rt.rubric_id
    where rt.required
      and p.id = p_process_id
);

set @requirements_achieved = (
    select count(*) = @required_points as required
    from processes p
             join rubrics r on p.id = r.process_id
             join rubric_tasks rt on r.id = rt.rubric_id
             join rubric_task_items rti on rt.id = rti.rubric_task_id
             join scores s on rti.id = s.score_id
             join execution_student_tasks est on est.id = s.execution_student_task_id
             join execution_students es on es.id = est.execution_student_id
    where rt.required
      and rti.points > 0
      and p.id = p_process_id
      and es.id = p_execution_student_id
);

if @requirements_achieved = 0 then
                    return -1;
end if;

return (
    select sum(rti.points) as points
    from processes p
             join rubrics r on p.id = r.process_id
             join rubric_tasks rt on r.id = rt.rubric_id
             join rubric_task_items rti on rt.id = rti.rubric_task_id
             join scores s on s.score_id = rti.id
             join execution_student_tasks est on est.id = s.execution_student_task_id
             join execution_students es on es.id = est.execution_student_id
    where p.id = p_process_id
      and es.id = p_execution_student_id
);
end;
```

### Usage

```sql
-- calculate the points achieved for execution_student 1 and process 1
select calculate_process_points(1, 1) as points
```

## Calculate score for a execution student's process

### Function

```sql
-- calculate the score for an execution student's process
create
function calculate_process_score(p_execution_student_id int, p_process_id int) returns double(3, 1)
    reads sql data
begin set @points = calculate_process_points(p_execution_student_id, p_process_id);

if @points is null then
        return null;
end if;

set @max_points = calculate_max_points(p_process_id);
return calculate_score(@points, @max_points);
end;
```

### Usage

```sql
-- calculate the score achieved for process 3 of execution student 1
select calculate_process_score(1, 3) as score;
```
