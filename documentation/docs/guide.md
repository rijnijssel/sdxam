# Handleiding

## Stappen om te beoordelen

1. Kies uitvoering
1. Wil je beoordelen of definitieve resultaten invoeren?
1. Kies een student en werkproces
1. Vul beoordeling of definitieve resultaten in
1. Klik op opslaan
1. Via de broodkruimels, boven in het scherm, kan je terug naar het overzicht

## Overzicht van examenuitvoeringen

Op deze pagina selecteer je de uitvoering die je wilt beoordelen.

Klik op "Beoordeling" om naar het overzicht met jouw individuele beoordelingen te gaan.

Klik op "Resultaten" om naar het overzicht met definitieve resulataten te gaan.

![Overzicht met uitvoeringen](img/executions.png)

## Overzicht van beoordelingen student

Op deze pagina worden alle studenten weergegeven in een tabel, vervolgens kan een student per werkproces beoordeeld
worden.

![Overzicht beoordelingen](img/score.index.png)

## Beoordelen van een student

Op deze pagina wordt de individuele beoordeling ingevuld van een student en werkproces. Als allebei de beoordelingen
ingevuld zijn, klik je rechtsboven op "Resultaat werkproces" om naar de definitieve beoordeling te gaan.

![Individuele beoordeling student](img/score.show-input.png)

## Invullen van definitieve beoordeling

Om tot een definitieve beoordeling te komen, moeten de beoordelingen van twee examinatoren vergeleken worden. De
beoordeling van de opdrachtgever en leidinggevende staan respectievelijk in blauw en geel weergegeven. De definitieve
beoordeling zal in groen gemarkeerd worden.

![Definitieve beoordeling](img/results.show-input.png)

## Exporteren van resultaten

> Work in progress
