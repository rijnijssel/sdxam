# SDXam

[![pipeline status](https://gitlab.com/rijnijssel/sdxam/badges/main/pipeline.svg)](https://gitlab.com/rijnijssel/sdxam/-/commits/main)
[![coverage report](https://gitlab.com/rijnijssel/sdxam/badges/main/coverage.svg)](https://gitlab.com/rijnijssel/sdxam/-/commits/main)
[![latest release](https://gitlab.com/rijnijssel/sdxam/-/badges/release.svg)](https://gitlab.com/rijnijssel/sdxam/-/releases)

## Contributing

### Setting up your environment

#### Requirements

Below you'll find software that needs to be installed, to be able to run SDXam.

- [Docker](https://docs.docker.com/) ([Installation guide](https://docs.docker.com/get-docker/))

#### Cloning the project

If you are making a Merge Request, please fork the SDXam repository before continuing; more information on forking
repositories is located in the [How to fork a project](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html)
documentation.

```bash
# Example of cloning the SDXam repository (non fork)

# Using SSH
git clone git@gitlab.com:rijnijssel/sdxam.git
```

Once cloned, navigate to the folder by typing `cd sdxam` and then running the following commands:

#### First time setup

To install the Laravel Sail package, run this Docker command.

```bash
# Run "composer install" inside a temporary Docker container
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php83-composer:latest \
    composer install --ignore-platform-reqs
```

It is useful to make an alias in your shell for `sail`. This can be done by putting the following code snippet in
your `.bashrc` or `.zshrc` file.

```bash
alias sail='sh $([ -f sail ] && echo sail || echo vendor/bin/sail)'
```

#### Regular setup

```bash
# Install all project dependencies
sail composer install
sail pnpm install

# Build the frontend packages
sail pnpm dev
```

### Starting your environment

```bash
# To start the webserver
sail up -d

# To start watching for frontend changes
sail pnpm dev
```

### Testing Guidelines

#### Feature tests

Feature tests are used to test bigger parts of the application flow, for example "create a student". The tests for this
functionality would then cover:

- Authentication
- Authorization
- Validation
    - Session errors
- Events & Listeners
- Database
- HTTP response

When testing a resource controller, it is recommended to follow the naming convention highlighted in the list below.
Shown is an example feature test for a resource controller.

- Testing the `index()` method
    - `test_index_guest()`
        - authentication status is correct
    - `test_index()`
        - authentication status is correct
        - all crud actions are present
        - all fields are correct
- Testing the `create()` method
    - `test_create_guest()`
        - authentication status is correct
    - `test_create()`
        - authentication status is correct
        - all inputs / buttons are shown
- Testing the `store(Request $request)` method
    - `test_store_guest()`
        - authentication status is correct
        - data didn't insert into database
    - `test_store()`
        - authentication status is correct
        - data passed validation
        - data did insert into database
        - http response is correct
    - `test_store_invalid_email()` (example)
        - authentication status is correct
        - data did not pass validation
        - data didn't insert into database
        - http response is correct
- Testing the `show(Model $model)` method
    - `test_show_guest()`
        - authentication status is correct
    - `test_show_missing_guest()`
        - authentication status is correct
        - not a 404, but redirect to login
    - `test_show()`
        - authentication status is correct
        - all fields / buttons are shown
    - `test_show_missing()`
        - authentication status is correct
        - a 404 is shown
- Testing the `edit(Model $model)` method
    - `test_edit_guest()`
        - authentication status is correct
    - `test_edit_missing_guest()`
        - authentication status is correct
        - not a 404, but redirect to login
    - `test_edit()`
        - authentication status is correct
        - all fields are correct
    - `test_edit_missing()`
        - authentication status is correct
        - a 404 is shown
- Testing the `update(Request $request, Model $model)` method
    - `test_update_guest()`
        - authentication status is correct
        - data in database didn't update
    - `test_update_missing_guest()`
        - authentication status is correct
        - a 404 is shown
    - `test_update()`
        - authentication status is correct
        - data passed validation
        - data did insert into database
        - http response is correct
    - `test_update_missing()`
        - authentication status is correct
        - a 404 is shown
    - `test_update_invalid_email()` (example)
        - authentication status is correct
        - data did not pass validation
        - data didn't insert into database
        - http response is correct
- Testing the `destroy(Model $model)` method
    - `test_destroy_guest()`
        - authentication status is correct
        - data is not removed from database
    - `test_destroy()`
        - authentication status is correct
        - data is removed from database
    - `test_destroy_missing()`
        - authentication status is correct
        - a 404 is shown

## Berekenen van scores

### MySQL function

```sql
create function calculate_score(points double, max_points double)
    returns decimal(3, 1)
    reads sql data
begin
return CEILING((9 * points / max_points + 1) * 10) / 10;
end;
```
