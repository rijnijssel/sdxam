#!/usr/bin/env bash

cd "$FORGE_SITE_PATH" || exit

# Pull the latest changes from the repository
git fetch --all --tags --prune

if [[ -n "$FORGE_VAR_TAG" ]]; then
    git switch --detach "${FORGE_VAR_TAG}"
else
    git switch "$FORGE_SITE_BRANCH"
    git pull origin "$FORGE_SITE_BRANCH"
fi

$FORGE_COMPOSER install --no-interaction --prefer-dist --optimize-autoloader --no-dev

(
    flock -w 10 9 || exit 1
    echo 'Restarting FPM...'
    sudo -S service "$FORGE_PHP_FPM" reload
) 9>/tmp/fpmlock

$FORGE_PHP artisan optimize:clear
$FORGE_PHP artisan migrate --force
$FORGE_PHP artisan optimize
$FORGE_PHP artisan queue:restart

pnpm install --frozen-lockfile
pnpm build
