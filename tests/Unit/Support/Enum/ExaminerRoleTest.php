<?php

namespace Tests\Unit\Support\Enum;

use App\Support\Enum\ExaminerRole;
use PHPUnit\Framework\TestCase;

class ExaminerRoleTest extends TestCase
{
    public function test_client(): void
    {
        // Run
        $role = ExaminerRole::Client;
        // Assert
        $this->assertSame('client_count', $role->getProgressColumn());
        $this->assertSame('client_id', $role->getExecutionStudentTaskColumn());
        $this->assertSame('client_score_id', $role->getScoreColumn());
    }

    public function test_supervisor(): void
    {
        // Run
        $role = ExaminerRole::Supervisor;
        // Assert
        $this->assertSame('supervisor_count', $role->getProgressColumn());
        $this->assertSame('supervisor_id', $role->getExecutionStudentTaskColumn());
        $this->assertSame('supervisor_score_id', $role->getScoreColumn());
    }
}
