<?php

namespace Tests\Unit\Support\DTO;

use App\Support\Data\ScoreProgress;
use PHPUnit\Framework\TestCase;

class ScoreProgressDTOTest extends TestCase
{
    public function test_in_progress(): void
    {
        // Run
        $dto = new ScoreProgress(1, 0);
        // Assert
        $this->assertFalse($dto->filled());
    }

    public function test_filled(): void
    {
        // Run
        $dto = new ScoreProgress(1, 1);
        // Assert
        $this->assertTrue($dto->filled());
    }

    public function test_html_output(): void
    {
        // Run
        $dto = new ScoreProgress(10, 5);
        // Assert
        $this->assertSame('5 / 10', $dto->toHtml());
    }
}
