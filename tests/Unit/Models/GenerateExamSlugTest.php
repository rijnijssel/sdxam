<?php

namespace Tests\Unit\Models;

use App\Models\Exam;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GenerateExamSlugTest extends TestCase
{
    use RefreshDatabase;

    public function test_generate_exam_slug(): void
    {
        // Declare
        $exam = Exam::query()->create(['name' => 'Test Exam']);
        // Assert
        $this->assertSame('test-exam', $exam->slug);
        $this->assertDatabaseHas('exams', [
            'slug' => 'test-exam',
        ]);
    }
}
