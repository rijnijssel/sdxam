<?php

namespace Tests\Unit\Models;

use App\Models\Group;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GenerateGroupSlugTest extends TestCase
{
    use RefreshDatabase;

    public function test_generate_slug(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $group = Group::query()->create(['name' => 'Test Group', 'execution_id' => $execution->id]);
        // Assert
        $this->assertSame('test-group', $group->slug);
        $this->assertDatabaseHas('groups', [
            'slug' => 'test-group',
        ]);
    }
}
