<?php

namespace Tests\Unit\Models;

use App\Models\Execution;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GenerateExecutionSlugTest extends TestCase
{
    use RefreshDatabase;

    public function test_generate_slug(): void
    {
        // Declare
        $exam = $this->makeExam();
        $execution = Execution::query()->create(['name' => 'Test Execution', 'exam_id' => $exam->id]);
        // Assert
        $this->assertSame('test-execution', $execution->slug);
        $this->assertDatabaseHas('executions', [
            'slug' => 'test-execution',
        ]);
    }
}
