<?php

namespace Tests\Unit\Models;

use App\Models\Exam;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GenerateTaskSlugTest extends TestCase
{
    use RefreshDatabase;

    public function test_generate_slug(): void
    {
        // Declare
        /** @var Exam $exam */
        $exam = Exam::factory()->create();
        $role = Task::query()->create([
            'name' => 'Test Task',
            'code' => fake()->word(),
            'description' => fake()->sentence(),
            'exam_id' => $exam->id,
        ]);
        // Assert
        $this->assertSame('test-task', $role->slug);
        $this->assertDatabaseHas('tasks', [
            'name' => 'Test Task',
            'slug' => 'test-task',
        ]);
    }
}
