<?php

namespace Tests\Feature\Models;

use App\Exceptions\NoExaminerException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_examiner_null(): void
    {
        $this->expectException(NoExaminerException::class);
        // declare
        $user = $this->makeUser();
        // run
        $user->getExaminer();
    }
}
