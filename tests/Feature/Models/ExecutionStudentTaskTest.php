<?php

namespace Tests\Feature\Models;

use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\ExecutionStudentTask;
use App\Models\Student;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Override;
use Tests\TestCase;

class ExecutionStudentTaskTest extends TestCase
{
    use RefreshDatabase;

    protected Execution $execution;

    protected Student $student;

    protected ExecutionStudent $es;

    protected ExecutionStudentTask $est;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        $this->execution = $this->makeExecution();
        $this->student = $this->makeStudent();
        $this->es = $this->execution->addStudent($this->student);
        $task = Task::query()->firstOrFail();
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $this->est = $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
    }

    public function test_get_examiners_not_cached(): void
    {
        // Declare
        DB::enableQueryLog();
        // Run
        $this->est->getExaminers();
        $this->assertCount(2, DB::getQueryLog()); // two queries should be executed, one for each examiner
    }

    public function test_get_examiners_cached(): void
    {
        // Declare
        DB::enableQueryLog();
        $this->est->getExaminers();
        $queries = count(DB::getQueryLog()); // only 2 queries should be executed
        // Run
        $this->est->getExaminers();
        $this->assertCount($queries, DB::getQueryLog()); // no new queries should be executed by the second call
    }

    public function test_is_achieved_return_false_when_task_is_null(): void
    {
        // Declare
        $response = $this->est->isAchieved(null);
        // Assert
        $this->assertFalse($response);
    }

    public function test_execution_student_relationship(): void
    {
        $student = $this->makeStudent();
        $es = $this->execution->addStudent($student);
        $this->est->executionStudent()->associate($es);
        $this->est->save();
        // Assert
        $this->assertDatabaseHas('execution_student_tasks', [
            'id' => $this->est->id,
            'execution_student_id' => $es->id,
        ]);
    }

    public function test_context_relationship(): void
    {
        // Declare
        $this->assertDatabaseCount('contexts', 0);
        $context = $this->makeContext();
        // Run
        $this->est->context()->associate($context);
        $this->est->save();
        // Assert
        $this->assertDatabaseHas('execution_student_tasks', [
            'id' => $this->est->id,
            'context_id' => $context->id,
        ]);
    }
}
