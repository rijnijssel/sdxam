<?php

namespace Tests\Feature\Models;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\TestsRubrics;

class ProcessTest extends TestCase
{
    use RefreshDatabase;
    use TestsRubrics;

    public function test_rubric_string1(): void
    {
        // Declare
        $process = $this->createProcessWithRubrics(2);
        // Run
        $rubricString = $process->rubricString();
        // Assert
        $this->assertSame('WP1 en WP2', $rubricString);
    }

    public function test_rubric_string2(): void
    {
        // Declare
        $process = $this->createProcessWithRubrics(4);
        // Run
        $rubricString = $process->rubricString();
        // Assert
        $this->assertSame('WP1, WP2, WP3 en WP4', $rubricString);
    }

    public function test_task_relationship(): void
    {
        // Declare
        $exam = $this->makeExam();
        $task = $this->makeTask($exam);
        $process = $this->makeProcess($task);
        // Assert
        $this->assertNotNull($process->task);
        $this->assertTrue($process->task()->where('name', $task->name)->exists());
    }

    public function test_task_null_relationship(): void
    {
        // Declare
        $exam = $this->makeExam();
        $task = $this->makeTask($exam);
        $process = $this->makeProcess($task);
        $this->expectException(QueryException::class);
        $this->expectExceptionMessage('Integrity constraint violation');
        // Run
        $process->task()->dissociate();
        $process->save();
        // Assert
        $this->assertNotNull($process->task);
        $this->assertDatabaseHas('processes', [
            'name' => $process->name,
            'task_id' => $task->id,
        ]);
    }
}
