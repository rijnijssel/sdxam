<?php

namespace Tests\Feature\Models;

use App\Models\Execution;
use Illuminate\Foundation\Testing\RefreshDatabase;
use stdClass;
use Tests\TestCase;

class ExecutionTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_current(): void
    {
        // Declare
        $exam = $this->makeExam();
        Execution::query()->create(['name' => 'Execution 1', 'start_date' => today()->addWeeks(2), 'exam_id' => $exam->id]);
        Execution::query()->create(['name' => 'Execution 2', 'start_date' => today()->addWeeks(1), 'exam_id' => $exam->id]);
        $execution = Execution::query()->create(['name' => 'Execution 3', 'start_date' => today()->subWeek(), 'exam_id' => $exam->id]);
        Execution::query()->create(['name' => 'Execution 4', 'start_date' => today()->subWeeks(2), 'exam_id' => $exam->id]);
        Execution::query()->create(['name' => 'Execution 5', 'start_date' => today()->subWeeks(3), 'exam_id' => $exam->id]);
        // Run
        $current = Execution::getCurrent();
        // Assert
        $this->assertInstanceOf(Execution::class, $current);
        $this->assertTrue($execution->is($current));
    }

    public function test_format_start_date(): void
    {
        // Declare
        $exam = $this->makeExam();
        $execution = Execution::query()->create(['name' => 'Execution', 'start_date' => '2020-01-01', 'exam_id' => $exam->id]);
        // Run
        $startDate = $execution->formatStartDate();
        // Assert
        $this->assertSame('2020-01-01', $startDate);
    }

    public function test_format_null_start_date(): void
    {
        // Declare
        $exam = $this->makeExam();
        $execution = Execution::query()->create(['name' => 'Execution', 'start_date' => null, 'exam_id' => $exam->id]);
        // Run
        $startDate = $execution->formatStartDate();
        // Assert
        $this->assertNull($startDate);
    }

    public function test_groups_relationship(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $execution->groups()->create(['name' => 'Group A']);
        // Assert
        $this->assertCount(1, $execution->groups);
        $this->assertTrue($execution->groups()->where('name', 'Group A')->exists());
    }

    public function test_exam_relationship(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $execution->exam()->associate($exam);
        // Assert
        $this->assertTrue($execution->exam()->where('name', $exam->name)->exists());
    }

    public function test_get_scores(): void
    {
        // Declare
        $execution = $this->makeExecution();
        //  set up rubric
        $task = $execution->exam->tasks()->create(['name' => 'Task 1', 'code' => 'T1', 'description' => 'Task Description']);
        $process = $task->processes()->create(['name' => 'Process 1', 'description' => 'Process Description']);
        $rubric = $process->rubrics()->create(['title' => 'Rubric 1']);
        $rubricTask = $rubric->tasks()->create(['name' => 'RT1', 'criteria' => 'Criteria', 'required' => false]);
        $rubricTaskItem1 = $rubricTask->items()->create(['text' => 'Item 1', 'points' => 0]);
        $rubricTaskItem2 = $rubricTask->items()->create(['text' => 'Item 2', 'points' => 1]);
        $rubricTaskItem3 = $rubricTask->items()->create(['text' => 'Item 3', 'points' => 2]);
        $rubricTaskItem4 = $rubricTask->items()->create(['text' => 'Item 4', 'points' => 3]);
        //  set up execution student
        $student = $this->makeStudent();
        $executionStudent = $execution->addStudent($student);
        $examinerClient = $this->makeExaminer();
        $examinerSupervisor = $this->makeExaminer();
        $context = $this->makeContext();
        $executionStudentTask = $executionStudent->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examinerClient->id,
            'supervisor_id' => $examinerSupervisor->id,
            'context_id' => $context->id,
        ]);

        $tests = collect([
            $rubricTaskItem1->id => ['points' => 0, 'score' => 1.0],
            $rubricTaskItem2->id => ['points' => 1, 'score' => 4.0],
            $rubricTaskItem3->id => ['points' => 2, 'score' => 7.0],
            $rubricTaskItem4->id => ['points' => 3, 'score' => 10.0],
        ]);

        foreach ($tests as $id => $expected) {
            $executionStudentTask->scores()->updateOrCreate([
                'rubric_task_id' => $rubricTask->id,
            ], [
                'client_score_id' => $id,
                'supervisor_score_id' => $id,
                'score_id' => $id,
            ]);
            // Run
            $execution = Execution::query()->findOrFail($execution->id);
            $scores = $execution->getScores();
            $result = $scores->where('process_id', $process->id)->first();
            // Assert
            $this->assertInstanceOf(stdClass::class, $result);
            $this->assertSame($expected['points'], $result->points);
            $this->assertSame($expected['score'], $result->score);
        }
    }
}
