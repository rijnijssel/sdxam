<?php

namespace Tests\Feature\Models;

use App\Exceptions\NoExaminerException;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\ExecutionStudentTask;
use App\Models\Student;
use App\Models\Task;
use App\Support\Enum\ExaminerRole;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Override;
use Tests\TestCase;

class ExecutionStudentTest extends TestCase
{
    use RefreshDatabase;

    protected Execution $execution;

    protected Student $student;

    protected ExecutionStudent $es;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();

        $this->execution = $this->makeExecution();
        $this->student = $this->makeStudent();
        $this->es = $this->execution->addStudent($this->student);
    }

    public function test_get_examiners_not_cached(): void
    {
        // Declare
        DB::enableQueryLog();
        // Run
        $this->es->getExaminers();
        $this->assertCount(1, DB::getQueryLog()); // two queries should be executed, one for each examiner
    }

    public function test_get_examiners_cached(): void
    {
        // Declare
        DB::enableQueryLog();
        $this->es->getExaminers();
        $queries = count(DB::getQueryLog()); // only 2 queries should be executed
        // Run
        $this->es->getExaminers();
        $this->assertCount($queries, DB::getQueryLog()); // no new queries should be executed by the second call
    }

    public function test_get_points_not_cached(): void
    {
        // Declare
        DB::enableQueryLog();
        // Run
        $this->es->getPoints();
        $this->assertCount(1, DB::getQueryLog()); // two queries should be executed, one for each examiner
    }

    public function test_get_points_cached(): void
    {
        // Declare
        DB::enableQueryLog();
        $this->es->getPoints();
        $queries = count(DB::getQueryLog()); // only 2 queries should be executed
        // Run
        $this->es->getPoints();
        $this->assertCount($queries, DB::getQueryLog()); // no new queries should be executed by the second call
    }

    public function test_get_task_returns_null_when_task_is_null(): void
    {
        // Run
        $response = $this->es->getTask(null);
        // Assert
        $this->assertNotInstanceOf(ExecutionStudentTask::class, $response);
    }

    public function test_get_score_text_null(): void
    {
        // Run
        $response = $this->es->getScoreText(null);
        // Assert
        $this->assertNull($response);
    }

    public function test_get_score_text_less_than_zero(): void
    {
        // Run
        $response = $this->es->getScoreText(-1);
        // Assert
        $this->assertSame('N.B.', $response);
    }

    public function test_get_score_text_equals_zero(): void
    {
        // Run
        $response = $this->es->getScoreText(0);
        // Assert
        $this->assertSame('0.0', $response);
    }

    public function test_get_score_text_decimals(): void
    {
        // Run
        $response = $this->es->getScoreText(6.12376);
        // Assert
        $this->assertSame('6.1', $response);
    }

    public function test_executions_relationship(): void
    {
        // Assert
        $this->assertTrue($this->es->execution()->exists());
        $this->assertEquals($this->execution->id, $this->es->execution()->value('id'));
    }

    public function test_get_examiner_role_client(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->getExaminerRole($task, $examiner);
        // Assert
        $this->assertSame(ExaminerRole::Client, $response);
    }

    public function test_get_examiner_role_no_client(): void
    {
        $this->expectException(NoExaminerException::class);
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);
        // Run
        $response = $this->es->getExaminerRole($task, $examiner);
        // Assert
        $this->assertSame(ExaminerRole::Client, $response);
    }

    public function test_get_examiner_role_supervisor(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'supervisor_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->getExaminerRole($task, $examiner);
        // Assert
        $this->assertSame(ExaminerRole::Supervisor, $response);
    }

    public function test_get_other_examiner_both_linked(): void
    {
        // Declare
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
        // Run
        $response = $this->es->getOtherExaminer($task, $client);
        // Assert
        $this->assertInstanceOf(Examiner::class, $response);
        $this->assertEquals($supervisor->id, $response->id);
    }

    public function test_get_other_examiner_client_not_linked(): void
    {
        // Declare
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'supervisor_id' => $supervisor->id,
        ]);
        // Run
        $response = $this->es->getOtherExaminer($task, $client);
        // Assert
        $this->assertNotInstanceOf(Examiner::class, $response);
    }

    public function test_get_other_examiner_supervisor_not_linked(): void
    {
        // Declare
        $client = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
        ]);
        // Run
        $response = $this->es->getOtherExaminer($task, $client);
        // Assert
        $this->assertNotInstanceOf(Examiner::class, $response);
    }

    public function test_get_examiner_client_linked(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->getExaminer($task, ExaminerRole::Client);
        // Assert
        $this->assertInstanceOf(Examiner::class, $response);
        $this->assertEquals($examiner->id, $response->id);
    }

    public function test_get_examiner_client_null(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'supervisor_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->getExaminer($task, ExaminerRole::Client);
        // Assert
        $this->assertNotInstanceOf(Examiner::class, $response);
    }

    public function test_get_examiner_supervisor_linked(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'supervisor_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->getExaminer($task, ExaminerRole::Supervisor);
        // Assert
        $this->assertInstanceOf(Examiner::class, $response);
        $this->assertEquals($examiner->id, $response->id);
    }

    public function test_get_examiner_supervisor_null(): void
    {
        // Declare
        $task = Task::query()->firstOrFail();
        $examiner = $this->makeExaminer();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->getExaminer($task, ExaminerRole::Supervisor);
        // Assert
        $this->assertNotInstanceOf(Examiner::class, $response);
    }

    public function test_is_task_examiner_linked(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examiner->id,
        ]);
        // Run
        $response = $this->es->isTaskExaminer($task, $examiner);
        // Assert
        $this->assertTrue($response);
    }

    public function test_is_task_examiner_no_examiner(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        $this->es->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);
        // Run
        $response = $this->es->isTaskExaminer($task, $examiner);
        // Assert
        $this->assertFalse($response);
    }

    public function test_is_task_examiner_not_linked(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $task = Task::query()->firstOrFail();
        // Run
        $response = $this->es->isTaskExaminer($task, $examiner);
        // Assert
        $this->assertFalse($response);
    }
}
