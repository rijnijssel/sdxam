<?php

namespace Tests\Feature\Models;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    public function test_execution_student_tasks(): void
    {
        // declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $task = $execution->exam->tasks->firstOrFail();
        $es = $execution->addStudent($student);
        $est = $es->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);
        // run
        $tasks = $student->executionStudentTasks;
        // asserts
        $this->assertTrue($tasks->contains($est));
    }

    public function test_get_username_no_middle_name(): void
    {
        // declare
        $student = Student::factory()->createOne([
            'first_name' => 'John',
            'middle_name' => null,
            'last_name' => 'Doe',
        ]);
        // run
        $response = $student->getUsername();
        // assert
        $this->assertSame('j.doe', $response);
    }

    public function test_get_username_with_middle_name(): void
    {
        // declare
        $student = Student::factory()->createOne([
            'first_name' => 'Jane',
            'middle_name' => 'Peter',
            'last_name' => 'Doe',
        ]);
        // run
        $response = $student->getUsername();
        // assert
        $this->assertSame('j.p.doe', $response);
    }
}
