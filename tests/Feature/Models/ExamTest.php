<?php

namespace Tests\Feature\Models;

use App\Models\Exam;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Date;
use Tests\TestCase;

class ExamTest extends TestCase
{
    use RefreshDatabase;

    public function insertDelayedExams(): void
    {
        Exam::query()->insert([
            ['name' => 'exam1', 'slug' => 'exam1', 'updated_at' => Date::make('2020-01-01 00:00:00')],
            ['name' => 'exam2', 'slug' => 'exam2', 'updated_at' => Date::make('2020-02-01 00:00:00')],
            ['name' => 'exam3', 'slug' => 'exam3', 'updated_at' => Date::make('2020-03-01 00:00:00')],
            ['name' => 'exam4', 'slug' => 'exam4', 'updated_at' => Date::make('2020-04-01 00:00:00')],
            ['name' => 'exam5', 'slug' => 'exam5', 'updated_at' => Date::make('2020-05-01 00:00:00')],
        ]);
    }

    public function test_global_scope_order_by_updated_at(): void
    {
        // Declare
        $this->insertDelayedExams();
        // Run
        $exams = Exam::query()->pluck('id');
        $sorted = Exam::query()->orderByDesc('updated_at')->pluck('id');
        // Assert
        $this->assertCount(5, $exams);
        $this->assertCount(5, $sorted);
        $this->assertEquals($sorted, $exams);
    }

    public function test_tasks_string1(): void
    {
        // Declare
        $exam = $this->makeExam(false);
        $exam->tasks()->saveMany([
            new Task(['name' => 'Taak 1', 'description' => fake()->sentence(), 'code' => 't1']),
            new Task(['name' => 'Taak 2', 'description' => fake()->sentence(), 'code' => 't2']),
        ]);
        // Run
        $tasksString = $exam->tasksString();
        // Assert
        $this->assertSame('Taak 1 en Taak 2', $tasksString);
    }

    public function test_tasks_string2(): void
    {
        // Declare
        $exam = $this->makeExam(false);
        $exam->tasks()->saveMany([
            new Task(['name' => 'Taak 1', 'description' => fake()->sentence(), 'code' => 't1']),
            new Task(['name' => 'Taak 2', 'description' => fake()->sentence(), 'code' => 't2']),
            new Task(['name' => 'Taak 3', 'description' => fake()->sentence(), 'code' => 't3']),
        ]);
        // Run
        $tasksString = $exam->tasksString();
        // Assert
        $this->assertSame('Taak 1, Taak 2 en Taak 3', $tasksString);
    }

    public function test_execution_relationship(): void
    {
        // Declare
        $exam = $this->makeExam();
        $execution = $this->makeExecution();
        // Run
        $exam->executions()->save($execution);
        // Assert
        $this->assertCount(1, $exam->executions);
        $this->assertTrue($exam->executions()->where('name', $execution->name)->exists());
    }
}
