<?php

namespace Tests\Feature\Models;

use App\Models\BatchExport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Tests\TestCase;

class BatchExportTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_pending(): void
    {
        // Declare
        $this->travelTo('2022-06-01 16:30:00');
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);
        // Run
        $response = BatchExport::createPending($execution, Collection::make([$student]));
        // Assert
        $this->assertDatabaseHas('batch_exports', [
            'id' => $response->id,
            'execution_id' => $execution->id,
            'started_at' => null,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
        $this->assertDatabaseHas('batch_export_students', [
            'batch_export_id' => $response->id,
            'student_id' => $student->id,
        ]);
    }

    public function test_finish(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        $export->update(['started_at' => $date]);

        $this->assertDatabaseHas('batch_exports', [
            'started_at' => $date,
            'finished_at' => null,
            'failed' => false,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
        // Run;
        $this->travel(30)->minutes();
        $export->finish();
        // Assert
        $this->assertDatabaseHas('batch_exports', [
            'started_at' => $date,
            'finished_at' => '2022-06-01 17:00:00',
            'failed' => false,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
    }

    public function test_failed(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        $export->update(['started_at' => $date]);

        $this->assertDatabaseHas('batch_exports', [
            'started_at' => $date,
            'failed' => false,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
        // Run;
        $export->failed();
        // Assert
        $this->assertDatabaseHas('batch_exports', [
            'started_at' => $date,
            'failed' => true,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
    }

    public function test_is_finished_false_with_no_failure(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        $export->update(['started_at' => $date]);

        // Run
        $this->travel(30)->minutes();
        // Assert
        $this->assertFalse($export->isFinished());
    }

    public function test_is_finished_true_with_no_failure(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        // Run
        $this->travel(30)->minutes();
        $export->finish();
        // Assert
        $this->assertTrue($export->isFinished());
    }

    public function test_is_finished_true_with_failure(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        // Run
        $this->travel(30)->minutes();
        $export->finish();
        $export->failed();
        // Assert
        $this->assertFalse($export->isFinished());
    }

    public function test_is_failed_false(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        $export->update(['started_at' => $date]);

        // Run
        $response = $export->isFailed();
        // Assert
        $this->assertFalse($response);
        $this->assertDatabaseHas('batch_exports', [
            'started_at' => $date,
            'failed' => false,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
    }

    public function test_is_failed_true(): void
    {
        // Declare
        $this->travelTo($date = Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $export = BatchExport::createPending($execution, collect());
        $export->update(['started_at' => $date]);
        $export->failed();
        // Run
        $response = $export->isFailed();
        // Assert
        $this->assertTrue($response);
        $this->assertDatabaseHas('batch_exports', [
            'started_at' => $date,
            'failed' => true,
            'directory' => "/exports/{$execution->slug}/20220601_163000",
        ]);
    }

    public function test_get_student_path(): void
    {
        // Declare
        $this->travelTo(Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);
        $export = BatchExport::createPending($execution, collect([$student]));
        // Run
        $response = $export->getStudentPath($student);
        // Assert
        $this->assertSame("/exports/{$execution->slug}/20220601_163000/{$student->sid}.pdf", $response);
    }

    public function test_get_student_path_not_linked(): void
    {
        // Declare
        $this->travelTo(Date::make('2022-06-01 16:30:00'));
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);
        $export = BatchExport::createPending($execution, Collection::make([]));
        // Run
        $response = $export->getStudentPath($student);
        // Assert
        $this->assertFalse($response);
    }
}
