<?php

namespace Tests\Feature\Models;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;
use Tests\TestsRubrics;

class RubricTest extends TestCase
{
    use RefreshDatabase;
    use TestsRubrics;

    public function test_get_max_items1(): void
    {
        // Declare
        $rubric = $this->setupRubricMaxItems([2, 4, 3]);
        Cache::flush();
        // Run
        $maxItems = $rubric->max_items;
        // Assert
        $this->assertSame(4, $maxItems);
    }

    public function test_get_max_items2(): void
    {
        // Declare
        $rubric = $this->setupRubricMaxItems([2, 4, 6]);
        Cache::flush();
        // Run
        $maxItems = $rubric->max_items;
        // Assert
        $this->assertSame(6, $maxItems);
    }

    public function test_tasks_relationship(): void
    {
        // Declare
        $process = $this->createProcessWithRubrics();
        $rubric = $this->makeRubric($process);
        $rubric->tasks()->create(['name' => 'Task 1', 'criteria' => 'Criteria 1']);
        // Assert
        $this->assertTrue($rubric->tasks()->where('name', 'Task 1')->exists());
        $this->assertSame(1, $rubric->tasks()->count());
        $this->assertDatabaseHas('rubric_tasks', [
            'name' => 'Task 1',
            'rubric_id' => $rubric->id,
        ]);
    }

    public function test_tasks_empty_relationship(): void
    {
        // Declare
        $process = $this->createProcessWithRubrics();
        $rubric = $this->makeRubric($process);
        // Assert
        $this->assertSame(0, $rubric->tasks()->count());
        $this->assertDatabaseCount('rubric_tasks', 0);
    }

    public function test_process_relationship(): void
    {
        // Declare
        $process = $this->createProcessWithRubrics();
        $rubric = $this->makeRubric($process);
        $this->expectException(QueryException::class);
        $this->expectExceptionMessage('Integrity constraint violation');
        // Run
        $rubric->process()->dissociate();
        $rubric->save();
        // Assert
        $this->assertNotNull($rubric->process);
        $this->assertDatabaseHas('rubrics', [
            'title' => $rubric->title,
            'process_id' => $process->id,
        ]);
    }

    public function test_process_null_relationship(): void
    {
        // Declare
        $process = $this->createProcessWithRubrics();
        $rubric = $this->makeRubric($process);
        $this->expectException(QueryException::class);
        $this->expectExceptionMessage('Integrity constraint violation');
        // Run
        $rubric->process()->dissociate();
        $rubric->save();
        // Assert
        $this->assertNotNull($rubric->process);
        $this->assertDatabaseHas('rubrics', [
            'title' => $rubric->title,
            'process_id' => $process->id,
        ]);
    }
}
