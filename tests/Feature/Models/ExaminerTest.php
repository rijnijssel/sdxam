<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExaminerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_relationship(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $user = $this->makeUser();
        // Run
        $examiner->user()->associate($user);
        // Assert
        $this->assertNotNull($examiner->user);
        $this->assertTrue($examiner->user()->where('name', $user->name)->exists());
    }

    public function test_null_user_relationship(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $examiner->user()->dissociate();
        // Assert
        $this->assertNull($examiner->user);
    }

    public function test_get_email(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $email = $examiner->getEmail();
        // Assert
        $this->assertNotNull($examiner->user);
        $this->assertEquals($examiner->user->email, $email);
    }

    public function test_null_get_email(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $examiner->user()->dissociate();
        // Run
        $email = $examiner->getEmail();
        // Assert
        $this->assertNull($email);
    }

    public function test_get_label(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $label = $examiner->getLabel();
        // Assert
        $this->assertSame("{$examiner->name}", $label);
    }

    public function test_null_user_get_label(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $examiner->user()->dissociate();
        // Run
        $label = $examiner->getLabel();
        // Assert
        $this->assertSame("{$examiner->name}", $label);
    }
}
