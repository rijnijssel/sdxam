<?php

namespace Tests\Feature\Jobs;

use App\Jobs\ProcessBatchExport;
use App\Models\BatchExport;
use App\Models\Execution;
use App\Notifications\BatchExportFinished;
use Database\Seeders\DatabaseSeeder;
use Database\Seeders\ExecutionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class BatchExportExecutionResultsTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle(): void
    {
        // Prepare
        Storage::fake();
        Notification::fake();

        $this->seed([
            DatabaseSeeder::class,
            ExecutionSeeder::class,
        ]);

        // Declare
        $execution = Execution::query()->with('exam.tasks')->firstOrFail();
        $student = $this->makeStudent();
        $es = $execution->students()->create([
            'student_id' => $student->id,
        ]);

        $task = $execution->exam->tasks->firstOrFail();
        $examinerClient = $this->makeExaminer();
        $examinerSupervisor = $this->makeExaminer();
        $es->setExaminers($task, $examinerClient, $examinerSupervisor);

        $user = $this->makeUser();

        // Run
        $export = BatchExport::createPending($execution, collect([$student]));
        dispatch_sync(new ProcessBatchExport($user, $export));

        // Assert
        $batches = $execution->batchExports;
        $this->assertCount(1, $batches);

        $batch = $batches->first();
        $this->assertNotNull($batch);

        Storage::assertExists($batch->directory);
        Storage::assertExists($batch->directory."/{$student->sid}.pdf");
        Storage::deleteDirectory($batch->directory);

        Notification::assertNotSentTo($user, BatchExportFinished::class);
    }
}
