<?php

namespace Tests\Feature\Jobs;

use App\Jobs\ProcessExport;
use App\Models\Execution;
use App\Models\Export;
use App\Notifications\ExportFinished;
use Database\Seeders\DatabaseSeeder;
use Database\Seeders\ExecutionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ExportExecutionResultsTest extends TestCase
{
    use RefreshDatabase;

    public function test_file_created(): void
    {
        // Prepare
        Storage::fake();
        Notification::fake();

        $this->seed([
            DatabaseSeeder::class,
            ExecutionSeeder::class,
        ]);

        // Declare
        $user = $this->makeUser();
        $execution = Execution::query()->firstOrFail();

        // Run
        $export = Export::createPending($execution);
        dispatch_sync(new ProcessExport($user, $export));

        // Assert
        $exports = $execution->exports;
        $this->assertCount(1, $exports);

        $export = $exports->first();
        $this->assertNotNull($export);

        Storage::assertExists($export->filename);
        Notification::assertNotSentTo($user, ExportFinished::class);
    }
}
