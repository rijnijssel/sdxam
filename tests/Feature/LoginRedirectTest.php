<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginRedirectTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest_empty_path_redirect(): void
    {
        $response = $this->get('/');
        $response->assertRedirect('/login');
    }

    public function test_user_empty_path_redirect(): void
    {
        /** @var User $user */
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/');
        $response->assertRedirect('/login');
    }
}
