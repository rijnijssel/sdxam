<?php

namespace Tests\Feature\Livewire\Export;

use App\Models\Execution;
use Database\Seeders\ExecutionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest(): void
    {
        // Declare
        $this->seed();
        $this->seed(ExecutionSeeder::class);
        $execution = Execution::query()->firstOrFail();
        // Run
        $response = $this->get("executions/{$execution->slug}/exports");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_user(): void
    {
        // Declare
        $this->seed();
        $this->seed(ExecutionSeeder::class);
        $execution = Execution::query()->firstOrFail();
        // Run
        $response = $this->withAuth(admin: false)->get("executions/{$execution->slug}/exports");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_admin_export_done(): void
    {
        // Declare
        $this->seed();
        $this->seed(ExecutionSeeder::class);
        $execution = Execution::query()->firstOrFail();
        $export = $execution->exports()->create([
            'filename' => 'today.pdf',
            'started_at' => now()->subHour(),
            'finished_at' => now(),
        ]);
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/exports");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertSee([
            'Exports',
            'Klaar',
            $export->url(),
        ]);
    }

    public function test_admin_export_in_progress(): void
    {
        // Declare
        $this->seed();
        $this->seed(ExecutionSeeder::class);
        $execution = Execution::query()->firstOrFail();
        $export = $execution->exports()->create([
            'filename' => 'today.pdf',
            'started_at' => now()->subHour(),
        ]);
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/exports");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertSee([
            'Exports',
            'Bezig',
        ]);
        $response->assertDontSee([
            $export->url(),
        ]);
    }
}
