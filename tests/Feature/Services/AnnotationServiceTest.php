<?php

namespace Tests\Feature\Services;

use App\Models\Examiner;
use App\Models\ExecutionStudentTask;
use App\Models\RubricTask;
use App\Services\AnnotationService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Iterator;
use Override;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class AnnotationServiceTest extends TestCase
{
    use RefreshDatabase;

    private RubricTask $rubricTask1;

    private RubricTask $rubricTask2;

    private ExecutionStudentTask $executionStudentTask;

    private Examiner $examinerClient;

    private Examiner $examinerSupervisor;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        // Declare
        $execution = $this->makeExecution();
        //  set up rubric
        $task = $execution->exam->tasks()->create(['name' => 'Task 1', 'code' => 'T1', 'description' => 'Task Description']);
        $process = $task->processes()->create(['name' => 'Process 1', 'description' => 'Process Description']);
        $rubric = $process->rubrics()->create(['title' => 'Rubric 1']);
        $this->rubricTask1 = $rubric->tasks()->create(['name' => 'RT1', 'criteria' => 'Criteria', 'required' => false]);
        $this->rubricTask2 = $rubric->tasks()->create(['name' => 'RT2', 'criteria' => 'Criteria', 'required' => false]);
        //  set up execution student
        $student = $this->makeStudent();
        $executionStudent = $execution->addStudent($student);
        //  make execution student task
        $context = $this->makeContext();
        $this->examinerClient = $this->makeExaminer();
        $this->examinerSupervisor = $this->makeExaminer();
        $this->executionStudentTask = $executionStudent->executionStudentTasks()->create([
            'context_id' => $context->id,
            'task_id' => $task->id,
            'client_id' => $this->examinerClient->id,
            'supervisor_id' => $this->examinerSupervisor->id,
        ]);
    }

    public static function annotations(): Iterator
    {
        yield 'null value' => [null];
        yield 'filled value' => ['test annotation'];
    }

    #[DataProvider('annotations')]
    public function test_update(?string $text): void
    {
        // Declare
        $score = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask1->id]);
        // Run
        $service = app(AnnotationService::class);
        $service->update($score, $text, $this->examinerClient);
        // Assert
        $data = [
            'annotatable_type' => 'score',
            'annotatable_id' => $score->id,
            'examiner_id' => $this->examinerClient->id,
            'text' => $text,
        ];

        if ($text === null) {
            $this->assertDatabaseMissing('annotations', $data);
        } else {
            $this->assertDatabaseHas('annotations', $data);
        }
    }

    public function test_get_examiner_annotations(): void
    {
        // Declare
        $score1 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask1->id]);
        $score2 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask2->id]);
        $service = app(AnnotationService::class);
        $service->update($score1, 'client annotation 1', $this->examinerClient);
        $service->update($score2, 'client annotation 2', $this->examinerClient);
        $service->update($score1, 'supervisor annotation 1', $this->examinerSupervisor);
        $service->update($score2, 'supervisor annotation 2', $this->examinerSupervisor);
        // Run
        $clientAnnotations = $service->getExaminerAnnotations($this->executionStudentTask, $this->examinerClient);
        $supervisorAnnotations = $service->getExaminerAnnotations($this->executionStudentTask, $this->examinerSupervisor);
        // Assert
        $this->assertTrue($clientAnnotations->has($this->rubricTask1->id));
        $this->assertTrue($clientAnnotations->has($this->rubricTask2->id));
        $this->assertTrue($supervisorAnnotations->has($this->rubricTask1->id));
        $this->assertTrue($supervisorAnnotations->has($this->rubricTask2->id));
        $this->assertContains('client annotation 1', $clientAnnotations);
        $this->assertContains('client annotation 2', $clientAnnotations);
        $this->assertContains('supervisor annotation 1', $supervisorAnnotations);
        $this->assertContains('supervisor annotation 2', $supervisorAnnotations);
    }

    public function test_definitive_annotations(): void
    {
        // Declare
        $score1 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask1->id]);
        $score2 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask2->id]);
        $score1->annotations()->create(['examiner_id' => null, 'text' => 'definitive annotation 1']);
        $score2->annotations()->create(['examiner_id' => null, 'text' => 'definitive annotation 2']);
        $service = app(AnnotationService::class);
        // Run
        $annotations = $service->getDefinitiveAnnotations($this->executionStudentTask);
        // Assert
        $this->assertTrue($annotations->has($this->rubricTask1->id));
        $this->assertTrue($annotations->has($this->rubricTask2->id));
        $this->assertContains('definitive annotation 1', $annotations);
        $this->assertContains('definitive annotation 2', $annotations);
    }

    public function test_definitive_annotation(): void
    {
        // Declare
        $score1 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask1->id]);
        $score2 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask2->id]);
        $score1->annotations()->create(['examiner_id' => null, 'text' => 'definitive annotation 1']);
        $score2->annotations()->create(['examiner_id' => null, 'text' => 'definitive annotation 2']);
        $service = app(AnnotationService::class);
        // Run
        $annotation1 = $service->getDefinitiveAnnotation($this->executionStudentTask, $this->rubricTask1);
        $annotation2 = $service->getDefinitiveAnnotation($this->executionStudentTask, $this->rubricTask2);
        // Assert
        $this->assertNotNull($annotation1);
        $this->assertNotNull($annotation2);
        $this->assertSame('definitive annotation 1', $annotation1);
        $this->assertSame('definitive annotation 2', $annotation2);
    }

    public function test_get_default_annotations(): void
    {
        // Declare
        $score1 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask1->id]);
        $score2 = $this->executionStudentTask->scores()->create(['rubric_task_id' => $this->rubricTask2->id]);
        $score1->annotations()->create(['examiner_id' => $this->examinerClient->id, 'text' => 'client annotation 1']);
        $score1->annotations()->create(['examiner_id' => $this->examinerSupervisor->id, 'text' => 'supervisor annotation 1']);
        $score2->annotations()->create(['examiner_id' => $this->examinerClient->id, 'text' => 'client annotation 2']);
        $score2->annotations()->create(['examiner_id' => $this->examinerSupervisor->id, 'text' => 'supervisor annotation 2']);
        $service = app(AnnotationService::class);
        // Run
        $annotations = $service->getDefaultAnnotations($this->executionStudentTask);
        // Assert
        $this->assertTrue($annotations->has($this->rubricTask1->id));
        $this->assertTrue($annotations->has($this->rubricTask2->id));
        $this->assertContains(sprintf("%s: client annotation 1\n%s: supervisor annotation 1", $this->examinerClient->name, $this->examinerSupervisor->name), $annotations);
        $this->assertContains(sprintf("%s: client annotation 2\n%s: supervisor annotation 2", $this->examinerClient->name, $this->examinerSupervisor->name), $annotations);
    }
}
