<?php

namespace Tests\Feature\Services;

use App\Models\Student;
use App\Services\StudentService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentServiceTest extends TestCase
{
    use RefreshDatabase;

    public function test_hide_one_student(): void
    {
        // Declare
        /** @var Student $graduated */
        $graduated = Student::factory()->create(['graduated' => true]);
        /** @var Student $student */
        $student = Student::factory()->create(['graduated' => false]);
        $service = $this->app->make(StudentService::class);
        // Run
        $service->setArchived([
            $graduated->sid,
        ]);
        $graduated->refresh();
        $student->refresh();
        // Assert
        $this->assertTrue($graduated->isGraduated());
        $this->assertFalse($student->isGraduated());
    }

    public function test_hide_all_student(): void
    {
        // Declare
        /** @var Student $graduated */
        $graduated = Student::factory()->create(['graduated' => true]);
        /** @var Student $student */
        $student = Student::factory()->create(['graduated' => false]);
        $service = $this->app->make(StudentService::class);
        // Run
        $service->setArchived([
            $graduated->sid,
            $student->sid,
        ]);
        $graduated->refresh();
        $student->refresh();
        // Assert
        $this->assertTrue($graduated->isGraduated());
        $this->assertTrue($student->isGraduated());
    }
}
