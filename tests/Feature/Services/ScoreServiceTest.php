<?php

namespace Tests\Feature\Services;

use App\Services\ScoreService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class ScoreServiceTest extends TestCase
{
    use RefreshDatabase;

    #[DataProvider('scores')]
    public function test_get_raw_score(int $points, int $maxPoints, float $score): void
    {
        // Declare
        $service = $this->app->make(ScoreService::class);
        // Run
        $response = $service->getRawScore($points, $maxPoints);
        // Assert
        $this->assertSame($score, $response);
    }

    public function test_get_task_progress(): void
    {
        // Declare
        $service = $this->app->make(ScoreService::class);
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        // Run
        $response = $service->getTaskProgress($es);
        // Assert
        $this->assertEmpty($response);
    }

    public static function scores(): Iterator
    {
        yield [15, 19, 8.2];
        yield [19, 19, 10.0];
        yield [13, 19, 7.2];
        yield [12, 19, 6.7];
        yield [8, 19, 4.8];
        yield [13, 14, 9.4];
        yield [1, 21, 1.5];
        yield [2, 25, 1.8];
        yield [12, 25, 5.4];
        yield [13, 25, 5.7];
        yield [14, 25, 6.1];
        yield [15, 25, 6.4];
        yield [16, 25, 6.8];
        yield [17, 25, 7.2];
        yield [18, 25, 7.5];
        yield [19, 25, 7.9];
        yield [20, 25, 8.2];
        yield [21, 25, 8.6];
        yield [22, 25, 9.0];
        yield [23, 25, 9.3];
        yield [24, 25, 9.7];
        yield [25, 25, 10.0];
    }
}
