<?php

namespace Tests\Feature\Services;

use App\Services\CsvService;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class CsvServiceTest extends TestCase
{
    /**
     * Test the "make" function.
     *
     * @param  array<int, string>  $headers
     * @param  array<int, mixed>  $data
     */
    #[DataProvider('provider')]
    public function test_make(array $headers, array $data, string $expected): void
    {
        // declare
        /** @var CsvService $service */
        $service = app(CsvService::class);
        // run
        $response = $service->make($headers, $data);
        // assert
        $this->assertSame($expected, $response);
    }

    public static function provider(): Iterator
    {
        yield 'normal' => [
            ['ID', 'Name', 'Email'],
            [[1, 'User1', 'user1@example.com'], [2, 'User2', 'user2@example.com']],
            "ID,Name,Email\n1,User1,user1@example.com\n2,User2,user2@example.com\n",
        ];
        yield 'quoted' => [
            ['Name', 'Tasks'],
            [['User1', '1,2,3'], ['User2', '2,5,6']],
            "Name,Tasks\nUser1,\"1,2,3\"\nUser2,\"2,5,6\"\n",
        ];
    }
}
