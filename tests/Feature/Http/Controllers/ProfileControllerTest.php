<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ProfileControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_guest(): void
    {
        // Run
        $response = $this->get('profile');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user, false)->get('profile');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('profile.show');
        $response->assertSeeInOrder([
            'Profiel',
            'Persoonlijk',
            'Naam', e($user->name),
            'E-mail', e($user->email),
            'Beveiliging',
            'Wachtwoord',
        ]);
    }

    public function test_show(): void
    {
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->get('profile');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('profile.show');
        $response->assertSeeInOrder([
            'Profiel',
            'Persoonlijk',
            'Naam', e($user->name),
            'E-mail', $user->email,
            'Beveiliging',
            'Wachtwoord',
        ]);
    }

    public function test_update_guest(): void
    {
        // Declare
        $this->makeUser();
        // Run
        $response = $this->put('profile');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_user(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user, false)->put('profile', [
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        $response->assertRedirect('profile');
    }

    public function test_update(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile', [
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        $response->assertRedirect('profile');
    }

    public function test_update_null_name(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile', [
            'name' => null,
            'email' => 'admin@example.org',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('email');
        $response->assertSessionHasErrors('name');
        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        $response->assertRedirect();
    }

    public function test_update_integer_name(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile', [
            'name' => 1,
            'email' => 'admin@example.org',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('email');
        $response->assertSessionHasErrors('name');
        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        $response->assertRedirect();
    }

    public function test_update_null_email(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile', [
            'name' => 'Admin 1',
            'email' => null,
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('email');
        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        $response->assertRedirect();
    }

    public function test_update_invalid_email(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile', [
            'name' => 'Admin 1',
            'email' => 'invalid email',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('email');
        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'name' => 'Admin 1',
            'email' => 'admin@example.org',
        ]);
        $response->assertRedirect();
    }

    public function test_update_password_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->put('profile/password', [
            'password' => 'Welcome01!',
            'password_confirmation' => 'Welcome01!',
        ]);
        // Assert
        $this->assertGuest();
        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => 'Welcome01!']));
        $response->assertRedirect();
    }

    public function test_update_password_as_user_bad_password(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user, false)->put('profile/password', [
            'password' => 'new-password',
            'password_confirmation' => 'new-password',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionHasErrors('password');
        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => 'new-password']));
        $response->assertRedirect();
    }

    public function test_update_password_bad_password(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile/password', [
            'password' => 'new-password',
            'password_confirmation' => 'new-password',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionHasErrors('password');
        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => 'new-password']));
        $response->assertRedirect();
    }

    public function test_update_password_no_confirmation(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile/password', [
            'password' => 'Welcome01!',
            'password_confirmation' => 'Welcome01!_different',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionHasErrors(['current_password', 'password']);
        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => 'Welcome01!']));
        $response->assertRedirect();
    }

    public function test_update_password_wrong_current_password(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile/password', [
            'current_password' => 'wrong-password',
            'password' => null,
            'password_confirmation' => null,
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionHasErrors('current_password');
        $response->assertSessionDoesntHaveErrors('password');
        $this->assertTrue(Auth::attempt(['email' => $user->email, 'password' => 'password']));
        $response->assertRedirect();
    }

    public function test_update_password(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth($user)->put('profile/password', [
            'current_password' => 'password',
            'password' => 'Welcome01!',
            'password_confirmation' => 'Welcome01!',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertSessionDoesntHaveErrors();
        $this->assertTrue(Auth::attempt(['email' => $user->email, 'password' => 'Welcome01!']));
        $response->assertRedirect();
    }

    public function test_update_password_with_null_value(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $this
            ->withAuth($user)
            ->put('profile/password', [
                'current_password' => 'password',
                'password' => null,
                'password_confirmation' => null,
            ])
            ->assertSessionDoesntHaveErrors()
            ->assertRedirect();
        // Assert
        $this->assertGuest(); // User should be logged o
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'password' => null,
        ]);
        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => null]));
    }

    public function test_update_password_minimum_length(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $this
            ->withAuth($user)
            ->put('profile/password', [
                'password' => 'short',
                'password_confirmation' => 'short',
            ])
            ->assertSessionHasErrors('password')
            ->assertRedirect();
        // Assert
        $this->assertAuthenticated();
        $this->assertFalse(Auth::attempt(['email' => $user->email, 'password' => 'short']));
    }
}
