<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ImpersonatedSessionControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_store_guest(): void
    {
        // Declare
        $user = $this->makeUser();

        // Run
        $response = $this->post(sprintf('/impersonate/%s', $user->email));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_store_normal_user(): void
    {
        // Declare
        $user = $this->makeUser(false);

        // Run
        $response = $this->withAuth(admin: false)->post(sprintf('/impersonate/%s', $user->email));

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $response->assertSessionMissing('original_user');
    }

    public function test_store_admin(): void
    {
        // Declare
        $user = $this->makeUser(true);
        $impersonating = $this->makeUser(false);

        // Run
        $response = $this->withAuth($user)->post(sprintf('/impersonate/%s', $impersonating->email));

        // Assert
        $this->assertAuthenticatedAs($impersonating);
        $response->assertRedirect('executions');
        $response->assertSessionHas([
            'success',
            'original_user' => $user->id,
        ]);
    }

    public function test_store_self_admin(): void
    {
        // Declare
        $user = $this->makeUser(true);

        // Run
        $response = $this->withAuth($user)->post(sprintf('/impersonate/%s', $user->email));

        // Assert
        $this->assertAuthenticatedAs($user);
        $response->assertForbidden();
        $response->assertSessionMissing(['original_user', 'success']);
    }

    public function test_store_already_impersonating_admin(): void
    {
        // Declare
        $user = $this->makeUser(true);
        $impersonating = $this->makeUser(true);

        // Run
        $this->withAuth($user)->post(sprintf('/impersonate/%s', $impersonating->email));
        $response = $this->withAuth($impersonating)->post(sprintf('/impersonate/%s', $user->email));

        // Assert
        $this->assertAuthenticatedAs($impersonating);
        $response->assertRedirect();
        $response->assertSessionHas([
            'danger',
            'original_user' => $user->id,
        ]);
    }

    public function test_destroy_guest(): void
    {
        // Declare
        $this->makeUser(false);

        // Run
        $response = $this->delete('/impersonate');

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_destroy_normal_user(): void
    {
        // Declare
        $user = $this->makeUser(false);
        $impersonating = $this->makeUser();

        $this->actingAs($impersonating);
        $this->withSession([
            'original_user' => $user->id,
        ]);

        // Run
        $response = $this->delete('/impersonate');

        // Assert
        $this->assertAuthenticatedAs($impersonating);
        $response->assertForbidden();
    }

    public function test_destroy_not_impersonating_admin(): void
    {
        // Declare
        $user = $this->makeUser();

        $this->actingAs($user);

        // Run
        $response = $this->delete('/impersonate');

        // Assert
        $this->assertAuthenticatedAs($user);
        $response->assertRedirect();
        $response->assertSessionHas([
            'danger',
        ]);
    }

    public function test_destroy_admin(): void
    {
        // Declare
        $user = $this->makeUser();
        $impersonating = $this->makeUser();

        $this->actingAs($impersonating);
        $this->withSession([
            'original_user' => $user->id,
        ]);

        // Run
        $response = $this->delete('/impersonate');

        // Assert
        $this->assertAuthenticatedAs($user);
        $response->assertRedirect('users');
        $response->assertSessionHas([
            'success',
        ]);
        $response->assertSessionMissing([
            'original_user',
        ]);
    }
}
