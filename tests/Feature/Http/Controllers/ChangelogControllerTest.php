<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ChangelogControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest(): void
    {
        // Run
        $response = $this->get('changelog');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get('changelog');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
    }

    public function test_admin(): void
    {
        // Run
        $response = $this->withAuth()->get('changelog');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
    }
}
