<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\RubricTask;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RubricTaskGuidelineControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_edit_guest(): void
    {
        // Declare
        $this->seed();
        $task = RubricTask::query()->firstOrFail();
        // Run
        $response = $this->get("guidelines/{$task->id}/edit");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_user(): void
    {
        // Declare
        $this->seed();
        $task = RubricTask::query()->firstOrFail();
        // Run
        $response = $this->withAuth(admin: false)->get("guidelines/{$task->id}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit_admin(): void
    {
        // Declare
        $this->seed();
        $task = RubricTask::query()
            ->with('items')
            ->firstOrFail();
        // Run
        $response = $this->withAuth()->get("guidelines/{$task->id}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('guidelines.edit');
        $response->assertSeeTextInOrder([
            'Onderlegger',
            $task->name,
            $task->criteria,
            ...$task->items->pluck('guideline'),
        ]);
    }

    public function test_update_guest(): void
    {
        // Declare
        $this->seed();
        $task = RubricTask::query()->firstOrFail();
        // Run
        $response = $this->put("guidelines/{$task->id}");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_user(): void
    {
        // Declare
        $this->seed();
        $task = RubricTask::query()->firstOrFail();
        // Run
        $response = $this->withAuth(admin: false)->put("guidelines/{$task->id}");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_update_admin(): void
    {
        // Declare
        $this->seed();
        $task = RubricTask::query()->firstOrFail();
        // Run
        $response = $this->withAuth()->put("guidelines/{$task->id}", [
            'guidelines' => [
                $id = $task->items->value('id') => $guideline = 'test text',
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect();
        $this->assertDatabaseHas('rubric_task_items', [
            'id' => $id,
            'rubric_task_id' => $task->id,
            'guideline' => $guideline,
        ]);
    }
}
