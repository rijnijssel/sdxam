<?php

namespace Tests\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_login_page_guest(): void
    {
        // Run
        $response = $this->get('login');
        // Assert
        $this->assertGuest();
        $response->assertOk();
        $response->assertSeeInOrder([
            'Inloggen',
            'E-mailadres',
            'Wachtwoord',
            'Onthouden',
            'Log in', 'Wachtwoord vergeten?',
        ]);
    }

    public function test_login_page_as_user(): void
    {
        // Run
        $response = $this->withAuth(null, false)->get('login');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions');
    }

    public function test_login_page(): void
    {
        // Run
        $response = $this->withAuth()->get('login');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions');
    }

    public function test_login_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->post('login', [
            'email' => $user->email,
            'password' => 'password',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions');
    }

    public function test_login_guest_as_user(): void
    {
        // Declare
        $user = $this->makeUser(false);
        // Run
        $response = $this->post('login', [
            'email' => $user->email,
            'password' => 'password',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions');
    }

    public function test_login_null_email_guest(): void
    {
        // Run
        $response = $this->post('login', [
            'email' => null,
            'password' => 'password',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertSessionDoesntHaveErrors('password');
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_login_invalid_email_guest(): void
    {
        // Run
        $response = $this->post('login', [
            'email' => 'invalid email',
            'password' => 'password',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertSessionDoesntHaveErrors('password');
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_login_null_password_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->post('login', [
            'email' => $user->email,
            'password' => null,
        ]);
        // Assert
        $this->assertGuest();
        $response->assertSessionDoesntHaveErrors('email');
        $response->assertSessionHasErrors('password');
        $response->assertRedirect();
    }

    public function test_login_invalid_password_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->post('login', [
            'email' => $user->email,
            'password' => 'invalid password',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertSessionDoesntHaveErrors('password');
        $response->assertSessionHasErrors('email'); // errors are always thrown on email field
        $response->assertRedirect();
    }

    public function test_login(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()->post('login', [
            'email' => $user->email,
            'password' => 'password',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions');
    }

    public function test_logout_guest(): void
    {
        // Run
        $response = $this->post('logout');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('/');
    }

    public function test_logout(): void
    {
        // Run
        $response = $this->withAuth()->post('logout');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('/');
    }
}
