<?php

namespace Tests\Feature\Http\Controllers\Auth;

use App\Models\User as UserModel;
use App\Providers\AppServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Iterator;
use Laravel\Socialite\Contracts\Provider;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;
use PHPUnit\Framework\Attributes\DataProvider;
use SocialiteProviders\Manager\OAuth2\User;
use Tests\TestCase;

class ExternalAuthenticationControllerTest extends TestCase
{
    use RefreshDatabase;

    public static function services(): Iterator
    {
        yield 'gitlab disabled, without existing user' => ['gitlab', false, false];
        yield 'gitlab disabled, with existing user' => ['gitlab', false, true];
        yield 'gitlab enabled, without existing user' => ['gitlab', true, false];
        yield 'gitlab enabled, with existing user' => ['gitlab', true, true];
        yield 'surfconext disabled, without existing user' => ['surfconext', false, false];
        yield 'surfconext disabled, with existing user' => ['surfconext', false, true];
        yield 'surfconext enabled, without existing user' => ['surfconext', true, false];
        yield 'surfconext enabled, with existing user' => ['surfconext', true, true];
        yield 'non-existent service' => ['non-existent', false, true];
    }

    #[DataProvider('services')]
    public function test_create(string $service, bool $enabled): void
    {
        Config::set("services.{$service}.enabled", $enabled);

        $response = $this->get("sso/{$service}/redirect");

        if ($enabled) {
            $response->assertRedirect();
        } else {
            $response->assertNotFound();
        }
    }

    #[DataProvider('services')]
    public function test_store(string $service, bool $enabled, bool $exists): void
    {
        Config::set("services.{$service}.enabled", $enabled);

        if ($exists) {
            $user = UserModel::factory()->createOne([
                'name' => 'Test User',
                'email' => 'test@example.com',
            ]);
        }

        /* @phpstan-ignore-next-line */
        Socialite::partialMock()
            ->shouldReceive('driver')
            ->with($service)
            ->andReturn(new class implements Provider
            {
                public function redirect()
                {
                    return redirect('/');
                }

                public function user()
                {
                    $user = new User;
                    $user->name = 'Test User';
                    $user->email = 'test@example.com';

                    return $user;
                }
            });

        $response = $this->get("sso/{$service}/callback");

        if (! $enabled) {
            $response->assertNotFound();
        } elseif (! $exists) {
            $response->assertSessionHasErrors(['socialite' => 'test@example.com']);
            $response->assertForbidden();
        } else {
            $this->assertAuthenticatedAs($user);
            $response->assertRedirect(AppServiceProvider::HOME);
        }
    }

    #[DataProvider('services')]
    public function test_store_with_invalid_state_exception(string $service, bool $enabled, bool $exists): void
    {
        Config::set("services.{$service}.enabled", $enabled);

        /* @phpstan-ignore-next-line */
        Socialite::partialMock()
            ->shouldReceive('driver')
            ->with($service)
            ->andReturn(new class implements Provider
            {
                public function redirect()
                {
                    return redirect('/');
                }

                public function user(): void
                {
                    throw new InvalidStateException;
                }
            });

        $response = $this->get("sso/{$service}/callback");

        if (! $enabled) {
            $response->assertNotFound();
        } else {
            $this->assertGuest();
            $response->assertRedirect('login');
        }
    }
}
