<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_guest(): void
    {
        // Run
        $response = $this->get('students');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_user(): void
    {
        // Run
        $response = $this->withAuth(null, false)
            ->get('students');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.index');
        $response->assertDontSeeText('Toevoegen');
        $response->assertSeeTextInOrder([
            'Studenten',
            'SID', 'Voornaam', 'Tussenvoegsel', 'Achternaam', 'Acties',
        ]);
        $response->assertSeeHtml('table');
    }

    public function test_index(): void
    {
        $student = $this->makeStudent();
        $graduated = $this->makeStudent(true);
        $response = $this->withAuth()
            ->get('students');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.index');
        $response->assertSeeTextInOrder([
            'Studenten',
            'Toevoegen',
            'SID', 'Voornaam', 'Tussenvoegsel', 'Achternaam', 'Acties',
            $student->sid, $student->first_name,
        ]);
        $response->assertDontSee($graduated->sid);
        $response->assertSeeHtml('table');
    }

    public function test_create_guest(): void
    {
        // Run
        $response = $this->get('students/create');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_create_as_user(): void
    {
        // Run
        $response = $this->withAuth(null, false)
            ->get('students/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_create(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('students/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.create');
        $response->assertSeeTextInOrder([
            'Toevoegen',
            'Algemene informatie',
            'Studentnummer',
            'Voornaam', 'Tussenvoegsel', 'Achternaam',
            'Toevoegen', 'Annuleren',
        ]);
    }

    public function test_store_guest(): void
    {
        // Run
        $response = $this->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        $response->assertRedirect('login');
    }

    public function test_store_as_user(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth(null, false)->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseCount('students', 0);
    }

    public function test_store(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseHas('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('students');
    }

    public function test_store_null_sid(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => null,
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseMissing('students', [
            'sid' => null,
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['first_name', 'middle_name', 'last_name', 'email']);
        $response->assertSessionHasErrors('sid');
        $response->assertRedirect();
    }

    public function test_store_invalid_sid(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => '00ABABAB',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseMissing('students', [
            'sid' => '00ABABAB',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['first_name', 'middle_name', 'last_name', 'email']);
        $response->assertSessionHasErrors('sid');
        $response->assertRedirect();
    }

    public function test_store_null_middle_name(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => null,
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseHas('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => null,
            'last_name' => 'last',
            'email' => 'user@example.org',
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('students');
    }

    public function test_store_null_email(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => null,
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseHas('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => null,
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('students');
    }

    public function test_store_invalid_email(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'invalid email',
            'cohort' => '2016',
        ]);
        // Assert
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => 'invalid email',
            'cohort' => '2016',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['sid', 'first_name', 'middle_name', 'last_name']);
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_store_invalid_cohort(): void
    {
        $this->assertDatabaseCount('students', 0);
        // Run
        $response = $this->withAuth()->post('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => null,
            'cohort' => '1900',
        ]);
        // Assert
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => 'first',
            'middle_name' => 'middle',
            'last_name' => 'last',
            'email' => null,
            'cohort' => '1900',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['sid', 'first_name', 'middle_name', 'last_name', 'email']);
        $response->assertSessionHasErrors('cohort');
        $response->assertRedirect();
    }

    public function test_show_guest(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->get('students/'.$student->sid);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_missing_guest(): void
    {
        // Run
        $response = $this->get('students/00121212');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->withAuth(null, false)
            ->get('students/'.$student->sid);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.show');
        $response->assertSeeInOrder([
            'Student',
            'Algemene informatie',
            'Studentnummer', $student->sid,
            'Voornaam', $student->first_name,
            'Tussenvoegsel', $student->middle_name,
            'Achternaam', $student->last_name,
        ]);
    }

    public function test_show(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->withAuth()
            ->get('students/'.$student->sid);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.show');
        $response->assertSeeInOrder([
            'Student', 'Bewerken',
            'Algemene informatie',
            'Studentnummer', $student->sid,
            'Voornaam', $student->first_name,
            'Tussenvoegsel', $student->middle_name,
            'Achternaam', $student->last_name,
        ]);
    }

    public function test_show_missing(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('students/00121212');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_edit_guest(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->get("students/{$student->sid}/edit");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_missing_guest(): void
    {
        // Run
        $response = $this->get('students/00121212/edit');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_as_user(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->withAuth(null, false)
            ->get("students/{$student->sid}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->withAuth()
            ->get("students/{$student->sid}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.edit');
        $response->assertSeeInOrder([
            'Student',
            'Algemene informatie',
            'Studentnummer', $student->sid,
            'Voornaam', $student->first_name,
            'Tussenvoegsel', $student->middle_name,
            'Achternaam', $student->last_name,
        ]);
    }

    public function test_edit_missing(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('students/00121212/edit');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_guest(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->put('students/'.$student->sid);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_missing_guest(): void
    {
        // Run
        $response = $this->put('students/00121212');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_user(): void
    {
        $student = $this->makeStudent();
        $response = $this->withAuth(null, false)
            ->put('students/'.$student->sid, [
                'sid' => '00121212',
                'first_name' => 'first updated',
                'middle_name' => 'middle updated',
                'last_name' => 'last updated',
                'email' => 'user.updated@example.org',
                'cohort' => '2016',
            ]);
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => 'first updated',
            'middle_name' => 'middle updated',
            'last_name' => 'last updated',
            'email' => 'user.updated@example.org',
            'cohort' => '2016',
        ]);
    }

    public function test_update(): void
    {
        $student = $this->makeStudent();
        $response = $this->withAuth()
            ->put('students/'.$student->sid, [
                'sid' => '00121212',
                'first_name' => 'first updated',
                'middle_name' => 'middle updated',
                'last_name' => 'last updated',
                'email' => 'user.updated@example.org',
                'cohort' => '2016',
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseHas('students', [
            'sid' => '00121212',
            'first_name' => 'first updated',
            'middle_name' => 'middle updated',
            'last_name' => 'last updated',
            'email' => 'user.updated@example.org',
            'cohort' => '2016',
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('students/00121212');
    }

    public function test_update_missing(): void
    {
        // Run
        $response = $this->withAuth()->put('students/00121212');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_invalid_sid(): void
    {
        $student = $this->makeStudent();
        $response = $this->withAuth()
            ->put('students/'.$student->sid, [
                'sid' => '00ABABAB',
                'first_name' => 'first updated',
                'middle_name' => 'middle updated',
                'last_name' => 'last updated',
                'email' => 'user.updated@example.org',
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('students', [
            'sid' => '00ABABAB',
            'first_name' => 'first updated',
            'middle_name' => 'middle updated',
            'last_name' => 'last updated',
            'email' => 'user.updated@example.org',
        ]);
        $response->assertSessionDoesntHaveErrors(['first_name', 'middle_name', 'last_name', 'email']);
        $response->assertSessionHasErrors('sid');
        $response->assertRedirect();
    }

    public function test_update_null_sid(): void
    {
        $student = $this->makeStudent();
        $response = $this->withAuth()
            ->put('students/'.$student->sid, [
                'sid' => null,
                'first_name' => 'first updated',
                'middle_name' => 'middle updated',
                'last_name' => 'last updated',
                'email' => 'user.updated@example.org',
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('students', [
            'sid' => null,
            'first_name' => 'first updated',
            'middle_name' => 'middle updated',
            'last_name' => 'last updated',
            'email' => 'user.updated@example.org',
        ]);
        $response->assertSessionDoesntHaveErrors(['first_name', 'middle_name', 'last_name', 'email']);
        $response->assertSessionHasErrors('sid');
        $response->assertRedirect();
    }

    public function test_update_null_name(): void
    {
        $student = $this->makeStudent();
        $response = $this->withAuth()
            ->put('students/'.$student->sid, [
                'sid' => '00121212',
                'first_name' => null,
                'middle_name' => null,
                'last_name' => null,
                'email' => 'user.updated@example.org',
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => null,
            'middle_name' => null,
            'last_name' => null,
            'email' => 'user.updated@example.org',
        ]);
        $response->assertSessionDoesntHaveErrors(['sid', 'middle_name', 'email']);
        $response->assertSessionHasErrors(['first_name', 'last_name']);
        $response->assertRedirect();
    }

    public function test_update_invalid_email(): void
    {
        $student = $this->makeStudent();
        $response = $this->withAuth()
            ->put('students/'.$student->sid, [
                'sid' => '00121212',
                'first_name' => 'first updated',
                'middle_name' => null,
                'last_name' => 'last updated',
                'email' => 'invalid email',
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('students', [
            'sid' => '00121212',
            'first_name' => 'first updated',
            'middle_name' => null,
            'last_name' => 'last updated',
            'email' => 'invalid email',
        ]);
        $response->assertSessionDoesntHaveErrors(['sid', 'first_name', 'middle_name', 'last_name']);
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_destroy_guest(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->delete('students/'.$student->id);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseHas('students', [
            'sid' => $student->sid,
            'first_name' => $student->first_name,
            'middle_name' => $student->middle_name,
            'last_name' => $student->last_name,
            'email' => $student->email,
        ]);
        $response->assertRedirect('login');
    }

    public function test_destroy_as_user(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->withAuth(null, false)->delete('students/'.$student->sid);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseHas('students', [
            'sid' => $student->sid,
            'first_name' => $student->first_name,
            'middle_name' => $student->middle_name,
            'last_name' => $student->last_name,
            'email' => $student->email,
        ]);
    }

    public function test_destroy(): void
    {
        // Declare
        $student = $this->makeStudent();
        // Run
        $response = $this->withAuth()->delete('students/'.$student->sid);
        // Assert
        $this->assertAuthenticated();
        $this->assertSoftDeleted('students', [
            'sid' => $student->sid,
            'first_name' => $student->first_name,
            'middle_name' => $student->middle_name,
            'last_name' => $student->last_name,
            'email' => $student->email,
        ]);
        $response->assertRedirect('students');
    }

    public function test_destroy_missing(): void
    {
        // Run
        $response = $this->withAuth()->delete('students/00121212');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }
}
