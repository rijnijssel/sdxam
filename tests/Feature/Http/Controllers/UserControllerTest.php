<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_guest(): void
    {
        // Run
        $response = $this->get('users');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get('users');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_index(): void
    {
        // Run
        $response = $this->withAuth()->get('users');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('users.index');
        $response->assertSeeInOrder([
            'Gebruikers', 'Toevoegen',
            'Naam', 'Acties',
        ]);
    }

    public function test_create_guest(): void
    {
        // Run
        $response = $this->get('users/create');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_create_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get('users/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_create(): void
    {
        // Run
        $response = $this->withAuth()->get('users/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('users.create');
        $response->assertSeeInOrder([
            'Gebruiker toevoegen',
            'Persoonlijk',
            'Naam', 'E-mail',
            'Beveiliging',
            'Wachtwoord', 'Wachtwoord bevestiging',
            'Toevoegen', 'Annuleren',
        ]);
    }

    public function test_store_guest(): void
    {
        // Run
        $response = $this->post('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_store_as_user(): void
    {
        $this->assertDatabaseCount('users', 0);
        // Run
        $response = $this->withAuth(admin: false)->post('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        // Assert
        $this->assertDatabaseMissing('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
        ]);
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_store(): void
    {
        $this->assertDatabaseCount('users', 0);
        // Run
        $response = $this->withAuth()->post('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        // Assert
        $this->assertDatabaseHas('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('users');
    }

    public function test_store_null_name(): void
    {
        $this->assertDatabaseCount('users', 0);
        // Run
        $response = $this->withAuth()->post('users', [
            'name' => null,
            'email' => 'user@example.org',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        // Assert
        $this->assertDatabaseMissing('users', [
            'name' => null,
            'email' => 'user@example.org',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['email', 'password']);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_store_null_email(): void
    {
        $this->assertDatabaseCount('users', 0);
        // Run
        $response = $this->withAuth()->post('users', [
            'name' => 'Test User',
            'email' => null,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        // Assert
        $this->assertDatabaseMissing('users', [
            'name' => 'Test User',
            'email' => null,
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['name', 'password']);
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_store_invalid_password(): void
    {
        $this->assertDatabaseCount('users', 0);
        // Run
        $response = $this->withAuth()->post('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
            'password' => 'short',
            'password_confirmation' => 'short',
        ]);
        // Assert
        $this->assertDatabaseMissing('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['name', 'email']);
        $response->assertSessionHasErrors('password');
        $response->assertRedirect();
    }

    public function test_store_no_password_confirmation(): void
    {
        $this->assertDatabaseCount('users', 0);
        // Run
        $response = $this->withAuth()->post('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
            'password' => 'password',
            'password_confirmation' => 'different',
        ]);
        // Assert
        $this->assertDatabaseMissing('users', [
            'name' => 'Test User',
            'email' => 'user@example.org',
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors(['name', 'email']);
        $response->assertSessionHasErrors('password');
        $response->assertRedirect();
    }

    public function test_show_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->get('users/'.$user->email);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_missing_guest(): void
    {
        // Run
        $response = $this->get('users/user@example.org');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_self(): void
    {
        // Declare
        $user = $this->makeUser(admin: false);
        // Run
        $response = $this->withAuth(user: $user)
            ->get('users/'.$user->email);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_show_as_user(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth(admin: false)
            ->get('users/'.$user->email);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_show(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()
            ->get('users/'.$user->email);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('users.show');
        $response->assertSeeHtmlInOrder([
            'Gebruiker', 'Bewerken',
            'Algemene informatie',
            'Naam', $user->name,
            'E-mail', $user->email,
        ]);
    }

    public function test_show_missing(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('users/user@example.org');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_edit_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->get("users/{$user->email}/edit");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_missing_guest(): void
    {
        // Run
        $response = $this->get('students/user@example.org/edit');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_as_user(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth(admin: false)
            ->get("users/{$user->email}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit_as_self(): void
    {
        // Declare
        $user = $this->makeUser(admin: false);
        // Run
        $response = $this->withAuth(user: $user)
            ->get("users/{$user->email}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()
            ->get("users/{$user->email}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('users.edit');
        $response->assertSeeInOrder([
            'Gebruiker',
            'Algemene informatie',
            'Naam', $user->name,
            'E-mail', $user->email,
            'Opslaan', 'Verwijderen', 'Annuleren',
        ]);
    }

    public function test_edit_missing(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('users/user@example.org/edit');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->put('users/'.$user->email);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_missing_guest(): void
    {
        // Run
        $response = $this->put('users/user@example.org');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_user(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth(admin: false)
            ->put('users/'.$user->email, [
                'name' => 'name updated',
                'email' => 'user.updated@example.org',
            ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('users', [
            'name' => 'name updated',
            'email' => 'user.updated@example.org',
        ]);
    }

    public function test_update_self(): void
    {
        // Declare
        $user = $this->makeUser(admin: false);
        // Run
        $response = $this->withAuth(user: $user)
            ->put('users/'.$user->email, [
                'name' => 'name updated',
                'email' => 'user.updated@example.org',
            ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('users', [
            'name' => 'name updated',
            'email' => 'user.updated@example.org',
        ]);
    }

    public function test_update(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()
            ->put('users/'.$user->email, [
                'name' => 'name updated',
                'email' => 'user.updated@example.org',
            ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('users', [
            'name' => 'name updated',
            'email' => 'user.updated@example.org',
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('users/user.updated@example.org');
    }

    public function test_update_missing(): void
    {
        // Run
        $response = $this->withAuth()->put('users/user@example.org');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_null_name(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()
            ->put('users/'.$user->email, [
                'name' => null,
                'email' => 'user.updated@example.org',
            ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('users', [
            'name' => null,
            'email' => 'user.updated@example.org',
        ]);
        $response->assertSessionDoesntHaveErrors('email');
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_update_null_email(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()
            ->put('users/'.$user->email, [
                'name' => 'name updated',
                'email' => null,
            ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('users', [
            'name' => 'name updated',
            'email' => null,
        ]);
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_update_invalid_email(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()
            ->put('users/'.$user->email, [
                'name' => 'name updated',
                'email' => 'invalid email',
            ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('users', [
            'name' => 'name updated',
            'email' => 'invalid email',
        ]);
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('email');
        $response->assertRedirect();
    }

    public function test_destroy_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->delete('users/'.$user->email);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
        ]);
        $response->assertRedirect('login');
    }

    public function test_destroy_as_user(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth(admin: false)->delete('users/'.$user->email);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }

    public function test_destroy_as_self(): void
    {
        // Declare
        $user = $this->makeUser(admin: false);
        // Run
        $response = $this->withAuth(user: $user)->delete('users/'.$user->email);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
        ]);
    }

    public function test_destroy(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()->delete('users/'.$user->email);
        // Assert
        $this->assertAuthenticated();
        $this->assertSoftDeleted('users', [
            'email' => $user->email,
        ]);
        $response->assertRedirect('users');
    }

    public function test_destroy_missing(): void
    {
        // Run
        $response = $this->withAuth()->delete('users/user@example.org');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }
}
