<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExaminerControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_guest(): void
    {
        // Run
        $response = $this->get('examiners');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)
            ->get('examiners');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_index(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth()
            ->get('examiners');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('examiners.index');
        $response->assertSeeTextInOrder([
            'Examinatoren',
            'Toevoegen',
            'Naam', 'E-mail',
            $examiner->name, $examiner->getEmail(), 'Bewerken',
        ]);
        $response->assertSeeHtml('table');
    }

    public function test_create_guest(): void
    {
        // Run
        $response = $this->get('examiners/create');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_create_as_user(): void
    {
        // Run
        $response = $this->withAuth(null, false)
            ->get('examiners/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_create_as_admin(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('examiners/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('examiners.create');
        $response->assertSeeTextInOrder([
            'Examinator toevoegen',
            'Algemene informatie',
            'Naam',
            'Gebruiker',
            'Toevoegen',
            'Annuleren',
        ]);
    }

    public function test_store_guest(): void
    {
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->post('examiners', [
            'name' => 'Test Examiner',
            'user' => $user->email,
        ]);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_store_as_user(): void
    {
        $this->assertDatabaseCount('examiners', 0);
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth(admin: false)
            ->post('examiners', [
                'name' => 'Test Examiner',
                'user' => $user->email,
            ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseCount('examiners', 0);
    }

    public function test_store_as_admin(): void
    {
        $this->assertDatabaseCount('examiners', 0);
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()->post('examiners', [
            'name' => 'Test Examiner',
            'abbreviation' => 'ABCDE',
            'user' => $user->email,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('examiners', [
            'name' => 'Test Examiner',
            'abbreviation' => 'ABCDE',
            'user_id' => $user->id,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('examiners');
    }

    public function test_store_null_name(): void
    {
        $this->assertDatabaseCount('examiners', 0);
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()->post('examiners', [
            'name' => null,
            'user' => $user->email,
        ]);
        // Assert
        $this->assertDatabaseMissing('examiners', [
            'name' => null,
            'user_id' => $user->id,
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('user');
        $response->assertSessionHasErrors('name');
    }

    public function test_store_invalid_name(): void
    {
        $this->assertDatabaseCount('examiners', 0);
        // Declare
        $user = $this->makeUser();
        // Run
        $response = $this->withAuth()->post('examiners', [
            'name' => 1,
            'user' => $user->email,
        ]);
        // Assert
        $this->assertDatabaseMissing('examiners', [
            'name' => 1,
            'user_id' => $user->id,
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('user');
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_store_null_user(): void
    {
        $this->assertDatabaseCount('examiners', 0);
        // Run
        $response = $this->withAuth()->post('examiners', [
            'name' => 'Test Examiner',
            'user' => null,
        ]);
        // Assert
        $this->assertDatabaseMissing('examiners', [
            'name' => 'Test Examiner',
            'user_id' => null,
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('user');
        $response->assertRedirect();
    }

    public function test_store_invalid_user(): void
    {
        $this->assertDatabaseCount('examiners', 0);
        // Run
        $response = $this->withAuth()->post('examiners', [
            'name' => 'Test Examiner',
            'user' => 'user@example.org',
        ]);
        // Assert
        $this->assertDatabaseMissing('examiners', [
            'name' => 'Test Examiner',
            'user_id' => null,
        ]);
        $this->assertAuthenticated();
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('user');
        $response->assertRedirect();
    }

    public function test_show_guest(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->get('examiners/'.$examiner->abbreviation);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_missing_guest(): void
    {
        // Run
        $response = $this->get('examiners/abcde');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth(null, false)
            ->get('examiners/'.$examiner->abbreviation);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_show(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth()
            ->get('examiners/'.$examiner->abbreviation);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('examiners.show');
        $response->assertSeeInOrder([
            'Examinator', 'Bewerken',
            'Algemene informatie',
            'Naam',
            $examiner->name,
            'E-mail',
            "{$examiner->getEmail()}",
        ]);
    }

    public function test_show_missing(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('examiners/abcde');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_edit_guest(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->get("examiners/{$examiner->abbreviation}/edit");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_missing_guest(): void
    {
        // Run
        $response = $this->get('examiners/abcde/edit');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_as_user(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth(null, false)
            ->get("examiners/{$examiner->abbreviation}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth()
            ->get("examiners/{$examiner->abbreviation}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('examiners.edit');
        $response->assertSeeInOrder([
            'Examinator',
            'Algemene informatie',
            'Naam',
            $examiner->name,
            'Gebruiker',
            "{$examiner->getUserName()} ({$examiner->getEmail()})",
            'Opslaan', 'Verwijderen', 'Annuleren',
        ]);
    }

    public function test_edit_missing(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('examiners/abcde/edit');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_guest(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->put('examiners/'.$examiner->abbreviation, [
            'name' => 'Examiner Name',
            'abbreviation' => 'ABCDE',
        ]);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseMissing('examiners', [
            'name' => 'Examiner Name',
            'abbreviation' => 'ABCDE',
        ]);
        $response->assertRedirect('login');
    }

    public function test_update_missing_guest(): void
    {
        // Run
        $response = $this->put('examiners/abcde');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_user(): void
    {
        $examiner = $this->makeExaminer();
        $user = $this->makeUser();
        $response = $this->withAuth(null, false)
            ->put('examiners/'.$examiner->abbreviation, [
                'name' => 'Test Examiner updated',
                'abbreviation' => 'ABCDE',
                'user' => $user->email,
            ]);
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('examiners', [
            'name' => 'Test Examiner updated',
            'abbreviation' => 'ABCDE',
            'user_id' => $user->id,
        ]);
    }

    public function test_update(): void
    {
        $examiner = $this->makeExaminer();
        $user = $this->makeUser();
        $response = $this->withAuth()
            ->put('examiners/'.$examiner->abbreviation, [
                'name' => 'Test Examiner updated',
                'abbreviation' => 'ABCDE',
                'user' => $user->email,
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseHas('examiners', [
            'name' => 'Test Examiner updated',
            'abbreviation' => 'ABCDE',
            'user_id' => $user->id,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('examiners/ABCDE');
    }

    public function test_update_missing(): void
    {
        // Run
        $response = $this->withAuth()->put('examiners/abcde');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_invalid_name(): void
    {
        $examiner = $this->makeExaminer();
        $user = $this->makeUser();
        $response = $this->withAuth()
            ->put('examiners/'.$examiner->abbreviation, [
                'name' => null,
                'user' => $user->email,
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('examiners', [
            'name' => null,
            'user_id' => $user->id,
        ]);
        $response->assertSessionDoesntHaveErrors('user');
        $response->assertSessionHasErrors('name');
    }

    public function test_update_invalid_user(): void
    {
        $examiner = $this->makeExaminer();
        $response = $this->withAuth()
            ->put('examiners/'.$examiner->abbreviation, [
                'name' => 'Test Examiner updated',
                'user' => null,
            ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('examiners', [
            'name' => 'Test Examiner updated',
            'user_id' => null,
        ]);
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('user');
    }

    public function test_destroy_guest(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->delete('examiners/'.$examiner->abbreviation);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseHas('examiners', [
            'name' => $examiner->name,
            'user_id' => $examiner->user_id,
        ]);
        $response->assertRedirect('login');
    }

    public function test_destroy_as_user(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth(null, false)->delete('examiners/'.$examiner->abbreviation);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('examiners', [
            'name' => $examiner->name,
            'user_id' => $examiner->user_id,
        ]);
        $response->assertForbidden();
    }

    public function test_destroy(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        // Run
        $response = $this->withAuth()->delete('examiners/'.$examiner->abbreviation);
        // Assert
        $this->assertAuthenticated();
        $this->assertSoftDeleted('examiners', [
            'name' => $examiner->name,
            'user_id' => $examiner->user_id,
        ]);
        $response->assertRedirect('examiners');
    }

    public function test_destroy_missing(): void
    {
        // Run
        $response = $this->withAuth()->delete('examiners/abcde');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }
}
