<?php

namespace Tests\Feature\Http\Controllers\Execution;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CurrentExecutionControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_invoke_as_guest(): void
    {
        // Run
        $response = $this->get('executions/current');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_invoke_without_current_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get('executions/current');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions');
    }

    public function test_invoke_with_current_as_user(): void
    {
        // Declare
        $current = $this->makeExecution();
        $current->start_date = now()->subDay();
        $current->save();
        // Run
        $response = $this->withAuth(admin: false)->get('executions/current');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions/'.$current->slug);
    }

    public function test_invoke_without_current_as_admin(): void
    {
        // Run
        $response = $this->withAuth()->get('executions/current');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions');
    }

    public function test_invoke_with_current_as_admin(): void
    {
        // Declare
        $current = $this->makeExecution();
        $current->start_date = now()->subDay();
        $current->save();
        // Run
        $response = $this->withAuth()->get('executions/current');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions/'.$current->slug);
    }

    public function test_invoke_with_latest_as_admin(): void
    {
        // Declare
        $current = $this->makeExecution();
        $current->start_date = now()->addYear();
        $current->save();
        // Run
        $response = $this->withAuth()->get('executions/current');
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect('executions/'.$current->slug);
    }
}
