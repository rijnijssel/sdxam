<?php

namespace Tests\Feature\Http\Controllers\Execution\Result;

use App\Models\Exam;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\Process;
use App\Models\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Override;
use Tests\TestCase;

class ResultShowControllerTest extends TestCase
{
    use DatabaseMigrations;

    private Examiner $client;

    private Examiner $supervisor;

    private Execution $execution;

    private Student $student;

    private Process $process;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
        $this->client = $this->makeExaminer();
        $this->supervisor = $this->makeExaminer();
        $exam = Exam::query()->where('name', 'Crebo 25187')->firstOrFail();
        $this->execution = $this->makeExecution()->fill([
            'exam_id' => $exam->id,
        ]);
        $this->execution->save();
        $this->student = $this->makeStudent();
        $this->process = Process::query()->orderBy('id')->firstOrFail();
        $es = $this->execution->students()->updateOrCreate([
            'execution_id' => $this->execution->id,
            'student_id' => $this->student->id,
        ]);
        $es->executionStudentTasks()->updateOrCreate([
            'task_id' => $this->process->task?->id,
            'client_id' => $this->client->id,
            'supervisor_id' => $this->supervisor->id,
        ]);
    }

    public function test_invoke_guest(): void
    {
        $url = sprintf('executions/%s/results/%s/%s', $this->execution->slug, $this->student->sid, $this->process->id);
        $response = $this->get($url);

        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_invoke(): void
    {
        $url = sprintf('executions/%s/results/%s/%s', $this->execution->slug, $this->student->sid, $this->process->id);
        $response = $this->withAuth($this->client->user)
            ->get($url);

        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertSeeTextInOrder([
            'Opdrachtgever', $this->client->name,
            'Leidinggevende', $this->supervisor->name,
            ...$this->process->rubrics()->pluck('title'),
        ]);
    }

    public function test_invoke_no_examiner(): void
    {
        // Run
        $url = sprintf('executions/%s/results/%s/%s', $this->execution->slug, $this->student->sid, $this->process->id);
        $response = $this->withAuth()
            ->get($url);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }
}
