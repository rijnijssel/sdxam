<?php

namespace Tests\Feature\Http\Controllers\Execution;

use App\Http\Controllers\Execution\SharedResultsController;
use App\Notifications\SharedResults;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Attributes\CoversClass;
use Tests\TestCase;

#[CoversClass(SharedResultsController::class)]
class SharedResultsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_as_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->get("executions/{$execution->slug}/results/share");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
        // Run
        $response = $this->withAuth(admin: false)->get("executions/{$execution->slug}/results/share");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertHeader('content-type', 'application/pdf');
        $response->assertHeader('content-disposition', sprintf('inline; filename="Examenresultaten %s.pdf"', $execution->name));
    }

    public function test_show_as_admin(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/results/share");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertHeader('content-type', 'application/pdf');
        $response->assertHeader('content-disposition', sprintf('inline; filename="Examenresultaten %s.pdf"', $execution->name));
    }

    public function test_store_as_guest(): void
    {
        Storage::fake();
        Notification::fake();

        // Declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);
        // Run
        $response = $this->post("executions/{$execution->slug}/results/share");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
        Storage::assertMissing("results/{$execution->slug}.pdf");
        Notification::assertNothingSent();
    }

    public function test_store_as_user(): void
    {
        Storage::fake();
        Notification::fake();

        // Declare
        $user = $this->makeUser(admin: false);
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
        // Run
        $response = $this->withAuth($user)->post("executions/{$execution->slug}/results/share");
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect();
        $response->assertSessionHas('success');
        Storage::assertExists("results/{$execution->slug}.pdf");
        Notification::assertSentTo($user, SharedResults::class);
    }

    public function test_store_as_admin(): void
    {
        Storage::fake();
        Notification::fake();

        // Declare
        $user = $this->makeUser();
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
        // Run
        $response = $this->withAuth($user)->post("executions/{$execution->slug}/results/share");
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect();
        $response->assertSessionHas('success');
        Storage::assertExists("results/{$execution->slug}.pdf");
        Notification::assertSentTo($user, SharedResults::class);
    }
}
