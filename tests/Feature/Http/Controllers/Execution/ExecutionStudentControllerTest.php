<?php

namespace Tests\Feature\Http\Controllers\Execution;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExecutionStudentControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_edit_as_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->get("executions/{$execution->slug}/students/examiners");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->withAuth(admin: false)->get("executions/{$execution->slug}/students/examiners");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit_as_admin_with_student(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $execution->exam;

        $examiner = $this->makeExaminer();

        $student = $this->makeStudent();
        $executionStudent = $execution->students()->create([
            'student_id' => $student->id,
        ]);

        $task = $this->makeTask($exam);
        $executionStudent->executionStudentTasks()->create([
            'client_id' => $examiner->id,
            'task_id' => $task->id,
        ]);

        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/students/examiners");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.students.examiner');
        $response->assertSeeInOrder([
            'Examinatoren & context koppelen', 'Kerntaken koppelen',
            'Student', ...$execution->exam->tasks->pluck('name'),
            $student->getFullName(), $context->name, $examiner->name,
            'Opslaan', 'Annuleren',
        ]);
    }

    public function test_edit_as_admin_without_student(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $execution->exam;
        $student = $this->makeStudent();
        $examiner = $this->makeExaminer();
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/students/examiners");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.students.examiner');
        $response->assertDontSeeText([
            $student->getFullName(), $examiner->name, $context->name,
        ]);
        $response->assertSeeTextInOrder([
            'Examinatoren & context koppelen', 'Kerntaken koppelen',
            'Student', ...$exam->tasks->pluck('name'),
            'Opslaan', 'Annuleren',
        ]);
    }

    public function test_update_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $execution->exam;
        $student = $this->makeStudent();
        $executionStudent = $execution->students()->create([
            'student_id' => $student->id,
        ]);

        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $task = $this->makeTask($exam);
        $executionStudent->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);

        $context = $this->makeContext();
        // Run
        $response = $this->withAuth(admin: false)->put("executions/{$execution->slug}/students/examiners", [
            'students' => [
                $student->sid => [
                    $task->slug => [
                        'context' => $context->slug,
                        'client' => $client->id,
                        'supervisor' => $supervisor->id,
                    ],
                ],
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('execution_student_tasks', [
            'execution_student_id' => $executionStudent->id,
            'task_id' => $task->id,
            'context_id' => $context->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
    }

    public function test_update_as_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $execution->exam;
        $student = $this->makeStudent();
        $executionStudent = $execution->students()->create([
            'student_id' => $student->id,
        ]);

        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $task = $this->makeTask($exam);
        $executionStudent->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);

        $context = $this->makeContext();
        // Run
        $response = $this->put("executions/{$execution->slug}/students/examiners", [
            'students' => [
                $student->sid => [
                    $task->slug => [
                        'context' => $context->slug,
                        'client' => $client->id,
                        'supervisor' => $supervisor->id,
                    ],
                ],
            ],
        ]);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_admin(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $execution->exam;
        $student = $this->makeStudent();
        $executionStudent = $execution->students()->create([
            'student_id' => $student->id,
        ]);

        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $task = $this->makeTask($exam);
        $executionStudent->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);

        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->put("executions/{$execution->slug}/students/examiners", [
            'students' => [
                $student->sid => [
                    $task->slug => [
                        'context' => $context->slug,
                        'client' => $client->id,
                        'supervisor' => $supervisor->id,
                    ],
                ],
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('execution_student_tasks', [
            'execution_student_id' => $executionStudent->id,
            'task_id' => $task->id,
            'context_id' => $context->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
    }

    public function test_update_invalid_sid_as_admin(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $execution->exam;
        $student = $this->makeStudent();
        $executionStudent = $execution->students()->create([
            'student_id' => $student->id,
        ]);

        $client = $this->makeExaminer();
        $supervisor = $this->makeExaminer();
        $task = $this->makeTask($exam);
        $executionStudent->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);

        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->put("executions/{$execution->slug}/students/examiners", [
            'students' => [
                'not-found' => [
                    $task->slug => [
                        'context' => $context->slug,
                        'client' => $client->id,
                        'supervisor' => $supervisor->id,
                    ],
                ],
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect();
        $response->assertSessionHasErrors('students');
        $response->assertSessionMissing('success');
        $this->assertDatabaseMissing('execution_student_tasks', [
            'execution_student_id' => $executionStudent->id,
            'task_id' => $task->id,
            'context_id' => $context->id,
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
    }
}
