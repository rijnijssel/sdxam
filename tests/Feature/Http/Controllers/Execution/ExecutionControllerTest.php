<?php

namespace Tests\Feature\Http\Controllers\Execution;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExecutionControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_guest(): void
    {
        // Run
        $response = $this->get('executions');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->withAuth(admin: false)->get('executions');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.index');
        $response->assertDontSeeText('Toevoegen');
        $response->assertDontSeeText('Bewerken');
        $response->assertSeeInOrder([
            'Uitvoeringen',
            'Examen', $exam->name,
            'Groepen',
            'Studenten',
            'Ga naar uitvoering',
        ]);
        $response->assertDontSee(['Resultaten', 'Beoordelen']);
    }

    public function test_index(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->withAuth()->get('executions');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.index');
        $response->assertSeeInOrder([
            'Uitvoeringen', 'Toevoegen',
            'Examen', $exam->name,
            'Groepen',
            'Studenten',
            'Ga naar uitvoering',
        ]);
        $response->assertDontSee(['Resultaten', 'Beoordelen']);
    }

    public function test_index_as_examiner(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();

        $examiner = $this->makeExaminer();
        $task = $this->makeTask($exam);

        $student = $this->makeStudent();
        $executionStudent = $execution->students()->create([
            'student_id' => $student->id,
        ]);
        $executionStudent->executionStudentTasks()->create([
            'client_id' => $examiner->id,
            'task_id' => $task->id,
        ]);

        // Run
        $response = $this->withAuth(user: $examiner->user)->get('executions');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.index');
        $response->assertSeeInOrder([
            'Uitvoeringen',
            'Examen', $exam->name,
            'Groepen',
            'Studenten',
            'Ga naar uitvoering', 'Resultaten', 'Beoordelen',
        ]);
    }

    public function test_create_guest(): void
    {
        // Run
        $response = $this->get('executions/create');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_create_as_user(): void
    {
        // Run
        $response = $this->withAuth(null, false)->get('executions/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_create(): void
    {
        // Run
        $response = $this->withAuth()->get('executions/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.create');
        $response->assertSeeInOrder([
            'Uitvoering toevoegen',
            'Naam', 'Naam waaronder deze uitvoering bekend staat.',
            'Start datum',
            'Examen',
            'Toevoegen', 'Annuleren',
        ]);
    }

    public function test_store_guest(): void
    {
        // Run
        $response = $this->post('executions', [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01',
        ]);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01 00:00:00',
        ]);
        $response->assertRedirect('login');
    }

    public function test_store_as_user(): void
    {
        $this->assertDatabaseCount('executions', 0);
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth(null, false)->post('executions', [
            'name' => 'Execution 1',
            'exam' => $exam->slug,
            'start_date' => '2020-01-01',
        ]);
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseCount('executions', 0);
    }

    public function test_store(): void
    {
        $this->assertDatabaseCount('executions', 0);
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->post('executions', [
            'name' => 'Execution 1',
            'exam' => $exam->slug,
            'start_date' => '2020-01-01',
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('executions', [
            'name' => 'Execution 1',
            'slug' => 'execution-1',
            'exam_id' => $exam->id,
            'start_date' => '2020-01-01 00:00:00',
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions/execution-1');
    }

    public function test_store_null_exam(): void
    {
        $this->assertDatabaseCount('executions', 0);
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->post('executions', [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01',
            'exam' => null,
        ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01 00:00:00',
            'exam_id' => $exam->id,
        ]);
        $response->assertSessionDoesntHaveErrors(['name', 'start_date']);
        $response->assertSessionHasErrors('exam');
        $response->assertRedirect();
    }

    public function test_store_null_name(): void
    {
        $this->assertDatabaseCount('executions', 0);
        // Run
        $response = $this->withAuth()->post('executions', [
            'name' => null,
            'start_date' => '2020-01-01',
        ]);
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('executions', [
            'name' => null,
            'start_date' => '2020-01-01 00:00:00',
        ]);
        $response->assertSessionDoesntHaveErrors('start_date');
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_store_null_start_date(): void
    {
        $this->assertDatabaseCount('executions', 0);
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->post('executions', [
            'name' => 'Execution 1',
            'exam' => $exam->slug,
            'start_date' => null,
        ]);
        $this->assertAuthenticated();
        $this->assertDatabaseHas('executions', [
            'name' => 'Execution 1',
            'exam_id' => $exam->id,
            'start_date' => null,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions/execution-1');
    }

    public function test_show_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->get('executions/'.$execution->slug);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_missing_guest(): void
    {
        // Run
        $response = $this->get('executions/execution-1');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->withAuth(null, false)->get('executions/'.$execution->slug);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.show');
        $response->assertDontSeeText('Toevoegen');
        $response->assertDontSeeText('Bewerken');
        $response->assertSeeInOrder([
            'Uitvoering',
            'Algemene informatie',
            'Naam', $execution->name, 'Naam waaronder deze uitvoering bekend staat.',
            'Start datum', $execution->formatStartDate(),
            'Examen', $exam->name,
            'Examenversie die gebruikt wordt bij deze uitvoering.',
            'Groepen',
            'Naam', 'Studenten',
        ]);
    }

    public function test_show(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->withAuth()->get('executions/'.$execution->slug);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.show');
        $response->assertSeeInOrder([
            'Uitvoering',
            'Algemene informatie', 'Bewerken',
            'Naam', $execution->name, 'Naam waaronder deze uitvoering bekend staat.',
            'Start datum', $execution->formatStartDate(),
            'Examen', $exam->name,
            'Examenversie die gebruikt wordt bij deze uitvoering.',
            'Groepen', 'Toevoegen',
            'Naam', 'Studenten', 'Acties',
        ]);
    }

    public function test_show_missing(): void
    {
        // Run
        $response = $this->withAuth()->get('executions/execution-1');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_edit_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->get('executions/'.$execution->slug.'/edit');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_missing_guest(): void
    {
        // Run
        $response = $this->get('executions/execution-1/edit');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->withAuth(null, false)->get('executions/'.$execution->slug.'/edit');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        $execution->exam()->associate($exam);
        $execution->save();
        // Run
        $response = $this->withAuth()->get('executions/'.$execution->slug.'/edit');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.edit');
        $response->assertSeeInOrder([
            'Uitvoering bewerken',
            'Naam', $execution->name, 'Naam waaronder deze uitvoering bekend staat.',
            'Start datum', $execution->formatStartDate(),
            'Examen', $exam->name,
            'Opslaan', 'Verwijderen', 'Annuleren',
        ]);
    }

    public function test_edit_missing(): void
    {
        // Run
        $response = $this->withAuth()->get('executions/execution-1');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $response = $this->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01',
            'exam' => $exam->slug,
        ]);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
            'slug' => 'execution-1',
            'start_date' => '2020-01-01',
            'exam_id' => $exam->slug,
        ]);
        $response->assertRedirect('login');
    }

    public function test_update_missing_guest(): void
    {
        // Run
        $response = $this->put('executions/execution-1');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth(null, false)->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01',
            'exam' => $exam->slug,
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
            'slug' => 'execution-1',
            'start_date' => '2020-01-01 00:00:00',
            'exam_id' => $exam->id,
        ]);
    }

    public function test_update(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'start_date' => '2020-01-01',
            'exam' => $exam->slug,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('executions', [
            'name' => 'Execution 1',
            'slug' => 'execution-1',
            'start_date' => '2020-01-01 00:00:00',
            'exam_id' => $exam->id,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions/execution-1');
    }

    public function test_update_null_name(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->put('executions/'.$execution->slug, [
            'name' => null,
            'start_date' => '2020-01-01',
            'exam' => $exam->slug,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('executions', [
            'name' => null,
            'start_date' => '2020-01-01 00:00:00',
            'exam_id' => $exam->id,
        ]);
        $response->assertSessionDoesntHaveErrors(['start_date', 'exam']);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_update_null_start_date(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'exam' => $exam->slug,
            'start_date' => null,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('executions', [
            'name' => 'Execution 1',
            'exam_id' => $exam->id,
            'start_date' => null,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('executions/execution-1');
    }

    public function test_update_invalid_start_date(): void
    {
        // Declare
        $execution = $this->makeExecution();
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'exam' => $exam->slug,
            'start_date' => 'invalid date',
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
            'exam_id' => $exam->id,
        ]);
        $response->assertSessionDoesntHaveErrors(['name', 'slug', 'exam']);
        $response->assertSessionHasErrors('start_date');
        $response->assertRedirect();
    }

    public function test_update_null_exam(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->withAuth()->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'exam' => null,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
            'exam_id' => null,
        ]);
        $response->assertSessionDoesntHaveErrors('name');
        $response->assertSessionHasErrors('exam');
        $response->assertRedirect();
    }

    public function test_update_invalid_exam(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->withAuth()->put('executions/'.$execution->slug, [
            'name' => 'Execution 1',
            'exam' => 'exam-1',
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('executions', [
            'name' => 'Execution 1',
        ]);
        $response->assertSessionDoesntHaveErrors(['name', 'slug', 'start_date']);
        $response->assertSessionHasErrors('exam');
        $response->assertRedirect();
    }

    public function test_destroy_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->delete('executions/'.$execution->slug);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseHas('executions', [
            'name' => $execution->name,
        ]);
        $response->assertRedirect('login');
    }

    public function test_destroy_as_user(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->withAuth(null, false)->delete('executions/'.$execution->slug);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseHas('executions', [
            'name' => $execution->name,
        ]);
    }

    public function test_destroy(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->withAuth()->delete('executions/'.$execution->slug);
        // Assert
        $this->assertAuthenticated();
        $this->assertSoftDeleted('executions', [
            'name' => $execution->name,
        ]);
        $response->assertRedirect('executions');
    }

    public function test_destroy_missing(): void
    {
        // Run
        $response = $this->withAuth()->delete('executions/execution-1');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }
}
