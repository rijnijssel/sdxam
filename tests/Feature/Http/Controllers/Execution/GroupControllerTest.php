<?php

namespace Tests\Feature\Http\Controllers\Execution;

use App\Models\Examiner;
use App\Models\Execution;
use App\Models\Group;
use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Override;
use Tests\TestCase;

class GroupControllerTest extends TestCase
{
    use RefreshDatabase;

    private Execution $execution;

    private Group $group;

    private Examiner $examiner;

    private Student $student;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        $this->execution = $this->makeExecution();
        $this->group = $this->makeGroup($this->execution);
        $this->examiner = $this->makeExaminer();
        $this->group->examiners()->save($this->examiner);
        $this->student = $this->makeStudent();
        $this->group->students()->save($this->student);
        $this->execution->students()->create([
            'execution_id' => $this->execution->id,
            'student_id' => $this->student->id,
        ]);
    }

    public function test_create_guest(): void
    {
        // Run
        $response = $this->get(sprintf('executions/%s/groups/create', $this->execution->slug));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_create_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get(sprintf('executions/%s/groups/create', $this->execution->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_create_admin(): void
    {
        // Run
        $response = $this->withAuth()->get(sprintf('executions/%s/groups/create', $this->execution->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.groups.create');
        $response->assertSeeTextInOrder([
            'Groep toevoegen',
            'Naam',
            'Examinatoren',
            'Studenten',
            'Toevoegen', 'Annuleren',
        ]);
    }

    public function test_store_guest(): void
    {
        // Run
        $response = $this->post(sprintf('executions/%s/groups', $this->execution->slug), [
            'name' => 'Group A',
        ]);

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_store_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->post(sprintf('executions/%s/groups', $this->execution->slug), [
            'name' => 'Group A',
        ]);

        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('groups', [
            'name' => 'Group A',
        ]);
        $response->assertForbidden();
    }

    public function test_store_admin(): void
    {
        // Declare
        $examiner = Examiner::factory()->createOne();
        // Run
        $response = $this->withAuth()->post(sprintf('executions/%s/groups', $this->execution->slug), [
            'name' => 'Group A',
            'examiners' => [$examiner->id],
        ]);

        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('groups', [
            'name' => 'Group A',
        ]);
        $group = Group::query()->where('name', 'Group A')->firstOrFail();
        $this->assertDatabaseHas('group_examiners', [
            'group_id' => $group->id,
            'examiner_id' => $examiner->id,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect(sprintf('executions/%s', $this->execution->slug));
    }

    public function test_store_null_name_admin(): void
    {
        // Run
        $response = $this->withAuth()->post(sprintf('executions/%s/groups', $this->execution->slug), [
            'name' => null,
        ]);

        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('groups', [
            'name' => null,
        ]);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_show_guest(): void
    {
        // Run
        $response = $this->get(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_missing_guest(): void
    {
        // Run
        $response = $this->get(sprintf('executions/%s/groups/missing-group', $this->execution->slug));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.groups.show');
        $response->assertDontSeeText('Bewerken');
        $response->assertSeeInOrder([
            'Algemene informatie',
            'Naam', $this->group->name,
            'Gekoppelde examinatoren',
            e($this->examiner->name), e($this->examiner->getEmail()),
            'Gekoppelde studenten',
            e($this->student->first_name), e($this->student->last_name),
        ]);
    }

    public function test_show_admin(): void
    {
        // Run
        $response = $this->withAuth()->get(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.groups.show');
        $response->assertSeeInOrder([
            'Bewerken',
            'Algemene informatie',
            'Naam', $this->group->name,
            'Gekoppelde examinatoren',
            e($this->examiner->name), $this->examiner->getEmail(),
            'Gekoppelde studenten',
            e($this->student->first_name), e($this->student->last_name),
        ]);
    }

    public function test_edit_guest(): void
    {
        // Run
        $response = $this->get(sprintf('executions/%s/groups/%s/edit', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get(sprintf('executions/%s/groups/%s/edit', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit_admin(): void
    {
        // Run
        $response = $this->withAuth()->get(sprintf('executions/%s/groups/%s/edit', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('executions.groups.edit');
        $response->assertSeeInOrder([
            'Groep bewerken',
            'Naam', $this->group->name, 'Naam waaronder deze groep bekend staat.',
            'Examinatoren',
            $this->examiner->name,
            'Studenten',
            $this->student->getFullName(),
        ]);
    }

    public function test_edit_missing_admin(): void
    {
        // Run
        $response = $this->withAuth()->get(sprintf('executions/%s/groups/missing-group/edit', $this->execution->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_guest(): void
    {
        // Run
        $response = $this->put(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->put(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_update_admin(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $student = $this->makeStudent();

        // Run
        $response = $this->withAuth()->put(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug), [
            'name' => 'New Name',
            'examiners' => [
                $examiner->id,
            ],
            'students' => [
                $student->sid,
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('groups', [
            'name' => 'New Name',
        ]);
        $this->assertDatabaseHas('group_examiners', [
            'group_id' => $this->group->id,
            'examiner_id' => $examiner->id,
        ]);
        $this->assertDatabaseHas('group_students', [
            'group_id' => $this->group->id,
            'student_id' => $student->id,
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect();
    }

    public function test_update_null_name_admin(): void
    {
        // Declare
        $examiner = $this->makeExaminer();
        $student = $this->makeStudent();

        // Run
        $response = $this->withAuth()->put(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug), [
            'name' => null,
            'examiners' => [
                $examiner->id,
            ],
            'students' => [
                $student->sid,
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('groups', [
            'name' => null,
        ]);
        $this->assertDatabaseMissing('group_examiners', [
            'group_id' => $this->group->id,
            'examiner_id' => $examiner->id,
        ]);
        $this->assertDatabaseMissing('group_students', [
            'group_id' => $this->group->id,
            'student_id' => $student->id,
        ]);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_update_missing_admin(): void
    {
        // Run
        $response = $this->withAuth()->put(sprintf('executions/%s/groups/missing-group', $this->execution->slug), [
            'name' => 'New Name',
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('groups', [
            'name' => 'New Name',
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertNotFound();
    }

    public function test_destroy_guest(): void
    {
        // Run
        $response = $this->delete(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_destroy_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->delete(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseHas('groups', [
            'name' => $this->group->name,
        ]);
    }

    public function test_destroy_admin(): void
    {
        // Run
        $response = $this->withAuth()->delete(sprintf('executions/%s/groups/%s', $this->execution->slug, $this->group->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect();
        $this->assertDatabaseMissing('groups', [
            'name' => $this->group->name,
        ]);
    }

    public function test_destroy_missing_admin(): void
    {
        // Run
        $response = $this->withAuth()->delete(sprintf('executions/%s/groups/missing-group', $this->execution->slug));

        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }
}
