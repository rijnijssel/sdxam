<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Student;
use Database\Seeders\StudentSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentStatusControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_edit_guest(): void
    {
        // Run
        $response = $this->get('students/status');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get('students/status');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit_admin(): void
    {
        // Declare
        $this->seed(StudentSeeder::class);
        $student = Student::query()->first();
        // Run
        $response = $this->withAuth()->get('students/status');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('students.archive.edit');
        $response->assertSee([
            'Studenten archiveren',
            $student->getFullName(),
            'Opslaan',
        ]);
    }

    public function test_update_guest(): void
    {
        // Run
        $response = $this->put('students/status');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->put('students/status');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_update_admin(): void
    {
        // Declare
        $graduate = Student::factory()->create([
            'graduated' => false,
        ]);
        $student = Student::factory()->create([
            'graduated' => true,
        ]);
        // Run
        $response = $this->withAuth()->put('students/status', [
            'students' => [
                $graduate->sid,
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->isRedirect('students/status');
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas(Student::class, [
            'sid' => $graduate->sid,
            'graduated' => true,
        ]);
        $this->assertDatabaseHas(Student::class, [
            'sid' => $student->sid,
            'graduated' => false,
        ]);
    }

    public function test_update_admin_fake_sid(): void
    {
        // Declare
        $graduate = Student::factory()->create([
            'sid' => '00111111',
            'graduated' => false,
        ]);
        // Run
        $response = $this->withAuth()->put('students/status', [
            'students' => [
                '00222222',
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->isRedirect('students/status');
        $response->assertSessionHasErrors('students.*');
        $this->assertDatabaseHas(Student::class, [
            'sid' => $graduate->sid,
            'graduated' => false,
        ]);
    }

    public function test_update_admin_wrong_type(): void
    {
        // Declare
        $graduate = Student::factory()->create([
            'sid' => '00111111',
            'graduated' => false,
        ]);
        // Run
        $response = $this->withAuth()->put('students/status', [
            'students' => '00111111',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->isRedirect('students/status');
        $response->assertSessionHasErrors('students');
        $this->assertDatabaseHas(Student::class, [
            'sid' => $graduate->sid,
            'graduated' => false,
        ]);
    }
}
