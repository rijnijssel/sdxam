<?php

namespace Tests\Feature\Http\Controllers\Export;

use App\Jobs\ProcessExport;
use App\Models\Execution;
use App\Models\Student;
use Database\Seeders\ExecutionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class ExportStoreControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest(): void
    {
        // Prepare
        Bus::fake();

        $this->seed();
        $this->seed(ExecutionSeeder::class);

        // Declare
        $execution = Execution::query()->firstOrFail();

        // Run
        $response = $this->post("/executions/{$execution->slug}/exports");

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
        Bus::assertNotDispatched(ProcessExport::class);
    }

    public function test_unlinked_examiner(): void
    {
        // Prepare
        Bus::fake();

        $this->seed();
        $this->seed(ExecutionSeeder::class);

        // Declare
        $examiner = $this->makeExaminer();
        $execution = Execution::query()->firstOrFail();
        $student = Student::factory()->createOne();
        $executionStudent = $execution->students()->updateOrCreate([
            'execution_id' => $execution->id,
            'student_id' => $student->id,
        ]);
        $executionStudent->executionStudentTasks()->create([
            'execution_student_id' => $executionStudent->id,
            'task_id' => $execution->exam->tasks()->firstOrFail()->id,
            'client_id' => $examiner->id,
        ]);

        // Run
        $response = $this->withAuth(admin: false)->post("/executions/{$execution->slug}/exports");

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        Bus::assertNotDispatched(ProcessExport::class);
    }

    public function test_linked_examiner(): void
    {
        // Prepare
        Bus::fake();

        $this->seed();
        $this->seed(ExecutionSeeder::class);

        // Declare
        $examiner = $this->makeExaminer();
        $execution = Execution::query()->firstOrFail();
        $student = Student::factory()->createOne();
        $executionStudent = $execution->students()->updateOrCreate([
            'execution_id' => $execution->id,
            'student_id' => $student->id,
        ]);
        $executionStudent->executionStudentTasks()->create([
            'execution_student_id' => $executionStudent->id,
            'task_id' => $execution->exam->tasks()->firstOrFail()->id,
            'client_id' => $examiner->id,
        ]);

        // Run
        $this
            ->withAuth($examiner->user, false)
            ->post("/executions/{$execution->slug}/exports")
            ->assertRedirect();

        // Assert
        $this->assertAuthenticated();
        Bus::assertDispatched(ProcessExport::class);
    }

    public function test_admin(): void
    {
        // Prepare
        Bus::fake();

        $this->seed();
        $this->seed(ExecutionSeeder::class);

        // Declare
        $execution = Execution::query()->firstOrFail();

        // Run
        $response = $this->withAuth()->post("/executions/{$execution->slug}/exports");

        // Assert
        $this->assertAuthenticated();
        $response->assertRedirect("/executions/{$execution->slug}/exports");
        Bus::assertDispatched(ProcessExport::class);
    }

    public function test_admin_view(): void
    {
        // Prepare
        Bus::fake();

        $this->seed();
        $this->seed(ExecutionSeeder::class);

        // Declare
        $execution = Execution::query()->firstOrFail();

        // Run
        $response = $this->withAuth()->post("/executions/{$execution->slug}/exports?view=true");

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('export.results');
        Bus::assertNotDispatched(ProcessExport::class);
    }
}
