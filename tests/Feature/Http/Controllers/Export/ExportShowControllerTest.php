<?php

namespace Tests\Feature\Http\Controllers\Export;

use App\Jobs\ProcessExport;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\Export;
use App\Models\Student;
use Database\Seeders\ExecutionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\TestResponse;
use Override;
use Tests\TestCase;

class ExportShowControllerTest extends TestCase
{
    use RefreshDatabase;

    private Execution $execution;

    private string $date;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake();

        $this->seed();
        $this->seed(ExecutionSeeder::class);

        $user = $this->makeUser();
        $this->execution = Execution::query()->first();

        $export = Export::createPending($this->execution);
        dispatch_sync(new ProcessExport($user, $export));
        $export->refresh();

        $this->assertNotNull($export);
        $this->date = $export->formatStartDate();
    }

    public function test_guest(): void
    {
        // Run
        $response = $this->get("executions/{$this->execution->slug}/export/{$this->date}");

        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_unlinked_examiner(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get("executions/{$this->execution->slug}/export/{$this->date}");

        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_linked_examiner(): void
    {
        // Declare
        $this->execution->loadMissing('exam.tasks');
        $examiner = Examiner::factory()->create();
        $user = $examiner->user;
        $student = Student::factory()->create();

        $es = $this->execution->students()->create([
            'execution_id' => $this->execution->id,
            'student_id' => $student->id,
        ]);
        $es->executionStudentTasks()->create([
            'task_id' => $this->execution->exam->tasks->first()->id,
            'client_id' => $examiner->id,
        ]);

        // Run
        $response = $this->withAuth($user)->get("executions/{$this->execution->slug}/export/{$this->date}");

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();

        $this->checkExportResponse($response);
    }

    public function test_admin(): void
    {
        // Run
        $response = $this->withAuth()->get("executions/{$this->execution->slug}/export/{$this->date}");

        // Assert
        $this->assertAuthenticated();
        $response->assertOk();

        $this->checkExportResponse($response);
    }

    private function checkExportResponse(TestResponse $response): void
    {
        $contentDisposition = explode(';', (string) $response->headers->get('content-disposition'));
        // content disposition header has two fields, response type (can be "inline" or "attachment")
        // and a filename field, which needs to be split by the "=" character.
        // Example: "Content-Disposition: inline; filename=example.pdf"
        $type = $contentDisposition[0];
        $filename = trim(explode('=', $contentDisposition[1])[1]);

        $this->assertSame('inline', $type);
        $this->assertSame("{$this->execution->slug}_{$this->date}.pdf", $filename);
    }
}
