<?php

namespace Tests\Feature\Http\Controllers\Export;

use App\Jobs\ProcessBatchExport;
use App\Models\BatchExport;
use App\Models\Examiner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class BatchExportControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_store_as_guest(): void
    {
        // Declare
        $execution = $this->makeExecution();
        // Run
        $response = $this->post("executions/{$execution->slug}/batch-exports");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_store_as_user(): void
    {
        // Declare
        Storage::fake();
        Bus::fake();

        // Declare
        $execution = $this->makeExecution();
        $student1 = $this->makeStudent();
        $student2 = $this->makeStudent();
        $execution->addStudent($student1);
        $execution->addStudent($student2);
        // Run
        $response = $this->withAuth(admin: false)->post("executions/{$execution->slug}/batch-exports", [
            'students' => [
                $student1->sid,
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        Bus::assertNotDispatched(ProcessBatchExport::class);
    }

    public function test_store_as_examiner(): void
    {
        Storage::fake();
        Bus::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        // Declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $user = $this->makeUser(admin: false);
        $examiner = Examiner::factory()->createOne([
            'user_id' => $user->id,
        ]);
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examiner->id,
        ]);
        // Run
        $response = $this->withAuth($user)->post("executions/{$execution->slug}/batch-exports", [
            'students' => [
                $student->sid,
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionHasNoErrors();
        $response->assertRedirect("executions/{$execution->slug}/exports");
        Bus::assertDispatched(ProcessBatchExport::class);
    }

    public function test_store_as_admin(): void
    {
        Storage::fake();
        Bus::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        // Declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
        ]);
        // Run
        $response = $this->withAuth()->post("executions/{$execution->slug}/batch-exports", [
            'students' => [
                $student->sid,
            ],
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertSessionHasNoErrors();
        $response->assertRedirect("executions/{$execution->slug}/exports");
        Bus::assertDispatched(ProcessBatchExport::class);
    }

    public function test_show_as_guest(): void
    {
        // Declare
        Storage::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        $user = $this->makeUser();
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);

        $export = BatchExport::createPending($execution, collect([$student]));
        dispatch_sync(new ProcessBatchExport($user, $export));
        $export = BatchExport::query()->whereDate('started_at', '=', $date)->firstOrFail();
        // Run
        $response = $this->get("executions/{$execution->slug}/batch-exports/{$export->id}");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_examiner(): void
    {
        Storage::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        // Declare
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $es = $execution->addStudent($student);
        $task = $this->makeTask($execution->exam);
        $user = $this->makeUser(admin: false);
        $examiner = Examiner::factory()->createOne([
            'user_id' => $user->id,
        ]);
        $es->executionStudentTasks()->create([
            'task_id' => $task->id,
            'client_id' => $examiner->id,
        ]);

        $export = BatchExport::createPending($execution, collect([$student]));
        dispatch_sync(new ProcessBatchExport($user, $export));
        $export = BatchExport::query()->whereDate('started_at', '=', $date)->firstOrFail();
        // Run
        $response = $this->withAuth($user)->get("executions/{$execution->slug}/batch-exports/{$export->id}");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertSeeText([
            'Export', 'Download',
            'Student', 'Link',
            $student->getFullName(), 'Toon', 'Download',
        ]);
    }

    public function test_show_as_admin(): void
    {
        // Declare
        Storage::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        $user = $this->makeUser();
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);

        $export = BatchExport::createPending($execution, collect([$student]));
        dispatch_sync(new ProcessBatchExport($user, $export));
        $export = BatchExport::query()->whereDate('started_at', '=', $date)->firstOrFail();
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/batch-exports/{$export->id}");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertSeeText([
            'Export', 'Download',
            'Student', 'Link',
            $student->getFullName(), 'Toon', 'Download',
        ]);
    }
}
