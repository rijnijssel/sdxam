<?php

namespace Tests\Feature\Http\Controllers\Export;

use App\Jobs\ProcessBatchExport;
use App\Models\BatchExport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class BatchExportDownloadControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_as_guest(): void
    {
        // Declare
        Storage::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        $user = $this->makeUser();
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);

        $export = BatchExport::createPending($execution, collect([$student]));
        dispatch_sync(new ProcessBatchExport($user, $export));
        $export = BatchExport::query()->whereDate('started_at', '=', $date)->firstOrFail();
        // Run
        $response = $this->get("executions/{$execution->slug}/batch-exports/{$export->id}/download");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_admin(): void
    {
        // Declare
        Storage::fake();

        $date = Date::make('2022-06-01 16:30:00');
        $this->travelTo($date);

        $user = $this->makeUser();
        $execution = $this->makeExecution();
        $student = $this->makeStudent();
        $execution->addStudent($student);

        $export = BatchExport::createPending($execution, collect([$student]));
        dispatch_sync(new ProcessBatchExport($user, $export));
        $export = BatchExport::query()->whereDate('started_at', '=', $date)->firstOrFail();
        // Run
        $response = $this->withAuth()->get("executions/{$execution->slug}/batch-exports/{$export->id}/download");
        // Assert
        $this->assertAuthenticated();
        $response->assertDownload("Resultaten {$execution->name}.zip");
    }
}
