<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContextControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_as_guest(): void
    {
        // Run
        $response = $this->get('/contexts');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_user(): void
    {
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth(admin: false)
            ->get('/contexts');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('contexts.index');
        $response->assertSeeTextInOrder([
            'Contexten',
            'Naam',
            $context->name,
        ]);
        $response->assertDontSeeText(['Toevoegen', 'Bewerken']);
    }

    public function test_index_as_admin(): void
    {
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()
            ->get('/contexts');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('contexts.index');
        $response->assertSeeTextInOrder([
            'Contexten',
            'Toevoegen',
            'Naam',
            $context->name, 'Bewerken',
        ]);
    }

    public function test_create_as_guest(): void
    {
        // Run
        $response = $this->get('contexts/create');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_create_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)
            ->get('contexts/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_create_as_admin(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('contexts/create');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('contexts.create');
        $response->assertSeeTextInOrder([
            'Context toevoegen',
            'Naam',
            'Toevoegen',
            'Annuleren',
        ]);
    }

    public function test_store_as_guest(): void
    {
        // Run
        $response = $this->post('contexts', [
            'name' => 'Test Examiner',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_store_as_user(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Run
        $response = $this->withAuth(admin: false)
            ->post('contexts', [
                'name' => 'Test Examiner',
            ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseCount('contexts', 0);
    }

    public function test_store_as_admin(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Run
        $response = $this->withAuth()->post('contexts', [
            'name' => 'Test Context',
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('contexts', [
            'name' => 'Test Context',
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('contexts');
    }

    public function test_store_null_name_as_admin(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Run
        $response = $this->withAuth()->post('contexts', [
            'name' => null,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('contexts', [
            'name' => null,
        ]);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_edit_as_guest(): void
    {
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->get("contexts/{$context->slug}/edit");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_missing_as_guest(): void
    {
        // Run
        $response = $this->get('contexts/abc/edit');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_edit_as_user(): void
    {
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth(admin: false)
            ->get("contexts/{$context->slug}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
    }

    public function test_edit_as_admin(): void
    {
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()
            ->get("contexts/{$context->slug}/edit");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('contexts.edit');
        $response->assertSeeInOrder([
            'Context bewerken',
            'Naam',
            $context->name,
            'Opslaan', 'Annuleren',
        ]);
    }

    public function test_edit_missing_as_admin(): void
    {
        // Run
        $response = $this->withAuth()
            ->get('contexts/abc/edit');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }

    public function test_update_as_guest(): void
    {
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->put('contexts/'.$context->slug, [
            'name' => 'Test Context',
        ]);
        // Assert
        $this->assertGuest();
        $this->assertDatabaseMissing('contexts', [
            'name' => 'Test Context',
        ]);
        $response->assertRedirect('login');
    }

    public function test_update_missing_as_guest(): void
    {
        // Run
        $response = $this->put('contexts/abc', [
            'name' => 'Test Context',
        ]);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_update_as_user(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth(admin: false)->put('contexts/'.$context->slug, [
            'name' => 'Test Context',
        ]);
        // Assert
        $this->assertAuthenticated();
        $response->assertForbidden();
        $this->assertDatabaseMissing('contexts', [
            'name' => 'Test Context',
        ]);
    }

    public function test_update_as_admin(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->put('contexts/'.$context->slug, [
            'name' => 'Test Context',
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseHas('contexts', [
            'name' => 'Test Context',
        ]);
        $response->assertSessionDoesntHaveErrors();
        $response->assertRedirect('contexts');
    }

    public function test_update_null_name_as_admin(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Declare
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->put('contexts/'.$context->slug, [
            'name' => null,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('contexts', [
            'name' => null,
        ]);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }

    public function test_update_unique_name_as_admin(): void
    {
        $this->assertDatabaseCount('contexts', 0);
        // Declare
        $other = $this->makeContext();
        $context = $this->makeContext();
        // Run
        $response = $this->withAuth()->put('contexts/'.$context->slug, [
            'name' => $other->name,
        ]);
        // Assert
        $this->assertAuthenticated();
        $this->assertDatabaseMissing('contexts', [
            'id' => $context->id,
            'name' => $other->name,
        ]);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect();
    }
}
