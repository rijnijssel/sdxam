<?php

namespace Tests\Feature\Http\Controllers\Exam;

use App\Models\Exam;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Override;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{
    use RefreshDatabase;

    private Exam $exam;

    private Task $task;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
        $this->exam = Exam::query()->with('tasks.processes.rubrics.tasks.items')->firstOrFail();
        $this->task = $this->exam->tasks->firstOrFail();
        $this->task->processes->firstOrFail();
    }

    public function test_show_as_guest(): void
    {
        // Run
        $response = $this->get("exams/{$this->exam->slug}/tasks/{$this->task->slug}");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get("exams/{$this->exam->slug}/tasks/{$this->task->slug}");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.tasks.show');
        $response->assertSee([
            ...$this->task->processes->map->name,
            ...$this->task->processes->flatMap->rubrics->flatMap->title,
            ...$this->task->processes->flatMap->rubrics->flatMap->tasks->map->criteria,
        ]);
    }

    public function test_show_as_admin(): void
    {
        // Run
        $response = $this->withAuth()->get("exams/{$this->exam->slug}/tasks/{$this->task->slug}");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.tasks.show');
        $response->assertSee([
            ...$this->task->processes->map->name,
            ...$this->task->processes->flatMap->rubrics->flatMap->title,
            ...$this->task->processes->flatMap->rubrics->flatMap->tasks->map->criteria,
        ]);
    }
}
