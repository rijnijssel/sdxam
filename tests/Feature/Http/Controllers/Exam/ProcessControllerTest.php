<?php

namespace Tests\Feature\Http\Controllers\Exam;

use App\Models\Exam;
use App\Models\Process;
use App\Models\RubricTask;
use App\Models\RubricTaskItem;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Override;
use Tests\TestCase;

class ProcessControllerTest extends TestCase
{
    use RefreshDatabase;

    private Exam $exam;

    private Task $task;

    private Process $process;

    #[Override]
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
        $this->exam = Exam::query()->with('tasks.processes.rubrics.tasks.items')->firstOrFail();
        $this->task = $this->exam->tasks->firstOrFail();
        $this->process = $this->task->processes->firstOrFail();
    }

    public function test_show_as_guest(): void
    {
        // Run
        $response = $this->get("exams/{$this->exam->slug}/tasks/{$this->task->slug}/processes/{$this->process->id}");
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Run
        $response = $this->withAuth(admin: false)->get("exams/{$this->exam->slug}/tasks/{$this->task->slug}/processes/{$this->process->id}");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.processes.show');
        $response->assertSeeHtml([
            ...$this->process->rubrics->pluck('title')->map(fn (string $title): string => $title),
            ...$this->process->rubrics->flatMap->tasks->map(fn (RubricTask $task) => $task->name),
            ...$this->process->rubrics->flatMap->tasks->map(fn (RubricTask $task) => $task->criteria),
            ...$this->process->rubrics->flatMap->tasks->flatMap->items->map(fn (RubricTaskItem $item) => $item->text),
        ]);
    }

    public function test_show_as_admin(): void
    {
        // Run
        $response = $this->withAuth()->get("exams/{$this->exam->slug}/tasks/{$this->task->slug}/processes/{$this->process->id}");
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.processes.show');
        $response->assertSeeHtml([
            ...$this->process->rubrics->pluck('title')->map(fn (string $title): string => $title),
            ...$this->process->rubrics->flatMap->tasks->map(fn (RubricTask $task) => $task->name),
            ...$this->process->rubrics->flatMap->tasks->map(fn (RubricTask $task) => $task->criteria),
            ...$this->process->rubrics->flatMap->tasks->flatMap->items->map(fn (RubricTaskItem $item) => $item->text),
        ]);
    }
}
