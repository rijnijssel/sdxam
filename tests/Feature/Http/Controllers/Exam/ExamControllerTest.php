<?php

namespace Tests\Feature\Http\Controllers\Exam;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExamControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_guest(): void
    {
        // Run
        $response = $this->get('exams');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_index_as_user(): void
    {
        // Run
        $response = $this->withAuth(null, false)->get('exams');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.index');
        $response->assertDontSeeText('Toevoegen');
        $response->assertSeeInOrder([
            'Examens',
            'Naam', 'Kerntaken',
        ]);
    }

    public function test_index(): void
    {
        // Run
        $response = $this->withAuth()->get('exams');
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.index');
        $response->assertSeeInOrder([
            'Examens',
            'Naam', 'Kerntaken',
        ]);
    }

    public function test_show_guest(): void
    {
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->get('exams/'.$exam->slug);
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_missing_guest(): void
    {
        // Run
        $response = $this->get('exams/exam-1');
        // Assert
        $this->assertGuest();
        $response->assertRedirect('login');
    }

    public function test_show_as_user(): void
    {
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth(null, false)->get('exams/'.$exam->slug);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.show');
        $response->assertDontSeeText('Bewerken');
        $response->assertSeeInOrder([
            'Examen',
            'Naam', $exam->name,
            'Overzicht kerntaken',
        ]);
    }

    public function test_show(): void
    {
        // Declare
        $exam = $this->makeExam();
        // Run
        $response = $this->withAuth()->get('exams/'.$exam->slug);
        // Assert
        $this->assertAuthenticated();
        $response->assertOk();
        $response->assertViewIs('exams.show');
        $response->assertSeeInOrder([
            'Examen',
            'Naam', $exam->name,
            'Overzicht kerntaken',
        ]);
    }

    public function test_show_missing(): void
    {
        // Run
        $response = $this->withAuth()->get('exams/exam-1');
        // Assert
        $this->assertAuthenticated();
        $response->assertNotFound();
    }
}
