<?php

namespace Tests\Feature\Console\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminListCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle(): void
    {
        $user = $this->makeUser();

        $this
            ->artisan('app:admin:list')
            ->expectsTable(
                ['ID', 'Email', 'Name', 'Is Admin'],
                [
                    [$user->id, $user->email, $user->name, $user->is_admin],
                ],
            )
            ->assertSuccessful();
    }
}
