<?php

namespace Tests\Feature\Console\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class AdminCreateCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle_valid_data(): void
    {
        $this
            ->artisan('app:admin:create', [
                'name' => $name = fake()->name(),
                'email' => $email = fake()->email(),
                'password' => $password = fake()->password(),
            ])
            ->assertSuccessful()
            ->run();

        $this->assertDatabaseHas('users', [
            'name' => $name,
            'email' => $email,
            'is_admin' => true,
        ]);
        $this->assertTrue(Auth::attempt(['email' => $email, 'password' => $password]));
    }

    public function test_handle_name_max_length(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Naam mag niet uit meer dan 255 tekens bestaan.');

        $this
            ->artisan('app:admin:create', [
                'name' => $name = Str::random(256),
                'email' => $email = fake()->email(),
            ])
            ->assertFailed()
            ->run();

        $this->assertDatabaseMissing('users', [
            'name' => $name,
            'email' => $email,
            'is_admin' => true,
        ]);
        $this->assertFalse(Auth::attempt(['email' => $email, 'password' => $password]));
    }

    public function test_handle_email_invalid(): void
    {
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('Email is geen geldig e-mailadres.');

        $this
            ->artisan('app:admin:create', [
                'name' => $name = fake()->name(),
                'email' => $email = 'invalid-email',
                'password' => $password = fake()->password(),
            ])
            ->assertFailed()
            ->run();

        $this->assertDatabaseMissing('users', [
            'name' => $name,
            'email' => $email,
            'is_admin' => true,
        ]);
        $this->assertFalse(Auth::attempt(['email' => $email, 'password' => $password]));
    }
}
