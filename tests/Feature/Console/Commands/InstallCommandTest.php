<?php

namespace Tests\Feature\Console\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\PendingCommand;
use Tests\TestCase;

class InstallCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle(): void
    {
        /** @var PendingCommand $cmd */
        $cmd = $this->artisan('app:install');
        $cmd
            ->assertSuccessful();

        /** @var PendingCommand $cmd */
        $cmd = $this->artisan('migrate');
        $cmd
            ->expectsOutputToContain('Nothing to migrate.')
            ->assertSuccessful();

        $this->assertDatabaseHas('exams', [
            'name' => 'Crebo 25604',
        ]);
        $this->assertDatabaseHas('exams', [
            'name' => 'Crebo 25187',
        ]);
    }
}
