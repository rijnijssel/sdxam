<?php

namespace Tests;

use App\Models\Context;
use App\Models\Exam;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\Group;
use App\Models\Process;
use App\Models\Rubric;
use App\Models\Student;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

trait GeneratesModels
{
    public function withAuth(?User $user = null, bool $admin = true): TestCase
    {
        if (! $user instanceof User) {
            $user = $this->makeUser($admin);
        }
        $this->withSession(['auth.password_confirmed_at' => time()]);

        return $this->actingAs($user);
    }

    protected function makeExaminer(): Examiner
    {
        $examiner = Examiner::factory()->createOne();
        self::assertNotNull($examiner->user);

        return $examiner;
    }

    protected function makeUser(bool $admin = true): User
    {
        return User::factory()->createOne([
            'is_admin' => $admin,
        ]);
    }

    protected function makeExam(bool $tasks = true): Exam
    {
        return Exam::factory()
            ->when($tasks, fn (Factory $factory) => $factory->has(Task::factory()->count(4), 'tasks'))
            ->createOne();
    }

    protected function makeTask(Exam $exam): Task
    {
        $task = Task::factory()->makeOne();
        $exam->tasks()->save($task);

        return $task;
    }

    protected function makeProcess(Task $task): Process
    {
        $process = Process::factory()->makeOne();
        $task->processes()->save($process);

        return $process;
    }

    protected function makeRubric(Process $process): Rubric
    {
        $rubric = Rubric::factory()->makeOne();
        $process->rubrics()->save($rubric);

        return $rubric;
    }

    protected function makeExecution(): Execution
    {
        $exam = $this->makeExam();

        return Execution::factory()
            ->for($exam, 'exam')
            ->createOne();
    }

    protected function makeGroup(Execution $execution): Group
    {
        return Group::query()->create([
            'name' => fake()->streetName,
            'execution_id' => $execution->id,
        ]);
    }

    protected function makeStudent(bool $graduated = false): Student
    {
        return Student::factory()->createOne([
            'graduated' => $graduated,
        ]);
    }

    protected function makeContext(): Context
    {
        return Context::factory()->createOne();
    }
}
