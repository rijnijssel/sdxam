<?php

namespace Tests;

use App\Models\Process;
use App\Models\Rubric;
use App\Models\RubricTask;

trait TestsRubrics
{
    /**
     * Make a process with rubrics, to test <code>\App\Models\Process#rubricString()</code>.
     *
     * @param  int  $amount  amount of rubrics to generate
     */
    protected function createProcessWithRubrics(int $amount = 0): Process
    {
        $exam = $this->makeExam();
        $task = $this->makeTask($exam);
        $process = $this->makeProcess($task);
        for ($i = 0; $i < $amount; $i++) {
            $title = 'WP'.($i + 1);
            Rubric::query()->create(['title' => $title, 'process_id' => $process->id]);
        }

        return $process;
    }

    /**
     * Create a rubric with items.
     *
     * @param  int[]  $amounts  array of the amount of items that need to be generated per rubric task
     */
    protected function setupRubricMaxItems(array $amounts = [4, 3, 2]): Rubric
    {
        $exam = $this->makeExam();
        $task = $this->makeTask($exam);
        $process = $this->makeProcess($task);
        /** @var Rubric $rubric */
        $rubric = $process->rubrics()->create(['title' => 'Rubric 1']);
        foreach ($amounts as $amount) {
            /** @var RubricTask $task */
            $task = $rubric->tasks()->create(['name' => 'Task 1', 'criteria' => 'Criteria 1']);
            $items = [];
            for ($i = 0; $i < $amount; $i++) {
                $number = $i + 1;
                $items[] = ['text' => "Test Item {$number} for Rubric 1", 'points' => $number];
            }
            $task->items()->createMany($items);
        }

        return $rubric;
    }
}
