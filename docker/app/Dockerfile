FROM docker.io/serversideup/php:8.4-unit AS base
USER root
RUN install-php-extensions ctype curl dom fileinfo filter hash intl mbstring openssl pcre pdo pdo_mysql session tokenizer xml
USER www-data
ENV COMPOSER_FUND=0

FROM base AS local
ARG USER_ID
ARG GROUP_ID
USER root
RUN docker-php-serversideup-set-id www-data $USER_ID:$GROUP_ID && \
    docker-php-serversideup-set-file-permissions --owner $USER_ID:$GROUP_ID --service unit && \
    chown -R www-data:www-data /composer
USER www-data

FROM docker.io/library/node:22-alpine AS assets
WORKDIR /app
COPY /package.json /pnpm-lock.yaml ./
RUN corepack enable && pnpm install --frozen-lockfile
COPY . .
RUN pnpm build

FROM base AS production
COPY --chown=www-data /composer.json /composer.lock ./
RUN composer install --no-dev --no-autoloader --no-scripts --prefer-dist
COPY --chown=www-data /app/ app/
COPY --chown=www-data /bootstrap/ bootstrap/
COPY --chown=www-data /config/ config/
COPY --chown=www-data /database/ database/
COPY --chown=www-data /lang/ lang/
COPY --chown=www-data /public/ public/
COPY --chown=www-data /resources/views/ resources/views/
COPY --chown=www-data /routes/ routes/
COPY --chown=www-data /storage/ storage/
COPY --chown=www-data /artisan artisan
COPY --chown=www-data --from=assets /app/public/build public/build
RUN composer dump-autoload --optimize && \
    php artisan event:cache && \
    php artisan route:cache && \
    php artisan view:cache
