<?php

namespace App\Jobs;

use App\Models\Export;
use App\Models\User;
use Barryvdh\DomPDF\PDF;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Attributes\WithoutRelations;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

#[WithoutRelations]
class ProcessExport implements ShouldBeUnique, ShouldQueue
{
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /** Amount of seconds for the job timeout. */
    public int $timeout = 60 * 10;

    /** Create a new job instance. */
    public function __construct(
        private readonly User $user, // @phpstan-ignore-line
        private readonly Export $export
    ) {}

    /**
     * Execute the job.
     *
     * @throws Exception
     */
    public function handle(PDF $pdf): void
    {
        try {
            $this->export->loadMissing([
                'execution.exam',
                'execution.students.executionStudentTasks.task.processes.rubrics.tasks.items',
                'execution.students.student',
            ]);

            $this->export->update(['started_at' => now()]);

            Storage::makeDirectory(Export::BASE_DIRECTORY);

            $path = Storage::path($this->export->filename);

            if (Storage::fileMissing($path)) {
                $students = $this->export->execution->students;

                $pdf->loadView('export.results', [
                    'execution' => $this->export->execution,
                    'students' => $students,
                ]);
                $pdf->setPaper('a4');
                $pdf->save($path);
            }

            $this->export->finish();

            // $this->user->notify(new ExportFinished($this->export));
        } catch (Exception $exception) {
            Storage::delete($this->export->filename);
            $this->export->failed();

            throw $exception;
        }
    }

    public function uniqueId(): string
    {
        return $this->export->execution->slug.'_'.now()->format('Y-m-d_H-i');
    }
}
