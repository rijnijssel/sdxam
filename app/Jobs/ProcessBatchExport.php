<?php

namespace App\Jobs;

use App\Models\BatchExport;
use App\Models\ExecutionStudent;
use App\Models\Student;
use App\Models\User;
use App\Notifications\BatchExportFinished;
use Barryvdh\DomPDF\PDF;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\Attributes\WithoutRelations;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use function Illuminate\Filesystem\join_paths;

#[WithoutRelations]
class ProcessBatchExport implements ShouldQueue
{
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        private readonly User $user,
        private readonly BatchExport $export,
    ) {}

    /**
     * Execute the job.
     *
     * @throws Exception
     */
    public function handle(): void
    {
        try {
            $this->export->loadMissing([
                'execution',
                'students',
            ]);

            $this->export->update(['started_at' => now()]);

            Storage::makeDirectory($this->export->directory);

            foreach ($this->export->students as $student) {
                Log::debug('batch-export:start', [
                    'execution' => $this->export->execution->slug,
                    'student' => $student->sid,
                ]);

                $es = $this->export->execution->getExecutionStudent($student);

                if (! $es instanceof ExecutionStudent) {
                    // mark export as failed when execution student can not be found
                    $this->export->failed();
                    Log::error('batch-export:execution-student-missing', [
                        'execution' => $this->export->execution->slug,
                        'student' => $student->sid,
                    ]);

                    continue;
                }

                Log::debug('batch-export:execution-student-found', [
                    'execution-student' => $es->id,
                ]);

                $es->loadMissing([
                    'executionStudentTasks.task.processes.rubrics.tasks.items',
                    'executionStudentTasks.task.processes.rubrics.requiredTasks',
                ]);

                $path = Storage::path(join_paths($this->export->directory, "{$student->sid}.pdf"));

                /** @var array<int, ExecutionStudent> $students */
                $students = Arr::wrap($es);

                // Instantiate dompdf for each pdf, otherwise a crash occurs
                // See: https://github.com/dompdf/dompdf/issues/1056
                $pdf = app(PDF::class);
                $pdf->loadView('export.results', [
                    'execution' => $this->export->execution,
                    'students' => $students,
                ]);
                $pdf->save($path);

                $this->verifyStudentFile($this->export, $student);
            }

            $this->export->finish();

            // $this->sendNotification($this->export);

            Log::info('batch-export:finished', [
                'execution' => $this->export->execution->slug,
                'batch-export' => $this->export->id,
            ]);
        } catch (Exception $exception) {
            Storage::deleteDirectory($this->export->directory);
            $this->export->failed();

            throw $exception;
        }
    }

    /**
     * Send mail to the user who started the export.
     *
     * @param  BatchExport  $export  export to link to in the email
     */
    public function sendNotification(BatchExport $export): void
    {
        $this->user->notify(new BatchExportFinished($export));
    }

    /**
     * Check that a student file actually exists, otherwise mark the export as failed.
     *
     * @param  BatchExport  $export  export to mark when file is not found
     * @param  Student  $student  student to check for a file
     */
    public function verifyStudentFile(BatchExport $export, Student $student): void
    {
        if (Storage::missing($export->directory."/{$student->sid}.pdf")) {
            // mark export as failed when a student can not be found
            $export->failed();
        }
    }
}
