<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Log;

class LoginListener
{
    public function handle(Login $event): void
    {
        Log::debug('login:success', [
            'user' => $event->user->getAuthIdentifier(),
        ]);
    }
}
