<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Failed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

readonly class LoginFailedListener
{
    public function __construct(
        private Request $request,
    ) {}

    public function handle(Failed $event): void
    {
        Log::error('login:failed', [
            'ip' => $this->request->ip(),
            'credentials' => $event->credentials,
            'user' => $event->user,
        ]);
    }
}
