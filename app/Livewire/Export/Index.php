<?php

namespace App\Livewire\Export;

use App\Models\Execution;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\View\View;
use Livewire\Attributes\Computed;
use Livewire\Component;

/**
 * @property Execution $execution
 */
class Index extends Component
{
    public string $slug;

    public function mount(Execution $execution): void
    {
        $this->slug = $execution->getRouteKey();
    }

    public function deleteBatchExport(int $batchExportId): void
    {
        $this->authorize('delete-exports');

        $batchExport = $this->execution->batchExports()->findOrFail($batchExportId);
        $batchExport->students()->detach();
        $batchExport->delete();

        unset($this->execution);
    }

    public function deleteExport(int $exportId): void
    {
        $this->authorize('delete-exports');

        $this->execution->exports()->findOrFail($exportId)->delete();

        unset($this->execution);
    }

    #[Computed]
    public function execution(): Execution
    {
        return Execution::query()
            ->with([
                'batchExports' => fn (Builder $query) => $query->latest(),
                'exports' => fn (Builder $query) => $query->latest(),
                'students.student',
            ])
            ->where('slug', '=', $this->slug)
            ->firstOrFail();
    }

    public function render(): View
    {
        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($this->execution->name, route('executions.show', $this->execution))
            ->add('Exports', active: true)
            ->share();

        return view('livewire.export.index');
    }
}
