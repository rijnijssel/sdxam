<?php

namespace App\Livewire\Terraform\Executions;

use App\Models\Execution;
use App\Models\TerraformExecution;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Livewire\Attributes\Layout;
use Livewire\Component;

#[Layout('components.layouts.app')]
class Index extends Component
{
    /** @var Collection<int, Execution> */
    public Collection $executions;

    /** @var array<int, string> */
    public array $selected = [];

    public function mount(): void
    {
        $this->executions = Execution::query()
            ->with('terraformExecution')
            ->get();

        $this->selected = $this->executions
            ->filter(fn (Execution $execution): bool => $execution->terraformExecution !== null)
            ->pluck('slug')
            ->toArray();
    }

    public function create(): void
    {
        // delete terraform executions
        $delete = Execution::query()
            ->whereNotIn('slug', $this->selected)
            ->pluck('id');

        TerraformExecution::query()
            ->whereIn('execution_id', $delete)
            ->delete();

        // create terraform executions
        $create = Execution::query()
            ->whereIn('slug', $this->selected)
            ->pluck('id');

        $create->each(fn (int $id) => TerraformExecution::query()->updateOrCreate([
            'execution_id' => $id,
        ]));

        session()->flash('success', 'Terraform executions updated successfully.');

        $this->redirect(route('terraform.index'));
    }

    public function render(): View
    {
        return view('livewire.terraform.executions.index');
    }
}
