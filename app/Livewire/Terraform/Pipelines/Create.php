<?php

namespace App\Livewire\Terraform\Pipelines;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Livewire\Attributes\Session;
use Livewire\Component;

class Create extends Component
{
    #[Session('terraform_pipeline_url')]
    public ?string $pipelineUrl = null;

    #[Session('terraform_pipeline_date')]
    public ?Carbon $pipelineDate = null;

    public function store(): void
    {
        $this->authorize('terraform.pipeline-trigger');

        // Store the pipeline
        $response = Http::baseUrl('https://gitlab.com/api/v4')
            ->withUrlParameters([
                'project_id' => config('services.gitlab_pipeline_trigger.project_id'),
            ])
            ->withQueryParameters([
                'token' => config('services.gitlab_pipeline_trigger.token'),
                'ref' => 'main',
            ])
            ->post('/projects/{project_id}/trigger/pipeline');

        if ($response->successful()) {
            $this->pipelineUrl = $response->json('web_url');
            $this->pipelineDate = Carbon::parse($response->json('created_at'));
        } else {
            $this->pipelineUrl = null;
            $this->pipelineDate = null;
        }
    }

    public function render(): View
    {
        return view('livewire.terraform.pipelines.create');
    }
}
