<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Validate;
use Livewire\Form;

class ApiTokenStoreForm extends Form
{
    #[Validate(['required', 'string', 'max:100'])]
    public string $name = '';

    #[Validate(['date:Y-m-d', 'after_or_equal:today'])]
    public string $expiresAt = '';
}
