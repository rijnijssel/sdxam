<?php

namespace App\Livewire;

use App\Livewire\Forms\ApiTokenStoreForm;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Carbon;
use Livewire\Component;

class ApiTokenManager extends Component
{
    public User $user;

    public ApiTokenStoreForm $form;

    public ?string $storedToken = null;

    public function store(): void
    {
        $this->authorize('manage-api-tokens');

        /** @var User $user */
        $user = auth()->user();

        $data = $this->form->validate();
        $token = $user->createToken(
            name: $data['name'],
            expiresAt: Carbon::make($data['expiresAt']),
        );

        $this->setStoredToken($token->plainTextToken);
        $this->form->reset();
    }

    public function destroy(int $id): void
    {
        $this->authorize('manage-api-tokens');

        /** @var User $user */
        $user = auth()->user();

        $user->tokens()->where('id', $id)->delete();
        $this->setStoredToken();
    }

    public function setStoredToken(?string $token = null): void
    {
        $this->storedToken = $token;
    }

    public function render(): View
    {
        return view('livewire.api-token-manager', [
            'tokens' => $this->user->tokens,
        ]);
    }
}
