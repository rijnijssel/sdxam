<?php

namespace App\Models;

use App\Observers\BatchExportObserver;
use Database\Factories\BatchExportFactory;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Override;

use function Illuminate\Filesystem\join_paths;

/**
 * @property-read Execution $execution
 */
#[ObservedBy(BatchExportObserver::class)]
class BatchExport extends Model
{
    /** @use HasFactory<BatchExportFactory> */
    use HasFactory;

    public const string BASE_DIRECTORY = '/exports';

    public const string DATE_FORMAT = 'Ymd_His';

    /** @var list<string> */
    protected $fillable = ['directory', 'started_at', 'finished_at', 'failed'];

    /** @var list<string> */
    protected $with = ['execution'];

    /** @return array<string, string> */
    #[Override]
    protected function casts(): array
    {
        return [
            'started_at' => 'datetime',
            'finished_at' => 'datetime',
            'failed' => 'bool',
        ];
    }

    /**
     * Create a new BatchExport model that is marked as started.
     *
     * @param  Execution  $execution  the execution to create this export for
     * @param  Collection<int, Student>  $students  list of students to add to this batch export
     */
    public static function createPending(Execution $execution, Collection $students): BatchExport
    {
        $start = now();
        $date = $start->format(self::DATE_FORMAT);
        $directory = join_paths(self::BASE_DIRECTORY, $execution->slug, $date);

        $batch = $execution->batchExports()->create([
            'started_at' => null,
            'directory' => $directory,
        ]);
        $batch->students()->saveMany($students);

        return $batch;
    }

    /** @return BelongsTo<Execution, $this> */
    public function execution(): BelongsTo
    {
        return $this->belongsTo(Execution::class, 'execution_id');
    }

    /** @return BelongsToMany<Student, $this> */
    public function students(): BelongsToMany
    {
        return $this->belongsToMany(Student::class, 'batch_export_students')
            ->withTrashed();
    }

    public function start(): self
    {
        $this->update(['started_at' => now()]);

        return $this;
    }

    public function finish(): self
    {
        $this->update(['finished_at' => now()]);

        return $this;
    }

    public function failed(): self
    {
        $this->update(['failed' => true]);

        return $this;
    }

    public function isProcessing(): bool
    {
        return $this->started_at !== null && ! $this->isFinished();
    }

    public function isFinished(): bool
    {
        return $this->finished_at !== null && ! $this->isFailed();
    }

    public function isFailed(): bool
    {
        return $this->failed === true;
    }

    public function getStudentPath(Student $student): string|false
    {
        if ($this->students->doesntContain($student)) {
            return false;
        }

        return "{$this->directory}/{$student->sid}.pdf";
    }
}
