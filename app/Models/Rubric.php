<?php

namespace App\Models;

use Database\Factories\RubricFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * @property-read int $max_items
 */
class Rubric extends Model
{
    /** @use HasFactory<RubricFactory> */
    use HasFactory;

    /** @var list<string> */
    protected $fillable = ['title', 'process_id'];

    /** @var list<string> */
    protected $appends = ['max_items', 'max_points'];

    /** @return Attribute<int, void> */
    protected function maxItems(): Attribute
    {
        $key = sprintf('rubric[%s].max_items', $this->id);

        return Attribute::get(fn () => Cache::remember(
            $key,
            fn () => now()->addHour(),
            fn () => RubricTaskItem::query()
                ->withoutGlobalScope('points')
                ->select(DB::raw('COUNT(rubric_task_id) AS count'))
                ->whereIn('rubric_task_id', $this->tasks()->pluck('id'))
                ->groupBy('rubric_task_id')
                ->orderByDesc('count')
                ->limit(1)
                ->get('count') // @phpstan-ignore-line count property does exist in this query
                ->pluck('count')
                ->first()
        ));
    }

    /** @return Attribute<int, void> */
    protected function maxPoints(): Attribute
    {
        return Attribute::make(get: function () {
            $key = sprintf('rubric[%s].max_points', $this->id);

            return Cache::remember(
                $key,
                fn () => now()->addHour(),
                fn () => DB::scalar('SELECT calculate_max_points(:id) as points', [
                    'id' => $this->id,
                ])
            );
        });
    }

    /** @return HasMany<RubricTask, $this> */
    public function requiredTasks(): HasMany
    {
        return $this->hasMany(RubricTask::class, 'rubric_id')->where('required', true);
    }

    /**
     * Query the "tasks" relationship.
     *
     * @return HasMany<RubricTask, $this>
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(RubricTask::class, 'rubric_id');
    }

    /**
     * Query the "workingProcess" relationship.
     *
     * @return BelongsTo<Process, $this>
     */
    public function process(): BelongsTo
    {
        return $this->belongsTo(Process::class, 'process_id');
    }
}
