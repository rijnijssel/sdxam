<?php

namespace App\Models;

use Database\Factories\ExamFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Override;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Exam extends Model
{
    /** @use HasFactory<ExamFactory> */
    use HasFactory;

    use HasSlug;
    use SoftDeletes;

    /** @var list<string> */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'course',
        'cohort',
        'crebo',
    ];

    public function tasksString(): string
    {
        return Cache::remember("exam.{$this->id}.tasks-string", 600, function (): string {
            $and = trans('words.and');

            return $this->tasks()->pluck('name')->join(', ', " {$and} ");
        });
    }

    /** @return HasMany<Task, $this> */
    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'exam_id', 'id')
            ->orderBy('name');
    }

    /** @return HasMany<Execution, $this> */
    public function executions(): HasMany
    {
        return $this->hasMany(Execution::class, 'exam_id', 'id');
    }

    /** Perform any actions required after the model boots. */
    #[Override]
    protected static function booted(): void
    {
        static::addGlobalScope('orderByUpdatedAt', function (Builder $builder): void {
            $builder->orderBy('updated_at', 'desc');
        });
    }

    #[Override]
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
