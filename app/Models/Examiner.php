<?php

namespace App\Models;

use App\Contracts\HasDropdownLabel;
use Database\Factories\ExaminerFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Override;

class Examiner extends Model implements HasDropdownLabel
{
    /** @use HasFactory<ExaminerFactory> */
    use HasFactory;

    use SoftDeletes;

    /** @var list<string> */
    protected $fillable = ['name', 'abbreviation'];

    /**
     * Query the "user" relationship.
     *
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /** Get this examiner's full name through the user relationship. */
    public function getUserName(): ?string
    {
        $this->loadMissing('user');

        return $this->user->name ?? null;
    }

    /** Get this examiner's email through the user relationship. */
    public function getEmail(): ?string
    {
        $this->loadMissing('user');

        return $this->user->email ?? null;
    }

    /** Format label for dropdowns. */
    #[Override]
    public function getLabel(): string
    {
        return $this->name;
    }
}
