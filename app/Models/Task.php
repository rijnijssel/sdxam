<?php

namespace App\Models;

use Database\Factories\TaskFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Override;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Task extends Model
{
    /** @use HasFactory<TaskFactory> */
    use HasFactory;

    use HasSlug;

    /** @var list<string> */
    protected $fillable = ['name', 'description', 'code', 'slug', 'exam_id'];

    /** @return BelongsTo<Exam, $this> */
    public function exam(): BelongsTo
    {
        return $this->belongsTo(Exam::class, 'exam_id');
    }

    /** @return HasMany<Process, $this> */
    public function processes(): HasMany
    {
        return $this->hasMany(Process::class, 'task_id');
    }

    public function getLabel(): string
    {
        return "{$this->name}: {$this->description}";
    }

    #[Override]
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
