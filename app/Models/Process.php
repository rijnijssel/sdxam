<?php

namespace App\Models;

use App\Contracts\HasDropdownLabel;
use Database\Factories\ProcessFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Override;

class Process extends Model implements HasDropdownLabel
{
    /** @use HasFactory<ProcessFactory> */
    use HasFactory;

    /** @var list<string> */
    protected $fillable = ['name', 'description'];

    /** @var list<string> */
    protected $appends = ['max_items', 'max_points'];

    /** @var SupportCollection<int, RubricTask>|null */
    private ?SupportCollection $requiredTasks = null;

    /** @return Attribute<int, void> */
    protected function maxItems(): Attribute
    {
        return Attribute::get(function () {
            $key = sprintf('process[%s].max_items', $this->id);

            return Cache::remember($key, now()->addHour(), fn () => $this->rubrics->sum('max_items'));
        });
    }

    /** @return Attribute<int, void> */
    protected function maxPoints(): Attribute
    {
        return Attribute::get(function () {
            $key = sprintf('process[%s].max_points', $this->id);

            return Cache::remember($key, now()->addHour(), fn () => $this->rubrics->sum('max_points'));
        });
    }

    /** @return SupportCollection<int, array<string, float|int>> */
    public function getScoreTable(): SupportCollection
    {
        $key = sprintf('process[%s].points', $this->id);

        return Cache::flexible(
            $key,
            [now()->addDay(), now()->addDays(2)],
            fn () => collect(DB::select(/* @lang MySQL */ 'CALL generate_process_score_table(:process)', [
                'process' => $this->id,
            ]))
        );
    }

    /** @return SupportCollection<int, RubricTask> */
    public function getRequiredTasks(): SupportCollection
    {
        if ($this->requiredTasks instanceof SupportCollection) {
            return $this->requiredTasks;
        }

        return $this->requiredTasks = $this->rubrics
            ->loadMissing('requiredTasks')
            ->flatMap(fn (Rubric $rubric) => $rubric->requiredTasks)
            ->unique('id');
    }

    public function getRequiredTasksAchieved(ExecutionStudentTask $task): bool
    {
        return $this->getRequiredTasks()
            ->every(fn (RubricTask $t): bool => $task->isAchieved($t));
    }

    public function rubricString(): string
    {
        $and = trans('words.and');

        return $this->rubrics->pluck('title')->join(', ', " $and ");
    }

    /** @return BelongsTo<Task, $this> */
    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    /** @return HasMany<Rubric, $this> */
    public function rubrics(): HasMany
    {
        return $this->hasMany(Rubric::class, 'process_id');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'id';
    }

    #[Override]
    public function getLabel(): string
    {
        return "$this->name: $this->description";
    }
}
