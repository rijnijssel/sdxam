<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Override;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Group extends Model
{
    use HasSlug;

    /** @var list<string> */
    protected $fillable = ['name', 'slug', 'execution_id'];

    /**
     * Query the "execution" relationship.
     *
     * @return BelongsTo<Execution, $this>
     */
    public function execution(): BelongsTo
    {
        return $this->belongsTo(Execution::class, 'execution_id');
    }

    /**
     * Query the "students" relationship.
     *
     * @return BelongsToMany<Student, $this>
     */
    public function students(): BelongsToMany
    {
        return $this->belongsToMany(Student::class, 'group_students', 'group_id', 'student_id');
    }

    /**
     * Query the "examiners" relationship.
     *
     * @return BelongsToMany<Examiner, $this>
     */
    public function examiners(): BelongsToMany
    {
        return $this->belongsToMany(Examiner::class, 'group_examiners', 'group_id', 'examiner_id');
    }

    #[Override]
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
