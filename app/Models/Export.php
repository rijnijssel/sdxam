<?php

namespace App\Models;

use App\Observers\ExportObserver;
use Database\Factories\ExportFactory;
use Exception;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Override;

/**
 * @property Carbon|null $started_at
 * @property Carbon|null $finished_at
 * @property-read Execution $execution
 */
#[ObservedBy(ExportObserver::class)]
class Export extends Model
{
    /** @use HasFactory<ExportFactory> */
    use HasFactory;

    public const string BASE_DIRECTORY = '/exports';

    public const string DATE_FORMAT = 'Ymd_His';

    /** @var list<string> */
    protected $fillable = ['filename', 'started_at', 'finished_at'];

    /** @var list<string> */
    protected $with = ['execution'];

    /** @return array<string, string> */
    #[Override]
    protected function casts(): array
    {
        return [
            'started_at' => 'datetime',
            'finished_at' => 'datetime',
        ];
    }

    /**
     * Start a new Export, based on the given Execution model.
     *
     * @param  Execution  $execution  execution to start the export for
     */
    public static function createPending(Execution $execution): Export
    {
        $date = now()->format(self::DATE_FORMAT);
        $filename = sprintf('%s/%s_%s.pdf', self::BASE_DIRECTORY, $execution->slug, $date);

        return $execution->exports()->create([
            'started_at' => null,
            'filename' => $filename,
        ]);
    }

    /**
     * Remove the export.
     *
     * @throws Exception
     */
    public function failed(): void
    {
        $this->delete();
    }

    public function finish(): void
    {
        $this->update(['finished_at' => now()]);
    }

    public function isProcessing(): bool
    {
        return $this->started_at !== null && ! $this->isFinished();
    }

    public function isFinished(): bool
    {
        return $this->finished_at !== null;
    }

    public function formatStartDate(): string
    {
        return $this->started_at?->format(self::DATE_FORMAT) ?? '';
    }

    public function url(): string
    {
        $date = $this->formatStartDate();

        return route('executions.export.show', [$this->execution, $date]);
    }

    /** @return BelongsTo<Execution, $this> */
    public function execution(): BelongsTo
    {
        return $this->belongsTo(Execution::class, 'execution_id');
    }
}
