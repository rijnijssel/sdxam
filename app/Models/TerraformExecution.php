<?php

namespace App\Models;

use Database\Factories\TerraformExecutionFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TerraformExecution extends Model
{
    /** @use HasFactory<TerraformExecutionFactory> */
    use HasFactory;

    protected $fillable = [
        'execution_id',
    ];

    /** @return BelongsTo<Execution, $this> */
    public function execution(): BelongsTo
    {
        return $this->belongsTo(Execution::class, 'execution_id');
    }
}
