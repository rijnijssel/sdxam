<?php

namespace App\Models;

use App\Exceptions\NoExaminerException;
use App\Services\AnnotationService;
use App\Support\Enum\ExaminerRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property-read Task $task
 */
class ExecutionStudentTask extends Model
{
    public const string CLIENT_COLUMN = 'client_id';

    public const string SUPERVISOR_COLUMN = 'supervisor_id';

    /** @var list<string> */
    protected $fillable = ['execution_student_id', 'task_id', 'context_id', self::CLIENT_COLUMN, self::SUPERVISOR_COLUMN];

    /** @var list<string> */
    protected $with = ['client', 'supervisor'];

    /** @var Collection<int, Examiner>|null */
    private ?Collection $examiners = null;

    /** @return Collection<int, Examiner> */
    public function getExaminers(): Collection
    {
        if ($this->examiners instanceof Collection) {
            return $this->examiners;
        }

        return $this->examiners = Collection::make([$this->client, $this->supervisor])
            ->filter(fn (?Examiner $examiner): bool => $examiner instanceof Examiner);
    }

    /** Generate a string that contains all the examiner's names. */
    public function getExaminersLabel(): string
    {
        $examiners = $this->getExaminers()
            ->map(fn (Examiner $examiner): string => $examiner->abbreviation);

        return $examiners->join(' / ');
    }

    /** Check if an examiner is linked as a client or supervisor to this student. */
    public function hasExaminer(Examiner $examiner): bool
    {
        try {
            $this->getExaminerRole($examiner);

            return true;
        } catch (NoExaminerException) {
            return false;
        }
    }

    /**
     * Get the examiner role for a given examiner.
     *
     * @throws NoExaminerException when no examiner was found
     */
    public function getExaminerRole(Examiner $examiner): ExaminerRole
    {
        return match ($examiner->id) {
            $this->getClientId() => ExaminerRole::Client,
            $this->getSupervisorId() => ExaminerRole::Supervisor,
            default => throw new NoExaminerException,
        };
    }

    public function getExaminer(ExaminerRole $role): ?Examiner
    {
        return match ($role) {
            ExaminerRole::Client => $this->client,
            ExaminerRole::Supervisor => $this->supervisor,
        };
    }

    public function isItemSelected(?RubricTaskItem $item): bool
    {
        if (! $item instanceof RubricTaskItem) {
            return false;
        }
        $this->loadMissing(['scores']);

        return $this->scores->contains(Score::RESULT_COLUMN, $item->id);
    }

    public function isAchieved(?RubricTask $rt): bool
    {
        if (! $rt instanceof RubricTask) {
            return false;
        }

        return $this->scores()
            ->join('rubric_task_items', 'scores.score_id', 'rubric_task_items.id')
            ->join('rubric_tasks', 'rubric_task_items.rubric_task_id', 'rubric_tasks.id')
            ->where('scores.rubric_task_id', $rt->id)
            ->where('rubric_tasks.required', true)
            ->where('rubric_task_items.points', '>', 0)
            ->exists();
    }

    public function getDefinitiveAnnotation(RubricTask $rubricTask): ?string
    {
        $service = app(AnnotationService::class);

        return $service->getDefinitiveAnnotation($this, $rubricTask);
    }

    /** @return BelongsTo<ExecutionStudent, $this> */
    public function executionStudent(): BelongsTo
    {
        return $this->belongsTo(ExecutionStudent::class, 'execution_student_id');
    }

    /** @return BelongsTo<Task, $this> */
    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    /** @return BelongsTo<Examiner, $this> */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Examiner::class, 'client_id');
    }

    /** @return BelongsTo<Examiner, $this> */
    public function supervisor(): BelongsTo
    {
        return $this->belongsTo(Examiner::class, 'supervisor_id');
    }

    /** @return BelongsTo<Context, $this> */
    public function context(): BelongsTo
    {
        return $this->belongsTo(Context::class, 'context_id');
    }

    /** @return HasMany<Score, $this> */
    public function scores(): HasMany
    {
        return $this->hasMany(Score::class, 'execution_student_task_id');
    }

    public function getClientId(): ?int
    {
        return data_get($this, self::CLIENT_COLUMN);
    }

    public function getSupervisorId(): ?int
    {
        return data_get($this, self::SUPERVISOR_COLUMN);
    }
}
