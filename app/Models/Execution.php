<?php

namespace App\Models;

use App\Contracts\HasDropdownLabel;
use Database\Factories\ExecutionFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Override;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use stdClass;

/**
 * @property Carbon|null $start_date
 * @property-read Exam $exam
 */
class Execution extends Model implements HasDropdownLabel
{
    /** @use HasFactory<ExecutionFactory> */
    use HasFactory;

    use HasSlug;
    use SoftDeletes;

    public const float SCORE_PASS_MINIMUM = 5.5;

    /** @var list<string> */
    protected $fillable = ['name', 'slug', 'start_date', 'exam_id'];

    /** @var Collection<int, stdClass>|null */
    private ?Collection $scores = null;

    /** @return array<string, string> */
    #[Override]
    protected function casts(): array
    {
        return [
            'start_date' => 'date',
        ];
    }

    /** @return Collection<int, stdClass> */
    public function getScores(): Collection
    {
        if ($this->scores instanceof Collection) {
            return $this->scores;
        }

        return $this->scores = DB::query()
            ->from('execution_scores')
            ->where('execution_id', $this->id)
            ->get();
    }

    public function getProcessScore(ExecutionStudent $es, Process $process): ?float
    {
        $score = $this->getScores()
            ->where('execution_student_id', $es->id)
            ->where('process_id', $process->id)
            ->value('score', fn (): float => 0.0);

        if (! is_float($score)) {
            return null;
        }

        return $score;
    }

    public function isScoreFailed(ExecutionStudent $es, Process $process): bool
    {
        $score = $this->getProcessScore($es, $process);

        if ($score == null) {
            return false;
        }

        return $score < 0 || ($score > 0 && $score < Execution::SCORE_PASS_MINIMUM);
    }

    /** Check if a student passed the given process. */
    public function isScorePassed(ExecutionStudent $es, Process $process): bool
    {
        return $this->getProcessScore($es, $process) >= self::SCORE_PASS_MINIMUM;
    }

    public function getProcessScoreText(?ExecutionStudent $es, ?Process $process): ?string
    {
        if (! $es instanceof ExecutionStudent || ! $process instanceof Process) {
            return null;
        }

        $score = $this->getProcessScore($es, $process);

        if ($score === null) {
            return $process->name;
        }

        if ($score < 0) {
            return trans('N.B.');
        }

        return number_format($score, 1);
    }

    /**
     * Get the current exam execution.
     *
     * @param  string[]  $columns
     */
    public static function getCurrent(array $columns = ['*']): ?Execution
    {
        return Execution::query()
            ->orderByDesc('start_date')
            ->whereDate('start_date', '<', Date::now()->addWeek())
            ->first($columns);
    }

    /** Format the start date into a user-friendly string. */
    public function formatStartDate(): ?string
    {
        return $this->start_date?->format('Y-m-d');
    }

    public function localizedStartDate(): ?string
    {
        return $this->start_date?->translatedFormat('d F Y');
    }

    /** Get an execution student for this execution. */
    public function getExecutionStudent(Student $student): ?ExecutionStudent
    {
        return $this->students()->firstWhere('student_id', $student->id);
    }

    public function addStudent(Student $student): ExecutionStudent
    {
        return $this->students()->updateOrCreate(['student_id' => $student->id]);
    }

    public function hasExaminer(?Examiner $examiner): bool
    {
        if (! $examiner instanceof Examiner) {
            return false;
        }

        return $this->students()
            ->orWhereRelation('executionStudentTasks.client', ExecutionStudentTask::CLIENT_COLUMN, $examiner->id)
            ->orWhereRelation('executionStudentTasks.supervisor', ExecutionStudentTask::SUPERVISOR_COLUMN, $examiner->id)
            ->count() > 0;
    }

    /**
     * Get all students for a given examiner in an execution.
     *
     * @param  Examiner  $examiner  the examiner to query students for
     * @return Collection<int, ExecutionStudent>
     */
    public function getExaminerStudents(Examiner $examiner): Collection
    {
        return $this->students()
            ->with(['student', 'tasks'])
            ->whereHas('tasks', fn ($query) => $query
                ->where(ExecutionStudentTask::CLIENT_COLUMN, $examiner->id)
                ->orWhere(ExecutionStudentTask::SUPERVISOR_COLUMN, $examiner->id))
            ->get();
    }

    #[Override]
    public function getLabel(): string
    {
        return sprintf('%s', $this->name);
    }

    public function getGroupNames(): string
    {
        $names = $this->groups->sortBy('name')->pluck('name');

        return $names->isEmpty() ? trans('Geen') : $names->join(', ');
    }

    /** @return BelongsTo<Exam, $this> */
    public function exam(): BelongsTo
    {
        return $this->belongsTo(Exam::class, 'exam_id');
    }

    /** @return HasMany<ExecutionStudent, $this> */
    public function students(): HasMany
    {
        return $this->hasMany(ExecutionStudent::class, 'execution_id')
            ->join('students', 'execution_students.student_id', '=', 'students.id')
            ->select('execution_students.*', 'students.first_name')
            ->orderBy('students.first_name');
    }

    /** @return HasMany<Group, $this> */
    public function groups(): HasMany
    {
        return $this->hasMany(Group::class, 'execution_id');
    }

    /** @return HasMany<Export, $this> */
    public function exports(): HasMany
    {
        return $this->hasMany(Export::class)->orderByDesc('started_at');
    }

    /** @return HasMany<BatchExport, $this> */
    public function batchExports(): HasMany
    {
        return $this->hasMany(BatchExport::class)->orderByDesc('started_at');
    }

    /** @return HasOne<TerraformExecution, $this> */
    public function terraformExecution(): HasOne
    {
        return $this->hasOne(TerraformExecution::class, 'execution_id');
    }

    /** Add global sorting scope to Execution. */
    #[Override]
    protected static function booted(): void
    {
        static::addGlobalScope('orderByStartDate', function (Builder $builder): void {
            $builder->orderByDesc('start_date');
        });
    }

    #[Override]
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
