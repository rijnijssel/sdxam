<?php

namespace App\Models;

use Database\Factories\ContextFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Override;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Context extends Model
{
    /** @use HasFactory<ContextFactory> */
    use HasFactory;

    use HasSlug;

    /** @var list<string> */
    protected $fillable = ['name'];

    #[Override]
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
