<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Score extends Model
{
    public const string CLIENT_COLUMN = 'client_score_id';

    public const string SUPERVISOR_COLUMN = 'supervisor_score_id';

    public const string RESULT_COLUMN = 'score_id';

    /** @var list<string> */
    protected $fillable = [
        'execution_student_task_id',
        'rubric_task_id',
        'client_score_id',
        'supervisor_score_id',
        'score_id',
    ];

    /** @return BelongsTo<ExecutionStudentTask, $this> */
    public function executionStudentTask(): BelongsTo
    {
        return $this->belongsTo(ExecutionStudentTask::class, 'execution_student_task_id');
    }

    /** @return BelongsTo<RubricTask, $this> */
    public function rubricTask(): BelongsTo
    {
        return $this->belongsTo(RubricTask::class, 'rubric_task_id');
    }

    /** @return BelongsTo<RubricTaskItem, $this> */
    public function clientScore(): BelongsTo
    {
        return $this->belongsTo(RubricTaskItem::class, 'client_score_id');
    }

    /** @return BelongsTo<RubricTaskItem, $this> */
    public function supervisorScore(): BelongsTo
    {
        return $this->belongsTo(RubricTaskItem::class, 'supervisor_score_id');
    }

    /** @return MorphMany<Annotation, $this> */
    public function annotations(): MorphMany
    {
        return $this->morphMany(Annotation::class, 'annotatable');
    }
}
