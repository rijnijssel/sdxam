<?php

namespace App\Models;

use App\Contracts\HasDropdownLabel;
use App\Exceptions\NoExaminerException;
use App\Observers\UserObserver;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Override;

#[ObservedBy(UserObserver::class)]
class User extends Authenticatable implements HasDropdownLabel
{
    use HasApiTokens;

    /** @use HasFactory<UserFactory> */
    use HasFactory;

    use Notifiable;
    use SoftDeletes;

    /** @var list<string> */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
        'email_verified_at',
    ];

    /** @var list<string> */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /** @return array<string, string> */
    #[Override]
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'is_admin' => 'bool',
        ];
    }

    /** Format label for dropdowns. */
    #[Override]
    public function getLabel(): string
    {
        return "$this->name ($this->email)";
    }

    /** Get the avatar URL for this user. */
    public function getAvatarUrl(): string
    {
        return sprintf('https://eu.ui-avatars.com/api/?name=%s', urlencode((string) $this->name));
    }

    /** Hash a password for this user and set it to the password field. */
    public function setPlainPassword(?string $password): void
    {
        $this->password = $password === null ? null : Hash::make($password);
    }

    /**
     * Get the examiner linked to this user.
     *
     * @throws NoExaminerException when no examiner is linked to this user
     */
    public function getExaminer(): Examiner
    {
        throw_if($this->examiner === null, new NoExaminerException($this));

        return $this->examiner;
    }

    /** @return HasOne<Examiner, $this> */
    public function examiner(): HasOne
    {
        return $this->hasOne(Examiner::class, 'user_id', 'id');
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'email';
    }
}
