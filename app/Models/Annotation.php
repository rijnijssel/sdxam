<?php

namespace App\Models;

use Database\Factories\AnnotationFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Annotation extends Model
{
    /** @use HasFactory<AnnotationFactory> */
    use HasFactory;

    /** @var list<string> */
    protected $fillable = [
        'examiner_id',
        'text',
    ];

    /** @return BelongsTo<Examiner, $this> */
    public function examiner(): BelongsTo
    {
        return $this->belongsTo(Examiner::class, 'examiner_id');
    }

    /** @return MorphTo<Model, $this> */
    public function annotatable(): MorphTo
    {
        return $this->morphTo('annotatable');
    }
}
