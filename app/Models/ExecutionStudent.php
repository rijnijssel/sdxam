<?php

namespace App\Models;

use App\Exceptions\NoExaminerException;
use App\Services\ScoreService;
use App\Support\Data\ScoreProgress;
use App\Support\Enum\ExaminerRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Number;
use stdClass;

/**
 * @property-read Student $student
 */
class ExecutionStudent extends Model
{
    use SoftDeletes;

    /** @var list<string> */
    protected $fillable = ['execution_id', 'student_id'];

    /** @var Collection<int, stdClass>|null */
    private ?Collection $points = null;

    /** @var Collection<int, stdClass>|null */
    private ?Collection $examiners = null;

    /** @var Collection<int, stdClass> */
    private static Collection $progress;

    /** @var array<int, array<int, ScoreProgress>> */
    private array $progressCache = [];

    public function getTask(?Task $task): ?ExecutionStudentTask
    {
        if (! $task instanceof Task) {
            return null;
        }

        return $this->executionStudentTasks
            ->firstWhere('task_id', $task->id);
    }

    public function getProcessScore(Process $process): ?string
    {
        /** @var float $score */
        $score = $this->getPoints()
            ->where('process_id', $process->id)
            ->value('score', fn (): float => 0.0);

        return $this->getScoreText($score);
    }

    public function getScoreText(?float $score): ?string
    {
        if ($score === null) {
            return null;
        }

        if ($score < 0) {
            return trans('N.B.');
        }

        $format = Number::format($score, 1);

        if ($format === false) {
            return null;
        }

        return $format;
    }

    /** @return Collection<int, stdClass> */
    public function getPoints(): Collection
    {
        if ($this->points instanceof Collection) {
            return $this->points;
        }

        return $this->points = DB::query()
            ->from('execution_scores')
            ->where('execution_student_id', $this->id)
            ->get(['process_id', 'points', 'score']);
    }

    public function isProcessFilled(Task $task, Process $process, ?Examiner $examiner): bool
    {
        if (! $examiner instanceof Examiner) {
            Log::error('examiner missing', [
                'task' => $task->id,
                'process' => $process->id,
            ]);

            return false;
        }

        return $this->getProcessProgress($task, $process, $examiner)->filled();
    }

    public function getProcessProgress(Task $task, Process $process, Examiner $examiner): ScoreProgress
    {
        if (! isset(self::$progress)) {
            self::$progress = app(ScoreService::class)->getTaskProgressForExecution($this->execution_id);
        }

        $processId = $process->id;

        if (isset($this->progressCache[$processId][$examiner->id])) {
            return $this->progressCache[$processId][$examiner->id];
        }

        $progress = self::$progress
            ->where('es_id', $this->getKey())
            ->firstWhere('process_id', $processId);

        $total = $progress->rt_count ?? 0;

        try {
            $role = $this->getExaminerRole($task, $examiner);
        } catch (NoExaminerException) {
            return new ScoreProgress($total);
        }

        $column = $role->getProgressColumn();
        $filled = data_get($progress, $column, 0);

        return $this->progressCache[$processId][$examiner->id] = new ScoreProgress($total, $filled);
    }

    /**
     * Get the role of a certain examiner.
     *
     * @throws NoExaminerException
     */
    public function getExaminerRole(Task $task, Examiner $examiner): ExaminerRole
    {
        $task = $this->getTask($task);

        throw_if(! $task instanceof ExecutionStudentTask, new NoExaminerException);

        return transform($task, fn (ExecutionStudentTask $task): ExaminerRole => $task->getExaminerRole($examiner));
    }

    /**
     * Get the accompanying examiner, if the given Examiner is the Client, the Supervisor Examiner is returned.
     *
     * @param  Task  $task  the task to get the other examiner for
     * @param  Examiner  $examiner  the examiner to get the opposing examiner for
     * @return Examiner|null the examiner that accompanies
     */
    public function getOtherExaminer(Task $task, Examiner $examiner): ?Examiner
    {
        try {
            $role = $this->getExaminerRole($task, $examiner);
        } catch (NoExaminerException) {
            return null;
        }

        $otherRole = $role === ExaminerRole::Client ? ExaminerRole::Supervisor : ExaminerRole::Client;

        return $this->getExaminer($task, $otherRole);
    }

    /**
     * Get the examiner that is in a specified role.
     *
     * @param  Task  $task  the task to retrieve the examiner for
     * @param  ExaminerRole  $role  role that the examiner is in
     * @return Examiner|null the examiner or null if not found
     */
    public function getExaminer(Task $task, ExaminerRole $role): ?Examiner
    {
        $est = $this->getTask($task);

        return transform($est, fn (ExecutionStudentTask $est): ?Examiner => $est->getExaminer($role));
    }

    /** @return Collection<int, stdClass> */
    public function getExaminers(): Collection
    {
        if ($this->examiners instanceof Collection) {
            return $this->examiners;
        }

        return tap(
            collect(DB::select(/* @lang MySQL */ 'CALL get_process_examiners(?)', [$this->id])),
            fn ($result): Collection => $this->examiners = $result,
        );
    }

    /** Check if task has an examiner as client or supervisor. */
    public function isTaskExaminer(Task $task, Examiner $examiner): bool
    {
        // load missing relationship if it's not already loaded
        $this->loadMissing(['executionStudentTasks']);

        /** @var ?ExecutionStudentTask $task */
        $task = $this->executionStudentTasks->firstWhere('task_id', $task->id);

        if ($task === null) {
            return false;
        }

        return $task->hasExaminer($examiner);
    }

    public function setExaminers(Task $task, Examiner $client, Examiner $supervisor): ExecutionStudentTask
    {
        return $this->executionStudentTasks()->updateOrCreate([
            'task_id' => $task->id,
        ], [
            'client_id' => $client->id,
            'supervisor_id' => $supervisor->id,
        ]);
    }

    public function isScoreRowSelected(Process $process, int $points): bool
    {
        return $this->getPoints()
            ->contains(fn ($data): bool => $data->process_id === $process->id && $data->points === $points);
    }

    /** @return BelongsTo<Execution, $this> */
    public function execution(): BelongsTo
    {
        return $this->belongsTo(Execution::class, 'execution_id');
    }

    /** @return BelongsTo<Student, $this> */
    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'student_id')
            ->withTrashed();
    }

    /** @return HasMany<ExecutionStudentTask, $this> */
    public function executionStudentTasks(): HasMany
    {
        return $this->hasMany(ExecutionStudentTask::class, 'execution_student_id');
    }

    /** @return HasManyThrough<Process, ExecutionStudentTask, $this> */
    public function processes(): HasManyThrough
    {
        return $this->hasManyThrough(
            Process::class,
            ExecutionStudentTask::class,
            'execution_student_id',
            'task_id',
            'id',
            'task_id'
        );
    }

    /** @return BelongsToMany<Task, $this> */
    public function tasks(): BelongsToMany
    {
        return $this->belongsToMany(Task::class, 'execution_student_tasks', 'execution_student_id', 'task_id')
            ->withPivot([
                ExecutionStudentTask::CLIENT_COLUMN,
                ExecutionStudentTask::SUPERVISOR_COLUMN,
            ]);
    }
}
