<?php

namespace App\Models;

use App\Observers\StudentObserver;
use Database\Factories\StudentFactory;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use Override;

#[ObservedBy(StudentObserver::class)]
class Student extends Model
{
    /** @use HasFactory<StudentFactory> */
    use HasFactory;

    use SoftDeletes;

    /** @var list<string> */
    protected $fillable = ['sid', 'first_name', 'middle_name', 'last_name', 'email', 'cohort', 'graduated'];

    /** Add global sorting scope to Execution. */
    #[Override]
    protected static function booted(): void
    {
        static::addGlobalScope('orderByFirstName', function (Builder $builder): void {
            $builder->orderBy('first_name');
        });
    }

    /** @return array<string, string> */
    #[Override]
    protected function casts(): array
    {
        return [
            'graduated' => 'boolean',
        ];
    }

    /**
     * Scope this query to only non-graduated students.
     *
     * @param  Builder<Student>  $query
     * @return Builder<Student>
     */
    public function scopeActive(Builder $query): Builder
    {
        return $this->scopeGraduated($query, false);
    }

    /**
     * Scope this query to only graduated students.
     *
     * @param  Builder<Student>  $query
     * @return Builder<Student>
     */
    public function scopeGraduated(Builder $query, bool $graduated = true): Builder
    {
        return $query->where('graduated', $graduated);
    }

    /** @return HasMany<ExecutionStudent, $this> */
    public function executionStudents(): HasMany
    {
        return $this->hasMany(ExecutionStudent::class, 'student_id');
    }

    /** @return BelongsToMany<Group, $this> */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_students', 'student_id', 'group_id');
    }

    /** @return HasManyThrough<ExecutionStudentTask, ExecutionStudent, $this> */
    public function executionStudentTasks(): HasManyThrough
    {
        return $this->hasManyThrough(ExecutionStudentTask::class, ExecutionStudent::class, 'student_id',
            'execution_student_id');
    }

    /** Format the full name of Student. */
    public function getFullName(): string
    {
        return collect([$this->first_name, $this->middle_name, $this->last_name])
            ->filter()
            ->join(' ');
    }

    public function getUsername(): string
    {
        return collect([$this->first_name[0] ?? null, $this->middle_name[0] ?? null, $this->last_name])
            ->filter()
            ->map(static fn (string $name): string => str($name)->slug('.')->transliterate()->value())
            ->join('.');
    }

    public function isGraduated(): bool
    {
        return $this->graduated;
    }

    #[Override]
    public function getRouteKeyName(): string
    {
        return 'sid';
    }
}
