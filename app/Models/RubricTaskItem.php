<?php

namespace App\Models;

use Database\Factories\RubricTaskItemFactory;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Override;

class RubricTaskItem extends Model
{
    /** @use HasFactory<RubricTaskItemFactory> */
    use HasFactory;

    /** @var list<string> */
    protected $fillable = ['text', 'points', 'guideline'];

    /** @return BelongsTo<RubricTask, $this> */
    public function rubricTask(): BelongsTo
    {
        return $this->belongsTo(RubricTask::class, 'rubric_task_id');
    }

    #[Override]
    protected static function booted(): void
    {
        static::addGlobalScope('points', function (Builder $builder): void {
            $builder->orderBy('points', 'asc');
        });
    }
}
