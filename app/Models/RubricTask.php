<?php

namespace App\Models;

use Database\Factories\RubricTaskFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Override;

class RubricTask extends Model
{
    /** @use HasFactory<RubricTaskFactory> */
    use HasFactory;

    /** @var list<string> */
    protected $fillable = ['name', 'criteria', 'required'];

    /** @return array<string, string> */
    #[Override]
    protected function casts(): array
    {
        return [
            'required' => 'boolean',
        ];
    }

    /**
     * Query the "rubric" relationship.
     *
     * @return BelongsTo<Rubric, $this>
     */
    public function rubric(): BelongsTo
    {
        return $this->belongsTo(Rubric::class, 'rubric_id');
    }

    /**
     * Query the "items" relationship.
     *
     * @return HasMany<RubricTaskItem, $this>
     */
    public function items(): HasMany
    {
        return $this->hasMany(RubricTaskItem::class, 'rubric_task_id');
    }
}
