<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\Log;

class UserObserver
{
    public function created(User $user): void
    {
        Log::debug('user:created', ['data' => $user]);
    }

    public function updated(User $user): void
    {
        Log::debug('user:updated', ['data' => $user]);
    }

    public function deleted(User $user): void
    {
        Log::debug('user:deleted', ['data' => $user]);
    }
}
