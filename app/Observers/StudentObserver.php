<?php

namespace App\Observers;

use App\Models\Student;
use Illuminate\Support\Facades\Log;

class StudentObserver
{
    public function created(Student $student): void
    {
        Log::debug('student:created', ['data' => $student]);
    }

    public function updated(Student $student): void
    {
        Log::debug('student:updated', ['data' => $student]);
    }

    public function deleted(Student $student): void
    {
        Log::debug('student:deleted', ['data' => $student]);
    }
}
