<?php

namespace App\Observers;

use App\Models\BatchExport;
use Illuminate\Support\Facades\Storage;

class BatchExportObserver
{
    public function deleting(BatchExport $batchExport): void
    {
        Storage::deleteDirectory($batchExport->directory);
    }
}
