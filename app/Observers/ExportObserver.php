<?php

namespace App\Observers;

use App\Models\Export;
use Illuminate\Support\Facades\Storage;

class ExportObserver
{
    public function deleting(Export $export): void
    {
        Storage::delete($export->filename);
    }
}
