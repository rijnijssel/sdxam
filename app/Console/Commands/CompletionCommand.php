<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * @codeCoverageIgnore
 */
class CompletionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'completion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup code autocompletion';

    /** Create a new command instance. */
    public function __construct()
    {
        parent::__construct();
    }

    /** Execute the console command. */
    public function handle(): int
    {
        $this->call('ide-helper:generate', [
            '-W' => true, // write mixins
        ]);
        $this->call('ide-helper:models', [
            '-M' => true, // write mixins
            '-p' => true, // phpstorm noinspection
        ]);
        $this->call('ide-helper:eloquent');
        $this->call('ide-helper:meta');

        return 0;
    }
}
