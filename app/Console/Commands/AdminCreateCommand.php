<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\PromptsForMissingInput;
use Override;
use Symfony\Component\Console\Input\InputArgument;

use function Laravel\Prompts\password;
use function Laravel\Prompts\text;

class AdminCreateCommand extends Command implements PromptsForMissingInput
{
    protected $name = 'app:admin:create';

    protected $description = 'Create an admin user';

    public function handle(): int
    {
        validator($this->arguments(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'password' => ['nullable', 'string', 'max:255'],
        ])->validate();

        $user = new User([
            'name' => $this->argument('name'),
            'email' => $this->argument('email'),
            'email_verified_at' => now(),
            'is_admin' => true,
        ]);

        if ($password = $this->argument('password')) {
            $user->setPlainPassword($password);
        }

        $user->save();

        $this->info(sprintf('User "%s" created.', $user->name));

        return self::SUCCESS;
    }

    /** @return (callable(): string)[] */
    #[Override]
    protected function promptForMissingArgumentsUsing(): array
    {
        return [
            'name' => fn (): string => text(
                label: 'What is the name of the admin user?',
                required: true,
                validate: fn (string $value): ?string => match (true) {
                    strlen($value) > 255 => 'Name must be less than 255 characters',
                    default => null,
                }
            ),
            'email' => fn (): string => text(
                label: 'What is the email of the admin user?',
                required: true,
                validate: fn (string $value): ?string => match (true) {
                    ! filter_var($value, FILTER_VALIDATE_EMAIL) => 'Email must be a valid email address',
                    default => null,
                }
            ),
            'password' => fn (): string => password(
                label: 'What is the password of the admin user?',
                hint: 'Leave blank to use Single Sign-On (SSO)'
            ),
        ];
    }

    /** @return InputArgument[] */
    #[Override]
    protected function getArguments(): array
    {
        return [
            new InputArgument('name', InputArgument::REQUIRED, 'Name of the admin user'),
            new InputArgument('email', InputArgument::REQUIRED, 'Email of the admin user'),
            new InputArgument('password', InputArgument::OPTIONAL, 'Password of the admin user'),
        ];
    }
}
