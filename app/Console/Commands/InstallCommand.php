<?php

namespace App\Console\Commands;

use Database\Seeders\ExamSeeder;
use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'app:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set up initial database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /** Execute the console command. */
    public function handle(): int
    {
        $this->call('migrate', [
            '--force' => true,
        ]);
        $this->call('db:seed', [
            '--class' => ExamSeeder::class,
            '--force' => true,
        ]);

        return 0;
    }
}
