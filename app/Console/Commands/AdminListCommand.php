<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class AdminListCommand extends Command
{
    protected $name = 'app:admin:list';

    protected $description = 'List all admin users';

    public function handle(): int
    {
        $this->table(
            ['ID', 'Email', 'Name', 'Is Admin'],
            User::all(['id', 'email', 'name', 'is_admin']),
        );

        return self::SUCCESS;
    }
}
