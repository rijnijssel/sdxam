<?php

namespace App\Providers;

use App\Models\Execution;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /** Register any authentication / authorization services. */
    public function boot(): void
    {
        Gate::define('impersonate', fn (User $user, ?User $subject = null): bool => $user->is_admin && $user->isNot($subject));
        Gate::define('manage-api-tokens', fn (User $user): bool => $user->is_admin);
        Gate::define('groups.has-actions', fn (User $user): bool => $user->is_admin);
        Gate::define('guidelines.update', fn (User $user): bool => $user->is_admin);
        Gate::define('executions.score', fn (User $user, Execution $execution): bool => $execution->hasExaminer($user->examiner));
        Gate::define('executions.export', fn (User $user, Execution $execution): bool => $user->is_admin || $execution->hasExaminer($user->examiner));
        Gate::define('delete-exports', fn (User $user): bool => $user->is_admin);
        Gate::define('students.archive', fn (User $user): bool => $user->is_admin);
        Gate::define('profile.show', fn (User $user, User $subject): bool => $user->is($subject));
        Gate::define('profile.update', fn (User $user, User $subject): bool => $user->is($subject));
        Gate::define('profile.update-password', fn (User $user): bool => true);
        Gate::define('terraform.index', fn (User $user): bool => $user->is_admin);
        Gate::define('terraform.pipeline-trigger', fn (User $user): bool => $user->is_admin);
    }
}
