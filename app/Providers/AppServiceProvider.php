<?php

namespace App\Providers;

use App\Models\Execution;
use App\Models\RubricTask;
use App\Models\Score;
use App\Models\Student;
use App\Models\User;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\PersonalAccessToken;
use SocialiteProviders\GitLab\GitLabExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;
use SocialiteProviders\SURFconext\SURFconextExtendSocialite;

class AppServiceProvider extends ServiceProvider
{
    public const string HOME = '/executions';

    public function boot(): void
    {
        Paginator::useBootstrapFive();

        Relation::enforceMorphMap([
            'execution' => Execution::class,
            'score' => Score::class,
            'user' => User::class,
            'pat' => PersonalAccessToken::class,
        ]);

        Model::shouldBeStrict(! app()->isProduction());

        RateLimiter::for('api', fn (Request $request) => Limit::perMinute(100)->by($request->user()->id ?? $request->ip()));

        Route::bind(
            'student',
            fn ($value): Student => Student::withTrashed()
                ->where('sid', $value)
                ->firstOrFail()
        );

        Route::bind(
            'rubric-task',
            fn ($value): RubricTask => RubricTask::query()
                ->where('id', $value)
                ->firstOrFail()
        );

        Event::listen(SocialiteWasCalled::class, SURFconextExtendSocialite::class);
        Event::listen(SocialiteWasCalled::class, GitLabExtendSocialite::class);
    }
}
