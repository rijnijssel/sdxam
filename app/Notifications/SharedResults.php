<?php

namespace App\Notifications;

use App\Models\Execution;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Storage;

class SharedResults extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(
        private readonly Execution $execution
    ) {}

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /** Get the mail representation of the notification. */
    public function toMail(object $notifiable): MailMessage
    {
        $file = Storage::path("results/{$this->execution->slug}.pdf");

        return (new MailMessage)
            ->subject("Overzicht examenresultaten {$this->execution->name}")
            ->greeting('Overzicht examenresultaten')
            ->line(trans('Er is een overzicht van de examenresultaten gedeeld.'))
            ->attach($file, [
                'as' => "Examenresultaten {$this->execution->name}.pdf",
            ]);
    }
}
