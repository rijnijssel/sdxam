<?php

namespace App\Notifications;

use App\Models\Export;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ExportFinished extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(
        private readonly Export $export
    ) {}

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /** Get the mail representation of the notification. */
    public function toMail(object $notifiable): MailMessage
    {
        $execution = $this->export->execution;

        return (new MailMessage)
            ->subject('Geëxporteerde examenresultaten')
            ->greeting('Examenresultaten')
            ->line(trans('Er zijn resultaten geëxporteerd voor ":execution"', [
                'execution' => $execution->name,
            ]))
            ->action('Open PDF', $this->export->url());
    }
}
