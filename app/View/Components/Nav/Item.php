<?php

namespace App\View\Components\Nav;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\View\Component;
use Illuminate\View\View;
use Override;

class Item extends Component
{
    /**
     * Create a new component instance.
     *
     * @param  string  $route  route for this navbar item
     * @param  string|null  $icon  icon for this navbar item
     * @param  string|null  $active  string that the URL to start with, in order to display the active class
     */
    public function __construct(
        public string $route,
        public ?string $icon = null,
        public ?string $active = null,
    ) {}

    /** Check if an icon has been defined on this nav item. */
    public function hasIcon(): bool
    {
        return is_string($this->icon);
    }

    /** Check if the current Route URI starts with the "active" attribute given on this component. */
    public function isActive(): bool
    {
        $uri = Route::current()?->uri() ?? '';

        return $this->active !== null && Str::startsWith($uri, $this->active);
    }

    /** Get the view / contents that represent the component. */
    #[Override]
    public function render(): View
    {
        return view('components.nav.item');
    }
}
