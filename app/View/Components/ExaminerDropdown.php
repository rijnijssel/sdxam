<?php

namespace App\View\Components;

use App\Models\Examiner;
use App\Models\ExecutionStudent;
use App\Models\Task;
use App\Support\Enum\ExaminerRole;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\View\Component;
use Override;

class ExaminerDropdown extends Component
{
    public string $sid;

    private readonly ExaminerRole $examinerRole;

    /**
     * Create a new component instance.
     *
     * @param  Collection<int, Examiner>  $examiners
     */
    public function __construct(
        public ExecutionStudent $executionStudent,
        public Task $task,
        public Collection $examiners,
        public string $role,
    ) {
        $this->examinerRole = ExaminerRole::from($role);
        $this->executionStudent->loadMissing(['student', 'tasks']);
        $this->sid = $this->executionStudent->student->sid;
    }

    public function disabled(): bool
    {
        return ! $this->executionStudent->tasks->contains('slug', $this->task->slug);
    }

    public function selected(Examiner $examiner): bool
    {
        $this->executionStudent->loadMissing('tasks');
        $column = $this->examinerRole->getExecutionStudentTaskColumn();
        $id = data_get($this->executionStudent->tasks->firstWhere('slug', $this->task->slug), "pivot.{$column}");

        return $id === $examiner->id;
    }

    public function roleText(): string
    {
        return match ($this->examinerRole) {
            ExaminerRole::Client => trans('words.client'),
            ExaminerRole::Supervisor => trans('words.supervisor'),
        };
    }

    /** Get the view / contents that represent the component. */
    #[Override]
    public function render(): View
    {
        return view('components.examiner-dropdown');
    }
}
