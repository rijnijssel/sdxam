<?php

namespace App\View\Components\Input;

use Illuminate\View\View;
use Override;

class Text extends BaseInput
{
    /** Get the view / contents that represent the component. */
    #[Override]
    public function render(): View
    {
        return view('components.input.text');
    }
}
