<?php

namespace App\View\Components\Input;

use Illuminate\View\Component;

abstract class BaseInput extends Component
{
    /**
     * Create a new component instance.
     *
     * @param  string  $title  title displayed in the label of this component
     * @param  string|null  $name  name to use for submitting forms
     * @param  string|null  $value  default value for the input field
     */
    public function __construct(
        public string $title,
        public ?string $name = null,
        public ?string $value = null
    ) {}
}
