<?php

namespace App\Services;

use App\Models\Student;

class StudentService
{
    /**
     * Sync the hidden students with the database.
     *
     * @param  array<string>  $studentNumbers
     */
    public function setArchived(array $studentNumbers): void
    {
        Student::query()
            ->whereIn('sid', $studentNumbers)
            ->update(['graduated' => true]);
        Student::query()
            ->whereNotIn('sid', $studentNumbers)
            ->update(['graduated' => false]);
    }
}
