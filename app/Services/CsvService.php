<?php

namespace App\Services;

class CsvService
{
    /**
     * Make a CSV from a header row and data array.
     *
     * @param  array<int, mixed>  $headers
     * @param  array<int, array<int, mixed>>  $data
     */
    public function make(array $headers, array $data = []): string|false
    {
        $handle = fopen('php://temp', 'r+');

        if ($handle === false) {
            return false; // @codeCoverageIgnore
        }

        if (fputcsv($handle, $headers) === false) {
            return false; // @codeCoverageIgnore
        }

        foreach ($data as $row) {
            if (fputcsv($handle, $row) === false) {
                return false; // @codeCoverageIgnore
            }
        }

        rewind($handle);
        $output = stream_get_contents($handle);
        fclose($handle);

        return $output;
    }
}
