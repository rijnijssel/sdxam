<?php

namespace App\Services;

use App\Models\ExecutionStudent;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use stdClass;

class ScoreService
{
    public function getRawScore(int $points, int $maxPoints): float
    {
        return DB::scalar(/* @lang MySQL */ 'SELECT calculate_score(:points, :max_points) AS score', [
            'points' => $points,
            'max_points' => $maxPoints,
        ]);
    }

    /** @return Collection<int, stdClass> */
    public function getTaskProgressForExecution(int $executionId): Collection
    {
        return DB::query()
            ->from('score_progress')
            ->where('e_id', $executionId)
            ->get();
    }

    /** @return Collection<int, stdClass> */
    public function getTaskProgress(ExecutionStudent $executionStudent): Collection
    {
        $executionStudent->loadMissing('executionStudentTasks');

        return DB::query()
            ->from('score_progress')
            ->whereIn('est_id', $executionStudent->executionStudentTasks->pluck('id'))
            ->get();
    }
}
