<?php

namespace App\Services;

use App\Models\Annotation;
use App\Models\Examiner;
use App\Models\ExecutionStudentTask;
use App\Models\RubricTask;
use App\Models\Score;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Collection;

class AnnotationService
{
    /**
     * Save annotation if filled, otherwise, delete annotation for score.
     *
     * @param  Score  $score  score object to update the annotation for
     * @param  string|null  $annotation  content of the annotation
     * @param  Examiner  $examiner  examiner that made the annotation
     */
    public function update(Score $score, ?string $annotation, Examiner $examiner): void
    {
        if ($annotation === null) {
            $score->annotations()->where('examiner_id', $examiner->id)->delete();
        } else {
            $score->annotations()->updateOrCreate(
                ['examiner_id' => $examiner->id],
                ['text' => $annotation],
            );
        }
    }

    /**
     * Get the annotations for a given examiner.
     *
     * @return Collection<int, string>
     */
    public function getExaminerAnnotations(ExecutionStudentTask $est, Examiner $examiner): Collection
    {
        return $est->scores()
            ->with([
                'annotations' => fn (Annotation|Relation $query) => $query
                    ->whereHasMorph('annotatable', 'score')
                    ->where('examiner_id', $examiner->id), // @phpstan-ignore-line
            ])
            ->get()
            ->mapWithKeys(fn (Score $score) => [
                $score->rubric_task_id => $score->annotations->pluck('text')->first(),
            ]);
    }

    /**
     * Get the definitive annotations, shared between examiners.
     *
     * @return Collection<int, string>
     */
    public function getDefinitiveAnnotations(ExecutionStudentTask $est): Collection
    {
        // @var Collection<string>
        return $est->scores()
            ->with(
                'annotations',
                fn ($query) => $query
                    ->whereHasMorph('annotatable', 'score')
                    ->whereNull('examiner_id')
            )
            ->get()
            ->mapWithKeys(fn (Score $score) => [
                $score->rubric_task_id => $score->annotations->pluck('text')->first() ?: null,
            ])
            ->whereNotNull();
    }

    public function getDefinitiveAnnotation(ExecutionStudentTask $est, RubricTask $rubricTask): ?string
    {
        return $est->scores()
            ->with(
                'annotations',
                fn ($query) => $query
                    ->whereHasMorph('annotatable', 'score')
                    ->whereNull('examiner_id')
            )
            ->where('rubric_task_id', $rubricTask->id)
            ->has('annotations')
            ->first()
            ?->annotations
            ?->first()
            ?->text;
    }

    /**
     * Get the default annotations (joined from client and supervisor).
     *
     * @param  ExecutionStudentTask  $est  the execution student task to get all default annotations for
     * @return Collection<int, string>
     */
    public function getDefaultAnnotations(ExecutionStudentTask $est): Collection
    {
        return $est->scores()
            ->with(
                'annotations',
                fn ($query) => $query
                    ->whereHasMorph('annotatable', 'score')
                    ->whereIn('examiner_id', [$est->client_id, $est->supervisor_id])
            )
            ->with('annotations.examiner')
            ->get()
            ->mapWithKeys(function (Score $score): array {
                $annotations = $score->annotations
                    ->whereNotNull('examiner')
                    ->map(fn (Annotation $annotation): string => sprintf('%s: %s', $annotation->examiner?->name, $annotation->text));

                $text = str($annotations->join("\n"))->trim()->toString();

                return [$score->rubric_task_id => $text];
            });
    }
}
