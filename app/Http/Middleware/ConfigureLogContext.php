<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ImpersonatedSessionController;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ConfigureLogContext
{
    /**
     * Field names that should not be logged.
     *
     * @var string[]
     */
    private array $except = [
        '_token',
        'current_password',
        'password',
        'password_confirmation',
    ];

    /** Handle an incoming request. */
    public function handle(Request $request, Closure $next): mixed
    {
        $requestId = Str::uuid()->toString();

        Log::withContext([
            'request-id' => $requestId,
        ]);

        $this->addUserContext($request);

        Log::debug('request', [
            'request-info' => [
                'method' => $request->method(),
                'path' => $request->path(),
                'input' => $request->except($this->except),
                'agent' => $request->userAgent(),
            ],
        ]);

        return $next($request);
    }

    /** Add the user metadata to the logging contexts. */
    private function addUserContext(Request $request): void
    {
        /** @var ?User $user */
        $user = $request->user();

        if ($user === null) {
            return;
        }

        if (Session::has(ImpersonatedSessionController::SESSION_IMPERSONATION_KEY)) {
            Log::withContext([
                'authenticated-user-id' => Session::get(ImpersonatedSessionController::SESSION_IMPERSONATION_KEY),
                'impersonating-user-id' => $user->id,
            ]);
        } else {
            Log::withContext([
                'authenticated-user-id' => $user->id,
            ]);
        }
    }
}
