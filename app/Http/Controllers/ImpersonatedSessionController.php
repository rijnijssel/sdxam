<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ImpersonatedSessionController extends Controller
{
    public const string SESSION_IMPERSONATION_KEY = 'original_user';

    /**
     * Impersonate a user.
     *
     * @throws AuthorizationException
     */
    public function store(Request $request, User $user): RedirectResponse
    {
        $this->authorize('impersonate', $user);

        if (Session::has(self::SESSION_IMPERSONATION_KEY)) {
            return to_route('executions.index')
                ->with('danger', __('Je bent al iemand aan het imiteren.'));
        }

        /** @var User $requestUser */
        $requestUser = $request->user();
        Session::put(self::SESSION_IMPERSONATION_KEY, $requestUser->id);
        Auth::login($user);

        Log::info('impersonate:started', [
            'user' => $user->id,
        ]);

        return to_route('executions.index')
            ->with('success', __(sprintf('Je bent nu %s aan het imiteren.', $user->name)));
    }

    /**
     * Destroy the impersonated session.
     *
     * @throws AuthorizationException
     */
    public function destroy(): RedirectResponse
    {
        if (Session::missing(self::SESSION_IMPERSONATION_KEY)) {
            return back()
                ->with('danger', trans('Je bent niemand aan het imiteren.'));
        }

        $id = Session::pull(self::SESSION_IMPERSONATION_KEY);
        /** @var User $user */
        $user = User::query()->findOrFail($id);
        $this->authorizeForUser($user, 'impersonate');
        Auth::login($user);

        Log::info('impersonate:stopped', [
            'user' => $user->id,
        ]);

        return to_route('users.index')
            ->with('success', trans('Je bent gestopt met imiteren.'));
    }
}
