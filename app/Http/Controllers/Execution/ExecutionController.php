<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExecutionStoreRequest;
use App\Http\Requests\ExecutionUpdateRequest;
use App\Models\Exam;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Support\Breadcrumbs\Breadcrumbs;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;

class ExecutionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Execution::class);
    }

    /** Display a listing of the resource. */
    public function index(): View
    {
        /** @var Execution|null $latest */
        $latest = Execution::getCurrent();

        $latest
            ?->load([
                'exam.tasks',
                'students',
                'groups' => fn (Builder|HasMany $query) => $query->withCount('students'),
            ])
            ->loadCount('students');

        $executions = Execution::query()
            ->with([
                'exam',
                'students',
                'groups' => fn ($query) => $query->withCount('students'),
            ])
            ->withCount('students')
            ->when($latest !== null, fn (Builder $builder) => $builder->whereKeyNot($latest?->id))
            ->get();

        Breadcrumbs::make()
            ->add('Uitvoeringen', route('executions.index'), true)
            ->share();

        return view('executions.index', [
            'latest' => $latest,
            'executions' => $executions,
        ]);
    }

    /** Show the form for creating a new resource. */
    public function create(): View
    {
        $exams = Exam::all();

        Breadcrumbs::make()
            ->add('Uitvoeringen', route('executions.index'))
            ->add('Toevoegen', route('executions.create'), true)
            ->share();

        return view('executions.create', [
            'exams' => $exams,
        ]);
    }

    /** Store a newly created resource in storage. */
    public function store(ExecutionStoreRequest $request): RedirectResponse
    {
        $execution = $request->getExecution();
        // If exam is selected, associate it with the exam execution
        if ($request->hasExam()) {
            $execution->exam()->associate($request->getExam());
        }
        // Save exam execution to the database
        $execution->save();

        // Redirect to the exam executions list
        return to_route('executions.show', $execution)
            ->with('success', trans('notifications.execution.store'));
    }

    /** Display the specified resource. */
    public function show(Execution $execution): View
    {
        $execution->loadMissing([
            'groups.examiners',
        ]);
        $execution->groups->loadCount('students');

        /** @var Collection<int, ExecutionStudent> $students */
        $students = $execution->students()
            ->with(['tasks', 'student', 'executionStudentTasks'])
            ->withCount('executionStudentTasks')
            ->has('executionStudentTasks')
            ->get();

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution), true)
            ->share();

        return view('executions.show', [
            'execution' => $execution,
            'groups' => $execution->groups,
            'students' => $students,
        ]);
    }

    /** Show the form for editing the specified resource. */
    public function edit(Execution $execution): View
    {
        $exams = Exam::all(['id', 'slug', 'name']);

        Breadcrumbs::make()
            ->add('Uitvoeringen', route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Bewerken', route('executions.edit', $execution), true)
            ->share();

        return view('executions.edit', [
            'execution' => $execution,
            'exams' => $exams,
        ]);
    }

    /** Update the specified resource in storage. */
    public function update(ExecutionUpdateRequest $request, Execution $execution): RedirectResponse
    {
        $execution->fill($request->getExecution());
        $execution->exam()->associate($request->getExam());
        $execution->save();

        return to_route('executions.show', $execution)
            ->with('success', trans('notifications.execution.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws Exception when no primary key exists on the model
     */
    public function destroy(Execution $execution): RedirectResponse
    {
        $execution->delete();

        return to_route('executions.index')
            ->with('danger', trans('notifications.execution.destroy'));
    }
}
