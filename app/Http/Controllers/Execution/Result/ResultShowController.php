<?php

namespace App\Http\Controllers\Execution\Result;

use App\Actions\GetScoreCellClass;
use App\Exceptions\NoExaminerException;
use App\Exceptions\ScoringException;
use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\ExecutionStudentTask;
use App\Models\Process;
use App\Models\RubricTaskItem;
use App\Models\Score;
use App\Models\Student;
use App\Models\User;
use App\Services\AnnotationService;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ResultShowController extends Controller
{
    public function __construct(
        private readonly AnnotationService $annotationService,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @throws AuthorizationException
     * @throws ScoringException
     */
    public function __invoke(Request $request, Execution $execution, Student $student, Process $process): View|RedirectResponse
    {
        $this->authorize('executions.score', $execution);

        $process->loadMissing([
            'task',
            'rubrics.tasks.items',
        ]);

        try {
            /** @var User $user */
            $user = $request->user();
            $examiner = $user->getExaminer();
        } catch (NoExaminerException) {
            return to_route('executions.show', $execution);
        }

        /** @var ExecutionStudent $es */
        $es = $execution->students()
            ->where('student_id', $student->id)
            ->whereHas('tasks', fn ($query) => $query
                ->whereKey($process->task_id)
                ->where(fn ($query) => $query->orWhere([
                    ExecutionStudentTask::CLIENT_COLUMN => $examiner->id,
                    ExecutionStudentTask::SUPERVISOR_COLUMN => $examiner->id,
                ]))
            )
            ->with('tasks')
            ->firstOrFail();

        $est = $es->getTask($process->task) ?? throw new ScoringException;

        $scores = $est->scores()->get([Score::CLIENT_COLUMN, Score::SUPERVISOR_COLUMN, Score::RESULT_COLUMN]);
        $selected = $scores->pluck(Score::RESULT_COLUMN);

        $color = fn (RubricTaskItem $item) => app(GetScoreCellClass::class)->run($item, $scores);

        // declare variables for annotations
        $annotations = $this->annotationService->getDefinitiveAnnotations($est);
        $annotationDefaults = $this->annotationService->getDefaultAnnotations($est);

        // function to get the amount of lines an annotation
        $annotationRows = fn (int $id): int => Str::substrCount($annotations->get($id, ''), "\n") + 1;

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Resultaten', route('result.index', $execution))
            ->add($student->getFullName())
            ->add($process->name, route('result.show', [$execution, $student, $process]), true)
            ->share();

        return view('executions.result.show', [
            'execution' => $execution,
            'student' => $student,
            'executionStudent' => $es,
            'task' => $process->task,
            'process' => $process,
            'annotationDefaults' => $annotationDefaults,
            'annotations' => $annotations,
            'annotationRows' => $annotationRows,
            'selected' => $selected,
            'color' => $color,
            'client' => $est->client,
            'supervisor' => $est->supervisor,
        ]);
    }
}
