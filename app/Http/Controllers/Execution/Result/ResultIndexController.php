<?php

namespace App\Http\Controllers\Execution\Result;

use App\Exceptions\NoExaminerException;
use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\User;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ResultIndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws AuthorizationException
     */
    public function __invoke(Execution $execution, Request $request): View|RedirectResponse
    {
        $this->authorize('executions.score', $execution);

        $execution->loadMissing([
            'groups.students',
            'exam.tasks' => fn (HasMany $query) => $query->withCount('processes'),
            'exam.tasks.processes',
            'students.student',
        ]);

        /** @var User $user */
        $user = $request->user();

        try {
            $examiner = $user->getExaminer();
        } catch (NoExaminerException) {
            return to_route('executions.show', $execution);
        }
        $students = $execution->getExaminerStudents($examiner);

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Resultaten', active: true)
            ->share();

        return view('executions.result.index', [
            'execution' => $execution,
            'examiner' => $examiner,
            'tasks' => $execution->exam->tasks,
            'students' => $students,
        ]);
    }
}
