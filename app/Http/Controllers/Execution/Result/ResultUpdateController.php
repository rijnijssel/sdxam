<?php

namespace App\Http\Controllers\Execution\Result;

use App\Exceptions\ScoringException;
use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\Process;
use App\Models\Score;
use App\Models\Student;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ResultUpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws AuthorizationException
     * @throws ScoringException
     */
    public function __invoke(Request $request, Execution $execution, Student $student, Process $process): RedirectResponse
    {
        $this->authorize('executions.score', $execution);

        $executionStudent = $execution->getExecutionStudent($student)
            ?? throw new ScoringException('Execution student could not be found.');
        $executionStudentTask = $executionStudent->getTask($process->task)
            ?? throw new ScoringException('Linked task could not be found.');

        $rows = $request->collect('rows');

        /**
         * @var int $id
         * @var array<string, string> $row
         */
        foreach ($rows as $id => $row) {
            $score = Score::query()->updateOrCreate(['execution_student_task_id' => $executionStudentTask->id, 'rubric_task_id' => $id], ['score_id' => $row['score'] ?? null]);
            $annotation = $row['annotation'] ?? null;
            $this->saveAnnotation($score, $annotation);
        }

        return to_route('result.show', [$execution, $student, $process]);
    }

    /** Save the annotation or remove it if empty. */
    private function saveAnnotation(Score $score, ?string $annotation): void
    {
        if ($annotation === null) {
            $score->annotations()->whereNull('examiner_id')->delete();
        } else {
            $score->annotations()->updateOrCreate(
                ['examiner_id' => null],
                ['text' => $annotation],
            );
        }
    }
}
