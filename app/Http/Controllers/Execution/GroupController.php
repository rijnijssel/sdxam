<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Requests\GroupStoreRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\Group;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Group::class);
    }

    public function create(Execution $execution): View
    {
        $examiners = Examiner::query()->get(['id', 'name', 'user_id']);
        $students = $execution->students()->with('student')->get();

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Groepen')
            ->add('Toevoegen', active: true)
            ->share();

        return view('executions.groups.create', [
            'execution' => $execution,
            'examiners' => $examiners,
            'students' => $students,
        ]);
    }

    public function store(GroupStoreRequest $request, Execution $execution): RedirectResponse
    {
        $group = $request->getGroup();
        $execution->groups()->save($group);
        $group->examiners()->sync($request->getExaminers());
        $group->students()->sync($request->getStudents());

        return to_route('executions.show', $execution)
            ->with('success', __('notifications.group.store'));
    }

    public function show(Execution $execution, Group $group): View
    {
        $group->loadMissing([
            'students',
            'examiners',
        ]);

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Groepen')
            ->add($group->name, active: true)
            ->share();

        return view('executions.groups.show', [
            'execution' => $execution,
            'group' => $group,
            'examiners' => $group->examiners,
            'students' => $group->students,
        ]);
    }

    public function edit(Execution $execution, Group $group): View
    {
        $examiners = Examiner::all();
        $examinerIds = $group->examiners()->pluck('id');
        $students = $execution->students()->with('student')->get();
        $studentIds = $group->students()->pluck('sid');

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Groepen')
            ->add($group->name, route('executions.groups.show', [$execution, $group]))
            ->add('Bewerken', active: true)
            ->share();

        return view('executions.groups.edit', [
            'execution' => $execution,
            'group' => $group,
            'examiners' => $examiners,
            'examiner_ids' => $examinerIds,
            'students' => $students,
            'student_ids' => $studentIds,
        ]);
    }

    public function update(GroupUpdateRequest $request, Execution $execution, Group $group): RedirectResponse
    {
        $group->update($request->getGroupData());
        $group->save();
        $group->examiners()->sync($request->getExaminers());
        $group->students()->sync($request->getStudents());

        return to_route('executions.show', $execution)
            ->with('success', trans('notifications.group.update'));
    }

    public function destroy(Execution $execution, Group $group): RedirectResponse
    {
        $group->examiners()->detach();
        $group->students()->detach();
        $group->delete();

        return to_route('executions.show', $execution)
            ->with('danger', __('notifications.group.destroy'));
    }
}
