<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\User;
use App\Notifications\SharedResults;
use Barryvdh\DomPDF\PDF;
use Illuminate\Container\Attributes\CurrentUser;
use Illuminate\Container\Attributes\RouteParameter;
use Illuminate\Container\Attributes\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class SharedResultsController extends Controller
{
    public function show(
        PDF $pdf,
        #[RouteParameter('execution')] Execution $execution
    ): Response {
        $execution->loadMissing([
            'students.tasks',
            'students.student',
            'exam.tasks' => fn (HasMany $query) => $query->withCount('processes'),
            'exam.tasks.processes',
        ]);

        $pdf
            ->loadView('export.results-table', [
                'execution' => $execution,
                'tasks' => $execution->exam->tasks,
                'students' => $execution->students,
            ])
            ->setPaper('a4', 'landscape');

        return $pdf->stream("Examenresultaten {$execution->name}.pdf");
    }

    public function store(
        PDF $pdf,
        #[CurrentUser] User $user,
        #[Storage] Filesystem $filesystem,
        #[RouteParameter('execution')] Execution $execution
    ): RedirectResponse {
        $execution->loadMissing([
            'students.tasks',
            'students.student',
            'exam.tasks' => fn (HasMany $query) => $query->withCount('processes'),
            'exam.tasks.processes',
        ]);

        $filesystem->makeDirectory('results');

        $pdf
            ->loadView('export.results-table', [
                'execution' => $execution,
                'tasks' => $execution->exam->tasks,
                'students' => $execution->students,
            ])
            ->setPaper('a4', 'landscape')
            ->save($filesystem->path("results/{$execution->slug}.pdf"));

        $user->notify(new SharedResults($execution));

        return back()
            ->with('success', trans('Email verzonden naar :email', [
                'email' => $user->email ?? '',
            ]));
    }
}
