<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExecutionStudentUpdateRequest;
use App\Models\Context;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\ExecutionStudentTask;
use App\Models\Student;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ExecutionStudentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     */
    public function edit(Execution $execution): View
    {
        $this->authorize('update', $execution);

        $execution->load([
            'exam.tasks',
        ]);

        $examiners = Examiner::with('user')->orderBy('name')->get();
        $students = $execution->students()
            ->with(['tasks', 'student', 'executionStudentTasks'])
            ->has('tasks')
            ->get();

        $contexts = Context::query()->get();

        $tasks = $execution->exam->tasks;

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Examinatoren koppelen', active: true)
            ->share();

        return view('executions.students.examiner', [
            'execution' => $execution,
            'students' => $students,
            'contexts' => $contexts,
            'examiners' => $examiners,
            'tasks' => $tasks,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws AuthorizationException
     */
    public function update(ExecutionStudentUpdateRequest $request, Execution $execution): RedirectResponse
    {
        $this->authorize('update', $execution);

        $execution->loadMissing('exam');

        $students = $request->collect('students');

        $exam = $execution->exam;

        foreach ($students as $sid => $tasks) {
            $student = Student::query()->where('sid', $sid)->withTrashed()->firstOrFail();
            foreach ($tasks as $slug => $data) {
                $task = $exam->tasks()->where('slug', $slug)->firstOrFail();

                $context = Context::query()->firstWhere('slug', data_get($data, 'context'));
                $client = Examiner::query()->firstWhere('id', data_get($data, 'client'));
                $supervisor = Examiner::query()->firstWhere('id', data_get($data, 'supervisor'));

                /** @var ExecutionStudent $es */
                $es = $execution->students()->where('student_id', $student->id)->firstOrFail();

                ExecutionStudentTask::query()->updateOrCreate([
                    'execution_student_id' => $es->id,
                    'task_id' => $task->id,
                ], [
                    'context_id' => $context?->id,
                    ExecutionStudentTask::CLIENT_COLUMN => $client?->id,
                    ExecutionStudentTask::SUPERVISOR_COLUMN => $supervisor?->id,
                ]);
            }
        }

        return back()
            ->with('success', __('Studenten succesvol bewerkt'));
    }
}
