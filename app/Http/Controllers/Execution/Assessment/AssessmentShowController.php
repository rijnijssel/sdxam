<?php

namespace App\Http\Controllers\Execution\Assessment;

use App\Exceptions\NoExaminerException;
use App\Exceptions\ScoringException;
use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\ExecutionStudentTask;
use App\Models\Process;
use App\Models\Student;
use App\Services\AnnotationService;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Throwable;

class AssessmentShowController extends Controller
{
    public function __construct(
        private readonly AnnotationService $annotationService,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @throws NoExaminerException when no examiner was found for the current user
     * @throws AuthorizationException
     * @throws Throwable
     */
    public function __invoke(Request $request, Execution $execution, Student $student, Process $process): RedirectResponse|View
    {
        $this->authorize('executions.score', $execution);

        $process->loadMissing([
            'task',
            'rubrics.tasks.items',
        ]);

        try {
            $examiner = $request->user()?->getExaminer();
        } catch (NoExaminerException) {
            return to_route('executions.show', $execution);
        }

        if ($examiner === null) {
            return to_route('executions.show', $execution);
        }

        $examinerId = $examiner->id;

        /** @var ExecutionStudent $es */
        $es = $execution->students()
            ->where('student_id', $student->id)
            ->whereHas('tasks', fn ($query): Builder => $query
                ->where('tasks.id', $process->task_id)
                ->where(fn ($query) => $query->orWhere([
                    ExecutionStudentTask::CLIENT_COLUMN => $examinerId,
                    ExecutionStudentTask::SUPERVISOR_COLUMN => $examinerId,
                ])))
            ->with(['student', 'tasks', 'processes'])
            ->firstOrFail();
        $est = $es->getTask($process->task) ?? throw new ScoringException;

        $role = $est->getExaminerRole($examiner);
        $column = $role->getScoreColumn();

        $selected = $est->scores()->pluck($column);
        $annotations = $this->annotationService->getExaminerAnnotations($est, $examiner);

        Breadcrumbs::make()
            ->add('Uitvoeringen', route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Beoordelingen', route('score.index', $execution))
            ->add($student->getFullName(), null, true)
            ->add($process->name, route('score.show', [$execution, $student, $process]), true)
            ->share();

        return view('executions.assessments.show', [
            'execution' => $execution,
            'student' => $student,
            'executionStudent' => $es,
            'task' => $process->task,
            'process' => $process,
            'selected' => $selected,
            'annotations' => $annotations,
            'client' => $est->client,
            'supervisor' => $est->supervisor,
        ]);
    }
}
