<?php

namespace App\Http\Controllers\Execution\Assessment;

use App\Exceptions\NoExaminerException;
use App\Http\Controllers\Controller;
use App\Models\Examiner;
use App\Models\Execution;
use App\Models\ExecutionStudentTask;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class AssessmentIndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws NoExaminerException
     * @throws AuthorizationException
     */
    public function __invoke(Request $request, Execution $execution): View
    {
        $this->authorize('executions.score', $execution);

        $execution->load([
            'groups.students',
            'exam.tasks.processes',
            'exam.tasks' => fn (HasMany $query) => $query->withCount('processes'),
            'students.executionStudentTasks',
        ]);

        try {
            $examiner = $request->user()?->getExaminer();
        } catch (NoExaminerException $e) {
            report($e);
            $examiner = null;
        }

        $students = $execution->students()
            ->unless(
                $examiner === null,
                fn ($query) => $query
                    ->whereHas(
                        'tasks',
                        fn ($query) => $this->hasExaminer($query, $examiner),
                    )
            )
            ->with([
                'student',
                'tasks',
                'executionStudentTasks.client',
                'executionStudentTasks.supervisor',
            ])
            ->get();

        Breadcrumbs::make()
            ->add('Uitvoeringen', route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Beoordelingen', route('score.index', $execution), true)
            ->share();

        return view('executions.assessments.index', [
            'examiner' => $examiner,
            'execution' => $execution,
            'tasks' => $execution->exam->tasks,
            'students' => $students,
        ]);
    }

    /**
     * Query to filter based on an examiner.
     *
     * @param  Builder<ExecutionStudentTask>  $query
     * @return Builder<ExecutionStudentTask>
     */
    public function hasExaminer(Builder $query, ?Examiner $examiner): Builder
    {
        return $query
            ->where(ExecutionStudentTask::CLIENT_COLUMN, $examiner?->id)
            ->orWhere(ExecutionStudentTask::SUPERVISOR_COLUMN, $examiner?->id);
    }
}
