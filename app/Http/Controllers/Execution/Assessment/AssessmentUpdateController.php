<?php

namespace App\Http\Controllers\Execution\Assessment;

use App\Exceptions\NoExaminerException;
use App\Exceptions\ScoringException;
use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\ExecutionStudentTask;
use App\Models\Process;
use App\Models\Score;
use App\Models\Student;
use App\Models\User;
use App\Services\AnnotationService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Throwable;

class AssessmentUpdateController extends Controller
{
    public function __construct(
        private readonly AnnotationService $annotationService,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @throws Throwable when the transaction couldn't be rolled back
     */
    public function __invoke(Request $request, Execution $execution, Student $student, Process $process): RedirectResponse
    {
        /** @var User $user */
        $user = $request->user();

        try {
            $examiner = $user->getExaminer();
            $task = $this->getExecutionStudentTask($execution, $student, $process);

            $role = $task->getExaminerRole($examiner);
            $column = $role->getScoreColumn(); // "client_score_id" or "supervisor_score_id"

            $rows = $request->collect('rows');

            /**
             * @var int $id
             * @var array<string, string> $row
             */
            foreach ($rows as $id => $row) {
                $score = Score::query()->updateOrCreate(['execution_student_task_id' => $task->id, 'rubric_task_id' => $id], [$column => $row['score'] ?? null]);
                $annotation = $row['annotation'] ?? null;
                $this->annotationService->update($score, $annotation, $examiner);
            }
        } catch (NoExaminerException) {
            return back()
                ->with('danger', trans('Deze gebruiker heeft geen koppeling met een examinator.'));
        }

        return to_route('score.show', [$execution, $student, $process]);
    }

    /**
     * Get the execution student task based on an execution, student and process.
     *
     * @throws ScoringException
     */
    private function getExecutionStudentTask(Execution $execution, Student $student, Process $process): ExecutionStudentTask
    {
        /** @var ExecutionStudent $executionStudent */
        $executionStudent = $execution->students()->firstWhere('student_id', $student->id)
            ?? throw new ScoringException('Execution student could not be found.');

        return $executionStudent->executionStudentTasks()->firstWhere('task_id', $process->task_id)
            ?? throw new ScoringException('Linked task could not be found.');
    }
}
