<?php

namespace App\Http\Controllers\Execution\Task;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Execution;
use App\Models\Student;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Throwable;

class TaskUpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws Throwable
     */
    public function __invoke(Execution $execution, Request $request): RedirectResponse
    {
        $this->authorize('update', $execution);

        $execution->loadMissing([
            'exam.tasks',
        ]);

        $exam = $execution->exam;

        /** @var Collection<int, Collection<int, int>> $students */
        $students = $request->collect('students')
            ->filter(fn (array $value, string $key): bool => Student::query()->where('sid', $key)->exists())
            ->mapWithKeys(function (array $slugs, string $key) use ($exam): array {
                $student = Student::query()->where('sid', $key)->firstOrFail();

                return [$student->id => $this->mapSlugs($exam, $slugs)];
            });

        foreach ($students as $id => $selected) {
            if ($selected->isEmpty()) {
                $execution->students()->where('student_id', $id)->delete();

                continue;
            }

            $executionStudent = $execution->students()
                ->withTrashed()
                ->updateOrCreate([
                    'execution_id' => $execution->id,
                    'student_id' => $id,
                ]);

            $executionStudent->restore();
            $executionStudent->tasks()->sync($selected);
            $executionStudent->executionStudentTasks()->touch();
        }

        return to_route('executions.show', $execution);
    }

    /**
     * Map the tasks slugs of an exam to the task models.
     *
     * @param  Exam  $exam  the exam to map the slugs to
     * @param  array<int, string>  $slugs  the slugs
     * @return Collection<int, int>
     */
    private function mapSlugs(Exam $exam, array $slugs): Collection
    {
        $exam->loadMissing('tasks');

        return $exam->tasks()
            ->whereIn('slug', $slugs)
            ->pluck('id');
    }
}
