<?php

namespace App\Http\Controllers\Execution\Task;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\Student;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class TaskEditController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws AuthorizationException
     */
    public function __invoke(Execution $execution, Request $request): View
    {
        $this->authorize('update', $execution);

        $execution->loadMissing([
            'students.student',
            'students.tasks',
            'exam.tasks',
        ]);

        $selected = $execution->students->mapWithKeys(function (ExecutionStudent $value): array {
            if ($value->student === null) {
                return [];
            }
            $sid = $value->student->sid;
            $tasks = $value->tasks->pluck('slug');

            return [$sid => $tasks];
        });

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Studenten koppelen', active: true)
            ->share();

        return view('executions.students.task', [
            'execution' => $execution,
            'tasks' => data_get($execution, 'exam.tasks'),
            'students' => Student::active()->get(),
            'selected' => $selected,
        ]);
    }
}
