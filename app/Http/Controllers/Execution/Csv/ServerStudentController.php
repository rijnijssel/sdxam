<?php

namespace App\Http\Controllers\Execution\Csv;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Services\CsvService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class ServerStudentController extends Controller
{
    public function __construct(
        private readonly CsvService $csvService,
    ) {}

    public function __invoke(Execution $execution): RedirectResponse|Response
    {
        $students = $execution->students
            ->loadMissing('student')
            ->sortBy('student.first_name')
            ->map(function (ExecutionStudent $es): array {
                $student = $es->student;
                $lastName = collect([$student->middle_name, $student->last_name])
                    ->filter()
                    ->join(' ');
                $username = $student->getUsername();

                return [
                    $student->first_name,
                    $lastName,
                    $username,
                    "{$username}@examens",
                ];
            })
            ->toArray();

        $output = $this->csvService->make(
            ['first_name', 'last_name', 'identity', 'email'],
            $students,
        );

        if ($output === false) {
            return to_route('executions.show', $execution);
        }

        return response($output)
            ->header('Content-Type', 'text/plain');
    }
}
