<?php

namespace App\Http\Controllers\Execution\Csv;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Models\Task;
use App\Services\CsvService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class BookGeneratorController extends Controller
{
    public function __construct(
        private readonly CsvService $csvService
    ) {}

    public function __invoke(Execution $execution): RedirectResponse|Response
    {
        $tasks = $execution->exam->tasks;

        $students = $execution->students
            ->load(['student', 'tasks', 'executionStudentTasks.context'])
            ->sortBy('student.first_name')
            ->map(fn (ExecutionStudent $student) => [
                $student->student->sid,
                $student->student->getFullName(),
                $student->executionStudentTasks->first()?->context?->name,
                ...$tasks->map(fn (Task $task): string => $student->tasks->contains($task) ? '1' : ''),
            ])
            ->toArray();

        $output = $this->csvService->make(
            ['SID', 'Name', 'Context', ...$tasks->pluck('name')],
            $students,
        );

        if ($output === false) {
            return to_route('executions.show', $execution);
        }

        return response($output)
            ->header('Content-Type', 'text/plain');
    }
}
