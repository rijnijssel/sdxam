<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CurrentExecutionController extends Controller
{
    /** Redirect to the current execution. */
    public function __invoke(Request $request): RedirectResponse
    {
        $current = Execution::getCurrent();

        if ($current instanceof Execution) {
            return to_route('executions.show', $current);
        }

        $latest = Execution::query()->latest()->first();

        return $latest !== null
            ? to_route('executions.show', $latest)
            : to_route('executions.index');

    }
}
