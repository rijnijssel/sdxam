<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Support\Breadcrumbs\Breadcrumbs;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    /** Display a listing of the resource. */
    public function index(Request $request): View
    {
        $users = User::query()
            ->orderByRaw('id = :id DESC', [
                'id' => $request->user()?->id,
            ])
            ->orderByDesc('is_admin')
            ->orderBy('name')
            ->get();

        Breadcrumbs::make()
            ->add(__('views.users'), active: true)
            ->share();

        return view('users.index', [
            'users' => $users,
        ]);
    }

    /** Show the form for creating a new resource. */
    public function create(): View
    {
        Breadcrumbs::make()
            ->add(__('views.users'), route('users.index'))
            ->add('Toevoegen', active: true)
            ->share();

        return view('users.create');
    }

    /** Store a newly created resource in storage. */
    public function store(UserStoreRequest $request): RedirectResponse
    {
        $user = $request->getUserModel();
        $user->save();

        return to_route('users.index')
            ->with('success', trans('notifications.user.store'));
    }

    /** Display the specified resource. */
    public function show(User $user): View
    {
        Breadcrumbs::make()
            ->add(__('views.users'), route('users.index'))
            ->add($user->name, active: true)
            ->share();

        return view('users.show', [
            'user' => $user,
        ]);
    }

    /** Show the form for editing the specified resource. */
    public function edit(User $user): View
    {
        Breadcrumbs::make()
            ->add(__('views.users'), route('users.index'))
            ->add($user->name, route('users.show', $user))
            ->add('Bewerken', active: true)
            ->share();

        return view('users.edit', [
            'user' => $user,
        ]);
    }

    /** Update the specified resource in storage. */
    public function update(UserUpdateRequest $request, User $user): RedirectResponse
    {
        $user->update($request->getUserData());

        return to_route('users.show', $user)
            ->with('success', trans('notifications.user.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws Exception when no primary key exists on the model
     */
    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return to_route('users.index')
            ->with('danger', trans('notifications.user.destroy'));
    }
}
