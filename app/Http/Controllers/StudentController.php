<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentStoreRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Models\Student;
use App\Support\Breadcrumbs\Breadcrumbs;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Student::class, 'student');
    }

    /** Display a listing of the resource. */
    public function index(): View
    {
        $students = Student::active()->get();

        Breadcrumbs::make()
            ->add(__('views.students'), active: true)
            ->share();

        return view('students.index', [
            'students' => $students,
        ]);
    }

    /** Show the form for creating a new resource. */
    public function create(): View
    {
        Breadcrumbs::make()
            ->add(__('views.students'), route('students.index'))
            ->add('Toevoegen', active: true)
            ->share();

        return view('students.create');
    }

    /** Store a newly created resource in storage. */
    public function store(StudentStoreRequest $request): RedirectResponse
    {
        $student = $request->getStudent();
        $student->save();

        return to_route('students.index')
            ->with('success', trans('notifications.student.store'));
    }

    /** Display the specified resource. */
    public function show(Student $student): View
    {
        $student->loadMissing([
            'executionStudents.execution',
            'executionStudents.executionStudentTasks.task',
            'executionStudents.executionStudentTasks.context',
        ]);

        Breadcrumbs::make()
            ->add(__('views.students'), route('students.index'))
            ->add($student->getFullName(), active: true)
            ->share();

        return view('students.show', [
            'student' => $student,
        ]);
    }

    /** Show the form for editing the specified resource. */
    public function edit(Student $student): View
    {
        Breadcrumbs::make()
            ->add(__('views.students'), route('students.index'))
            ->add($student->getFullName(), route('students.show', $student))
            ->add('Bewerken', active: true)
            ->share();

        return view('students.edit', [
            'student' => $student,
        ]);
    }

    /** Update the specified resource in storage. */
    public function update(StudentUpdateRequest $request, Student $student): RedirectResponse
    {
        $student->update($request->getStudent());

        return to_route('students.show', $student)
            ->with('success', trans('notifications.student.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws Exception when no primary key exists on the model
     */
    public function destroy(Student $student): RedirectResponse
    {
        $student->delete();

        return to_route('students.index')
            ->with('danger', trans('notifications.student.destroy'));
    }
}
