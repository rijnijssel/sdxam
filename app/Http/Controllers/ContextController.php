<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContextStoreRequest;
use App\Http\Requests\ContextUpdateRequest;
use App\Models\Context;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ContextController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Context::class, 'context');
    }

    /** Display a listing of the resource. */
    public function index(): View
    {
        Breadcrumbs::make()
            ->add('Contexten', active: true)
            ->share();

        $contexts = Context::query()
            ->get();

        return view('contexts.index', [
            'contexts' => $contexts,
        ]);
    }

    /** Show the form for creating a new resource. */
    public function create(): View
    {
        Breadcrumbs::make()
            ->add('Contexten', route('contexts.index'))
            ->add('Toevoegen', active: true)
            ->share();

        return view('contexts.create');
    }

    /** Store a newly created resource in storage. */
    public function store(ContextStoreRequest $request): RedirectResponse
    {
        $data = $request->validated();

        $context = new Context;
        $context->name = $data['name'];
        $context->save();

        return to_route('contexts.index');
    }

    /** Show the form for editing the specified resource. */
    public function edit(Context $context): View
    {
        Breadcrumbs::make()
            ->add('Contexten', route('contexts.index'))
            ->add($context->name, route('contexts.edit', $context))
            ->add('Bewerken', active: true)
            ->share();

        return view('contexts.edit', [
            'context' => $context,
        ]);
    }

    /** Update the specified resource in storage. */
    public function update(ContextUpdateRequest $request, Context $context): RedirectResponse
    {
        $data = $request->validated();

        $context->name = $data['name'];
        $context->save();

        return to_route('contexts.index');
    }
}
