<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use App\Models\ExecutionStudent;
use App\Services\CsvService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class StudentExportController extends Controller
{
    public function __construct(
        private readonly CsvService $csvService
    ) {}

    public function __invoke(Execution $execution): Response|RedirectResponse
    {
        $students = $execution->students
            ->load(['student', 'executionStudentTasks.context'])
            ->map(fn (ExecutionStudent $student): array => [
                $student->student->first_name,
                $student->student->middle_name,
                $student->student->last_name,
                $student->student->getUsername(),
                $student->student->sid,
            ])
            ->toArray();

        $output = $this->csvService->make(['first_name', 'middle_name', 'last_name', 'username', 'sid'], $students);

        if ($output === false) {
            return to_route('executions.show', $execution);
        }

        return response($output)
            ->header('Content-Type', 'text/plain');
    }
}
