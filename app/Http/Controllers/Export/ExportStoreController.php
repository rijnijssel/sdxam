<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessExport;
use App\Models\Execution;
use App\Models\Export;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ExportStoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws AuthorizationException
     */
    public function __invoke(Request $request, Execution $execution): RedirectResponse|View
    {
        $this->authorize('executions.export', $execution);

        $execution->loadMissing([
            'exam.tasks',
            'students.student',
            'students.executionStudentTasks',
            'students.executionStudentTasks.client',
            'students.executionStudentTasks.supervisor',
            'students.executionStudentTasks.task.processes.rubrics.tasks.items',
            'students.executionStudentTasks.task.processes.rubrics.requiredTasks.items',
            'students.executionStudentTasks.scores.rubricTask.items',
        ]);

        if ($request->has('view')) {
            $students = $execution->students;
            $data = [
                'execution' => $execution,
                'students' => $students,
            ];

            return view('export.results', $data);
        }

        /** @var User $user */
        $user = $request->user();
        $export = Export::createPending($execution);
        dispatch(new ProcessExport($user, $export));

        return to_route('executions.export.index', $execution)
            ->with('success', __('Je krijgt een email zodra de export klaar is.'));
    }
}
