<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Http\Requests\BatchExportStoreRequest;
use App\Jobs\ProcessBatchExport;
use App\Models\BatchExport;
use App\Models\Execution;
use App\Models\Student;
use App\Models\User;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;

class BatchExportController extends Controller
{
    /** @throws AuthorizationException when the user is not authorized to create an export */
    public function store(BatchExportStoreRequest $request, Execution $execution): RedirectResponse
    {
        $this->authorize('executions.export', $execution);

        $data = $request->validated();

        /** @var User $user */
        $user = $request->user();

        /** @var Collection<int, Student> $students */
        $students = Student::query()
            ->withTrashed()
            ->whereIn('sid', $data['students'])
            ->get(['id']);

        $batch = BatchExport::createPending($execution, $students);
        dispatch(new ProcessBatchExport($user, $batch));

        return to_route('executions.export.index', $execution)
            ->with([
                'success' => trans('Export gestart. Je zult een mail ontvangen wanneer deze klaar is.'),
            ]);
    }

    /** @throws AuthorizationException when the user is not authorized to see an export */
    public function show(Execution $execution, BatchExport $batchExport): View
    {
        $this->authorize('executions.export', $execution);

        $batchExport->loadMissing([
            'students',
        ]);

        Breadcrumbs::make()
            ->add(__('views.executions'), route('executions.index'))
            ->add($execution->name, route('executions.show', $execution))
            ->add('Exports', route('executions.export.index', $execution))
            ->add((string) $batchExport->started_at, active: true)
            ->share();

        return view('executions.batch-exports.show', [
            'execution' => $execution,
            'export' => $batchExport,
        ]);
    }
}
