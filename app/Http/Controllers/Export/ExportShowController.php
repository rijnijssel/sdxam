<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\Execution;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExportShowController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @throws AuthorizationException
     */
    public function __invoke(Request $request, Execution $execution, string $date): StreamedResponse
    {
        $this->authorize('executions.export', $execution);

        $file = sprintf('exports/%s_%s.pdf', $execution->slug, $date);

        return Storage::response($file);
    }
}
