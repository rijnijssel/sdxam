<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\BatchExport;
use App\Models\Execution;
use App\Models\Student;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use ZipArchive;

class BatchExportDownloadController extends Controller
{
    /** @throws AuthorizationException */
    public function index(Execution $execution, BatchExport $batchExport): StreamedResponse
    {
        $this->authorize('executions.export', $execution);

        $archive = $batchExport->directory.'.zip';

        if (Storage::missing($archive)) {
            $zip = new ZipArchive;

            abort_if($zip->open(Storage::path($archive), ZipArchive::CREATE | ZipArchive::OVERWRITE) !== true, Response::HTTP_INTERNAL_SERVER_ERROR, 'Creating archive failed.');

            $files = Storage::files($batchExport->directory);
            foreach ($files as $file) {
                $file = Storage::path($file);
                $name = basename($file);
                $zip->addFile($file, $name);
            }
            $zip->close();
        }

        $filename = trans('Resultaten :file', ['file' => $execution->name.'.zip']);

        return Storage::download($archive, $filename);
    }

    /** @throws AuthorizationException */
    public function show(Execution $execution, BatchExport $batchExport, Student $student): StreamedResponse
    {
        $this->authorize('executions.export', $execution);

        $path = $batchExport->getStudentPath($student);

        abort_if($path === false, Response::HTTP_NOT_FOUND);

        return Storage::response($path, "Resultaten {$student->getFullName()} {$execution->name}.pdf");
    }
}
