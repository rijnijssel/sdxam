<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display the profile of the current user.
     *
     * @throws AuthorizationException
     */
    public function show(Request $request): View
    {
        $user = $request->user();

        $this->authorize('profile.show', $user);

        return view('profile.show', [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @throws AuthorizationException
     */
    public function update(UpdateUserProfileRequest $request): RedirectResponse
    {
        /** @var User $user */
        $user = $request->user();

        $this->authorize('profile.update', $user);

        $user->update($request->getUserData());

        return to_route('profile.show')
            ->with('success', __('notifications.profile.save'));
    }
}
