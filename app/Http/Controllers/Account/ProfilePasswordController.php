<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserPasswordRequest;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\RedirectResponse;

class ProfilePasswordController extends Controller
{
    /**
     * Update the specified user's password.
     *
     * @throws AuthorizationException
     */
    public function update(
        UpdateUserPasswordRequest $request,
        Authenticatable $authenticatable,
        AuthManager $auth,
    ): RedirectResponse {
        // Authorize the updating of this user's password
        $this->authorize('profile.update-password');

        if ($authenticatable instanceof User) {
            // Handle setting of password, saving to database and logging the user out
            $authenticatable->setPlainPassword($request->getUserPassword());

            if ($authenticatable->save()) {
                $auth->logout();
            }
        }

        return back();
    }
}
