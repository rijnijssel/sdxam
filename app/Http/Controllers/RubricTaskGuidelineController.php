<?php

namespace App\Http\Controllers;

use App\Models\RubricTask;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class RubricTaskGuidelineController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @throws AuthorizationException
     */
    public function edit(RubricTask $task): View
    {
        $this->authorize('guidelines.update');

        $task->loadMissing([
            'rubric.process.task.exam',
            'rubric.process.rubrics',
            'items',
        ]);

        Breadcrumbs::make()
            ->add('Examens', route('exams.index'))
            ->add($task->rubric->process->task->exam->name ?? '', route('exams.show', $task->rubric->process->task->exam ?? null))
            ->add($task->rubric->process->task->name ?? '', route('exams.tasks.show', [$task->rubric->process->task->exam ?? null, $task->rubric->process->task ?? null]))
            ->add($task->rubric->process->name ?? '', route('exams.tasks.processes.show', [$task->rubric->process->task->exam ?? null, $task->rubric->process->task ?? null, $task->rubric->process ?? null]))
            ->add('Onderlegger')
            ->add($task->name, active: true)
            ->share();

        return view('guidelines.edit', [
            'task' => $task,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @throws AuthorizationException
     */
    public function update(Request $request, RubricTask $task): RedirectResponse
    {
        $this->authorize('guidelines.update');

        $request->validate([
            'guidelines' => ['array'],
            'guidelines.*' => ['nullable', 'string'],
        ]);

        $guidelines = $request->collect('guidelines');

        foreach ($task->items as $item) {
            $guideline = $guidelines->get($item->id);
            $item->update(['guideline' => $guideline]);
        }

        return back();
    }
}
