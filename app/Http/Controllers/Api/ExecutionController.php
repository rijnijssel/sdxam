<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ExecutionResource;
use App\Models\Execution;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ExecutionController extends Controller
{
    public function index(): ResourceCollection
    {
        $this->authorize('viewAny', Execution::class);

        $executions = Execution::query()
            ->has('terraformExecution')
            ->get();

        return ExecutionResource::collection($executions);
    }

    public function show(Execution $execution): ExecutionResource
    {
        $this->authorize('viewAny', Execution::class);

        return ExecutionResource::make($execution);
    }
}
