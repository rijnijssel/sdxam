<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ExecutionStudentResource;
use App\Models\Execution;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ExecutionStudentController extends Controller
{
    public function index(Execution $execution): AnonymousResourceCollection
    {
        $this->authorize('viewAny', Execution::class);

        $execution->loadMissing([
            'students.student',
            'students.student.groups' => function ($query) use ($execution): void {
                $query->where('execution_id', $execution->id);
            },
        ]);

        return ExecutionStudentResource::collection($execution->students);
    }
}
