<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\View\View;

class TerraformStateController extends Controller
{
    public function index(Guard $guard): View
    {
        /** @var User $user */
        $user = $guard->user();

        $hasPipelineTrigger = config('services.gitlab_pipeline_trigger.enabled') && $user->can('terraform.pipeline-trigger');

        Breadcrumbs::make()
            ->add('Terraform', active: true)
            ->share();

        return view('terraform.index', [
            'hasPipelineTrigger' => $hasPipelineTrigger,
        ]);
    }
}
