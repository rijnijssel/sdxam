<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\AppServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ExternalAuthenticationController extends Controller
{
    public const SURFCONEXT_DRIVER = 'surfconext';

    public const GITLAB_DRIVER = 'gitlab';

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Redirect the user to an external login page for SurfConext and GitLab.
     *
     * @param  string  $service  must be 'surfconext' or 'gitlab'
     */
    public function create(string $service): RedirectResponse
    {
        $enabled = $this->isEnabled($service);

        abort_if($enabled === false, Response::HTTP_NOT_FOUND);

        return Socialite::driver($service)->redirect();
    }

    /**
     * Store the response from SurfConext/GitLab to authenticate the user.
     *
     * @param  string  $service  must be 'surfconext' or 'gitlab'
     */
    public function store(string $service): RedirectResponse
    {
        $enabled = $this->isEnabled($service);

        abort_if($enabled === false, Response::HTTP_NOT_FOUND);

        try {
            $response = Socialite::driver($service)->user();
        } catch (InvalidStateException) {
            return to_route('login');
        }

        $email = $response->getEmail();

        if (User::query()->where('email', $email)->doesntExist()) {
            Log::error('auth-sso:failed', [
                'email' => $email,
                'service' => $service,
            ]);

            return to_route('login')
                ->setStatusCode(Response::HTTP_FORBIDDEN)
                ->withErrors([
                    'socialite' => $email,
                ]);
        }

        /** @var User $user */
        $user = User::query()->where('email', $email)->first();
        Auth::login($user);

        Log::debug('auth-sso:success', [
            'id' => $user->id,
            'service' => $service,
        ]);

        return redirect()->intended(AppServiceProvider::HOME);
    }

    /** Check if Socialite provider is enabled. Currently, this method supports either 'surfconext' or 'gitlab'. */
    private function isEnabled(string $service): bool
    {
        return match ($service) {
            self::SURFCONEXT_DRIVER => config('services.surfconext.enabled', false) === true,
            self::GITLAB_DRIVER => config('services.gitlab.enabled', false) === true,
            default => false,
        };
    }
}
