<?php

namespace App\Http\Controllers\Exam;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Task;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\View\View;

class TaskController extends Controller
{
    /** Display the specified resource. */
    public function show(Exam $exam, Task $task): View
    {
        $task->loadMissing('processes.rubrics.tasks');

        Breadcrumbs::make()
            ->add('Examens', route('exams.index'))
            ->add($exam->name, route('exams.show', $exam))
            ->add($task->name, route('exams.tasks.show', [$exam, $task]), true)
            ->share();

        return view('exams.tasks.show', [
            'exam' => $exam,
            'task' => $task,
        ]);
    }
}
