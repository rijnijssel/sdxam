<?php

namespace App\Http\Controllers\Exam;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Process;
use App\Models\Task;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\View\View;

class ProcessController extends Controller
{
    /** Display the specified resource. */
    public function show(Exam $exam, Task $task, Process $process): View
    {
        $process->loadMissing('rubrics.tasks.items');

        Breadcrumbs::make()
            ->add('Examens', route('exams.index'))
            ->add($exam->name, route('exams.show', $exam))
            ->add($task->name, route('exams.tasks.show', [$exam, $task]), true)
            ->add($process->name, route('exams.tasks.processes.show', [$exam, $task, $process]), true)
            ->share();

        return view('exams.processes.show', [
            'exam' => $exam,
            'task' => $task,
            'process' => $process,
        ]);
    }
}
