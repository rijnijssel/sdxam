<?php

namespace App\Http\Controllers\Exam;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\View\View;

class ExamController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Exam::class);
    }

    /** Display a listing of the resource. */
    public function index(): View
    {
        Breadcrumbs::make()
            ->add('Examens', route('exams.index'), true);

        $exams = Exam::query()->get(['id', 'slug', 'name']);

        return view('exams.index')
            ->with('exams', $exams);
    }

    /** Display the specified resource. */
    public function show(Exam $exam): View
    {
        $exam->loadMissing('tasks.processes.rubrics');

        $executions = $exam->executions()
            ->orderByDesc('start_date')
            ->get();

        Breadcrumbs::make()
            ->add('Examens', route('exams.index'))
            ->add($exam->name, route('exams.show', $exam), true)
            ->share();

        return view('exams.show', [
            'exam' => $exam,
            'executions' => $executions,
        ]);
    }
}
