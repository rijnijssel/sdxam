<?php

namespace App\Http\Controllers;

use BlameButton\Laravel\Changelog\Changelog;
use Illuminate\Contracts\View\View;
use Illuminate\Support\HtmlString;

class ChangelogController extends Controller
{
    public function __invoke(Changelog $changelog): View
    {
        return view('changelog', [
            'content' => rescue(fn (): HtmlString => $changelog->html(), 'No changelog.'),
        ]);
    }
}
