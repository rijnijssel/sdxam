<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExaminerStoreRequest;
use App\Http\Requests\ExaminerUpdateRequest;
use App\Models\Examiner;
use App\Models\User;
use App\Support\Breadcrumbs\Breadcrumbs;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ExaminerController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Examiner::class, 'examiner');
    }

    /** Display a listing of the resource. */
    public function index(): View
    {
        Breadcrumbs::make()
            ->add('Examinatoren', route('examiners.index'), true)
            ->share();

        $examiners = Examiner::query()
            ->with('user')
            ->orderBy('name')
            ->get();

        return view('examiners.index', [
            'examiners' => $examiners,
        ]);
    }

    /** Show the form for creating a new resource. */
    public function create(): View
    {
        Breadcrumbs::make()
            ->add('Examinatoren', route('examiners.index'))
            ->add('Toevoegen', route('examiners.create'))
            ->share();

        $existing = Examiner::query()->pluck('user_id');
        $users = User::query()->whereNotIn('id', $existing)->get();

        return view('examiners.create', [
            'users' => $users,
        ]);
    }

    /** Store a newly created resource in storage. */
    public function store(ExaminerStoreRequest $request): RedirectResponse
    {
        $examiner = $request->getExaminer();
        $examiner->user()->associate($request->getUserModel());
        $examiner->save();

        return to_route('examiners.index')
            ->with('success', __('notifications.examiner.store'));
    }

    /** Display the specified resource. */
    public function show(Examiner $examiner): View
    {
        Breadcrumbs::make()
            ->add('Examinatoren', route('examiners.index'))
            ->add($examiner->name, route('examiners.show', $examiner), true)
            ->share();

        return view('examiners.show', [
            'examiner' => $examiner,
        ]);
    }

    /** Show the form for editing the specified resource. */
    public function edit(Examiner $examiner): View
    {
        Breadcrumbs::make()
            ->add('Examinatoren', route('examiners.index'))
            ->add($examiner->name, route('examiners.show', $examiner))
            ->add('Bewerken', route('examiners.edit', $examiner), true)
            ->share();

        $users = User::all();

        return view('examiners.edit', [
            'examiner' => $examiner,
            'users' => $users,
        ]);
    }

    /** Update the specified resource in storage. */
    public function update(ExaminerUpdateRequest $request, Examiner $examiner): RedirectResponse
    {
        $examiner->user()->associate($request->getUserModel());
        $examiner->update($request->getExaminer());

        return to_route('examiners.show', $examiner)
            ->with('success', __('notifications.examiner.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws Exception when no primary key exists on the model
     */
    public function destroy(Examiner $examiner): RedirectResponse
    {
        $examiner->delete();

        return to_route('examiners.index')
            ->with('danger', trans('notifications.examiner.destroy'));
    }
}
