<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArchiveStudentsRequest;
use App\Models\Student;
use App\Services\StudentService;
use App\Support\Breadcrumbs\Breadcrumbs;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class StudentStatusController extends Controller
{
    public function __construct(
        public StudentService $studentService
    ) {}

    /**
     * Show the form for hiding students.
     *
     * @throws AuthorizationException
     */
    public function edit(): View
    {
        $this->authorize('students.archive');

        $students = Student::all();

        Breadcrumbs::make()
            ->add(__('views.students'), route('students.index'))
            ->add('Archiveren', active: true)
            ->share();

        return view('students.archive.edit', [
            'students' => $students,
        ]);
    }

    /**
     * Hide the given students.
     *
     * @throws AuthorizationException
     */
    public function update(ArchiveStudentsRequest $request): RedirectResponse
    {
        $this->authorize('students.archive');

        $studentNumbers = $request->getStudentNumbers();

        $this->studentService->setArchived($studentNumbers);

        return to_route('students.index');
    }
}
