<?php

namespace App\Http\Requests;

use App\Models\Exam;
use App\Models\Execution;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Override;

class ExecutionStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'max:100', Rule::unique(Execution::class, 'name')],
            'slug' => ['required', 'max:100', Rule::unique(Execution::class, 'slug')],
            'start_date' => ['nullable', 'date_format:Y-m-d'],
            'exam' => ['required', Rule::exists(Exam::class, 'slug')],
        ];
    }

    public function getExecution(): Execution
    {
        $execution = new Execution;
        $execution->name = data_get($this->validated(), 'name');
        $execution->start_date = data_get($this->validated(), 'start_date');

        return $execution;
    }

    /** Determine if the user has selected an exam. */
    public function hasExam(): bool
    {
        return $this->filled('exam');
    }

    /** Find the exam mentioned in this request, or return null. */
    public function getExam(): Exam|Model|null
    {
        $slug = data_get($this->validated(), 'exam');

        return Exam::query()->where('slug', $slug)->first('id');
    }

    #[Override]
    protected function prepareForValidation(): void
    {
        $this->merge([
            'slug' => Str::slug($this->input('name')),
        ]);
    }
}
