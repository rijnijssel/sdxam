<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email:filter', Rule::unique(User::class, 'email')],
            'name' => ['required'],
            'is_admin' => ['sometimes'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function getUserModel(): User
    {
        $user = new User;
        $user->name = (string) data_get($this->validated(), 'name');
        $user->email = (string) data_get($this->validated(), 'email');
        $user->is_admin = $this->boolean('is_admin');
        $user->setPlainPassword((string) data_get($this->validated(), 'password'));

        return $user;
    }
}
