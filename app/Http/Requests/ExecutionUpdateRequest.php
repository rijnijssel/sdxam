<?php

namespace App\Http\Requests;

use App\Models\Exam;
use App\Models\Execution;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Override;

class ExecutionUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        $execution = $this->route('execution');

        return [
            'name' => ['required', 'max:100', Rule::unique(Execution::class, 'name')->ignore($execution)],
            'slug' => ['required', 'max:100', Rule::unique(Execution::class, 'slug')->ignore($execution)],
            'start_date' => ['nullable', 'date_format:Y-m-d'],
            'exam' => ['required', Rule::exists(Exam::class, 'slug')],
        ];
    }

    /**
     * Get the data that was passed to this update.
     *
     * @return array<string, string>
     */
    public function getExecution(): array
    {
        return [
            'name' => data_get($this->validated(), 'name'),
            'start_date' => data_get($this->validated(), 'start_date'),
        ];
    }

    /** Find the exam mentioned in this request, or return null. */
    public function getExam(): ?Exam
    {
        $slug = data_get($this->validated(), 'exam');

        return Exam::query()->where('slug', $slug)->first('id');
    }

    #[Override]
    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => $this->str('name')->slug(),
        ]);
    }
}
