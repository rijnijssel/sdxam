<?php

namespace App\Http\Requests;

use App\Models\Examiner;
use App\Models\Execution;
use App\Models\Group;
use App\Models\Student;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;

class GroupStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        /** @var Execution $execution */
        $execution = $this->route('execution');

        return [
            'name' => ['required', 'max:100',
                Rule::unique(Group::class, 'name')
                    ->where('execution_id', $execution->id),
            ],
            'examiners' => ['array', 'min:0'],
            'examiners.*' => [Rule::exists(Examiner::class, 'id')],
            'students' => ['array', 'min:0'],
            'students.*' => [Rule::exists(Student::class, 'sid')],
        ];
    }

    public function getName(): string
    {
        return data_get($this->validated(), 'name');
    }

    /** @return int[] */
    public function getExaminerIds(): array
    {
        return data_get($this->validated(), 'examiners', []);
    }

    /** @return int[] */
    public function getStudentNumbers(): array
    {
        return data_get($this->validated(), 'students', []);
    }

    public function getGroup(): Group
    {
        $group = new Group;
        $group->name = $this->getName();

        return $group;
    }

    /** @return Collection<int, Examiner> */
    public function getExaminers(): Collection
    {
        return Examiner::query()->whereIn('id', $this->getExaminerIds())
            ->get('id');
    }

    /** @return Collection<int, Student> */
    public function getStudents(): Collection
    {
        return Student::query()->whereIn('sid', $this->getStudentNumbers())
            ->get(['id']);
    }
}
