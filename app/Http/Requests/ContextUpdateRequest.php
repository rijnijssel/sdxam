<?php

namespace App\Http\Requests;

use App\Models\Context;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContextUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        /** @var Context $context */
        $context = $this->route('context');

        return [
            'name' => [
                'required',
                'max:255',
                Rule::unique(Context::class, 'name')->ignore($context),
            ],
        ];
    }
}
