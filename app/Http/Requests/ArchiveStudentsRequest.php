<?php

namespace App\Http\Requests;

use App\Models\Student;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArchiveStudentsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        return [
            'students' => ['array', 'min:0'],
            'students.*' => [Rule::exists(Student::class, 'sid')],
        ];
    }

    /**
     * Get the list of student numbers submitted to the form.
     *
     * @return string[]
     */
    public function getStudentNumbers(): array
    {
        return (array) data_get($this->validated(), 'students', []);
    }
}
