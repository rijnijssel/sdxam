<?php

namespace App\Http\Requests;

use App\Models\Examiner;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExaminerUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        $examiner = $this->route('examiner');

        return [
            'name' => ['required', 'string'],
            'abbreviation' => ['required', 'string', 'max:5', Rule::unique(Examiner::class, 'abbreviation')->ignore($examiner)],
            'user' => ['required', Rule::exists(User::class, 'email')],
        ];
    }

    /**
     * Get the data to use for updating the examiner.
     *
     * @return array<string, string>
     */
    public function getExaminer(): array
    {
        return [
            'name' => data_get($this->validated(), 'name'),
            'abbreviation' => data_get($this->validated(), 'abbreviation'),
        ];
    }

    /**
     * Get the user ID selected for this examiner.
     *
     * @return Model|User|null
     */
    public function getUserModel()
    {
        $email = data_get($this->validated(), 'user');

        return User::query()->where('email', $email)->first('id');
    }
}
