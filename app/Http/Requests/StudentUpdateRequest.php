<?php

namespace App\Http\Requests;

use App\Models\Student;
use App\Rules\StudentNumberRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<object|string>>
     */
    public function rules(): array
    {
        $student = $this->route('student');

        return [
            'sid' => ['required',
                new StudentNumberRule,
                Rule::unique(Student::class, 'sid')->ignore($student), ],
            'first_name' => ['required'],
            'middle_name' => ['nullable'],
            'last_name' => ['required'],
            'email' => ['nullable', 'email:filter'],
            'cohort' => ['required', 'digits:4'],
        ];
    }

    /**
     * Get the data to use for updating this student.
     *
     * @return array<string, string>
     */
    public function getStudent(): array
    {
        return [
            'sid' => (string) data_get($this->validated(), 'sid'),
            'first_name' => (string) data_get($this->validated(), 'first_name'),
            'middle_name' => (string) data_get($this->validated(), 'middle_name'),
            'last_name' => (string) data_get($this->validated(), 'last_name'),
            'email' => (string) data_get($this->validated(), 'email'),
            'cohort' => (string) data_get($this->validated(), 'cohort'),
        ];
    }
}
