<?php

namespace App\Http\Requests;

use App\Models\Examiner;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExaminerStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'abbreviation' => ['required', 'string', 'max:5', Rule::unique(Examiner::class, 'abbreviation')],
            'user' => ['required', Rule::exists(User::class, 'email')],
        ];
    }

    public function getExaminer(): Examiner
    {
        $examiner = new Examiner;
        $examiner->name = data_get($this->validated(), 'name');
        $examiner->abbreviation = data_get($this->validated(), 'abbreviation');

        return $examiner;
    }

    /**
     * Get the user ID selected for this examiner.
     *
     * @return Model|User|null
     */
    public function getUserModel()
    {
        $email = data_get($this->validated(), 'user');

        return User::query()->where('email', $email)->first('id');
    }
}
