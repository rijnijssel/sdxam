<?php

namespace App\Http\Requests;

use App\Models\Student;
use App\Rules\StudentNumberRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<object|string>>
     */
    public function rules(): array
    {
        return [
            'sid' => [
                'required',
                new StudentNumberRule,
                Rule::unique(Student::class, 'sid'),
            ],
            'first_name' => ['required'],
            'middle_name' => ['nullable'],
            'last_name' => ['required'],
            'email' => ['nullable', 'email:filter'],
            'cohort' => ['required', 'gte:1901', 'lte:2155'],
        ];
    }

    /** Get a student instance with all fields assigned to it. */
    public function getStudent(): Student
    {
        $student = new Student;
        $student->sid = $this->validated()['sid'];
        $student->first_name = $this->validated()['first_name'];
        $student->middle_name = $this->validated()['middle_name'];
        $student->last_name = $this->validated()['last_name'];
        $student->email = $this->validated()['email'];
        $student->cohort = $this->validated()['cohort'];
        $student->graduated = false;

        return $student;
    }
}
