<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<object|string>>
     */
    public function rules(): array
    {
        $user = $this->route('user');

        return [
            'name' => ['required', 'string'],
            'email' => [
                'required',
                'email:filter',
                Rule::unique(User::class, 'email')->ignore($user),
            ],
        ];
    }

    /** @return array<string, string> */
    public function getUserData(): array
    {
        return [
            'name' => (string) data_get($this->validated(), 'name'),
            'email' => (string) data_get($this->validated(), 'email'),
        ];
    }
}
