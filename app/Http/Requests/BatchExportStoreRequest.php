<?php

namespace App\Http\Requests;

use App\Models\Student;
use App\Rules\StudentNumberRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BatchExportStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    public function rules(): array
    {
        return [
            'students' => ['required', 'array', 'min:1'],
            'students.*' => [
                'string',
                new StudentNumberRule,
                Rule::exists(Student::class, 'sid'),
            ],
        ];
    }
}
