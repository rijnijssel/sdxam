<?php

namespace App\Http\Requests;

use App\Models\Student;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ExecutionStudentUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        return [
            'students' => ['array', 'min:0'],
            'students.*' => ['array'],
            'students.*.*' => ['array:context,client,supervisor'],
        ];
    }

    /** Configure the validator instance. */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator): void {
            $numbers = $this->collect('students')->keys();
            $found = Student::query()
                ->whereIn('sid', $numbers)
                ->count();

            if ($found != count($numbers)) {
                $validator->errors()->add('students', __('validation.exists', ['attribute' => 'students']));
            }
        });
    }
}
