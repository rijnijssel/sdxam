<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        $user = $this->route('user');

        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email:filter',
                Rule::unique(User::class, 'email')->ignore($user), ],
            'is_admin' => ['sometimes'],
        ];
    }

    /** @return array<string, mixed> */
    public function getUserData(): array
    {
        return [
            'name' => data_get($this->validated(), 'name'),
            'email' => data_get($this->validated(), 'email'),
            'is_admin' => $this->has('is_admin'),
        ];
    }
}
