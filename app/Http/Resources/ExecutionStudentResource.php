<?php

namespace App\Http\Resources;

use App\Models\ExecutionStudent;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Override;

/**
 * @mixin ExecutionStudent
 */
class ExecutionStudentResource extends JsonResource
{
    /** @return array<string, mixed> */
    #[Override]
    public function toArray(Request $request): array
    {
        return [
            'slug' => $this->student->getUsername(),
            'name' => $this->student->getFullName(),
            'groups' => GroupResource::collection($this->when(
                $this->relationLoaded('student') && $this->student->relationLoaded('groups'),
                fn () => $this->student->groups,
            )),
        ];
    }
}
