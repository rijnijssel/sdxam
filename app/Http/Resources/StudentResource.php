<?php

namespace App\Http\Resources;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Override;

/**
 * @mixin Student
 */
class StudentResource extends JsonResource
{
    /** @return array<string, mixed> */
    #[Override]
    public function toArray(Request $request): array
    {
        return [
            'slug' => $this->getUsername(),
            'name' => $this->getFullName(),
            'student_number' => $this->sid,
        ];
    }
}
