<?php

namespace App\Contracts;

/**
 * Interface HasDropdownLabel.
 */
interface HasDropdownLabel
{
    /** Format label for dropdowns. */
    public function getLabel(): string;
}
