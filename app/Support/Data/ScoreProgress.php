<?php

namespace App\Support\Data;

use Illuminate\Contracts\Support\Htmlable;
use Override;

class ScoreProgress implements Htmlable
{
    public function __construct(
        public int $total,
        public int $filled = 0,
    ) {}

    public function filled(): bool
    {
        return $this->total === $this->filled;
    }

    #[Override]
    public function toHtml(): string
    {
        return sprintf('%d / %d', $this->filled, $this->total);
    }
}
