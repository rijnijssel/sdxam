<?php

namespace App\Support\Breadcrumbs;

use Illuminate\Support\Collection;

final readonly class Breadcrumbs
{
    /** @var Collection<int, Breadcrumb> */
    private Collection $items;

    public function __construct()
    {
        $this->items = new Collection;
    }

    public static function make(): self
    {
        return new Breadcrumbs;
    }

    public function add(string $name, ?string $url = null, bool $active = false): self
    {
        $this->items->push(new Breadcrumb($name, $url, $active));

        return $this;
    }

    public function share(): self
    {
        view()->share('breadcrumbs', $this->items);

        return $this;
    }
}
