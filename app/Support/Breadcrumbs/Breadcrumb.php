<?php

namespace App\Support\Breadcrumbs;

readonly class Breadcrumb
{
    public function __construct(
        public string $name,
        public ?string $url,
        public bool $active,
    ) {}
}
