<?php

namespace App\Support\Enum;

use App\Models\ExecutionStudentTask;
use App\Models\Score;

enum ExaminerRole: string
{
    case Client = 'client';
    case Supervisor = 'supervisor';

    /** Get the name of the progress column for this examiner role. */
    public function getProgressColumn(): string
    {
        return match ($this) {
            ExaminerRole::Client => 'client_count',
            ExaminerRole::Supervisor => 'supervisor_count',
        };
    }

    /** Get the execution student task column for this examiner role. */
    public function getExecutionStudentTaskColumn(): string
    {
        return match ($this) {
            ExaminerRole::Client => ExecutionStudentTask::CLIENT_COLUMN,
            ExaminerRole::Supervisor => ExecutionStudentTask::SUPERVISOR_COLUMN,
        };
    }

    /**
     * Get the scoring column for an examiner.
     *
     * @throw ScoringException when the examiner is not linked
     */
    public function getScoreColumn(): string
    {
        return match ($this) {
            ExaminerRole::Client => Score::CLIENT_COLUMN,
            ExaminerRole::Supervisor => Score::SUPERVISOR_COLUMN,
        };
    }
}
