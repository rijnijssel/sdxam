<?php

namespace App\Policies;

use App\Models\Context;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ContextPolicy
{
    use HandlesAuthorization;

    /** Determine whether the user can view any models. */
    public function viewAny(User $user): Response
    {
        return $this->allow();
    }

    /** Determine whether the user can create models. */
    public function create(User $user): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }

    /** Determine whether the user can update the model. */
    public function update(User $user, Context $context): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }
}
