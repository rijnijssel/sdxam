<?php

namespace App\Policies;

use App\Models\Examiner;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ExaminerPolicy
{
    use HandlesAuthorization;

    /** Determine whether the user can view any models. */
    public function viewAny(User $user): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }

    /** Determine whether the user can view the model. */
    public function view(User $user, Examiner $examiner): Response
    {
        return $user->is_admin || $user->is($examiner->user)
            ? $this->allow()
            : $this->deny();
    }

    /** Determine whether the user can create models. */
    public function create(User $user): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }

    /** Determine whether the user can update the model. */
    public function update(User $user, Examiner $examiner): Response
    {
        return $user->is_admin || $user->is($examiner->user)
            ? $this->allow()
            : $this->deny();
    }

    /** Determine whether the user can delete the model. */
    public function delete(User $user, Examiner $examiner): Response
    {
        return $user->is_admin || $user->is($examiner->user)
            ? $this->allow()
            : $this->deny();
    }
}
