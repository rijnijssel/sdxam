<?php

namespace App\Policies;

use App\Models\Student;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class StudentPolicy
{
    use HandlesAuthorization;

    /** Determine whether the user can view any models. */
    public function viewAny(User $user): Response
    {
        return $this->allow();
    }

    /** Determine whether the user can view the model. */
    public function view(User $user, Student $student): Response
    {
        return $this->allow();
    }

    /** Determine whether the user can create models. */
    public function create(User $user): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }

    /** Determine whether the user can update the model. */
    public function update(User $user, Student $student): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }

    /** Determine whether the user can delete the model. */
    public function delete(User $user, Student $student): Response
    {
        return $user->is_admin ? $this->allow() : $this->deny();
    }
}
