<?php

namespace App\Policies;

use App\Models\Exam;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ExamPolicy
{
    use HandlesAuthorization;

    /** Determine whether the user can view any models. */
    public function viewAny(User $user): Response
    {
        return $this->allow();
    }

    /** Determine whether the user can view the model. */
    public function view(User $user, Exam $exam): Response
    {
        return $this->allow();
    }
}
