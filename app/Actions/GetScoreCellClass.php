<?php

namespace App\Actions;

use App\Models\RubricTaskItem;
use App\Models\Score;
use Illuminate\Support\Collection;

class GetScoreCellClass
{
    /** @param  Collection<int, Score>  $scores */
    public function run(RubricTaskItem $item, Collection $scores): string
    {
        $client = $scores->contains(Score::CLIENT_COLUMN, $item->id);
        $supervisor = $scores->contains(Score::SUPERVISOR_COLUMN, $item->id);

        return match (true) {
            $client && $supervisor => 'both',
            $client => 'client',
            $supervisor => 'supervisor',
            default => '',
        };
    }
}
