<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Override;

class StudentNumberRule implements ValidationRule
{
    #[Override]
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (preg_match('/^00\d{6}$/', (string) $value) > 0) {
            return;
        }

        $fail(trans('Het :attribute moet het formaat hebben van een studentnummer (bijv. 00123456).'));
    }
}
