<?php

namespace App\Exceptions;

use Exception;

class InvalidScoreDataException extends Exception {}
