<?php

namespace App\Exceptions;

use Exception;

class NoExaminerException extends Exception {}
