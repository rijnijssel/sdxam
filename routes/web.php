<?php

use App\Http\Controllers\Account\ProfileController;
use App\Http\Controllers\Account\ProfilePasswordController;
use App\Http\Controllers\Auth\ExternalAuthenticationController;
use App\Http\Controllers\ChangelogController;
use App\Http\Controllers\ContextController;
use App\Http\Controllers\Exam\ExamController;
use App\Http\Controllers\Exam\ProcessController;
use App\Http\Controllers\Exam\TaskController;
use App\Http\Controllers\ExaminerController;
use App\Http\Controllers\Execution\Assessment\AssessmentIndexController;
use App\Http\Controllers\Execution\Assessment\AssessmentShowController;
use App\Http\Controllers\Execution\Assessment\AssessmentUpdateController;
use App\Http\Controllers\Execution\Csv\BookGeneratorController;
use App\Http\Controllers\Execution\Csv\ServerStudentController;
use App\Http\Controllers\Execution\CurrentExecutionController;
use App\Http\Controllers\Execution\ExecutionController;
use App\Http\Controllers\Execution\ExecutionStudentController;
use App\Http\Controllers\Execution\GroupController;
use App\Http\Controllers\Execution\Result\ResultIndexController;
use App\Http\Controllers\Execution\Result\ResultShowController;
use App\Http\Controllers\Execution\Result\ResultUpdateController;
use App\Http\Controllers\Execution\SharedResultsController;
use App\Http\Controllers\Execution\Task\TaskEditController;
use App\Http\Controllers\Execution\Task\TaskUpdateController;
use App\Http\Controllers\Export\BatchExportController;
use App\Http\Controllers\Export\BatchExportDownloadController;
use App\Http\Controllers\Export\ExportShowController;
use App\Http\Controllers\Export\ExportStoreController;
use App\Http\Controllers\Export\StudentExportController;
use App\Http\Controllers\ImpersonatedSessionController;
use App\Http\Controllers\RubricTaskGuidelineController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentStatusController;
use App\Http\Controllers\TerraformStateController;
use App\Http\Controllers\UserController;
use App\Livewire\Export\Index as ExecutionIndex;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;

Route::auth([
    'register' => false,
    'reset' => true,
    'confirm' => true,
    'verify' => false,
]);

Route::get('sso/{service}/redirect', [ExternalAuthenticationController::class, 'create'])
    ->name('auth.redirect');
Route::get('sso/{service}/callback', [ExternalAuthenticationController::class, 'store'])
    ->name('auth.callback');

// Apply auth middleware to all routes
Route::middleware(Authenticate::class)->group(function (): void {
    Route::singleton('profile', ProfileController::class)
        ->only(['show', 'update']);

    Route::put('profile/password', [ProfilePasswordController::class, 'update'])
        ->name('profile.update-password');

    Route::get('changelog', ChangelogController::class)->name('changelog');

    // User routes
    Route::resource('users', UserController::class);

    Route::post('impersonate/{user:email}', [ImpersonatedSessionController::class, 'store'])
        ->name('impersonated-session.store');
    Route::delete('impersonate', [ImpersonatedSessionController::class, 'destroy'])
        ->name('impersonated-session.destroy');

    // Controller for student CRUD
    Route::get('students/status', [StudentStatusController::class, 'edit'])->name('students.archive.edit');
    Route::put('students/status', [StudentStatusController::class, 'update'])->name('students.archive.update');
    Route::resource('students', StudentController::class)
        ->parameter('students', 'student:sid');

    // Controller for examiner CRUD
    Route::resource('examiners', ExaminerController::class)
        ->parameter('examiners', 'examiner:abbreviation');

    // Routes related to the execution of an exam
    Route::get('executions/current', CurrentExecutionController::class)
        ->name('executions.current');
    Route::resource('executions', ExecutionController::class)
        ->parameter('executions', 'execution:slug');

    Route::resource('executions.groups', GroupController::class)
        ->only('create', 'store', 'show', 'edit', 'update', 'destroy')
        ->parameter('executions', 'execution:slug')
        ->parameter('groups', 'group:slug');

    Route::get('executions/{execution:slug}/students/examiners', [ExecutionStudentController::class, 'edit'])
        ->name('executions.students.edit');
    Route::put('executions/{execution:slug}/students/examiners', [ExecutionStudentController::class, 'update'])
        ->name('executions.students.update');

    Route::get('executions/{execution:slug}/students/tasks', TaskEditController::class)
        ->name('executions.students.tasks.edit');
    Route::put('executions/{execution:slug}/students/tasks', TaskUpdateController::class)
        ->name('executions.students.tasks.update');

    Route::get('executions/{execution:slug}/assessment', AssessmentIndexController::class)
        ->name('score.index');
    Route::get('executions/{execution:slug}/assessment/{student}/{process}', AssessmentShowController::class)
        ->name('score.show');
    Route::put('executions/{execution:slug}/assessment/{student}/{process}', AssessmentUpdateController::class)
        ->name('score.update');

    Route::get('executions/{execution:slug}/results/share', [SharedResultsController::class, 'show'])
        ->name('shared-results.show');
    Route::post('executions/{execution:slug}/results/share', [SharedResultsController::class, 'store'])
        ->name('shared-results.store');

    Route::get('executions/{execution}/results', ResultIndexController::class)
        ->name('result.index');
    Route::get('executions/{execution}/results/{student}/{process}', ResultShowController::class)
        ->name('result.show');
    Route::put('executions/{execution}/results/{student}/{process}', ResultUpdateController::class)
        ->name('result.update');

    Route::get('executions/{execution:slug}/exports', ExecutionIndex::class)
        ->can('executions.export', 'execution')
        ->name('executions.export.index');
    Route::post('executions/{execution:slug}/exports', ExportStoreController::class)
        ->name('executions.export.store');
    Route::get('executions/{execution:slug}/export/{date}', ExportShowController::class)
        ->name('executions.export.show');

    Route::post('executions/{execution:slug}/batch-exports', [BatchExportController::class, 'store'])
        ->name('executions.batch-export.store');

    Route::get('executions/{execution:slug}/batch-exports/{batchExport:id}', [BatchExportController::class, 'show'])
        ->name('executions.batch-export.show');

    Route::get('executions/{execution:slug}/batch-exports/{batchExport:id}/download', [BatchExportDownloadController::class, 'index'])
        ->name('executions.batch-export.download.index');

    Route::get('executions/{execution:slug}/batch-exports/{batchExport:id}/download/{student}', [BatchExportDownloadController::class, 'show'])
        ->name('executions.batch-export.download.show');

    Route::get('terraform', [TerraformStateController::class, 'index'])
        ->name('terraform.index');

    Route::get('executions/{execution}/students.csv', StudentExportController::class);
    Route::get('executions/{execution}/books.csv', BookGeneratorController::class);
    Route::get('executions/{execution}/server.csv', ServerStudentController::class);

    // Routes related to exams
    Route::resource('exams', ExamController::class)
        ->only(['index', 'show']);
    Route::resource('exams.tasks', TaskController::class)
        ->scoped(['exam', 'task'])
        ->only(['show']);
    Route::resource('exams.tasks.processes', ProcessController::class)
        ->scoped(['exam', 'task', 'process'])
        ->only(['show']);

    Route::resource('guidelines', RubricTaskGuidelineController::class)
        ->parameter('guidelines', 'rubric-task')
        ->only(['edit', 'update']);

    Route::resource('contexts', ContextController::class)
        ->except(['show', 'destroy']);

    Route::redirect('/', '/login');
});
