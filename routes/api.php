<?php

use App\Http\Controllers\Api\ExecutionController;
use App\Http\Controllers\Api\ExecutionStudentController;
use App\Http\Controllers\Api\StudentController;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::name('api.')
    ->middleware(Authenticate::using('sanctum'))
    ->group(function (): void {
        Route::get('/user', fn (Request $request) => $request->user());

        Route::apiResource('executions', ExecutionController::class)
            ->only(['index', 'show']);

        Route::apiResource('executions.students', ExecutionStudentController::class)
            ->only(['index']);

        Route::apiResource('students', StudentController::class)
            ->only(['index']);
    });
