import { listener } from "@/execution-linker/select";
import { clear } from "@/execution-linker/clear";

const dropdownMap = new Map<string, HTMLSelectElement[]>();

const dropdowns = document.querySelectorAll<HTMLSelectElement>("select[data-student-linker]");
dropdowns.forEach((dropdown) => {
    const { student, role } = dropdown.dataset;
    const key = `${student}_${role}`;

    // if the student examiner type doesn't exist, initialize it as an empty array
    if (!dropdownMap.has(key)) {
        dropdownMap.set(key, []);
    }

    // add the dropdown to the dropdowns for this student examiner type
    const elements = dropdownMap.get(key)?.concat(dropdown) ?? [];
    dropdownMap.set(key, elements);
    // add event listener to the dropdown
    dropdown.addEventListener("change", (event) => {
        listener(event.target as HTMLSelectElement, key, dropdownMap);
    });
});

const buttons = document.querySelectorAll<HTMLButtonElement>("[data-clear]");
buttons.forEach((button) => {
    button.addEventListener("click", (event: MouseEvent) => {
        if (event.target === null) {
            return;
        }

        const student = (event.target as HTMLButtonElement).dataset.clear ?? "";
        return clear(student);
    });
});
