/**
 * Clear all dropdowns for a student.
 */
export function clear(student: string) {
    const dropdowns = document.querySelectorAll<HTMLSelectElement>(
        `select[data-student-linker][data-student='${student}']`,
    );
    dropdowns.forEach((dropdown) => (dropdown.value = ""));
}
