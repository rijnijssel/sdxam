/**
 * Function that sets all other inputs to the same value.
 */
export function listener(target: HTMLSelectElement, key: string, map: Map<string, HTMLSelectElement[]>) {
    console.debug("target data:", { target, key });
    if (!map.has(key)) {
        return;
    }

    map.get(key)?.forEach((input) => {
        if (input.disabled || input.value) {
            return;
        }
        input.value = target.value;
    });
}
