import hotkeys from "hotkeys-js";

import { Modal } from "bootstrap";

const modals = {
    info: new Modal("#modal_shortcuts"),
};

hotkeys("shift+/", (event) => {
    event.preventDefault();
    modals.info.show();
});

hotkeys("e", (event) => {
    event.preventDefault();
    location.replace("/executions");
});

hotkeys("c", (event) => {
    event.preventDefault();
    location.replace("/executions/current");
});

hotkeys("x", (event) => {
    event.preventDefault();
    location.replace("/exams");
});

hotkeys("s", (event) => {
    event.preventDefault();
    location.replace("/students");
});
