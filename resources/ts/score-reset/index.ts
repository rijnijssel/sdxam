const buttons = document.querySelectorAll<HTMLButtonElement>("[data-sdxam-clear-score]");

const radios = document.querySelectorAll<HTMLInputElement>('[data-sdxam-score-form] input[type="radio"]');

/**
 * Remove the checked attribute from a HTMLInputElement.
 */
const uncheck = (input: HTMLInputElement) => {
    input.removeAttribute("checked");
    input.checked = false;
};

buttons.forEach((button) => {
    button.addEventListener("click", () => radios.forEach(uncheck));
});
