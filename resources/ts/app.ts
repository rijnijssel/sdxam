import "../css/app.css";

import * as bootstrap from "bootstrap";
// Set up JS for Bootstrap (Tab, Toast, Tooltip)
import "@/bootstrap";
// Add global shortcuts to navigate the app with keyboard
import "@/shortcuts";
// Automatically fill all tasks with same examiner
import "@/execution-linker";
// Reset score table with button
import "@/score-reset";
// Theme switcher
import "@/theme-switcher";

window.bootstrap = bootstrap;
