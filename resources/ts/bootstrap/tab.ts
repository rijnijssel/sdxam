import { Tab } from "bootstrap";

const hash = location.hash;

const tabs = [].slice.call(document.querySelectorAll<HTMLAnchorElement>("[role=tablist] a"));

tabs.forEach((tab: HTMLAnchorElement) => {
    const href = tab.attributes.getNamedItem("href")?.value ?? "";
    const trigger = new Tab(tab);

    if (hash === href) {
        trigger.show();
    }

    tab.addEventListener("click", () => {
        trigger.show();
        location.hash = href;
    });
});
