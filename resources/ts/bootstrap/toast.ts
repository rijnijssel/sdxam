import { Toast } from "bootstrap";

const mapToast = (toast: Element) =>
    new Toast(toast, {
        delay: 3000,
    });

const elements = [].slice.call(document.querySelectorAll(".toast"));
const toasts = elements.map(mapToast);
toasts.forEach((value) => value.show());
