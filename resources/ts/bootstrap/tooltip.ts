import { Tooltip } from "bootstrap";

const elements: Element[] = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
const tooltips = elements.map(
    (tooltip) =>
        new Tooltip(tooltip, {
            placement: "bottom",
            boundary: window.document.body,
        }),
);

tooltips.forEach((tooltip: Tooltip) => tooltip.enable());
