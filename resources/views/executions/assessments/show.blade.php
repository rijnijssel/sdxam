<x-layouts.app>
    <div class="container-xl">
        <x-breadcrumbs />

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col me-auto">
                        <h2>{{ $student->getFullName() }}</h2>
                        <p class="h4">{{ $process->getLabel() }}</p>
                        <table class="table table-sm w-auto">
                            <tr>
                                <th class="bg-client">@lang("Opdrachtgever")</th>
                                <td class="bg-client">{{ $client?->getLabel() }}</td>
                            </tr>
                            <tr>
                                <th class="bg-supervisor">@lang("Leidinggevende")</th>
                                <td class="bg-supervisor">{{ $supervisor?->getLabel() }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-auto">
                        <div class="btn-group">
                            <button
                                class="btn btn-sm btn-outline-secondary dropdown-toggle"
                                type="button"
                                id="tasks_dropdown"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                            >
                                @lang("Werkprocessen")
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="tasks_dropdown">
                                @foreach ($executionStudent->processes as $p)
                                    <li>
                                        <a
                                            @class(["dropdown-item", "active" => $process->is($p)])
                                            href="{{ route("score.show", [$execution, $student, $p]) }}"
                                        >
                                            {{ $p->getLabel() }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <a
                            class="btn btn-sm btn-outline-primary"
                            data-bs-toggle="tooltip"
                            href="{{ route("result.show", [$execution, $student, $process]) }}"
                            title="Ga naar het resultaat van dit werkproces"
                        >
                            @lang("Resultaat werkproces")
                        </a>
                    </div>
                </div>
                <form
                    data-sdxam-score-form
                    action="{{ route("score.update", [$execution, $student, $process]) }}"
                    method="post"
                >
                    @csrf
                    @method("put")

                    <div class="row gap-3">
                        @foreach ($process->rubrics as $rubric)
                            <div class="col-12">
                                <h5>{{ data_get($rubric, "title") }}</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm table-hover table-score-show">
                                        <thead class="table-primary">
                                            <tr>
                                                <th scope="col">@lang("Taak")</th>
                                                @for ($i = 0; $i < $rubric->max_items; $i++)
                                                    <th scope="col">{{ $i }}</th>
                                                @endfor
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($rubric->tasks as $task)
                                                <tr>
                                                    <th scope="row" rowspan="3" class="table-primary">
                                                        {{ $task->name }}
                                                    </th>
                                                    <th scope="row" colspan="{{ $rubric->max_items }}">
                                                        <div class="d-flex flex-row">
                                                            <div class="me-auto">
                                                                {{ $task->criteria }}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tr class="h-100">
                                                    @foreach ($task->items->pad($rubric->max_items, null) as $item)
                                                        <td class="score-cell lh-sm w-25">
                                                            @if ($item)
                                                                <label class="d-flex flex-row gap-1">
                                                                    <input
                                                                        hidden
                                                                        type="radio"
                                                                        name="rows[{{ $task->id }}][score]"
                                                                        value="{{ $item->id }}"
                                                                        @checked($selected->contains($item->id))
                                                                    />

                                                                    <div
                                                                        class="w-100"
                                                                        @if ($item->guideline)
                                                                            data-bs-toggle="tooltip"
                                                                            data-bs-html="true"
                                                                            title="{{ nl2br($item->guideline) }}"
                                                                        @endif
                                                                    >
                                                                        {!! $item->text !!}
                                                                    </div>

                                                                    @if ($item->guideline)
                                                                        <x-icon
                                                                            class="bi bi-info-circle"
                                                                            data-bs-toggle="tooltip"
                                                                            :title="__('Onderlegger beschikbaar')"
                                                                        />
                                                                    @endif
                                                                </label>
                                                            @endif
                                                        </td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td colspan="{{ $rubric->max_items }}">
                                                        <div>
                                                            <textarea
                                                                id="annotation_{{ $task->id }}"
                                                                name="rows[{{ $task->id }}][annotation]"
                                                                rows="1"
                                                                class="form-control form-control-sm"
                                                            >
{{ $annotations->get($task->id) }}</textarea
                                                            >
                                                            <label for="annotation_{{ $task->id }}" hidden>
                                                                @lang("Notitie voor :name", ["name" => $task->criteria])
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success">Opslaan</button>
                                    </div>
                                    <div class="col-auto">
                                        <button type="reset" class="btn btn-outline-secondary" data-sdxam-clear-score>
                                            Leeg maken
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layouts.app>
