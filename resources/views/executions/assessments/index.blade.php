<x-layouts.app>
    <div class="container-xl">
        <x-breadcrumbs />

        <x-header title="{{ __('Beoordelingen') }}">
            <a
                class="btn btn-sm btn-outline-primary"
                data-bs-toggle="tooltip"
                href="{{ route('result.index', [$execution]) }}"
                title="Ga naar de uiteindelijke resultaten"
            >
                @lang('Resultaten')
            </a>
        </x-header>
        <div class="row">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-score-index">
                        <thead>
                            <tr class="table-primary">
                                <th rowspan="2" colspan="1">@lang('Student')</th>
                                @foreach ($tasks as $task)
                                    <th class="text-center" scope="col" colspan="{{ $task->processes_count }}">
                                        <strong>{{ data_get($task, 'name') }}</strong>
                                    </th>
                                @endforeach
                            </tr>
                            <tr class="table-primary">
                                @foreach ($tasks as $task)
                                    @foreach ($task->processes as $process)
                                        <th class="text-center" scope="col">
                                            <div
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"
                                                title="{{ data_get($process, 'description') }}"
                                            >
                                                {{ data_get($process, 'name') }}
                                            </div>
                                        </th>
                                    @endforeach
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($students as $es)
                                <tr>
                                    <th scope="row" class="table-shrink-col">
                                        <a href="{{ route('students.show', $es->student) }}">
                                            {{ $es->student->getFullName() }}
                                        </a>
                                    </th>
                                    @foreach ($tasks as $task)
                                        @foreach ($task->processes as $process)
                                            @if ($es->tasks->contains($task))
                                                @if ($examiner)
                                                    <td
                                                        @class([
                                                            'text-center',
                                                            'fw-bold' => $es->isProcessFilled($task, $process, $es->getOtherExaminer($task, $examiner)),
                                                            'bg-ready' => $es->isProcessFilled($task, $process, $examiner),
                                                        ])
                                                    >
                                                        @if ($es->isTaskExaminer($task, $examiner))
                                                            <a
                                                                href="{{ route('score.show', [$execution, $es->student, $process]) }}"
                                                            >
                                                                {{ $es->getProcessProgress($task, $process, $examiner) }}
                                                            </a>
                                                        @else
                                                            {{ $process->name }}
                                                        @endif
                                                    </td>
                                                @else
                                                    <td class="text-center">{{ $process->name }}</td>
                                                @endif
                                            @else
                                                <td></td>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
