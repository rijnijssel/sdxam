<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-card>
                    <x-header title="{{ __('Uitvoering toevoegen') }}"></x-header>

                    <form action="{{ route('executions.store') }}" method="post">
                        @csrf

                        <x-input.text title="{{ __('Naam') }}" name="name" required autofocus>
                            <div class="form-text">
                                @lang('Naam waaronder deze uitvoering bekend staat.')
                            </div>
                        </x-input.text>

                        <x-input.date title="Start datum" name="start_date">
                            <div class="form-text">
                                @lang('Start datum van deze uitvoering.')
                            </div>
                        </x-input.date>

                        <div class="mb-3">
                            <label for="exam">@lang('Examen')</label>
                            <select
                                id="exam"
                                name="exam"
                                @class(['form-select', 'is-invalid' => $errors->has('exam')])
                            >
                                @foreach ($exams as $exam)
                                    <option value="{{ $exam->slug }}" @selected(old('exam') == $exam->slug)>
                                        {{ $exam->name }}
                                    </option>
                                @endforeach
                            </select>

                            <div class="form-text">
                                @lang('CREBO waaronder dit examen afgenomen gaat worden.')
                            </div>

                            <x-input.error name="exam"></x-input.error>
                        </div>

                        <x-form.create-actions cancel="{{ route('executions.index') }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
