<x-layouts.app>
    <div class="container-xl">
        <x-breadcrumbs />

        <x-header :title="__('Resultaten')">
            <x-execution-share-dropdown :execution="$execution" />

            <a
                class="d-block btn btn-sm btn-outline-primary"
                data-bs-toggle="tooltip"
                href="{{ route('score.index', [$execution]) }}"
                title="Ga naar de inviduele beoordelingen"
            >
                @lang('Beoordelingen')
            </a>
        </x-header>
        <div class="row">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-score-index">
                        <thead>
                            <tr class="table-danger">
                                <th rowspan="2" colspan="1">@lang('Student')</th>
                                @foreach ($tasks as $task1)
                                    <th class="text-center" scope="col" colspan="{{ $task1->processes_count }}">
                                        <strong>{{ data_get($task1, 'name') }}</strong>
                                    </th>
                                @endforeach
                            </tr>
                            <tr class="table-danger">
                                @foreach ($tasks as $task2)
                                    @foreach ($task2->processes as $process)
                                        <th class="text-center" scope="col">
                                            <div
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"
                                                title="{{ data_get($process, 'description') }}"
                                            >
                                                {{ data_get($process, 'name') }}
                                            </div>
                                        </th>
                                    @endforeach
                                @endforeach
                            </tr>
                        </thead>
                        <tbody class="table-group-divider">
                            @foreach ($students as $es)
                                <tr>
                                    <th scope="row" class="table-shrink-col">
                                        <a href="{{ route('students.show', $es->student) }}">
                                            {{ $es->student->getFullName() }}
                                        </a>
                                    </th>
                                    @foreach ($tasks as $task)
                                        @foreach ($task->processes as $process)
                                            <td
                                                @class([
                                                    'text-center',
                                                    'bg-failed' => $execution->isScoreFailed($es, $process),
                                                    'bg-ready' => $execution->isScorePassed($es, $process),
                                                ])
                                            >
                                                @if ($es->tasks->contains($task))
                                                    @if ($es->isTaskExaminer($task, $examiner))
                                                        <a
                                                            href="{{ route('result.show', [$execution, $es->student, $process]) }}"
                                                        >
                                                            {{ $execution->getProcessScoreText($es, $process) }}
                                                        </a>
                                                    @else
                                                        {{ $execution->getProcessScoreText($es, $process) }}
                                                    @endif
                                                @endif
                                            </td>
                                        @endforeach
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
