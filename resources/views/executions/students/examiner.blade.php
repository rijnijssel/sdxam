<x-layouts.app>
    <div class="container-fluid">
        <x-breadcrumbs />

        <x-header :title="__('Examinatoren & context koppelen')">
            <a href="{{ route('executions.students.tasks.edit', $execution) }}">
                @lang('Kerntaken koppelen')
            </a>
        </x-header>

        <div class="row">
            <div class="col">
                <form action="{{ route('executions.students.update', $execution) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="table-card table-responsive mb-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th rowspan="2">@lang('Student')</th>
                                    @foreach ($tasks as $task)
                                        <th colspan="2">{{ $task->name }}</th>
                                    @endforeach

                                    <th></th>
                                </tr>
                                <tr>
                                    @foreach ($tasks as $task)
                                        <td>
                                            <div class="d-flex align-items-center gap-2">
                                                <span
                                                    style="height: 1rem; width: 1rem"
                                                    class="d-inline-block rounded-circle bg-client"
                                                ></span>
                                                <span class="small">@lang('Opdrachtgever')</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center gap-2">
                                                <span
                                                    style="height: 1rem; width: 1rem"
                                                    class="d-inline-block rounded-circle bg-supervisor"
                                                ></span>
                                                <span class="small">@lang('Leidinggevende')</span>
                                            </div>
                                        </td>
                                    @endforeach

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($students as $es)
                                    <tr>
                                        <td class="table-shrink-col text-nowrap">
                                            <a class="link-primary" href="{{ route('students.show', $es->student) }}">
                                                {{ $es->student->getFullName() }}
                                            </a>
                                        </td>
                                        @foreach ($tasks as $task)
                                            <td colspan="2" class="w-25 p-1">
                                                @if ($es->tasks->contains('slug', $task->slug))
                                                    <div class="mb-1">
                                                        <select
                                                            class="form-select form-select-sm"
                                                            name="students[{{ $es->student->sid }}][{{ $task->slug }}][context]"
                                                            aria-label="Context voor {{ $es->student->getFullName() }}"
                                                            data-student-linker
                                                            data-student="{{ $es->student->sid }}"
                                                            data-role="context"
                                                        >
                                                            <option value="">@lang('Context')</option>
                                                            @foreach ($contexts as $context)
                                                                <option
                                                                    value="{{ $context->slug }}"
                                                                    @selected($context->id === $es->getTask($task)?->context_id)
                                                                >
                                                                    {{ $context->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="d-flex flex-row gap-1">
                                                        <x-examiner-dropdown
                                                            class="bg-client"
                                                            role="client"
                                                            :executionStudent="$es"
                                                            :task="$task"
                                                            :examiners="$examiners"
                                                        />

                                                        <x-examiner-dropdown
                                                            class="bg-supervisor"
                                                            role="supervisor"
                                                            :executionStudent="$es"
                                                            :task="$task"
                                                            :examiners="$examiners"
                                                        />
                                                    </div>
                                                @endif
                                            </td>
                                        @endforeach

                                        <td class="table-shrink-col">
                                            <button
                                                class="btn btn-sm btn-outline-danger"
                                                type="button"
                                                role="button"
                                                data-clear="{{ $es->student->sid }}"
                                            >
                                                <x-icon class="pe-none bi bi-x-circle" />
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <x-form.edit-actions :delete="false" cancel="{{ route('executions.show', $execution) }}" />
                </form>
            </div>
        </div>
    </div>
</x-layouts.app>
