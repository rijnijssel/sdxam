<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-breadcrumbs />

                <x-header :title="__('Kerntaken koppelen')">
                    <a href="{{ route('executions.students.edit', $execution) }}">
                        @lang('Examinatoren & context koppelen')
                    </a>
                </x-header>

                <form action="{{ route('executions.students.tasks.update', $execution) }}" method="post">
                    @csrf
                    @method('put')

                    <div class="table-padded table-responsive mb-3">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="table-col-shrink" rowspan="2">SID</th>
                                    <th rowspan="2">Student</th>
                                    <th colspan="{{ $tasks->count() }}">Kerntaken</th>
                                </tr>
                                <tr>
                                    @foreach ($tasks as $task)
                                        <th class="table-shrink-col">{{ $task->name }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($students as $student)
                                    <tr class="align-middle">
                                        <td class="table-shrink-col">
                                            <a href="{{ route('students.edit', $student) }}">
                                                {{ $student->sid }}
                                            </a>
                                            <input type="hidden" name="students[{{ $student->sid }}][]" value="" />
                                        </td>
                                        <td>
                                            {{ $student->getFullName() }}
                                        </td>
                                        @foreach ($tasks as $task)
                                            <td class="table-shrink-col text-center">
                                                <input
                                                    class="form-check-input px-4 py-2"
                                                    type="checkbox"
                                                    role="switch"
                                                    id="{{ sprintf('%s_%s', $student->sid, $task->slug) }}"
                                                    name="students[{{ $student->sid }}][]"
                                                    value="{{ $task->slug }}"
                                                    aria-label="{{ $task->name }}"
                                                    @checked($selected->get($student->sid, collect())->contains($task->slug))
                                                />
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <x-form.edit-actions :delete="false" cancel="{{ route('executions.show', $execution) }}" />
                </form>
            </div>
        </div>
    </div>
</x-layouts.app>
