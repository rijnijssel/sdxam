<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-header title="{{ __('Groep bewerken') }}"></x-header>

                <x-card>
                    <form action="{{ route('executions.groups.update', [$execution, $group]) }}" method="post">
                        @csrf
                        @method('put')

                        <x-input.text :title="__('Naam')" name="name" value="{{ $group->name }}" required>
                            <small class="form-text text-muted">
                                @lang('Naam waaronder deze groep bekend staat.')
                            </small>
                        </x-input.text>

                        <div class="mb-3">
                            <label for="examiners">@lang('Examinatoren')</label>
                            <select
                                id="examiners"
                                @class(['form-select', 'is-invalid' => $errors->hasAny(['examiners', 'examiners.*'])])
                                name="examiners[]"
                                size="{{ $examiners->count() ?? 5 }}"
                                multiple
                            >
                                @foreach ($examiners as $examiner)
                                    <option
                                        value="{{ $examiner->id }}"
                                        @selected($examiner_ids->contains($examiner->id))
                                    >
                                        {{ $examiner->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="students">@lang('Studenten')</label>
                            <select
                                id="students"
                                @class(['form-select', 'is-invalid' => $errors->hasAny(['students', 'students.*'])])
                                name="students[]"
                                size="{{ $students->count() ?? 20 }}"
                                multiple
                            >
                                @foreach ($students as $es)
                                    <option
                                        value="{{ $es->student->sid }}"
                                        @selected($student_ids->contains($es->student->sid))
                                    >
                                        {{ $es->student->getFullName() }}
                                    </option>
                                @endforeach
                            </select>

                            <x-input.error name="students"></x-input.error>
                            <x-input.error name="students.*"></x-input.error>
                        </div>

                        <x-form.edit-actions cancel="{{ route('executions.groups.show', [$execution, $group]) }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>

    <x-form.delete-modal route="{{ route('executions.groups.destroy', [$execution, $group]) }}">
        <x-slot name="title">
            @lang('Groep verwijderen')
        </x-slot>
        Weet je zeker dat je "{{ $group->name }}" wilt verwijderen?
    </x-form.delete-modal>
</x-layouts.app>
