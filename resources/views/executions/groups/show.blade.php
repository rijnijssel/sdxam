<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-header title="{{ __('Uitvoering - Groep tonen') }}">
                    @can('update', $execution)
                        <a
                            class="btn btn-sm btn-primary"
                            href="{{ route('executions.groups.edit', [$execution, $group]) }}"
                        >
                            @lang('Bewerken')
                        </a>
                    @endcan
                </x-header>

                <div class="d-flex flex-column gap-3">
                    <x-card>
                        <x-form.head :title="__('Algemene informatie')"></x-form.head>
                        <x-input.text title="{{ __('Naam') }}" name="name" value="{{ $group->name }}" readonly>
                            <small class="form-text text-muted">
                                @lang('Naam waaronder deze groep bekend staat.')
                            </small>
                        </x-input.text>
                    </x-card>
                    <x-card>
                        <x-form.head :title="__('Gekoppelde examinatoren')"></x-form.head>
                        <div class="table-responsive">
                            <table class="table table-sm mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">{{ __('Naam') }}</th>
                                        <th scope="col">{{ __('E-mail') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($examiners as $examiner)
                                        <tr>
                                            <td>{{ $examiner->name }}</td>
                                            <td>{{ $examiner->getEmail() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </x-card>
                    <x-card>
                        <x-form.head :title="__('Gekoppelde studenten')"></x-form.head>
                        <div class="table-responsive">
                            <table class="table table-sm mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col" class="table-shrink-col">@lang('SID')</th>
                                        <th scope="col">@lang('Voornaam')</th>
                                        <th scope="col">@lang('Tussenvoegsel')</th>
                                        <th scope="col">@lang('Achternaam')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($students as $student)
                                        <tr>
                                            <th scope="row">
                                                <a href="{{ route('students.show', $student) }}">
                                                    {{ $student->sid }}
                                                </a>
                                            </th>
                                            <td>{{ $student->first_name }}</td>
                                            <td>{{ $student->middle_name }}</td>
                                            <td>{{ $student->last_name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </x-card>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
