<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-header title="{{ __('Groep toevoegen') }}"></x-header>

                <x-card>
                    <form action="{{ route('executions.groups.store', $execution) }}" method="post">
                        @csrf

                        <x-input.text title="{{ __('Naam') }}" name="name" required>
                            <small class="form-text text-muted">
                                @lang('Naam waaronder deze groep bekend staat.')
                            </small>
                        </x-input.text>

                        <div class="mb-3">
                            <label for="examiners">@lang('Examinatoren')</label>
                            <select
                                id="examiners"
                                @class(['form-select', 'is-invalid' => $errors->hasAny(['examiners', 'examiners.*'])])
                                name="examiners[]"
                                size="{{ $examiners->count() ?? 5 }}"
                                multiple
                            >
                                @foreach ($examiners as $examiner)
                                    <option value="{{ $examiner->id }}">
                                        {{ $examiner->name }}
                                    </option>
                                @endforeach
                            </select>

                            <x-input.error name="examiners"></x-input.error>
                            <x-input.error name="examiners.*"></x-input.error>
                        </div>

                        <div class="mb-3">
                            <label for="students">@lang('Studenten')</label>
                            <select
                                id="students"
                                @class(['form-select', 'is-invalid' => $errors->hasAny(['students', 'students.*'])])
                                name="students[]"
                                size="{{ $students->count() ?? 20 }}"
                                multiple
                            >
                                @foreach ($students as $es)
                                    <option
                                        value="{{ $es->student->sid }}"
                                        @selected(collect(old('students', []))->contains($es->student->sid))
                                    >
                                        {{ $es->student->getFullName() }}
                                    </option>
                                @endforeach
                            </select>

                            <x-input.error name="students"></x-input.error>
                            <x-input.error name="students.*"></x-input.error>
                        </div>

                        <x-form.create-actions cancel="{{ route('executions.show', $execution) }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
