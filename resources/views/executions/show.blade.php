<x-layouts.app>
    <div class="container-xl">
        <x-breadcrumbs />

        <x-header :title="__('Uitvoering')">
            <x-execution-share-dropdown :execution="$execution" />

            @can('executions.export', $execution)
                <a
                    class="d-block btn btn-sm btn-outline-primary"
                    data-bs-toggle="tooltip"
                    href="{{ route('executions.export.index', $execution) }}"
                    title="Overzicht van exports"
                >
                    <x-icon class="bi-file-pdf" />
                    &nbsp;
                    @lang('Exports')
                </a>
            @endcan

            @can('executions.score', $execution)
                <a
                    class="d-block btn btn-sm btn-outline-primary"
                    data-bs-toggle="tooltip"
                    href="{{ route('result.index', $execution) }}"
                    title="Definitieve examenresultaten"
                >
                    <x-icon class="bi-table" />
                    &nbsp;
                    @lang('Resultaten')
                </a>
                <a
                    class="d-block btn btn-sm btn-primary"
                    data-bs-toggle="tooltip"
                    href="{{ route('score.index', $execution) }}"
                    title="Individuele beoordeling van de examinator"
                >
                    <x-icon class="bi-pencil" />
                    &nbsp;
                    @lang('Beoordelen')
                </a>
            @endcan
        </x-header>

        {{-- Horizontal layouts --}}
        <div class="row g-3">
            <div class="col-md-5">
                <div class="row">
                    <div class="col d-flex flex-column gap-3">
                        <x-card>
                            <x-form.head title="Algemene informatie">
                                @can('update', $execution)
                                    <a
                                        href="{{ route('executions.edit', $execution) }}"
                                        data-toggle="tooltip"
                                        title="Bewerken van uitvoering"
                                    >
                                        @lang('Bewerken')
                                    </a>
                                @endcan
                            </x-form.head>

                            <div class="d-flex flex-column gap-3">
                                <div>
                                    <x-label for="name">@lang('Naam')</x-label>
                                    <x-input id="name" name="name" :value="$execution->name" readonly />
                                    <small class="form-text text-muted">
                                        @lang('Naam waaronder deze uitvoering bekend staat.')
                                    </small>
                                </div>

                                <div>
                                    <x-label for="start_date">@lang('Start datum')</x-label>
                                    <x-input
                                        id="start_date"
                                        name="start_date"
                                        type="date"
                                        :value="$execution->formatStartDate()"
                                        readonly
                                    />
                                    <small class="form-text text-muted">
                                        @lang('Start datum van het examen.')
                                    </small>
                                </div>

                                <div>
                                    <x-label for="exam">@lang('Examen')</x-label>
                                    <x-input
                                        id="exam"
                                        name="exam"
                                        :value="data_get($execution, 'exam.name', __('Geen'))"
                                        readonly
                                    />
                                    <small class="form-text text-muted">
                                        @lang('Examenversie die gebruikt wordt bij deze uitvoering.')
                                    </small>
                                </div>
                            </div>
                        </x-card>
                        <x-card>
                            <x-form.head title="Groepen">
                                @can('create', \App\Models\Group::class)
                                    <a
                                        href="{{ route('executions.groups.create', $execution) }}"
                                        data-toggle="tooltip"
                                        title="Groep toevoegen aan uitvoering"
                                    >
                                        @lang('Toevoegen')
                                    </a>
                                @endcan
                            </x-form.head>

                            <div class="table-responsive">
                                <table class="table table-sm mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">Naam</th>
                                            <th scope="col">Examinatoren</th>
                                            <th scope="col">Studenten</th>
                                            @can('groups.has-actions')
                                                <th scope="col">Acties</th>
                                            @endcan
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($groups as $group)
                                            <tr>
                                                <td>
                                                    <a
                                                        href="{{ route('executions.groups.show', [$execution, $group]) }}"
                                                    >
                                                        {{ data_get($group, 'name') }}
                                                    </a>
                                                </td>
                                                <td>
                                                    @if ($group->examiners->isEmpty())
                                                        Geen
                                                    @else
                                                        <ul class="list-unstyled mb-0">
                                                            @foreach ($group->examiners as $examiner)
                                                                <li>{{ $examiner->name }}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </td>
                                                <td>{{ data_get($group, 'students_count') }}</td>
                                                <td class="flex-nowrap">
                                                    @can('update', $group)
                                                        <a
                                                            href="{{ route('executions.groups.edit', [$execution, $group]) }}"
                                                        >
                                                            @lang('Bewerken')
                                                        </a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </x-card>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col">
                        <x-card>
                            <x-form.head title="Studenten">
                                @can('update', $execution)
                                    <div class="dropdown">
                                        <button
                                            class="btn btn-outline-primary btn-sm dropdown-toggle"
                                            type="button"
                                            id="linker"
                                            data-bs-toggle="dropdown"
                                            aria-expanded="false"
                                        >
                                            <x-icon class="bi-link-45deg" />
                                            &nbsp;
                                            @lang('Koppelen')
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="linker">
                                            <li>
                                                <a
                                                    class="dropdown-item"
                                                    href="{{ route('executions.students.tasks.edit', $execution) }}"
                                                    data-toggle="tooltip"
                                                    title="Koppel kerntaken aan studenten"
                                                >
                                                    @lang('Kerntaken')
                                                </a>
                                            </li>
                                            <li>
                                                <a
                                                    class="dropdown-item"
                                                    href="{{ route('executions.students.edit', $execution) }}"
                                                    data-toggle="tooltip"
                                                    title="Koppel examinatoren aan studenten"
                                                >
                                                    @lang('Examinatoren & Context')
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endcan
                            </x-form.head>

                            <div class="table-responsive">
                                <table class="table table-sm mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col">@lang('Voornaam')</th>
                                            <th scope="col">@lang('Tussenvoegsel')</th>
                                            <th scope="col">@lang('Achternaam')</th>
                                            <th scope="col">@lang('Status')</th>
                                            <th scope="col">@lang('Kerntaken')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($students as $student)
                                            <tr @class(['text-muted' => $student->student->isGraduated()])>
                                                <td>{{ data_get($student, 'student.first_name') }}</td>
                                                <td>{{ data_get($student, 'student.middle_name') }}</td>
                                                <td>{{ data_get($student, 'student.last_name') }}</td>
                                                <td>
                                                    @if ($student->student->isGraduated())
                                                        <span class="badge rounded-pill bg-success">
                                                            @lang('Diploma')
                                                        </span>
                                                    @else
                                                        <span class="badge rounded-pill bg-secondary">
                                                            @lang('Bezig')
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>{{ data_get($student, 'tasks')->pluck('name')->join(', ') }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </x-card>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
