<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-card>
                    <x-header title="{{ __('Uitvoering bewerken') }}"></x-header>

                    <form action="{{ route('executions.update', $execution) }}" method="post">
                        @csrf
                        @method('put')

                        <x-input.text
                            title="{{ __('Naam') }}"
                            name="name"
                            value="{{ $execution->name }}"
                            required
                            autofocus
                        >
                            <small class="form-text text-muted">
                                @lang('Naam waaronder deze uitvoering bekend staat.')
                            </small>
                        </x-input.text>

                        <x-input.date
                            title="Start datum"
                            name="start_date"
                            value="{{ $execution->formatStartDate() }}"
                        />

                        <div class="mb-3">
                            <label class="form-label" for="exam">@lang('Examen')</label>
                            <select
                                id="exam"
                                name="exam"
                                @class(['form-select', 'is-invalid' => $errors->has('exam')])
                            >
                                @foreach ($exams as $exam)
                                    <option
                                        value="{{ $exam->slug }}"
                                        @selected($exam->is($execution->exam) || old('exam') == $exam->slug)
                                    >
                                        {{ $exam->name }}
                                    </option>
                                @endforeach
                            </select>

                            <x-input.error name="exam"></x-input.error>
                        </div>

                        <x-form.edit-actions cancel="{{ route('executions.show', $execution) }}"></x-form.edit-actions>
                    </form>
                </x-card>
            </div>
        </div>
    </div>

    <x-form.delete-modal route="{{ route('executions.destroy', $execution) }}">
        <x-slot name="title">@lang('Uitvoering verwijderen')</x-slot>
        Weet je zeker dat je de uitvoering "{{ $execution->name }}" wilt verwijderen?
    </x-form.delete-modal>
</x-layouts.app>
