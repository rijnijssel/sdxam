<x-layouts.app>
    <div class="container-lg">
        <x-breadcrumbs />

        <x-header title="{{ __('views.executions') }}">
            @can('create', \App\Models\Execution::class)
                <a
                    class="btn btn-sm btn-primary"
                    href="{{ route('executions.create') }}"
                    data-toggle="tooltip"
                    title="Toevoegen van uitvoering"
                >
                    @lang('Toevoegen')
                </a>
            @endcan
        </x-header>

        <div class="row justify-content-center">
            <div class="col">
                @if ($latest)
                    <div class="row mb-3">
                        <div class="col">
                            <div class="card shadow-sm">
                                <h5 class="card-header">
                                    {{ $latest->getLabel() }}
                                </h5>
                                <div class="card-body">
                                    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 gy-3 gy-md-0">
                                        <div class="col">
                                            <div class="table-responsive">
                                                <table class="table table-sm mb-0">
                                                    <thead>
                                                        <th colspan="2">@lang('Informatie')</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th>Start datum</th>
                                                            <td>{{ $latest->localizedStartDate() }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Examen</th>
                                                            <td>
                                                                <a href="{{ route('exams.show', $latest->exam) }}">
                                                                    {{ $latest->exam->name }}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Studenten</th>
                                                            <td>{{ $latest->students_count }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Groepen</th>
                                                            <td>{{ $latest->getGroupNames() }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="table-responsive">
                                                <table class="table table-sm mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">
                                                                @lang('Groep')
                                                            </th>
                                                            <th scope="col">
                                                                @lang('Examinatoren')
                                                            </th>
                                                            <th scope="col">
                                                                @lang('Studenten')
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($latest->groups as $group)
                                                            <tr>
                                                                <td>
                                                                    <a
                                                                        href="{{ route('executions.groups.show', [$latest, $group]) }}"
                                                                    >
                                                                        {{ data_get($group, 'name') }}
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    @if ($group->examiners->isEmpty())
                                                                        Geen
                                                                    @else
                                                                        <ul class="list-unstyled mb-0">
                                                                            @foreach ($group->examiners as $examiner)
                                                                                <li>{{ $examiner->name }}</li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @endif
                                                                </td>
                                                                <td>{{ data_get($group, 'students_count') }}</td>
                                                            </tr>
                                                        @empty
                                                            <tr>
                                                                <td colspan="3">
                                                                    @lang('Er zijn nog geen groepen gemaakt.')
                                                                </td>
                                                            </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="table-responsive">
                                                <table class="table table-sm mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                @lang('Kerntaak')
                                                            </th>
                                                            <th>
                                                                @lang('Studenten')
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($latest->exam->tasks as $task)
                                                            <tr>
                                                                <td>
                                                                    {{ $task->getLabel() }}
                                                                </td>
                                                                <td>
                                                                    {{ $latest->students()->whereRelation('executionStudentTasks', 'task_id', $task->id)->count() }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row g-2">
                                        <div class="col">
                                            <a href="{{ route('executions.show', [$latest]) }}" class="btn btn-link">
                                                @lang('Ga naar uitvoering')
                                            </a>
                                        </div>
                                        @can('executions.score', $latest)
                                            <div class="col-auto">
                                                <a
                                                    href="{{ route('result.index', [$latest]) }}"
                                                    class="btn btn-outline-primary"
                                                >
                                                    @lang('Resultaten')
                                                </a>
                                            </div>
                                        @endcan

                                        @can('executions.score', $latest)
                                            <div class="col-auto">
                                                <a
                                                    href="{{ route('score.index', [$latest]) }}"
                                                    class="btn btn-primary"
                                                >
                                                    @lang('Beoordelen')
                                                </a>
                                            </div>
                                        @endcan
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="table-padded table-responsive-md">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col" class="table-shrink-col">@lang('Naam / Examen')</th>
                                <th scope="col">@lang('Startdatum')</th>
                                <th scope="col">@lang('Groepen')</th>
                                <th scope="col">@lang('Studenten')</th>
                                <th scope="col" class="text-end">
                                    <span class="visually-hidden">@lang('Acties')</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($executions as $execution)
                                <tr>
                                    <td class="table-shrink-col">
                                        <div class="py-2 d-flex align-items-center gap-4">
                                            <div>
                                                <a
                                                    class="d-block fw-bold"
                                                    href="{{ route('executions.show', $execution) }}"
                                                >
                                                    {{ $execution->name }}
                                                </a>
                                                <span class="d-block">{{ $execution->exam->name }}</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $execution->localizedStartDate() }}</td>
                                    <td>{{ $execution->getGroupNames() }}</td>
                                    <td>{{ $execution->students_count }}</td>
                                    <td>
                                        <div class="d-flex justify-content-end flex-column flex-md-row gap-2">
                                            @can('executions.score', $execution)
                                                <a
                                                    href="{{ route('result.index', [$execution]) }}"
                                                    class="btn btn-sm btn-outline-primary"
                                                >
                                                    Resultaten
                                                </a>
                                            @endcan

                                            @can('executions.score', $execution)
                                                <a
                                                    href="{{ route('score.index', [$execution]) }}"
                                                    class="btn btn-sm btn-primary"
                                                >
                                                    Beoordelen
                                                </a>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                @if ($latest === null)
                                    <tr>
                                        <td colspan="5">
                                            <div class="py-2 d-flex align-items-center gap-4">
                                                <x-icon class="w-auto bi-question-circle fs-2" />
                                                <div>@lang('Er zijn geen uitvoeringen')</div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
