<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <x-breadcrumbs />

                <x-header :title="__('Export')">
                    <a
                        class="btn btn-sm btn-primary"
                        href="{{ route('executions.batch-export.download.index', [$execution, $export]) }}"
                    >
                        <x-icon class="bi-save" />
                        &nbsp;
                        @lang('Download')
                    </a>
                </x-header>

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">@lang('Student')</th>
                                        <th scope="col">@lang('Link')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($export->students as $student)
                                        <tr>
                                            <td>{{ $student->getFullName() }}</td>
                                            <td class="table-shrink-col">
                                                <a
                                                    class="text-decoration-none"
                                                    href="{{ route('executions.batch-export.download.show', [$execution, $export, $student]) }}"
                                                >
                                                    <x-icon class="bi-eye pe-1" />
                                                    <span class="text-decoration-underline">Toon</span>
                                                </a>
                                                &nbsp;
                                                <a
                                                    class="text-decoration-none"
                                                    href="{{ route('executions.batch-export.download.show', [$execution, $export, $student]) }}"
                                                    download
                                                >
                                                    <x-icon class="bi-save pe-1" />
                                                    <span class="text-decoration-underline">Download</span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
