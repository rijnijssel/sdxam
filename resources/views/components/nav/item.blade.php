<li @class(['nav-item', 'active' => $isActive()])>
    <a @class(['nav-link', 'active' => $isActive()]) href="{{ $route }}">
        @if ($hasIcon())
            <x-icon :class="Arr::toCssClasses(['pe-0', 'pe-lg-1', $icon])" />
        @endif

        {{ $slot }}
    </a>
</li>
