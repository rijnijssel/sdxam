@props([
    'name' => null,
    'value' => null,
])

<input
    {{ $attributes->class(['form-control', 'is-invalid' => $errors->has($name)])->merge(['name' => $name, 'value' => old($name, $value)]) }}
/>
