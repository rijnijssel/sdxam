<nav class="navbar bg-body-tertiary navbar-expand-md shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'SDXam') }}
        </a>
        <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbar"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="{{ __('Toggle navigation') }}"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav me-auto">
                @can('viewAny', App\Models\Execution::class)
                    <x-nav.item route="{{ route('executions.index') }}" active="executions" icon="bi-calendar-week">
                        @lang('Uitvoeringen')
                    </x-nav.item>
                @endcan

                @can('viewAny', App\Models\Exam::class)
                    <x-nav.item route="{{ route('exams.index') }}" active="exams" icon="bi-table">
                        @lang('Examens')
                    </x-nav.item>
                @endcan

                @can('viewAny', App\Models\Student::class)
                    <x-nav.item route="{{ route('students.index') }}" active="students" icon="bi-mortarboard">
                        @lang('Studenten')
                    </x-nav.item>
                @endcan

                @can('viewAny', App\Models\Examiner::class)
                    <x-nav.item route="{{ route('examiners.index') }}" active="examiners" icon="bi-person-check">
                        @lang('Examinatoren')
                    </x-nav.item>
                @endcan

                @can('viewAny', App\Models\Context::class)
                    <x-nav.item route="{{ route('contexts.index') }}" active="contexts" icon="bi-card-text">
                        @lang('Contexten')
                    </x-nav.item>
                @endcan

                @can('viewAny', App\Models\User::class)
                    <x-nav.item route="{{ route('users.index') }}" active="users" icon="bi-people">
                        @lang('Gebruikers')
                    </x-nav.item>
                @endcan

                @can('terraform.index')
                    <x-nav.item route="{{ route('terraform.index') }}" active="terraform" icon="bi-hdd-rack">
                        @lang('Terraform')
                    </x-nav.item>
                @endcan
            </ul>
            <ul class="navbar-nav align-items-center">
                @if (config('app.debug'))
                    <li class="nav-item p-1">
                        <a
                            class="badge bg-danger d-inline-flex align-items-center font-monospace text-white text-decoration-none"
                            href="{{ url('/__clockwork') }}"
                            target="_blank"
                            data-bs-toggle="tooltip"
                            title="Environment: {{ config('app.env') }}"
                        >
                            @lang(':env', ['env' => config('app.env')])
                        </a>
                    </li>
                @endif

                @if (Session::has('original_user'))
                    <li class="nav-item d-flex p-1">
                        <form
                            hidden
                            id="form_stop_impersonating"
                            action="{{ route('impersonated-session.destroy') }}"
                            method="post"
                        >
                            @csrf
                            @method('delete')
                        </form>
                        <button class="btn btn-outline-warning btn-sm" type="submit" form="form_stop_impersonating">
                            <x-icon class="bi-box-arrow-right pe-md-1" />
                            @lang('Imiteren stopped')
                        </button>
                    </li>
                @endif

                <li class="nav-item dropdown" id="bd-theme">
                    <a
                        id="bd-theme-text"
                        class="nav-link dropdown-toggle"
                        href="#"
                        role="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                    >
                        Theme
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <button class="dropdown-item" data-bs-theme-value="auto">System</button>
                        </li>
                        <li>
                            <button class="dropdown-item" data-bs-theme-value="light">Light</button>
                        </li>
                        <li>
                            <button class="dropdown-item" data-bs-theme-value="dark">Dark</button>
                        </li>
                    </ul>
                </li>

                @guest
                    <x-nav.item route="{{ route('login') }}">{{ __('auth.nav.login') }}</x-nav.item>
                @else
                    <li class="nav-item dropdown">
                        <a
                            id="dropdown_account"
                            class="nav-link dropdown-toggle text-wrap"
                            href="#"
                            role="button"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                        >
                            <x-icon class="pe-0 pe-lg-1 bi-person" />
                            <span class="text-nowrap">{{ auth()->user()?->name }}</span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown_account">
                            <a class="dropdown-item" href="{{ route('profile.show') }}">
                                <x-icon class="bi-gear pe-md-1" />
                                @lang('auth.nav.account')
                            </a>

                            <button
                                type="button"
                                class="dropdown-item"
                                data-bs-toggle="modal"
                                data-bs-target="#modal_shortcuts"
                            >
                                <x-icon class="bi-keyboard pe-md-1" />
                                @lang('Sneltoetsen')
                            </button>

                            <div class="dropdown-divider"></div>

                            <form hidden id="form_logout" action="{{ route('logout') }}" method="post">
                                @csrf
                            </form>
                            <button class="dropdown-item" href="#" form="form_logout">
                                <x-icon class="bi-box-arrow-right pe-md-1" />
                                @lang('auth.nav.logout')
                            </button>

                            <div class="dropdown-divider"></div>

                            <a
                                class="dropdown-item"
                                href="{{ url('https://rijnijssel.gitlab.io/sdxam') }}"
                                target="_blank"
                            >
                                <x-icon class="bi-book pe-md-1" />
                                @lang('Documentatie')
                            </a>
                            <a
                                class="dropdown-item"
                                href="{{ url('https://gitlab.com/rijnijssel/sdxam') }}"
                                target="_blank"
                            >
                                <x-icon class="bi-code pe-md-1" />
                                @lang('Broncode')
                            </a>
                            <a class="dropdown-item" href="{{ route('changelog') }}">
                                <x-icon class="bi-card-list pe-md-1" />
                                @lang('Changelog')
                            </a>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
