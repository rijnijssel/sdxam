@if (isset($breadcrumbs))
    <nav aria-label="breadcrumbs">
        <ol class="breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                <x-breadcrumbs.item
                    :name="$breadcrumb->name"
                    :href="$breadcrumb->url"
                    :active="$breadcrumb->active"
                />
            @endforeach
        </ol>
    </nav>
@endif
