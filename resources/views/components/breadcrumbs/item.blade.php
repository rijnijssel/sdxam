@props([
    'name',
    'href' => null,
    'active' => false,
])

<li
    {{ $attributes->class(['breadcrumb-item', 'active' => $active]) }}
    @if ($active) aria-current="page" @endif
>
    @if ($active || $href === null)
        {{ $name }}
    @else
        <a href="{{ $href }}">
            {{ $name }}
        </a>
    @endif
</li>
