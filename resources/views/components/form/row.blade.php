<div class="row">
    <div class="col">
        {{ $slot }}
    </div>
    <div class="col-auto">
        {{ $right }}
    </div>
</div>
