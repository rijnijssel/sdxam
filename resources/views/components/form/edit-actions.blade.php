@props([
    'cancel',
    'delete' => true,
])
<x-form.row>
    <x-primary-button type="submit">
        @lang('Opslaan')
    </x-primary-button>
    <a href="{{ $cancel }}" class="btn btn-outline-secondary">@lang('Annuleren')</a>

    <x-slot name="right">
        @if ($delete)
            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal_delete">
                {{ __('Verwijderen') }}
            </button>
        @endif
    </x-slot>
</x-form.row>
