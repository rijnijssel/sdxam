@props([
    'cancel',
])

<x-form.row>
    <button type="submit" class="btn btn-primary">@lang('Toevoegen')</button>

    <x-slot name="right">
        <a href="{{ $cancel }}" class="btn btn-outline-secondary">@lang('Annuleren')</a>
    </x-slot>
</x-form.row>
