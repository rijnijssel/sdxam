@props([
    'route',
])

<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="label_delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ $route }}" method="post">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="label_delete">{{ $title }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {{ $slot }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                        @lang('Annuleren')
                    </button>
                    <button type="submit" class="btn btn-danger">@lang('Verwijderen')</button>
                </div>
            </form>
        </div>
    </div>
</div>
