@props([
    'title',
])

<div {{ $attributes->class(['row']) }}>
    <div class="col">
        <h5>{{ $title }}</h5>
    </div>
    <div class="col-auto">
        {{ $slot }}
    </div>
</div>
