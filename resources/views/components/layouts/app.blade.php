@props([
    'title' => null,
])
<x-layouts.base :title="$title">
    <x-navigation />

    <main {{ $attributes->class(['py-3', 'position-relative']) }}>
        {{ $slot }}
    </main>

    <x-shortcuts-modal />
</x-layouts.base>
