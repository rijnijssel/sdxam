<!DOCTYPE html>

@props([
    'title' => null,
])
<html lang="{{ str_replace('_', '-', app()->currentLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <meta name="referrer" content="same-origin" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>{{ ($title ? "$title - " : '') . config('app.name', 'SDXam') }}</title>
        <meta name="application-name" content="SDXam" />
        <meta name="theme-color" content="#ffffff" />
        <link rel="icon" type="image/svg+xml" href="{{ asset('favicon.svg') }}" />
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />
        @vite(['resources/ts/app.ts'])
        @livewireStyles
    </head>
    <body>
        <div id="app" {{ $attributes }}>
            {{ $slot }}

            @if (session('success'))
                <x-toast title="Succes" type="success">{{ session('success') }}</x-toast>
            @endif

            @if (session('danger'))
                <x-toast title="Let op" type="danger">{{ session('danger') }}</x-toast>
            @endif
        </div>
        @livewireScripts
    </body>
</html>
