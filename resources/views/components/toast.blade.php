@props([
    'title',
    'type',
])

<div class="toast-container position-fixed bottom-0 end-0 p-4">
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-toast="{{ $type }}">
        <div class="toast-header">
            <div class="rounded me-2 bg-{{ $type }}" style="width: 20px; height: 20px"></div>
            <strong class="me-auto">{{ $title }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            {{ $slot }}
        </div>
    </div>
</div>
