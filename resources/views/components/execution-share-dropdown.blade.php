@props([
    'execution',
])

<div {{ $attributes->class('dropdown') }}>
    <a class="btn btn-sm btn-outline-primary dropdown-toggle" data-bs-toggle="dropdown" role="button" href="#">
        <x-icon class="bi-share" />
        &nbsp;
        @lang('Delen')
    </a>

    <ul class="dropdown-menu">
        <li>
            <a class="dropdown-item" href="{{ route('shared-results.show', $execution) }}">
                @lang('PDF')
            </a>
        </li>
        <li>
            <form action="{{ route('shared-results.store', $execution) }}" method="post" hidden id="form_share_email">
                @csrf
            </form>
            <button class="dropdown-item" form="form_share_email">
                @lang('E-mail')
            </button>
        </li>
    </ul>
</div>
