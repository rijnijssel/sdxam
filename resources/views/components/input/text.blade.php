<div class="mb-3">
    <label class="form-label" for="{{ $name }}">{{ $title }}</label>
    <input
        {{
            $attributes
                ->merge([
                    'id' => $name,
                    'type' => 'text',
                    'name' => $name,
                    'value' => old($name, $value ?? null),
                ])
                ->class([
                    'form-control',
                    'is-invalid' => $errors->has($name),
                ])
        }}
    />
    {{ $slot }}
    <x-input.error name="{{ $name }}" />
</div>
