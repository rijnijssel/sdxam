@props([
    'name',
])

@error($name)
    <div class="invalid-feedback" role="alert">{{ $message }}</div>
@enderror
