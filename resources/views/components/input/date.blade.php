<x-input.text :title="$title" :name="$name" :value="$value" {{ $attributes->merge(['type' => 'date']) }}>
    {{ $slot }}
</x-input.text>
