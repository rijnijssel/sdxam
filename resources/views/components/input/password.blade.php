<x-input.text :title="$title" :name="$name" :value="$value" {{ $attributes->merge(['type' => 'password']) }}>
    {{ $slot }}
</x-input.text>
