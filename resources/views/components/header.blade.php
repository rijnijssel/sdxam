@props([
    'title',
])

<div {{ $attributes->class(['row', 'mb-3']) }}>
    <div class="col">
        <h4 class="mb-0">{{ $title }}</h4>
    </div>
    <div class="col-auto">
        <div class="d-flex flex-row gap-1">
            {{ $slot }}
        </div>
    </div>
</div>
