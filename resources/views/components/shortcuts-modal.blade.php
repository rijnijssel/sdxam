@php
    $shortcuts = [
        '?' => 'Open sneltoetsen overzicht',
        'e' => 'Open uitvoeringen overzicht',
        'c' => 'Open huidige uitvoering',
        'x' => 'Open examens overzicht',
        's' => 'Open studenten overzicht',
    ];
@endphp

<div class="modal fade" id="modal_shortcuts" tabindex="-1" aria-labelledby="modal_shortcuts_label">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_shortcuts_label">@lang('Sneltoetsen')</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table mb-0">
                    @foreach ($shortcuts as $shortcut => $description)
                        <tr>
                            <td><kbd>{{ $shortcut }}</kbd></td>
                            <td>{{ $description }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
