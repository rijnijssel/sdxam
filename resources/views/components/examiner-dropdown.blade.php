<div class="flex-grow-1">
    @if (! $disabled())
        <label class="form-label d-none" for="{{ $sid }}_{{ $task->slug }}_{{ $role }}">
            @lang('words.' . $role)
        </label>
        <select
            {{ $attributes->class(['form-select', 'form-select-sm']) }}
            id="{{ $sid }}_{{ $task->slug }}_{{ $role }}"
            name="students[{{ $sid }}][{{ $task->slug }}][{{ $role }}]"
            data-student-linker
            data-student="{{ $sid }}"
            data-role="{{ $role }}"
            @disabled($disabled())
        >
            <option selected value="">
                {{ $roleText() }}
            </option>
            @foreach ($examiners as $examiner)
                <option value="{{ $examiner->id }}" @selected($selected($examiner))>
                    {{ $examiner->getLabel() }}
                </option>
            @endforeach
        </select>
    @endif
</div>
