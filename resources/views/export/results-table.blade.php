<!DOCTYPE html>
<!--suppress CssUnusedSymbol -->
<html lang="nl" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet" />

        <title>@lang('Examenresultaten') {{ $execution->name }}</title>

        <style>
            @page {
                margin: 0.75cm 0.75cm;
            }

            body {
                font-size: 10pt;
                font-family: 'Roboto', sans-serif;
                font-weight: normal;
            }

            h1 {
                font-size: 1.5rem;
                margin-top: 0;
                margin-bottom: 0.5rem;
            }

            table {
                border-collapse: collapse;
            }

            table,
            td,
            th {
                vertical-align: top;
                text-align: left;
            }

            th,
            td {
                border: 1px solid rgba(0, 0, 0, 0.75);
                padding: 0.1rem 0.4rem;
            }

            th {
                background-color: rgba(173, 173, 173, 0.4);
                font-weight: bold;
            }

            .w-full {
                width: 100%;
            }

            .text-center {
                text-align: center;
            }

            .col-shrink {
                white-space: nowrap;
                width: 1%;
            }

            .cell-unlinked {
                background-color: rgba(173, 173, 173, 0.2);
            }

            .cell-passed {
                background-color: rgba(32, 201, 151, 0.25);
            }

            .cell-failed {
                background-color: rgba(220, 38, 38, 0.25);
            }
        </style>
    </head>
    <body>
        <h1>
            @lang('Examenresultaten')
            {{ $execution->name }}
        </h1>
        <table class="w-full">
            <tr>
                <th rowspan="2"></th>
                @foreach ($tasks as $task1)
                    <th class="text-center" colspan="{{ $task1->processes_count }}">{{ $task1->name }}</th>
                @endforeach
            </tr>
            <tr>
                @foreach ($tasks as $task2)
                    @foreach ($task2->processes as $process1)
                        <th class="text-center">{{ $process1->name }}</th>
                    @endforeach
                @endforeach
            </tr>
            @foreach ($execution->students as $student)
                <tr>
                    <th scope="row" class="col-shrink">
                        {{ $student->student->getFullName() }}
                    </th>
                    @foreach ($tasks as $task3)
                        @foreach ($task3->processes as $process2)
                            <td
                                @class([
                                    'text-center',
                                    'cell-unlinked' => $student->tasks->doesntContain($task3),
                                    'cell-failed' => $execution->isScoreFailed($student, $process2),
                                    'cell-passed' => $execution->isScorePassed($student, $process2),
                                ])
                            >
                                @if ($student->tasks->contains($task3))
                                    {{ $execution->getProcessScoreText($student, $process2) }}
                                @endif
                            </td>
                        @endforeach
                    @endforeach
                </tr>
            @endforeach
        </table>
    </body>
</html>
