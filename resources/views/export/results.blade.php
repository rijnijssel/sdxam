<!DOCTYPE html>
<html lang="nl" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet" />
        <title>@lang('Examenresultaten') {{ $execution->name }}</title>
        <!--suppress CssUnusedSymbol -->
        <style>
            @page {
                margin: 0.75cm 0.75cm;
            }

            body {
                font-size: 10pt;
                font-family: 'Lato', sans-serif;
                font-weight: normal;
            }

            h1 {
                font-size: 1.5rem;
                margin-top: 0;
                margin-bottom: 0.5rem;
            }

            h1.title {
                border: 1px solid rgba(0, 0, 0, 0.75);
                background-color: rgba(77, 161, 213, 0.68);
                padding: 0.25rem 0.5rem;
            }

            h2 {
                font-size: 1rem;
                margin-top: 0;
                margin-bottom: 0.375rem;
            }

            h3 {
                font-size: 0.9rem;
                margin-top: 0;
                margin-bottom: 0.25rem;
            }

            table {
                border-collapse: collapse;
            }

            table,
            td,
            th {
                vertical-align: top;
                text-align: left;
            }

            th,
            td {
                border: 1px solid rgba(0, 0, 0, 0.75);
                padding: 0.2rem 0.4rem;
            }

            th {
                background-color: rgba(173, 173, 173, 0.3);
            }

            .header th,
            .header td {
                background-color: rgba(0, 166, 255, 0.3);
                vertical-align: center;
            }

            table.compact th,
            table.compact td {
                padding: 0.1rem 0.4rem;
            }

            .w-full {
                width: 100%;
            }

            .font-small {
                font-size: 8pt;
            }

            .text-center {
                text-align: center;
            }

            .text-end {
                text-align: right;
            }

            .mb {
                margin-bottom: 0.75rem;
            }

            .col-align {
                width: 10rem;
            }

            .col-shrink {
                white-space: nowrap;
                width: 1%;
            }

            .cell-selected {
                background-color: rgba(58, 167, 59, 0.36);
            }

            .page-break {
                page-break-after: always;
            }

            .page-break:last-child {
                page-break-after: avoid;
            }

            .even-page {
                page-break-after: always;
            }

            .even-page:last-child {
                page-break-after: avoid;
            }
        </style>
    </head>
    <body>
        @foreach ($students as $es)
            <div class="even-page">
                <div id="cover" class="page-break">
                    <h1 class="title">@lang('Beoordelingsformulier praktijkexamen')</h1>
                    <table class="w-full mb">
                        <tr class="header">
                            <th colspan="3">@lang('Algemene informatie')</th>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Datum')</th>
                            <td colspan="2">{{ now()->format('Y m') }}</td>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Examenvorm')</th>
                            <td colspan="2">{{ $execution->exam->type }}</td>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Kwalificatiedossier')</th>
                            <td colspan="2">{{ $execution->exam->course }}</td>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Cohort en crebocode')</th>
                            <td>{{ $execution->exam->cohort }}</td>
                            <td>{{ $execution->exam->crebo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Examen')</th>
                            <td colspan="2">{{ $execution->exam->name }}</td>
                        </tr>
                    </table>
                    <table class="w-full mb">
                        <tr class="header">
                            <th colspan="2">@lang('Persoonsinformatie')</th>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Kandidaat')</th>
                            <td>{{ $es->student->getFullName() }}</td>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Leerlingnummer')</th>
                            <td>{{ $es->student->sid }}</td>
                        </tr>
                        <tr>
                            <th class="col-align">@lang('Cohort')</th>
                            <td>{{ $es->student->cohort }}</td>
                        </tr>
                    </table>
                    <table class="w-full mb">
                        <tr class="header">
                            <th colspan="3">@lang('Examinatoren')</th>
                        </tr>
                        <tr>
                            <th>@lang('Naam')</th>
                            <th class="text-center">@lang('Afkorting')</th>
                            <th>@lang('Handtekening')</th>
                        </tr>
                        @foreach ($es->getExaminers() as $examiner)
                            <tr>
                                <td class="col-align">{{ data_get($examiner, 'name') }}</td>
                                <td class="col-shrink text-center">{{ data_get($examiner, 'abbreviation') }}</td>
                                <td height="45"></td>
                            </tr>
                        @endforeach
                    </table>
                    <table class="w-full">
                        <tr class="header">
                            <th colspan="3">@lang('Geëxamineerde kerntaken en werkprocessen')</th>
                        </tr>
                        @foreach ($es->executionStudentTasks as $task)
                            <tr>
                                <th>{{ $task->task->getLabel() }} ({{ $task->task->code }})</th>
                                <th class="col-shrink">{{ $task->getExaminersLabel() }}</th>
                                <th>Resultaat</th>
                            </tr>
                            @foreach ($task->task->processes as $process)
                                <tr>
                                    <td colspan="2">{{ $process->name }}: {{ $process->description }}</td>
                                    <td class="col-shrink text-end">
                                        @if ($es->getProcessScore($process))
                                            {{ $es->getProcessScore($process) }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </table>
                </div>
                @foreach ($es->executionStudentTasks as $task)
                    @foreach ($task->task->processes as $process)
                        <div class="page-break">
                            <h1>{{ $task->task->getLabel() }}</h1>
                            <h2>{{ $process->getLabel() }}</h2>
                            <table class="w-full mb">
                                <tr class="header">
                                    <th colspan="2">@lang('Persoonsinformatie')</th>
                                </tr>
                                <tr>
                                    <th>@lang('Datum')</th>
                                    <td>{{ now()->format('Y m') }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('Kandidaat')</th>
                                    <td>{{ $es->student->getFullName() }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('Leerlingnummer')</th>
                                    <td>{{ $es->student->sid }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('Beoordelaar 1')</th>
                                    <td>{{ $task?->client?->getLabel() }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('Beoordelaar 2')</th>
                                    <td>{{ $task?->supervisor?->getLabel() }}</td>
                                </tr>
                            </table>
                            @foreach ($process->rubrics as $rubric)
                                <div class="page-break">
                                    {{-- Table with rubrics and scores --}}
                                    <table class="w-full mb">
                                        <tr class="header">
                                            <th colspan="{{ 2 + $rubric->max_items }}">{{ $rubric->title }}</th>
                                        </tr>
                                        <tr class="font-small">
                                            <th>@lang('Taak')</th>
                                            <th>@lang('Criterium')</th>
                                            @for ($i = 0; $i < $rubric->max_items; $i++)
                                                <th>{{ $i }}</th>
                                            @endfor
                                        </tr>
                                        @foreach ($rubric->tasks as $rt)
                                            <tr class="font-small">
                                                <th>{{ $rt->name }}</th>
                                                <td>{{ $rt->criteria }}</td>
                                                @foreach ($rt->items->pad($rubric->max_items, null) as $rti)
                                                    <td @class(['cell-selected' => $task->isItemSelected($rti)])>
                                                        {!! $rti->text ?? '' !!}
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @endforeach

                            {{-- Table with required criteria --}}
                            <h2>@lang('Cruciale criteria')</h2>
                            @if ($process->getRequiredTasks()->count() > 0)
                                <table class="mb compact">
                                    <tr class="header">
                                        <th>@lang('Cruciaal criterium')</th>
                                        <th>@lang('Behaald in kolom 1 of hoger')</th>
                                    </tr>
                                    @foreach ($process->getRequiredTasks() as $t)
                                        <tr>
                                            <td>{{ $t->criteria }}</td>
                                            <td>{{ $task->isAchieved($t) ? 'Ja' : 'Nee' }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                                @if ($process->getRequiredTasks()->count() > 1)
                                    <table class="mb compact">
                                        <tr class="header">
                                            <th colspan="3">@lang('Zijn alle cruciale criteria behaald?')</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                @if ($process->getRequiredTasksAchieved($task))
                                                    X
                                                @endif
                                            </td>
                                            <td>@lang('Ja')</td>
                                            <td>@lang('Reik cijfer uit volgens cijfertabel')</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                @if (! $process->getRequiredTasksAchieved($task))
                                                    X
                                                @endif
                                            </td>
                                            <td>@lang('Nee')</td>
                                            <td>@lang('Onvoldoende')</td>
                                        </tr>
                                    </table>
                                @endif
                            @else
                                <p>@lang('Deze rubric heeft geen cruciale criteria.')</p>
                            @endif

                            <h2>@lang('Onderbouwing')</h2>
                            <table class="mb compact">
                                @foreach ($process->rubrics as $rubric)
                                    <tr class="header">
                                        <th colspan="2">{{ $rubric->title }}</th>
                                    </tr>
                                    @foreach ($rubric->tasks as $rt)
                                        <tr>
                                            <td>
                                                <strong>{{ $rt->name }}</strong>
                                                <br />
                                                {{ $rt->criteria }}
                                            </td>
                                            <td>
                                                {!! nl2br($task->getDefinitiveAnnotation($rt) ?? trans('-')) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </table>

                            {{-- Table with points and grades --}}
                            <h2>@lang('Cijfertabel')</h2>
                            <table class="mb compact">
                                <tr class="header font-small">
                                    <th>@lang('Punten')</th>
                                    <th>@lang('Cijfer')</th>
                                </tr>
                                @foreach ($process->getScoreTable() as $row)
                                    <tr
                                        @class(['font-small', 'p-0', 'cell-selected' => $es->isScoreRowSelected($process, $row->points)])
                                    >
                                        <td class="text-end">{{ $row->points }}</td>
                                        <td class="text-end">{{ number_format($row->score, 1) }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endforeach
                @endforeach
            </div>
        @endforeach
    </body>
</html>
