<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        {{ $content }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
