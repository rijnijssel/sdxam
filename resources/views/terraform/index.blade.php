<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center gap-3">
            <div class="col">
                <x-header :title="__('Terraform')" />

                <p>
                    @lang('Terraform is een tool voor het bouwen, wijzigen en versiebeheer van infrastructuur veilig en efficiënt.')
                    <br />
                    @lang('Hier kun je de uitvoeringen beheren die je wilt blootstellen aan Terraform.')
                </p>

                @if ($hasPipelineTrigger)
                    <p>
                        @lang('Wanneer je klaar bent met het selecteren van de uitvoeringen, kun je automatisch een GitLab CI/CD pipeline started om de Terraform configuratie aan te passen.')
                    </p>

                    <livewire:terraform.pipelines.create />
                @endif
            </div>

            <div class="col">
                <x-header title="{{ __('Uitvoeringen') }}" />

                <p>
                    @lang('Selecteer de uitvoeringen die je wilt blootstellen aan Terraform.')
                </p>

                <livewire:terraform.executions.index />
            </div>
        </div>
    </div>
</x-layouts.app>
