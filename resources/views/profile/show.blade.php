<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <x-header title="Profiel" />
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            <div class="col-sm-4 col-md-3">
                <h5>@lang('Persoonlijk')</h5>
                <p class="text-muted">@lang('Werk jouw naam en email bij.')</p>
            </div>
            <div class="col-sm-8 col-md-6">
                <x-card>
                    <form action="{{ route('profile.update', $user) }}" method="post">
                        @csrf
                        @method('put')

                        <x-input.text title="Naam" name="name" value="{{ $user->name }}" />
                        <x-input.text title="E-mail" name="email" value="{{ $user->email }}" />

                        <div class="row">
                            <div class="col">
                                <x-primary-button type="submit">
                                    @lang('Opslaan')
                                </x-primary-button>
                            </div>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            <div class="col-sm-4 col-md-3">
                <h5>@lang('Beveiliging')</h5>
                <p class="text-muted">@lang('Verander jouw wachtwoord.')</p>
            </div>
            <div class="col-sm-8 col-md-6">
                <x-card>
                    <form action="{{ route('profile.update-password', $user) }}" method="post">
                        @csrf
                        @method('put')

                        @if ($user->password)
                            <x-input.password
                                :title="__('Huidig wachtwoord')"
                                name="current_password"
                                autocomplete="current-password"
                            />
                        @else
                            <div class="alert alert-info d-flex align-items-center" role="alert">
                                <x-icon class="bi-info-circle flex-shrink-0 me-2" />
                                <div>@lang('Er is nog geen wachtwoord ingesteld.')</div>
                            </div>
                        @endif

                        <div class="row row-cols-md-2">
                            <div class="col">
                                <x-input.password
                                    :title="__('Wachtwoord')"
                                    name="password"
                                    autocomplete="new-password"
                                />
                            </div>
                            <div class="col">
                                <x-input.password
                                    :title="__('Herhaal wachtwoord')"
                                    name="password_confirmation"
                                    autocomplete="new-password"
                                />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <x-primary-button type="submit">
                                    {{ __('Opslaan') }}
                                </x-primary-button>
                            </div>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
        @can('manage-api-tokens')
            <div class="row justify-content-center mb-4">
                <div class="col-sm-4 col-md-3">
                    <h5>@lang('API Tokens')</h5>
                    <p class="text-muted">@lang('Beheer jouw API tokens.')</p>
                </div>
                <div class="col-sm-8 col-md-6">
                    <x-card>
                        <livewire:api-token-manager :user="$user" />
                    </x-card>
                </div>
            </div>
        @endcan
    </div>
</x-layouts.app>
