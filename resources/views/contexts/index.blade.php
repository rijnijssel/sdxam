<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-header title="{{ __('Contexten') }}">
                    @if (Route::has('contexts.create'))
                        @can('create', \App\Models\Context::class)
                            <a class="btn btn-sm btn-primary" href="{{ route('contexts.create') }}">
                                @lang('Toevoegen')
                            </a>
                        @endcan
                    @endif
                </x-header>

                <div class="table-padded table-responsive-md">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('Naam')</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contexts as $context)
                                <tr>
                                    <td>
                                        {{ $context->name }}
                                    </td>
                                    <td class="text-end">
                                        @can('update', $context)
                                            <a href="{{ route('contexts.edit', $context) }}">
                                                @lang('Bewerken')
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
