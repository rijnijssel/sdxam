<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-header title="{{ __('Context bewerken') }}" />

                <x-card>
                    <form action="{{ route('contexts.update', $context) }}" method="post">
                        @csrf
                        @method('put')

                        <x-input.text title="{{ __('Naam') }}" name="name" :value="$context->name" required autofocus>
                            <div class="form-text">
                                @lang('Naam van deze context')
                            </div>
                        </x-input.text>

                        <x-form.edit-actions cancel="{{ route('contexts.index') }}" :delete="false" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
