<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-header title="{{ __('Context toevoegen') }}" />

                <x-card>
                    <form action="{{ route('contexts.store') }}" method="post">
                        @csrf

                        <x-input.text title="{{ __('Naam') }}" name="name" required autofocus>
                            <div class="form-text">
                                @lang('Naam van deze context')
                            </div>
                        </x-input.text>

                        <x-form.create-actions cancel="{{ route('contexts.index') }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
