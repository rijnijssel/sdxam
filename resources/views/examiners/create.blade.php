<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-card>
                    <x-header title="Examinator toevoegen"></x-header>
                    <form action="{{ route('examiners.store') }}" method="post">
                        @csrf

                        <x-form.head title="Algemene informatie" />

                        <x-input.text title="{{ __('Naam') }}" name="name" required />
                        <x-input.text title="{{ __('Afkorting') }}" name="abbreviation" required />

                        <div class="mb-3">
                            <label for="user" class="form-label">@lang('Gebruiker')</label>
                            <select
                                id="user"
                                name="user"
                                class="form-select @error('user') is-invalid @enderror"
                            >
                                <option value="">@lang('Geen')</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->email }}" @selected(old('user') == $user->email)>
                                        {{ $user->getLabel() }}
                                    </option>
                                @endforeach
                            </select>

                            <x-input.error name="user" />
                        </div>

                        <x-form.create-actions cancel="{{ route('examiners.index') }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
