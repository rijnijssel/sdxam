<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-card>
                    <x-header title="Examinator bewerken"></x-header>

                    <form action="{{ route('examiners.update', $examiner) }}" method="post">
                        @csrf
                        @method('put')

                        <x-form.head title="Algemene informatie"></x-form.head>

                        <x-input.text title="{{ __('Naam') }}" name="name" value="{{ $examiner->name }}" required />
                        <x-input.text
                            title="{{ __('Afkorting') }}"
                            name="abbreviation"
                            value="{{ $examiner->abbreviation }}"
                            required
                        />

                        <div class="mb-3">
                            <label for="user" class="form-label">@lang('Gebruiker')</label>
                            <select
                                id="user"
                                name="user"
                                class="form-select @error('user') is-invalid @enderror"
                            >
                                <option value="">@lang('Geen')</option>
                                @foreach ($users as $user)
                                    <option
                                        value="{{ $user->email }}"
                                        @selected($user->is($examiner->user) || old('user') == $user->email)
                                    >
                                        {{ $user->getLabel() }}
                                    </option>
                                @endforeach
                            </select>

                            <x-input.error name="user"></x-input.error>
                        </div>

                        <x-form.edit-actions cancel="{{ route('examiners.index') }}"></x-form.edit-actions>
                    </form>
                </x-card>
            </div>
        </div>
    </div>

    <x-form.delete-modal route="{{ route('examiners.destroy', $examiner) }}">
        <x-slot name="title">@lang('Examinator verwijderen')</x-slot>
        Weet je zeker dat je examinator "{{ $examiner->name }}" wilt verwijderen?
    </x-form.delete-modal>
</x-layouts.app>
