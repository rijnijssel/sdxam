<x-layouts.app>
    <div class="container">
        <div class="row">
            <div class="col">
                <x-breadcrumbs />

                <x-header title="{{ __('Examinatoren') }}">
                    <a
                        class="btn btn-sm btn-primary"
                        href="{{ route('examiners.create') }}"
                        data-toggle="tooltip"
                        title="Toevoegen van examinator"
                    >
                        @lang('Toevoegen')
                    </a>
                </x-header>

                <div class="table-padded table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Naam')</th>
                                <th scope="col">@lang('Afkorting')</th>
                                <th scope="col">@lang('E-mail')</th>
                                <th scope="col" class="text-end">
                                    <span class="visually-hidden">@lang('Acties')</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($examiners as $examiner)
                                <tr>
                                    <th scope="row">
                                        <a href="{{ route('examiners.show', $examiner) }}">{{ $examiner->name }}</a>
                                    </th>
                                    <td>{{ data_get($examiner, 'abbreviation') }}</td>
                                    <td>{{ data_get($examiner, 'user.email') }}</td>
                                    <td class="text-end">
                                        <a href="{{ route('examiners.edit', $examiner) }}">@lang('Bewerken')</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
