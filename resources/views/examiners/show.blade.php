<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-card>
                    <x-header title="Examinator">
                        <a
                            class="btn btn-sm btn-primary"
                            href="{{ route('examiners.edit', $examiner) }}"
                            data-toggle="tooltip"
                            title="Bewerken van examinator"
                        >
                            @lang('Bewerken')
                        </a>
                    </x-header>

                    <x-form.head title="Algemene informatie" />

                    <div class="d-flex flex-column gap-3">
                        <div>
                            <x-label for="name">@lang('Naam')</x-label>
                            <x-input id="name" name="name" :value="$examiner->name" readonly />
                        </div>
                        <div>
                            <x-label for="abbreviation">@lang('Afkorting')</x-label>
                            <x-input id="abbreviation" name="abbreviation" :value="$examiner->abbreviation" readonly />
                        </div>
                        <div>
                            <x-label for="email">@lang('E-mail')</x-label>
                            <x-input id="email" name="email" :value="$examiner->getEmail()" readonly />
                        </div>
                    </div>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
