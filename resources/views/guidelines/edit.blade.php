<x-layouts.app>
    <div class="container-xl">
        <x-breadcrumbs />

        <x-header :title="__('Onderlegger')" />
        <div class="card">
            <div class="card-body">
                <form action="{{ route('guidelines.update', $task) }}" method="post">
                    @csrf
                    @method('put')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="pb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <h5>{{ $task->name }}</h5>
                    <p>{{ $task->criteria }}</p>

                    @foreach ($task->items as $item)
                        <div class="mb-3 pb-3 row">
                            <div class="col-md-2">
                                {!! $item->text !!}
                            </div>
                            <div class="col">
                                <textarea
                                    class="form-control"
                                    aria-label="Rubric item {{ $item->points }}"
                                    name="guidelines[{{ $item->id }}]"
                                    rows="4"
                                >
{{ $item->guideline }}</textarea
                                >
                            </div>
                        </div>
                    @endforeach

                    <x-primary-button>
                        @lang('Opslaan')
                    </x-primary-button>
                </form>
            </div>
        </div>
    </div>
</x-layouts.app>
