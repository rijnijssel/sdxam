<x-layouts.base class="d-flex justify-content-center align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('login') }}" method="post">
                            @csrf

                            <h3 class="pb-3">@lang('Inloggen')</h3>

                            @error('socialite')
                                <div class="mb-3 alert alert-danger alert-dismissible fade show" role="alert">
                                    <span>
                                        <strong>@lang('Toegang geweigerd!')</strong>
                                        @lang('Account (:email) heeft niet voldoende rechten om in te loggen.', ['email' => $errors->first('socialite')])
                                    </span>
                                    <button
                                        type="button"
                                        class="btn-close"
                                        data-bs-dismiss="alert"
                                        aria-label="Close"
                                    ></button>
                                </div>
                            @enderror

                            @if (config('services.surfconext.enabled') || config('services.gitlab.enabled'))
                                <div class="mb-3 d-flex flex-row gap-3">
                                    @if (config('services.surfconext.enabled'))
                                        <a
                                            class="w-100 btn btn-primary"
                                            href="{{ route('auth.redirect', 'surfconext') }}"
                                        >
                                            @lang('SURFconext')
                                        </a>
                                    @endif

                                    @if (config('services.gitlab.enabled'))
                                        <a class="w-100 btn btn-primary" href="{{ route('auth.redirect', 'gitlab') }}">
                                            @lang('GitLab')
                                        </a>
                                    @endif
                                </div>

                                <div class="mb-3 d-flex flex-row align-items-center">
                                    <hr class="d-inline-block w-100" />
                                    <span class="d-inline-block px-3">of</span>
                                    <hr class="d-inline-block w-100" />
                                </div>
                            @endif

                            <x-input.text
                                name="email"
                                :title="__('auth.login.email')"
                                type="email"
                                :placeholder="__('bijv. gebruiker@example.nl')"
                                autocomplete="email"
                                required
                                autofocus
                            />

                            <x-input.password
                                name="password"
                                :title="__('auth.login.password')"
                                :placeholder="__('********')"
                                autocomplete="current-password"
                                required
                            />

                            <div class="mb-3 form-check">
                                <input
                                    id="remember"
                                    name="remember"
                                    value="remember"
                                    type="checkbox"
                                    role="switch"
                                    class="form-check-input"
                                />
                                <label for="remember" class="form-check-label">
                                    @lang('auth.login.remember-me')
                                </label>
                            </div>

                            <div class="d-flex">
                                <div class="me-auto">
                                    <x-primary-button>@lang('auth.login.submit')</x-primary-button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('auth.login.forgot-password') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.base>
