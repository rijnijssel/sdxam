<x-layouts.app>
    <div class="container-xl">
        <x-breadcrumbs />

        <div class="row">
            <div class="col">
                <x-header title="{{ __('Examen - Werkproces') }}" />

                @foreach ($process->rubrics as $rubric)
                    <div class="row">
                        <div class="col">
                            <p class="lead">{{ $rubric->title }}</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-sm table-hover">
                                    <thead class="table-dark">
                                        <tr>
                                            <th scope="col">@lang('Taak')</th>
                                            @for ($i = 0; $i < $rubric->max_items; $i++)
                                                <th>{{ $i }}</th>
                                            @endfor
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rubric->tasks as $task)
                                            <tr>
                                                <th scope="row" rowspan="2">
                                                    {{ $task->name }}
                                                </th>
                                                <th scope="row" colspan="{{ $rubric->max_items }}">
                                                    <div class="d-flex flex-row">
                                                        <div class="me-auto">
                                                            {{ $task->criteria }}
                                                        </div>
                                                        <div>
                                                            @can('guidelines.update')
                                                                <a
                                                                    role="link"
                                                                    class="text-muted"
                                                                    href="{{ route('guidelines.edit', $task) }}"
                                                                >
                                                                    <x-icon class="bi-pencil-square" />
                                                                </a>
                                                            @endcan
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                @foreach ($task->items->pad($rubric->max_items, null) as $item)
                                                    @if ($item?->guideline)
                                                        <td
                                                            class="lh-sm w-25"
                                                            data-bs-toggle="tooltip"
                                                            data-bs-html="true"
                                                            title="{{ nl2br($item->guideline) }}"
                                                        >
                                                            @if ($item->guideline)
                                                                <div class="small text-muted mb-2">
                                                                    @lang('Onderlegger beschikbaar')
                                                                </div>
                                                            @endif

                                                            {!! data_get($item, 'text') !!}
                                                        </td>
                                                    @else
                                                        <td class="lh-sm w-25">
                                                            {!! data_get($item, 'text') !!}
                                                        </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</x-layouts.app>
