<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col">
                <x-header title="{{ __('Examens') }}"></x-header>

                <div class="table-padded table-responsive-md">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('Naam')</th>
                                <th>@lang('Kerntaken')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($exams as $exam)
                                <tr>
                                    <td><a href="{{ route('exams.show', $exam) }}">{{ $exam->name }}</a></td>
                                    <td>{{ $exam->tasksString() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
