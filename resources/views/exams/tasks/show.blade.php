<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <x-header :title="__('Examen - Kerntaak')" />

        <div class="table-padded table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>@lang('Kerntaken')</th>
                        <th>@lang('Werkprocessen')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($task->processes as $process)
                        <tr>
                            <td>{{ $process->name }}</td>
                            <td>
                                @foreach ($process->rubrics as $rubric)
                                    <div class="py-1">
                                        <h6 class="mb-2">{{ $rubric->title }}</h6>
                                        <ul class="list-group">
                                            @foreach ($rubric->tasks as $row)
                                                <li class="list-group-item py-1">{{ $row->criteria }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-layouts.app>
