<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <x-header :title="__('Examen')" />

        <div class="row g-3">
            <div class="col-md-6 d-flex flex-column gap-3">
                <x-card>
                    <x-form.head title="Algemene informatie"></x-form.head>

                    <div class="mb-3">
                        <label for="name" class="form-label">@lang('Naam')</label>
                        <input id="name" class="form-control" value="{{ $exam->name }}" readonly />
                    </div>
                    <div class="mb-3">
                        <label for="type" class="form-label">@lang('Examenvorm')</label>
                        <input id="type" class="form-control" value="{{ $exam->type }}" readonly />
                    </div>
                    <div class="mb-3">
                        <label for="course" class="form-label">@lang('Kwalificatiedossier')</label>
                        <input id="course" class="form-control" value="{{ $exam->course }}" readonly />
                    </div>
                    <div class="mb-3">
                        <label for="cohort" class="form-label">@lang('Cohort')</label>
                        <input id="cohort" class="form-control" value="{{ $exam->cohort }}" readonly />
                    </div>
                    <div>
                        <label for="crebo" class="form-label">@lang('Crebocode')</label>
                        <input id="crebo" class="form-control" value="{{ $exam->crebo }}" readonly />
                    </div>
                </x-card>
                <x-card>
                    <x-form.head title="Uitvoeringen"></x-form.head>

                    <ul class="mb-0">
                        @foreach ($executions as $execution)
                            <li>
                                <a href="{{ route('executions.show', $execution) }}">
                                    {{ $execution->name }} ({{ $execution->localizedStartDate() }})
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </x-card>
            </div>
            <div class="col-md-6">
                <x-card>
                    <x-form.head title="Overzicht kerntaken"></x-form.head>

                    <ul class="mb-0">
                        @foreach ($exam->tasks as $task)
                            <li>
                                <a href="{{ route('exams.tasks.show', [$exam, $task]) }}">
                                    {{ $task->getLabel() }}
                                </a>
                            </li>
                            @if ($task->processes->isNotEmpty())
                                <ul>
                                    @foreach ($task->processes as $process)
                                        <li>
                                            <a
                                                href="{{ route('exams.tasks.processes.show', [$exam, $task, $process]) }}"
                                            >
                                                {{ $process->getLabel() }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
