<div class="d-flex flex-column gap-3">
    <form wire:submit="store" class="has-validation">
        <div class="row mb-3">
            <div class="col">
                <label for="api_token_name" class="form-label">
                    @lang('Token Name')
                </label>
                <input
                    id="api_token_name"
                    class="form-control form-control-sm"
                    @class(['is-invalid' => $errors->has('form.name')])
                    wire:model="form.name"
                />
                <x-input.error name="form.name" />
            </div>
            <div class="col">
                <label for="api_token_expires_at" class="form-label">
                    @lang('Expiry Date')
                </label>
                <input
                    id="api_token_expires_at"
                    type="date"
                    min="{{ now()->format('Y-m-d') }}"
                    class="form-control form-control-sm"
                    @class(['is-invalid' => $errors->has('storeForm.expiresAt')])
                    wire:model="form.expiresAt"
                />
                <x-input.error name="form.expiresAt" />
            </div>
        </div>
        <div></div>
        <button class="btn btn-sm btn-primary" type="submit">
            @lang('Toevoegen')
        </button>
    </form>

    @if ($storedToken)
        <div>
            <div class="alert alert-success d-flex align-items-center" role="alert">
                <x-icon class="bi-check-circle-fill flex-shrink-0 me-2" />
                <div>@lang('De API token is aangemaakt.')</div>
            </div>
            <div class="input-group">
                <input
                    class="form-control"
                    value="{{ $storedToken }}"
                    title="@lang('Aangemaakte API token')"
                    readonly
                />
                <button
                    class="btn btn-outline-secondary"
                    type="button"
                    data-bs-toggle="tooltip"
                    data-bs-placement="bottom"
                    title="@lang('Kopieer')"
                    onclick="navigator.clipboard.writeText('{{ $storedToken }}')"
                >
                    <x-icon class="bi-clipboard" />
                </button>
            </div>
        </div>
    @endif

    @if ($tokens->isEmpty())
        @lang('Er zijn nog geen tokens gemaakt.')
    @else
        <div class="table-responsive">
            <table class="table table-sm h-100">
                @foreach ($tokens as $token)
                    <tr>
                        <td>
                            <div>{{ $token->name }}</div>
                            <div class="small text-muted">{{ $token->expires_at ?: __('No Expiry') }}</div>
                        </td>
                        <td class="table-shrink-col h-100">
                            <div class="h-100 d-flex align-items-center">
                                <button class="btn btn-sm btn-danger">
                                    <i class="bi bi-trash" wire:click="destroy({{ $token->id }})"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endif
</div>
