<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <x-breadcrumbs />

            <x-header :title="__('Exports')">
                <x-execution-share-dropdown :execution="$this->execution" />

                <div class="dropdown" wire:ignore>
                    <button
                        class="btn btn-primary btn-sm dropdown-toggle"
                        type="button"
                        id="export_dropdown"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                    >
                        <x-icon class="bi-file-pdf" />
                        &nbsp;
                        @lang('Export')
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                        <li>
                            <form
                                id="form_export"
                                hidden
                                action="{{ route('executions.export.store', $this->execution) }}"
                                method="post"
                            >
                                @csrf
                            </form>
                            <button type="button" class="dropdown-item" onclick="doExport()">
                                <x-icon class="bi-file-pdf" />
                                &nbsp;
                                @lang('Alle kandidaten')
                            </button>

                            @env('local')
                                <form
                                    action="{{ route('executions.export.store', $this->execution) }}?view=true"
                                    method="post"
                                    id="view_export_form"
                                    hidden
                                >
                                    @csrf
                                </form>
                                <button type="submit" class="dropdown-item" form="view_export_form">
                                    <x-icon class="bi-file-pdf" />
                                    &nbsp;
                                    @lang('Alle kandidaten (preview)')
                                </button>
                            @endenv

                            <button
                                type="button"
                                class="dropdown-item"
                                data-bs-toggle="modal"
                                data-bs-target="#modal_batch_export"
                            >
                                <x-icon class="bi-file-pdf" />
                                &nbsp;
                                @lang('Individueel')
                            </button>
                        </li>
                    </ul>
                </div>
            </x-header>

            <h5 class="mt-3">@lang('Individuele exports')</h5>
            <div class="table-padded table-responsive">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col">@lang('Gestart')</th>
                            <th scope="col">@lang('Status')</th>
                            <th scope="col">@lang('Duur')</th>
                            <th scope="col" class="text-end">@lang('Acties')</th>
                        </tr>
                    </thead>
                    <tbody wire:poll.visible>
                        @if (count($this->execution->batchExports))
                            @foreach ($this->execution->batchExports as $export)
                                <tr>
                                    <td>
                                        <span title="{{ $export->started_at?->toDateTimeLocalString() }}">
                                            {{ $export->started_at?->diffForHumans() ?? '' }}
                                        </span>
                                    </td>
                                    <td>
                                        @if ($export->isProcessing())
                                            <span class="badge rounded-pill bg-warning">@lang('Bezig')</span>
                                        @elseif ($export->isFailed())
                                            <span class="badge rounded-pill bg-danger">@lang('Fout')</span>
                                        @elseif ($export->isFinished())
                                            <span class="badge rounded-pill bg-success">@lang('Klaar')</span>
                                        @else
                                            <span class="badge rounded-pill bg-warning">@lang('Wachtrij')</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($export->isFinished())
                                            {{ $export->finished_at->longAbsoluteDiffForHumans($export->started_at) }}
                                        @endif
                                    </td>
                                    <td class="table-shrink-col">
                                        @if ($export->isFinished())
                                            <div class="d-flex flex-row justify-content-end gap-1">
                                                <a
                                                    class="btn btn-link btn-sm d-flex gap-2"
                                                    href="{{ route('executions.batch-export.show', [$this->execution, $export]) }}"
                                                >
                                                    <x-icon class="bi-filetype-pdf" />
                                                    <span class="visually-hidden">
                                                        @lang('Toon')
                                                    </span>
                                                </a>
                                                <a
                                                    class="btn btn-link btn-sm d-flex gap-2"
                                                    href="{{ route('executions.batch-export.download.index', [$this->execution, $export]) }}"
                                                    download
                                                >
                                                    <x-icon class="bi-download" />
                                                    <span class="visually-hidden">@lang('Download')</span>
                                                </a>
                                                <button
                                                    class="btn btn-link btn-sm text-danger"
                                                    wire:click="deleteBatchExport({{ $export->id }})"
                                                    wire:confirm="Weet je zeker dat je deze export wilt verwijderen?"
                                                >
                                                    <x-icon class="bi-trash" />
                                                </button>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    @lang('Geen individuele exports gevonden')
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

            <h5 class="mt-3">@lang('Gecombineerde exports')</h5>
            <div class="table-padded table-responsive">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col">@lang('Gestart')</th>
                            <th scope="col">@lang('Status')</th>
                            <th scope="col">@lang('Duur')</th>
                            <th scope="col" class="text-end">@lang('Acties')</th>
                        </tr>
                    </thead>
                    <tbody wire:poll.visible>
                        @if (count($this->execution->exports))
                            @foreach ($this->execution->exports as $export)
                                <tr>
                                    <td>
                                        <span title="{{ $export->started_at?->toDateTimeLocalString() }}">
                                            {{ $export->started_at?->diffForHumans() ?? '' }}
                                        </span>
                                    </td>
                                    <td>
                                        @if ($export->isProcessing())
                                            <span class="badge rounded-pill bg-warning">@lang('Bezig')</span>
                                        @elseif ($export->isFinished())
                                            <span class="badge rounded-pill bg-success">@lang('Klaar')</span>
                                        @else
                                            <span class="badge rounded-pill bg-warning">@lang('Wachtrij')</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($export->isFinished())
                                            {{ $export->finished_at->longAbsoluteDiffForHumans($export->started_at) }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($export->isFinished())
                                            <div class="d-flex flex-row justify-content-end gap-1">
                                                <a
                                                    class="btn btn-link btn-sm d-flex gap-2"
                                                    href="{{ $export->url() }}"
                                                >
                                                    <x-icon class="bi-filetype-pdf" />
                                                    <span class="visually-hidden">
                                                        @lang('Toon')
                                                    </span>
                                                </a>
                                                <a
                                                    class="btn btn-link btn-sm d-flex gap-2"
                                                    href="{{ $export->url() }}"
                                                    download
                                                >
                                                    <x-icon class="bi-download" />
                                                    <span class="visually-hidden">@lang('Download')</span>
                                                </a>
                                                <button
                                                    class="btn btn-link btn-sm text-danger"
                                                    wire:click="deleteExport({{ $export->id }})"
                                                    wire:confirm="Weet je zeker dat je deze export wilt verwijderen?"
                                                >
                                                    <x-icon class="bi-trash" />
                                                </button>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">@lang('Geen gecombineerde exports gevonden')</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div
        class="modal fade"
        id="modal_batch_export"
        tabindex="-1"
        aria-labelledby="modal_batch_export_label"
        aria-hidden="true"
        wire:ignore
    >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_batch_export_label">
                        @lang('Selecteer studenten')
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form
                        id="form_batch_export"
                        action="{{ route('executions.batch-export.store', $this->execution) }}"
                        method="post"
                    >
                        @csrf
                        <div>
                            <label class="form-label" for="input_students">@lang('Studenten')</label>
                            @foreach ($this->execution->students as $student)
                                <div class="form-check">
                                    <input
                                        class="form-check-input"
                                        type="checkbox"
                                        name="students[]"
                                        value="{{ $student->student->sid }}"
                                        id="input_students_{{ $student->student->sid }}"
                                    />
                                    <label class="form-check-label" for="input_students_{{ $student->student->sid }}">
                                        {{ $student->student->getFullName() }}
                                    </label>
                                </div>
                            @endforeach

                            <div class="form-text">
                                @lang('Selecteer van welke studenten de resultaten geëxporteerd moeten worden')
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                        @lang('Annuleren')
                    </button>
                    <x-primary-button type="submit" form="form_batch_export">
                        @lang('Start export')
                    </x-primary-button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function doExport() {
            const message = 'Weet je zeker dat je de resultaten wilt exporten?';
            confirm(message) && document.querySelector('#form_export').submit();
        }
    </script>
</div>
