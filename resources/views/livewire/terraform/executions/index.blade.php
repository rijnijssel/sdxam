<div>
    <div class="table-padded table-responsive-md mb-3">
        <table class="table">
            <thead>
                <tr>
                    <th>@lang('Naam')</th>
                    <th>@lang('Stel bloot aan Terraform')</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($executions as $execution)
                    <tr>
                        <td>
                            {{ $execution->name }}
                        </td>
                        <td>
                            <input type="checkbox" value="{{ $execution->slug }}" wire:model="selected" />
                        </td>
                        <td class="text-end">
                            <a href="{{ route('executions.show', $execution) }}">
                                @lang('Bekijken')
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-grid gap-2">
        <button class="btn btn-primary" wire:click="create">
            @lang('Opslaan')
        </button>
    </div>
</div>
