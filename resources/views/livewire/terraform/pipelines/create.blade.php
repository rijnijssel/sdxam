<x-card>
    <x-header title="{{ __('GitLab CI/CD') }}" />

    <p>
        @lang('Start een GitLab CI/CD pipeline om de Terraform configuratie toe te passen.')
    </p>

    <form wire:submit="store" class="d-flex flex-row gap-3 align-items-center">
        <button type="submit" class="d-inline-block btn btn-primary">
            @lang('Start pipeline')
        </button>

        @if ($pipelineUrl)
            <div class="d-inline-flex flex-column" wire:poll.visible.10s>
                <a href="{{ $pipelineUrl }}" target="_blank">
                    @lang('Bekijk pipeline')
                </a>

                <span class="small text-muted align-middle" title="{{ $pipelineDate->toDateTimeLocalString() }}">
                    @lang(':date gestart', ['date' => $pipelineDate->diffForHumans()])
                </span>
            </div>
        @endif
    </form>
</x-card>
