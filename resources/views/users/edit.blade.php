<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-breadcrumbs />

                <x-card>
                    <x-header title="Gebruiker bewerken" />

                    <form action="{{ route('users.update', $user) }}" method="post">
                        @csrf
                        @method('put')

                        <x-form.head title="Algemene informatie" />

                        <x-input.text title="{{ __('Naam') }}" name="name" value="{{ $user->name }}" required />
                        <x-input.text title="{{ __('E-mail') }}" name="email" value="{{ $user->email }}" required />

                        <div class="form-check mb-3">
                            <input
                                id="is_admin"
                                @class(['form-check-input', 'is-invalid' => $errors->has('is_admin')])
                                type="checkbox"
                                role="switch"
                                name="is_admin"
                                @checked(old('is_admin', $user->is_admin))
                            />
                            <label class="form-check-label" for="is_admin">Administrator</label>
                            <x-input.error name="is_admin" />
                        </div>

                        <x-form.edit-actions cancel="{{ route('users.show', $user) }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>

    <x-form.delete-modal route="{{ route('users.destroy', $user) }}">
        <x-slot name="title">
            @lang('Gebruiker verwijderen')
        </x-slot>
        Weet je zeker dat je "{{ $user->name }}" wilt verwijderen?
    </x-form.delete-modal>
</x-layouts.app>
