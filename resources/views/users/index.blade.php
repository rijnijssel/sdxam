<x-layouts.app>
    <div class="container-lg">
        <x-breadcrumbs />

        <x-header :title="__('Gebruikers')">
            <a
                class="btn btn-sm btn-primary"
                href="{{ route('users.create') }}"
                data-toggle="tooltip"
                title="Toevoegen van gebruiker"
            >
                @lang('Toevoegen')
            </a>
        </x-header>

        <div class="row">
            <div class="col">
                <div class="table-padded table-responsive-md">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Naam')</th>
                                <th scope="col">@lang('Administrator')</th>
                                <th scope="col" class="text-end">
                                    <span class="visually-hidden">@lang('Acties')</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>
                                        <div class="ps-3 py-2 d-flex align-items-center gap-4">
                                            <div class="flex-shrink-0">
                                                <img
                                                    class="rounded-circle"
                                                    src="{{ $user->getAvatarUrl() }}"
                                                    height="32"
                                                    width="32"
                                                    alt=""
                                                />
                                            </div>
                                            <div>
                                                <a class="d-block fw-bold" href="{{ route('users.show', $user) }}">
                                                    {{ $user->name }}
                                                </a>
                                                <span class="d-block">{{ $user->email }}</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if ($user->is_admin)
                                            <span class="badge bg-success">@lang('Ja')</span>
                                        @else
                                            <span class="badge bg-danger">@lang('Nee')</span>
                                        @endif
                                    </td>
                                    <td class="text-end">
                                        <a
                                            class="btn btn-outline-primary btn-sm"
                                            href="{{ route('users.edit', $user) }}"
                                        >
                                            @lang('Bewerken')
                                        </a>

                                        @can('update', $user)
                                            <div class="btn-group">
                                                <button
                                                    class="btn btn-outline-secondary btn-sm dropdown-toggle"
                                                    type="button"
                                                    data-bs-toggle="dropdown"
                                                    aria-expanded="false"
                                                >
                                                    @lang('Acties')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    @can('update', $user)
                                                        <form
                                                            hidden
                                                            id="password_{{ $user->id }}"
                                                            action="{{ route('password.email') }}"
                                                            method="post"
                                                        >
                                                            @csrf
                                                            <input
                                                                name="email"
                                                                value="{{ $user->email }}"
                                                                type="hidden"
                                                            />
                                                        </form>

                                                        <button
                                                            class="dropdown-item"
                                                            type="submit"
                                                            form="password_{{ $user->id }}"
                                                        >
                                                            @lang('Wachtwoord herstellen')
                                                        </button>
                                                    @endcan

                                                    @can('impersonate', $user)
                                                        <form
                                                            hidden
                                                            id="impersonate_{{ $user->id }}"
                                                            action="{{ route('impersonated-session.store', $user) }}"
                                                            method="post"
                                                        >
                                                            @csrf
                                                        </form>

                                                        <button
                                                            class="dropdown-item"
                                                            type="submit"
                                                            form="impersonate_{{ $user->id }}"
                                                        >
                                                            Imiteren
                                                        </button>
                                                    @endcan
                                                </div>
                                            </div>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
