<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-breadcrumbs />

                <x-card>
                    <x-header title="Gebruiker toevoegen" />

                    <form action="{{ route('users.store') }}" method="post">
                        @csrf

                        <x-form.head :title="__('Persoonlijk')" />

                        <x-input.text :title="__('Naam')" name="name" required />
                        <x-input.text :title="__('E-mail')" name="email" required />

                        <div class="form-check mb-3">
                            <input
                                id="is_admin"
                                @class(['form-check-input', 'is-invalid' => $errors->has('is_admin')])
                                type="checkbox"
                                role="switch"
                                name="is_admin"
                                @checked(old('is_admin', false))
                            />
                            <label class="form-check-label" for="is_admin">Administrator</label>
                            <x-input.error name="is_admin" />
                        </div>

                        <x-form.head :title="__('Beveiliging')" />

                        <p class="form-text">
                            @lang('Laat het wachtwoord leeg, om automatisch een wachtwoord herstel email te sturen.')
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <x-input.password
                                    :title="__('Wachtwoord')"
                                    name="password"
                                    autocomplete="new-password"
                                />
                            </div>
                            <div class="col-md-6">
                                <x-input.password
                                    :title="__('Wachtwoord bevestiging')"
                                    name="password_confirmation"
                                    autocomplete="new-password"
                                />
                            </div>
                        </div>

                        <x-form.create-actions :cancel="route('users.index')" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
