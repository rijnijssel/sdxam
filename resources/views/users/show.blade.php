<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <x-breadcrumbs />

                <x-card>
                    <x-header title="Gebruiker">
                        @can('impersonate', $user)
                            <button
                                class="btn btn-sm btn-outline-secondary"
                                type="submit"
                                form="impersonate_{{ $user->id }}"
                            >
                                Imiteren
                            </button>
                        @endcan

                        <a
                            class="btn btn-sm btn-primary"
                            href="{{ route('users.edit', $user) }}"
                            data-toggle="tooltip"
                            title="Bewerken van gebruiker"
                        >
                            @lang('Bewerken')
                        </a>

                        @can('impersonate', $user)
                            <form
                                id="impersonate_{{ $user->id }}"
                                class="d-none"
                                action="{{ route('impersonated-session.store', $user) }}"
                                method="post"
                            >
                                @csrf
                            </form>
                        @endcan
                    </x-header>

                    <x-form.head title="Algemene informatie" />

                    <x-input.text title="{{ __('Naam') }}" name="name" value="{{ $user->name }}" readonly />
                    <x-input.text title="{{ __('E-mail') }}" name="email" value="{{ $user->email }}" readonly />

                    <div class="form-check mt-3">
                        <input
                            id="is_admin"
                            @class(['form-check-input', 'is-invalid' => $errors->has('is_admin')])
                            type="checkbox"
                            role="switch"
                            name="is_admin"
                            @checked($user->is_admin)
                            readonly
                            onclick="return false"
                        />
                        <label class="form-check-label" for="is_admin">Administrator</label>
                        <x-input.error name="is_admin" />
                    </div>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
