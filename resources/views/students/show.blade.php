<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center g-3">
            <div class="col-md-10 col-lg-8">
                <x-card>
                    <x-header title="Student">
                        @can('update', $student)
                            <a
                                class="btn btn-sm btn-primary"
                                href="{{ route('students.edit', $student) }}"
                                data-toggle="tooltip"
                                title="Bewerken van uitvoering"
                            >
                                @lang('Bewerken')
                            </a>
                        @endcan
                    </x-header>

                    <x-form.head title="Algemene informatie" />

                    <div class="row">
                        <div class="col-md-3 col-lg-2">
                            <x-label for="sid">@lang('Studentnummer')</x-label>
                            <x-input id="sid" name="sid" :value="$student->sid" readonly />
                        </div>
                        <div class="col-md-3 col-lg-2">
                            <x-label for="cohort">@lang('Cohort')</x-label>
                            <x-input id="cohort" name="cohort" :value="$student->cohort" readonly />
                        </div>
                        <div class="col-md-6 col-lg-8">
                            <x-label for="email">@lang('E-mail')</x-label>
                            <x-input id="email" name="email" :value="$student->email" readonly />
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-4">
                            <x-label for="first_name">@lang('Voornaam')</x-label>
                            <x-input id="first_name" name="first_name" :value="$student->first_name" readonly />
                        </div>
                        <div class="col-md-2">
                            <x-label for="middle_name">@lang('Tussenvoegsel')</x-label>
                            <x-input id="middle_name" name="middle_name" :value="$student->middle_name" readonly />
                        </div>
                        <div class="col-md-6">
                            <x-label for="last_name">@lang('Achternaam')</x-label>
                            <x-input id="last_name" name="last_name" :value="$student->last_name" readonly />
                        </div>
                    </div>
                </x-card>
            </div>
            <div class="col-md-10 col-lg-8">
                <x-card>
                    <x-header title="Vorige uitvoeringen"></x-header>
                    @empty($student->executionStudents)
                        <p>@lang('Geen uitvoeringen')</p>
                    @else
                        <ul class="mb-0">
                            @foreach ($student->executionStudents as $es)
                                <li>
                                    <a class="fw-bold" href="{{ route('executions.students.edit', $es->execution) }}">
                                        {{ $es->execution->name }} ({{ $es->execution->localizedStartDate() }})
                                    </a>
                                    <ul>
                                        @foreach ($es->executionStudentTasks->groupBy('context_id') as $est)
                                            <li>
                                                {{ sprintf('%s: %s', $est->pluck('task.name')->join(', '), $est->value('context.name')) }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    @endempty
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
