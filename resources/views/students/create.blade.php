<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8">
                <x-card>
                    <x-header title="Student toevoegen" />

                    <form action="{{ route('students.store') }}" method="post">
                        @csrf

                        <x-form.head title="Algemene informatie" />

                        <div class="row">
                            <div class="col-md-3 col-lg-2">
                                <x-input.text title="{{ __('Studentnummer') }}" name="sid" required autofocus />
                            </div>
                            <div class="col-md-3 col-lg-2">
                                <x-input.text
                                    title="{{ __('Cohort') }}"
                                    name="cohort"
                                    minlength="4"
                                    maxlength="4"
                                    required
                                />
                            </div>
                            <div class="col-md-6 col-lg-8">
                                <x-input.text title="{{ __('E-mail') }}" name="email" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <x-input.text title="{{ __('Voornaam') }}" name="first_name" required />
                            </div>
                            <div class="col-md-2">
                                <x-input.text title="{{ __('Tussenvoegsel') }}" name="middle_name" />
                            </div>
                            <div class="col-md-6">
                                <x-input.text title="{{ __('Achternaam') }}" name="last_name" required />
                            </div>
                        </div>

                        <x-form.create-actions cancel="{{ route('students.index') }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
