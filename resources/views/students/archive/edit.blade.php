<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-6">
                <x-card>
                    <x-header title="Studenten archiveren" />
                    <form action="{{ route('students.archive.update') }}" method="post">
                        @csrf
                        @method('put')

                        <div class="mb-3 d-flex flex-column">
                            <label class="form-label">@lang('Studenten')</label>
                            @foreach ($students as $student)
                                <div class="form-check">
                                    <input
                                        id="checkbox_{{ $student->sid }}"
                                        @class(['form-check-input', 'is-invalid' => $errors->hasAny(['students', 'students.*'])])
                                        type="checkbox"
                                        name="students[]"
                                        value="{{ $student->sid }}"
                                        @checked($student->isGraduated())
                                    />
                                    <label class="form-check-label" for="checkbox_{{ $student->sid }}">
                                        {{ $student->getFullName() }}
                                    </label>
                                </div>
                            @endforeach

                            <x-input.error name="students" />
                            <x-input.error name="students.*" />
                        </div>

                        <x-form.row>
                            <button type="submit" class="btn btn-success">@lang('Opslaan')</button>

                            <x-slot name="right">
                                <a href="{{ route('students.index') }}" class="btn btn-outline-secondary">
                                    @lang('Annuleren')
                                </a>
                            </x-slot>
                        </x-form.row>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-layouts.app>
