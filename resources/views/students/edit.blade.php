<x-layouts.app>
    <div class="container">
        <x-breadcrumbs />

        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8">
                <x-card>
                    <x-header title="Student bewerken" />

                    <form action="{{ route('students.update', $student) }}" method="post">
                        @csrf
                        @method('put')

                        <x-form.head title="Algemene informatie" />

                        <div class="row">
                            <div class="col-md-3 col-lg-2">
                                <x-input.text
                                    title="{{ __('Studentnummer') }}"
                                    name="sid"
                                    value="{{ $student->sid }}"
                                />
                            </div>
                            <div class="col-md-3 col-lg-2">
                                <x-input.text
                                    title="{{ __('Cohort') }}"
                                    name="cohort"
                                    minlength="4"
                                    maxlength="4"
                                    value="{{ $student->cohort }}"
                                    required
                                />
                            </div>
                            <div class="col-md-6 col-lg-8">
                                <x-input.text title="{{ __('E-mail') }}" name="email" value="{{ $student->email }}" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <x-input.text
                                    title="{{ __('Voornaam') }}"
                                    name="first_name"
                                    value="{{ $student->first_name }}"
                                    required
                                />
                            </div>
                            <div class="col-md-2">
                                <x-input.text
                                    title="{{ __('Tussenvoegsel') }}"
                                    name="middle_name"
                                    value="{{ $student->middle_name }}"
                                />
                            </div>
                            <div class="col-md-6">
                                <x-input.text
                                    title="{{ __('Achternaam') }}"
                                    name="last_name"
                                    value="{{ $student->last_name }}"
                                    required
                                />
                            </div>
                        </div>

                        <x-form.edit-actions cancel="{{ route('students.show', $student) }}" />
                    </form>
                </x-card>
            </div>
        </div>
    </div>

    <x-form.delete-modal route="{{ route('students.destroy', $student) }}">
        <x-slot name="title">
            @lang('Student verwijderen')
        </x-slot>
        Weet je zeker dat je "{{ $student->getFullName() }}" wilt verwijderen?
    </x-form.delete-modal>
</x-layouts.app>
