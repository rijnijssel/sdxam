<x-layouts.app>
    <div class="container-lg">
        <x-breadcrumbs />

        <x-header title="{{ __('Studenten') }}">
            @can('students.archive')
                <a
                    class="btn btn-sm btn-outline-secondary"
                    href="{{ route('students.archive.edit') }}"
                    data-toggle="tooltip"
                    title="Verbergen van studenten"
                >
                    @lang('Verbergen')
                </a>
            @endcan

            @can('create', \App\Models\Student::class)
                <a
                    class="btn btn-sm btn-primary"
                    href="{{ route('students.create') }}"
                    data-toggle="tooltip"
                    title="Toevoegen van student"
                >
                    @lang('Toevoegen')
                </a>
            @endcan
        </x-header>

        <div class="row justify-content-center">
            <div class="col">
                <div class="table-padded table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col" class="table-shrink-col">@lang('SID')</th>
                                <th scope="col">@lang('Voornaam')</th>
                                <th scope="col">@lang('Tussenvoegsel')</th>
                                <th scope="col">@lang('Achternaam')</th>
                                <th scope="col">@lang('Cohort')</th>
                                <th scope="col" class="text-end">
                                    <span class="visually-hidden">@lang('Acties')</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($students as $student)
                                <tr>
                                    <th scope="row">
                                        <a href="{{ route('students.show', $student) }}">{{ $student->sid }}</a>
                                    </th>
                                    <td>{{ $student->first_name }}</td>
                                    <td>{{ $student->middle_name }}</td>
                                    <td>{{ $student->last_name }}</td>
                                    <td>{{ $student->cohort }}</td>
                                    <td class="text-end">
                                        @can('update', $student)
                                            <a href="{{ route('students.edit', $student) }}">@lang('Bewerken')</a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
