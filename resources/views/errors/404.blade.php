<x-layouts.base>
    <div class="h-100 d-flex justify-content-center align-items-center">
        <div class="d-flex flex-column align-items-center">
            <h3 class="text-center">
                @lang('Pagina niet gevonden')
            </h3>
            <hr class="w-100" />
            <a class="link-primary" href="{{ url('/') }}">
                @lang('Terug naar applicatie')
            </a>
        </div>
    </div>
</x-layouts.base>
