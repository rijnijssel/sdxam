# [3.8.0](https://gitlab.com/rijnijssel/sdxam/compare/v3.7.2...v3.8.0) (2025-01-05)


### Features

* add forge deployment script to version control ([309c0fc](https://gitlab.com/rijnijssel/sdxam/commit/309c0fca4acb9a50974eca0da386f62e46679c6c))

## [3.7.2](https://gitlab.com/rijnijssel/sdxam/compare/v3.7.1...v3.7.2) (2025-01-03)


### Bug Fixes

* pint ([73b1d41](https://gitlab.com/rijnijssel/sdxam/commit/73b1d41709d8e35793eda922c9bf304c1f8ebd12))
* rector and pint ([8ef8805](https://gitlab.com/rijnijssel/sdxam/commit/8ef8805572b4545557f7acc852eaaf94fb4d8ab8))
