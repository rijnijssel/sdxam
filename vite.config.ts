import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import sri from "vite-plugin-manifest-sri";

export default defineConfig({
    resolve: {
        alias: {
            "@": "/resources/ts",
        },
    },
    plugins: [
        laravel({
            input: ["resources/ts/app.ts"],
        }),
        sri(),
    ],
});
