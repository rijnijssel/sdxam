<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop procedure if exists generate_process_score_table;');
        DB::unprepared(/* @lang MySQL */ <<<'SQL'
            create procedure generate_process_score_table(in p_process_id int)
            begin
                declare points int default 0;
                declare max_points int default 0;

                create temporary table t_score_table
                (
                    points int,
                    score  double(3, 1)
                );

                select calculate_max_points(p_process_id) into max_points;

                while points <= max_points
                    do
                        insert into t_score_table values (points, calculate_score(points, max_points));
                        set points = points + 1;
                    end while;

                select * from t_score_table;
                drop temporary table if exists t_score_table;
            end;
            SQL
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop procedure if exists generate_process_score_table;');
    }
};
