<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::table('tasks', function (Blueprint $table): void {
            $table->dropUnique('tasks_slug_unique');

            $table->unique(['exam_id', 'slug'], 'tasks_unique');
        });
    }
};
