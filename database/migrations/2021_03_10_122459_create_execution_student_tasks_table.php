<?php

use App\Models\Examiner;
use App\Models\ExecutionStudent;
use App\Models\Task;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('execution_student_tasks', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignIdFor(ExecutionStudent::class, 'execution_student_id')
                ->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Task::class, 'task_id')
                ->constrained();
            $table->foreignIdFor(Examiner::class, 'client_id')
                ->nullable()
                ->constrained('examiners');
            $table->foreignIdFor(Examiner::class, 'supervisor_id')
                ->nullable()
                ->constrained('examiners');

            $table->unique(['execution_student_id', 'task_id'], 'no_duplicate_tasks_per_student');
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('execution_student_tasks');
    }
};
