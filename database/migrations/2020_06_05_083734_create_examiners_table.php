<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('examiners', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->string('name', 50);
            $table->string('abbreviation', 5)->unique();
            $table->foreignIdFor(User::class, 'user_id')->constrained();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('examiners');
    }
};
