<?php

use App\Models\Examiner;
use App\Models\Group;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('group_examiners', function (Blueprint $table): void {
            $table->foreignIdFor(Group::class)->constrained();
            $table->foreignIdFor(Examiner::class)->constrained();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('group_examiners');
    }
};
