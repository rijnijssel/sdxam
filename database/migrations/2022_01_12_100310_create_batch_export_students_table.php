<?php

use App\Models\BatchExport;
use App\Models\Student;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('batch_export_students', function (Blueprint $table): void {
            $table->foreignIdFor(BatchExport::class, 'batch_export_id')->constrained();
            $table->foreignIdFor(Student::class, 'student_id')->constrained();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('batch_export_students');
    }
};
