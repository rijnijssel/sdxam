<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_max_points;');
        DB::unprepared(/* @lang MySQL */ <<<'SQL'
            create function calculate_max_points(p_process_id int)
                returns int
                reads sql data
            begin
                return (
                    select sum(points.max_points)
                    from (select MAX(rti.points) as max_points
                          from rubric_task_items rti
                                   join rubric_tasks rt on rt.id = rti.rubric_task_id
                                   join rubrics r on r.id = rt.rubric_id
                                   join processes p on p.id = r.process_id
                          where p.id = p_process_id
                          group by rt.id
                         ) as points
                );
            end;
            SQL
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_max_points;');
    }
};
