<?php

use App\Models\RubricTask;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('rubric_task_items', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignIdFor(RubricTask::class, 'rubric_task_id')->constrained();
            $table->tinyInteger('points');
            $table->string('text', 1000);

            $table->unique(['rubric_task_id', 'points']);
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('rubric_task_items');
    }
};
