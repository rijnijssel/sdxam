<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ <<<'SQL'
            create or replace view execution_scores as
            select e.id as execution_id,
                   es.id as execution_student_id,
                   t.id as task_id,
                   p.id as process_id,
                   calculate_process_points(es.id, p.id) as points,
                   calculate_process_score(es.id, p.id)  as score
            from executions e
                     join execution_students es on e.id = es.execution_id
                     join execution_student_tasks est on es.id = est.execution_student_id
                     join tasks t on t.id = est.task_id
                     join processes p on t.id = p.task_id;
            SQL
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop view execution_scores');
    }
};
