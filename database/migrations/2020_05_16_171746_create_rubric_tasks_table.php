<?php

use App\Models\Rubric;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('rubric_tasks', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->string('name', 200);
            $table->string('criteria', 100);
            $table->boolean('required')->default(false);
            $table->foreignIdFor(Rubric::class, 'rubric_id')->constrained();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('rubric_tasks');
    }
};
