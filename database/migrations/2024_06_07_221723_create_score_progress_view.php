<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ <<<'SQL'
            create or replace view score_progress
            as
            select e.id                         as e_id,
                   es.id                        as es_id,
                   est.id                       as est_id,
                   p.id                         as process_id,
                   get_rubric_task_count(p.id)  as rt_count,
                   count(s.client_score_id)     as client_count,
                   count(s.supervisor_score_id) as supervisor_count
            from executions e
                     join execution_students es on e.id = es.execution_id
                     join execution_student_tasks est on es.id = est.execution_student_id
                     join tasks t on t.id = est.task_id
                     join processes p on t.id = p.task_id
                     join rubrics r on p.id = r.process_id
                     join rubric_tasks rt on r.id = rt.rubric_id
                     left join scores s on est.id = s.execution_student_task_id and rt.id = s.rubric_task_id
            group by es.id, p.id
            order by p.id;
            SQL
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop view score_progress');
    }
};
