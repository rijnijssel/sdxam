<?php

use App\Models\Execution;
use App\Models\Student;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('execution_students', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignIdFor(Execution::class, 'execution_id')->constrained();
            $table->foreignIdFor(Student::class, 'student_id')->constrained();

            $table->unique(['execution_id', 'student_id'], 'unique_student_per_execution');
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('execution_students');
    }
};
