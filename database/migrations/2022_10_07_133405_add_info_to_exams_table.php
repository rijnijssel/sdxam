<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('exams', function (Blueprint $table): void {
            $table->string('type')->nullable();
            $table->string('course')->nullable();
            $table->string('cohort')->nullable();
            $table->string('crebo', 10)->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('exams', function (Blueprint $table): void {
            $table->dropColumn(['type', 'course', 'cohort', 'crebo']);
        });
    }
};
