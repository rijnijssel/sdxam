<?php

use App\Models\Execution;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('batch_exports', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignIdFor(Execution::class, 'execution_id')->constrained();
            $table->string('directory', 200)->unique();
            $table->dateTime('started_at');
            $table->dateTime('finished_at')->nullable();
            $table->boolean('failed')->default(false);
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('batch_exports');
    }
};
