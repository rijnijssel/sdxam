<?php

use App\Models\Exam;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('executions', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->string('name', 100);
            $table->string('slug', 100)->unique();
            $table->date('start_date')->nullable();
            $table->foreignIdFor(Exam::class, 'exam_id')->constrained();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('executions');
    }
};
