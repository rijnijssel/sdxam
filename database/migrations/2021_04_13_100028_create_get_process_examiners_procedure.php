<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop procedure if exists get_process_examiners');
        DB::unprepared(/* @lang MySQL */ <<<'PHP'
            create procedure get_process_examiners(in p_execution_student_id int)
            begin
                select distinct (e.id), e.name, e.abbreviation
                from (
                         select e.id, e.name, e.abbreviation
                         from execution_students es
                                  join execution_student_tasks est on es.id = est.execution_student_id
                                  join examiners e on e.id = est.client_id
                         where es.id = p_execution_student_id
                         union all
                         select e.id, e.name, e.abbreviation
                         from execution_students es
                                  join execution_student_tasks est on es.id = est.execution_student_id
                                  join examiners e on e.id = est.supervisor_id
                         where es.id = p_execution_student_id
                     ) e;
            end;
            PHP
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop procedure if exists get_process_examiners');
    }
};
