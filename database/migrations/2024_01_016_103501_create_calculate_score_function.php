<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_score;');
        DB::unprepared(/* @lang MySQL */ <<<'SQL'
            create function calculate_score(points double, max_points double)
                returns decimal(3, 1)
                reads sql data
            begin
                return CEILING((9 * points / max_points + 1) * 10) / 10;
            end;
            SQL
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_score;');
    }
};
