<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('terraform_executions', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();
            $table->foreignId('execution_id')->constrained('executions')->cascadeOnDelete();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('terraform_executions');
    }
};
