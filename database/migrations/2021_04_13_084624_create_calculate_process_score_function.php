<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_process_score');
        DB::unprepared(/* @lang MySQL */ <<<'PHP'
            create function calculate_process_score(p_execution_student_id int, p_process_id int) returns double(3, 1)
                reads sql data
            begin
                set @points = calculate_process_points(p_execution_student_id, p_process_id);

                if @points is null then
                    return null;
                end if;

                if @points < 0 then
                    return -1;
                end if;

                set @max_points = calculate_max_points(p_process_id);
                return calculate_score(@points, @max_points);
            end;
            PHP
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_process_score');
    }
};
