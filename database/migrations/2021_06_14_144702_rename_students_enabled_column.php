<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::table('students', function (Blueprint $table): void {
            $table->renameColumn('enabled', 'graduated');
        });

        $this->invert();
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        $this->invert();

        Schema::table('students', function (Blueprint $table): void {
            $table->renameColumn('graduated', 'enabled');
        });
    }

    private function invert(): void
    {
        DB::update(// @lang MySQL
            'UPDATE students SET graduated = CASE graduated WHEN 0 THEN 1 WHEN 1 THEN 0 ELSE graduated END'
        );
    }
};
