<?php

use App\Models\Examiner;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('annotations', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignIdFor(Examiner::class, 'examiner_id')->nullable()->constrained('examiners');
            $table->morphs('annotatable');

            $table->unique(['annotatable_type', 'annotatable_id', 'examiner_id'], 'annotation_examiner_unique');

            $table->text('text');
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('annotations');
    }
};
