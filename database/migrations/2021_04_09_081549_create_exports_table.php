<?php

use App\Models\Execution;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('exports', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignIdFor(Execution::class, 'execution_id')->constrained();
            $table->string('filename');
            $table->dateTime('started_at');
            $table->dateTime('finished_at')->nullable();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('exports');
    }
};
