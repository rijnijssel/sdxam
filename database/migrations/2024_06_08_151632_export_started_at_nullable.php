<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('exports', function (Blueprint $table): void {
            $table->dateTime('started_at')->nullable()->change();
        });

        Schema::table('batch_exports', function (Blueprint $table): void {
            $table->dateTime('started_at')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table('exports', function (Blueprint $table): void {
            $table->dateTime('started_at')->nullable(false)->change();
        });

        Schema::table('batch_exports', function (Blueprint $table): void {
            $table->dateTime('started_at')->nullable(false)->change();
        });
    }
};
