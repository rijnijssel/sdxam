<?php

use App\Models\Context;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::table('execution_student_tasks', function (Blueprint $table): void {
            $table->foreignIdFor(Context::class, 'context_id')->nullable()->constrained('contexts');
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::table('execution_student_tasks', function (Blueprint $table): void {
            $table->dropConstrainedForeignIdFor(Context::class, 'context_id');
        });
    }
};
