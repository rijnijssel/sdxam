<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_process_points');
        DB::unprepared(/* @lang MySQL */ <<<'PHP'
            create function calculate_process_points(p_execution_student_id int, p_process_id int) returns int
                reads sql data
            begin
                 set @filled_points = (
                    select count(*)
                    from scores s
                             join rubric_tasks t on t.id = s.rubric_task_id
                             join rubrics r on r.id = t.rubric_id
                             join execution_student_tasks est on est.id = s.execution_student_task_id
                             join execution_students es on es.id = est.execution_student_id
                    where es.id = p_execution_student_id
                      and r.process_id = p_process_id
                      and s.score_id is not null
                );

                set @missing = (
                    select (count(*) - @filled_points) as missing
                    from rubric_tasks rt
                             join rubrics r on rt.rubric_id = r.id
                             join processes p on p.id = r.process_id
                    where p.id = p_process_id
                );

                if @missing > 0 then
                    return null;
                end if;

                set @required_points = (
                    select count(*)
                    from processes p
                             join rubrics r on p.id = r.process_id
                             join rubric_tasks rt on r.id = rt.rubric_id
                    where rt.required
                      and p.id = p_process_id
                );

                set @requirements_achieved = (
                    select count(*) = @required_points as required
                    from processes p
                             join rubrics r on p.id = r.process_id
                             join rubric_tasks rt on r.id = rt.rubric_id
                             join rubric_task_items rti on rt.id = rti.rubric_task_id
                             join scores s on rti.id = s.score_id
                             join execution_student_tasks est on est.id = s.execution_student_task_id
                             join execution_students es on es.id = est.execution_student_id
                    where rt.required
                      and rti.points > 0
                      and p.id = p_process_id
                      and es.id = p_execution_student_id
                );

                if @requirements_achieved = 0 then
                    return -1;
                end if;

                return (
                    select sum(rti.points) as points
                    from processes p
                             join rubrics r on p.id = r.process_id
                             join rubric_tasks rt on r.id = rt.rubric_id
                             join rubric_task_items rti on rt.id = rti.rubric_task_id
                             join scores s on s.score_id = rti.id
                             join execution_student_tasks est on est.id = s.execution_student_task_id
                             join execution_students es on es.id = est.execution_student_id
                    where p.id = p_process_id
                      and es.id = p_execution_student_id
                );
            end;
            PHP
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists calculate_process_points');
    }
};
