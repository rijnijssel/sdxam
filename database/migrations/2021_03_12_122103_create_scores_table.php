<?php

use App\Models\RubricTask;
use App\Models\RubricTaskItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('scores', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->foreignId('execution_student_task_id');
            $table->foreign('execution_student_task_id')
                ->references('id')->on('execution_student_tasks');

            // this column is pre-generated when making an exam execution
            $table->foreignIdFor(RubricTask::class, 'rubric_task_id')
                ->constrained('rubric_tasks');
            $table->foreignIdFor(RubricTaskItem::class, 'client_score_id')
                ->nullable()
                ->constrained('rubric_task_items');
            $table->foreignIdFor(RubricTaskItem::class, 'supervisor_score_id')
                ->nullable()
                ->constrained('rubric_task_items');
            $table->foreignIdFor(RubricTaskItem::class, 'score_id')
                ->nullable()
                ->constrained('rubric_task_items');
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('scores');
    }
};
