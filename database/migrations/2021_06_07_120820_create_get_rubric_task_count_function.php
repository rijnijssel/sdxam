<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists get_rubric_task_count');
        DB::unprepared(/* @lang MySQL */ <<<'SQL'
            create function get_rubric_task_count(p_process_id bigint)
                returns int
                reads sql data
            begin
                return (
                    select count(rt.id)
                    from processes p
                             join rubrics r on p.id = r.process_id
                             join rubric_tasks rt on r.id = rt.rubric_id
                    where p.id = p_process_id
                );
            end;
        SQL
        );
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        DB::unprepared(/* @lang MySQL */ 'drop function if exists get_rubric_task_count');
    }
};
