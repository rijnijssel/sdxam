<?php

use App\Models\Task;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /** Run the migrations. */
    public function up(): void
    {
        Schema::create('processes', function (Blueprint $table): void {
            $table->id();
            $table->timestamps();

            $table->string('name', 100);
            $table->string('description', 255);
            $table->foreignIdFor(Task::class, 'task_id')->constrained();
        });
    }

    /** Reverse the migrations. */
    public function down(): void
    {
        Schema::dropIfExists('processes');
    }
};
