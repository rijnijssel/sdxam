<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        $this->create('John Doe', 'john@sdxam.test', 'DOEJO', true);
    }

    public function create(string $name, string $email, string $abbreviation, bool $admin = false): void
    {
        $user = User::query()->updateOrCreate([
            'name' => $name,
            'email' => $email,
            'is_admin' => $admin,
        ], [
            'password' => Hash::make('password'),
        ]);
        $user->examiner()->updateOrCreate(['name' => $name, 'abbreviation' => $abbreviation]);
    }
}
