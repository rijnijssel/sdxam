<?php

namespace Database\Seeders;

use App\Models\Context;
use Illuminate\Database\Seeder;

class ContextSeeder extends Seeder
{
    /** Run the database seeds. */
    public function run(): void
    {
        Context::factory(5)->create();
    }
}
