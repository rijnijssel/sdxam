<?php

namespace Database\Seeders;

use App\Models\Exam;
use App\Models\Process;
use App\Models\Rubric;
use App\Models\RubricTask;
use App\Models\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ExamSeeder extends Seeder
{
    /** Run the database seeds. */
    public function run(): void
    {
        // generate the exam rubrics for both crebo's
        $this->generate(Exam::query()->updateOrCreate([
            'name' => 'Crebo 25187',
        ], [
            'type' => 'Proeve van Bekwaamheid (praktijkexamen in de beroepspraktijk)',
            'course' => 'Applicatieontwikkeling',
            'cohort' => '2016 t/m 2019',
            'crebo' => '25187',
        ]), config('exams.25187'));

        $this->generate(Exam::query()->updateOrCreate([
            'name' => 'Crebo 25604',
        ], [
            'type' => 'Examenproject',
            'course' => 'Softwaredevelopment',
            'cohort' => '2020 en verder',
            'crebo' => '25604',
        ]), config('exams.25604'));
    }

    /**
     * Process the data file.
     *
     * @param  array<string, mixed>  $tasks
     */
    private function generate(Exam $exam, array $tasks): void
    {
        foreach ($tasks as $task) {
            /** @var Task $taskModel */
            $taskModel = $exam->tasks()->updateOrCreate(
                ['name' => data_get($task, 'name')],
                [
                    'code' => data_get($task, 'code'),
                    'description' => data_get($task, 'description'),
                ]
            );

            /** @var array<string, mixed> $processes */
            $processes = data_get($task, 'processes');

            foreach ($processes as $process) {
                /** @var Process $processModel */
                $processModel = $taskModel->processes()->updateOrCreate(
                    ['name' => data_get($process, 'name')],
                    ['description' => data_get($process, 'description')]
                );

                /** @var array<string, mixed> $rubrics */
                $rubrics = data_get($process, 'rubrics');

                foreach ($rubrics as $rubric) {
                    /** @var Rubric $rubricModel */
                    $rubricModel = $processModel->rubrics()->firstOrCreate([
                        'title' => data_get($rubric, 'name'),
                    ]);

                    /** @var array<string, mixed> $rows */
                    $rows = data_get($rubric, 'rows');

                    foreach ($rows as $row) {
                        /** @var RubricTask $rubricTaskModel */
                        $rubricTaskModel = $rubricModel->tasks()->updateOrCreate(
                            ['criteria' => data_get($row, 'criteria')],
                            [
                                'name' => data_get($row, 'name'),
                                'required' => data_get($row, 'required', false),
                            ]
                        );

                        /** @var array<string, string> $cells */
                        $cells = data_get($row, 'cells');

                        foreach ($cells as $key => $cell) {
                            $rubricTaskModel->items()->updateOrCreate(
                                ['points' => $key],
                                ['text' => Str::replace("\n", '<br>', $cell)]
                            );
                        }
                    }
                }
            }
        }
    }
}
