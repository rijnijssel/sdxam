<?php

namespace Database\Seeders;

use App\Models\Exam;
use App\Models\Execution;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class ExecutionSeeder extends Seeder
{
    /** Run the database seeds. */
    public function run(): void
    {
        $start = Date::parse('last monday of august');

        $exam = Exam::query()->where('name', 'Crebo 25187')->firstOrFail();

        Execution::query()->updateOrCreate(['name' => 'Week 10'], ['start_date' => $start->clone()->addWeeks(10), 'exam_id' => $exam->id]);
        Execution::query()->updateOrCreate(['name' => 'Week 20'], ['start_date' => $start->clone()->addWeeks(20), 'exam_id' => $exam->id]);
        Execution::query()->updateOrCreate(['name' => 'Week 30'], ['start_date' => $start->clone()->addWeeks(30), 'exam_id' => $exam->id]);
        Execution::query()->updateOrCreate(['name' => 'Week 38'], ['start_date' => $start->clone()->addWeeks(38), 'exam_id' => $exam->id]);
    }
}
