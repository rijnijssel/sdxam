<?php

namespace Database\Factories;

use App\Models\Execution;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<Execution>
 */
class ExecutionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'name' => fake()->unique()->words(3, true),
            'start_date' => fake()->date(),
        ];
    }
}
