<?php

namespace Database\Factories;

use App\Models\RubricTask;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<RubricTask>
 */
class RubricTaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'name' => fake()->lexify('???'),
            'criteria' => fake()->sentence(),
        ];
    }
}
