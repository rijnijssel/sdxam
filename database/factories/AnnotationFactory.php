<?php

namespace Database\Factories;

use App\Models\Annotation;
use App\Models\Examiner;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<Annotation>
 */
class AnnotationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'examiner_id' => Examiner::factory(),
            'text' => fake()->paragraph(),
        ];
    }
}
