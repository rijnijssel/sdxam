<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Execution;
use App\Models\TerraformExecution;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/** @extends Factory<TerraformExecution> */
class TerraformExecutionFactory extends Factory
{
    protected $model = TerraformExecution::class;

    #[Override]
    public function definition(): array
    {
        return [
            'execution_id' => Execution::factory(),
        ];
    }
}
