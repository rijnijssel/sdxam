<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Override;

/**
 * @extends Factory<Student>
 */
class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        $middleName = null;
        $lastName = Str::of(fake()->lastName);

        if ($lastName->split('/\s+/')->count() > 1) {
            $middleName = (string) $lastName->before(' ');
            $lastName = (string) $lastName->afterLast(' ');
        }

        return [
            'sid' => fake()->unique()->numerify('00######'),
            'first_name' => fake()->firstName(),
            'middle_name' => $middleName ?? null,
            'last_name' => (string) $lastName,
            'email' => fake()->safeEmail(),
            'graduated' => fake()->boolean(15),
            'cohort' => fake()->dateTimeBetween('-5 years')->format('Y'),
        ];
    }
}
