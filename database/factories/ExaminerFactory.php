<?php

namespace Database\Factories;

use App\Models\Examiner;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<Examiner>
 */
class ExaminerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'name' => fake()->firstName(),
            'user_id' => User::factory(),
            'abbreviation' => fake()->unique()->lexify('?????'),
        ];
    }
}
