<?php

namespace Database\Factories;

use App\Models\Process;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<Process>
 */
class ProcessFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'name' => fake()->slug(),
            'description' => fake()->sentence(),
        ];
    }
}
