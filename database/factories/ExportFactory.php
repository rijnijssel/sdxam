<?php

namespace Database\Factories;

use App\Models\Execution;
use App\Models\Export;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<Export>
 */
class ExportFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'execution_id' => Execution::factory(),
            'filename' => fake()->imageUrl(),
        ];
    }
}
