<?php

namespace Database\Factories;

use App\Models\RubricTaskItem;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<RubricTaskItem>
 */
class RubricTaskItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'text' => fake()->text(),
        ];
    }
}
