<?php

namespace Database\Factories;

use App\Models\BatchExport;
use App\Models\Execution;
use Illuminate\Database\Eloquent\Factories\Factory;
use Override;

/**
 * @extends Factory<BatchExport>
 */
class BatchExportFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
     */
    #[Override]
    public function definition(): array
    {
        return [
            'execution_id' => Execution::factory(),
            'directory' => 'exports/'.fake()->slug().'/'.fake()->date('Ymd_His'),
            'started_at' => fake()->dateTime(),
            'finished_at' => fake()->dateTime(),
        ];
    }
}
