<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'surfconext' => [
        'enabled' => env('SURFCONEXT_ENABLED', false),
        'client_id' => env('SURFCONEXT_CLIENT_ID'),
        'client_secret' => env('SURFCONEXT_CLIENT_SECRET'),
        'redirect' => env('SURFCONEXT_REDIRECT_URI', env('APP_URL').'/sso/surfconext/callback'),
        'test' => env('SURFCONEXT_TEST'),
    ],

    'gitlab' => [
        'enabled' => env('GITLAB_ENABLED', false),
        'client_id' => env('GITLAB_CLIENT_ID'),
        'client_secret' => env('GITLAB_CLIENT_SECRET'),
        'redirect' => env('GITLAB_REDIRECT_URI', env('APP_URL').'/sso/gitlab/callback'),
        'instance_uri' => env('GITLAB_INSTANCE_URI'),
    ],

    'gitlab_pipeline_trigger' => [
        'enabled' => env('GITLAB_PIPELINE_TRIGGER_ENABLED', false),
        'token' => env('GITLAB_PIPELINE_TRIGGER_TOKEN'),
        'project_id' => env('GITLAB_PIPELINE_TRIGGER_PROJECT_ID'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'resend' => [
        'key' => env('RESEND_KEY'),
    ],

    'slack' => [
        'notifications' => [
            'bot_user_oauth_token' => env('SLACK_BOT_USER_OAUTH_TOKEN'),
            'channel' => env('SLACK_BOT_USER_DEFAULT_CHANNEL'),
        ],
    ],
];
