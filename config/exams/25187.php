<?php

return [
    [
        'name' => 'B1-K1',
        'code' => 'BF_AMO_AO16-B1-K1_3',
        'description' => 'Levert een bijdrage aan het ontwikkeltraject',
        'processes' => [
            [
                'name' => 'B1-K1-W1',
                'description' => 'Stelt de opdracht vast',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T1, T2', 'criteria' => 'Voorbereiding',
                                'cells' => [
                                    'Heeft zich niet ingelezen en/of voorbereid op het gesprek.',
                                    'Heeft zich ingelezen en heeft zich voorbereid op gesprek.',
                                ],
                            ],
                            [
                                'name' => 'T1, T2', 'criteria' => 'Gespreksvorm',
                                'cells' => [
                                    "Leidt het gesprek niet in.\n\nStelt wel vragen, maar vraagt niet door.\n\nGaat weinig in op wat de opdrachtgever zegt.\n\nVat aan het eind niet samen, en vertelt de vervolgprocedure niet.",
                                    "Leidt het gesprek in.\n\nStelt vragen en vraagt, indien nodig, in de meeste gevallen door.\n\nGaat in de meeste gevallen in op wat de opdrachtgever zegt en de reactie is niet altijd adequaat.\n\nVat aan het eind niet samen, en vertelt de vervolgprocedure niet.",
                                    "Leidt het gesprek in.\n\nStelt vragen en vraagt indien nodig door.\n\nGaat in op wat de opdrachtgever zegt en de reactie is bijna altijd adequaat.\n\nVat aan het eind samen, en vertelt de vervolgprocedure niet.",
                                    "Leidt het gesprek in.\n\nStelt vragen en vraagt indien nodig door.\n\nGaat in op wat de opdrachtgever zegt en de reactie is altijd adequaat.\n\nVat aan het eind het gesprek samen, en vertelt de vervolgprocedure.",
                                ],
                            ],
                            [
                                'name' => 'T1, T2', 'criteria' => 'Gespreksinhoud',
                                'cells' => [
                                    "De vragen zijn in de meeste gevallen niet gerelateerd aan en/of niet relevant voor de opdracht.\n\nGebruikt veel vaktaal zonder dit uit te leggen.",
                                    "De vragen zijn in de meeste gevallen gerelateerd aan of relevant voor de opdracht.\n\nGebruikt soms vaktaal en/of legt deze in de meeste gevallen niet begrijpelijk uit.",
                                    "De vragen zijn in de meeste gevallen gerelateerd aan en relevant voor de opdracht.\n\nGebruikt weinig tot geen vaktaal en/of legt deze in de meeste gevallen begrijpelijk uit.",
                                    "De vragen zijn altijd gerelateerd aan en relevant voor de opdracht.\n\nGebruikt weinig tot geen vaktaal en legt deze begrijpelijk uit.",
                                ],
                            ],
                            [
                                'name' => 'T3, T4, T5, T6', 'criteria' => 'Inhoud Programma van Eisen*', 'required' => true,
                                'cells' => [
                                    "De eisen in het PvE sluiten deels niet aan op de opdracht en/of een groot aantal eisen zijn niet beschreven.\n\nGebruikt veel vaktaal zonder dit uit te leggen.",
                                    "De eisen in het PvE sluiten in de meeste gevallen aan op de opdracht, maar een klein aantal eisen zijn niet beschreven.\n\nGebruikt soms vaktaal en/of legt deze in de meeste gevallen niet begrijpelijk uit.",
                                    "De eisen in het PvE sluiten aan op de opdracht en een klein aantal eisen zijn niet beschreven.\n\nGebruikt weinig tot geen vaktaal en/of legt deze in de meeste gevallen begrijpelijk uit.",
                                    "Alle eisen in het PvE sluiten aan op de opdracht en zijn beschreven.\n\nGebruikt weinig tot geen vaktaal en legt deze begrijpelijk uit.",
                                ],
                            ],
                            [
                                'name' => 'T3, T4, T5, T6', 'criteria' => 'Vorm Programma van Eisen',
                                'cells' => [
                                    "Het PvE voldoet op de meeste punten niet aan de gevraagde onderdelen.\n\nDe onderdelen in het PvE zijn in een groot aantal gevallen niet begrijpelijk beschreven.",
                                    "Het PvE voldoet in de meeste gevallen aan de gevraagde onderdelen.\n\nDe onderdelen in het PvE zijn in de meeste gevallen begrijpelijk beschreven.",
                                    "Het PvE voldoet aan de gevraagde onderdelen.\n\nDe onderdelen in het PvE zijn in de meeste gevallen begrijpelijk beschreven.",
                                    "Het PvE voldoet aan de gevraagde onderdelen.\n\nDe onderdelen in het PvE zijn allemaal begrijpelijk beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T3, T4, T5, T6', 'criteria' => 'Onderbouwing Programma van Eisen',
                                'cells' => [
                                    "Er is niet adequaat gebruikgemaakt van aanvullende bronnen en/of onderzoek.\n\n(On)mogelijkheden en/of impact van het gevraagde zijn in het PvE niet beschreven.",
                                    "Er is weinig adequaat gebruikgemaakt van aanvullende bronnen en/of onderzoek.\n\n(On)mogelijkheden en/of impact van het gevraagde zijn niet volledig in het PvE beschreven.",
                                    "Er is adequaat gebruikgemaakt van aanvullende bronnen en/of onderzoek.\n\n(On)mogelijkheden en impact van het gevraagde zijn in het PvE in kaart gebracht en grotendeels volledig beschreven.",
                                    "Er is adequaat gebruikgemaakt van aanvullende bronnen en/of onderzoek.\n\n(On)mogelijkheden en impact van het gevraagde zijn in het PvE volledig beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T7', 'criteria' => 'Communiceren Programma van Eisen',
                                'cells' => [
                                    "Het PvE wordt niet overgedragen of niet toegelicht.\n\nEr wordt niet om goedkeuring gevraagd.",
                                    "Het PvE wordt overgedragen en toegelicht.\n\nEr wordt om goedkeuring gevraagd.",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W2',
                'description' => 'Levert een bijdrage aan het projectplan',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 2',
                        'rows' => [
                            [
                                'name' => 'T1, T2, T3, T4', 'criteria' => 'Project-doelstellingen',
                                'cells' => [
                                    'De project-doelstellingen zijn niet in lijn met de opdracht, duidelijk en/of haalbaar.',
                                    'De project-doelstellingen zijn in de meeste gevallen in lijn met de opdracht en óf duidelijk óf haalbaar.',
                                    'De project-doelstellingen zijn in de meeste gevallen in lijn met de opdracht, duidelijk en haalbaar.',
                                    'De project-doelstellingen zijn in lijn met de opdracht, duidelijk en haalbaar.',
                                ],
                            ],
                            [
                                'name' => 'T1, T2, T3, T4', 'criteria' => 'Projectactiviteiten',
                                'cells' => [
                                    "De werkzaamheden zijn onvolledig, incorrect of onoverzichtelijk weergegeven.\n\nEn/of de inzet en middelen voor de werkzaamheden zijn niet of onduidelijk benoemd.\n\nEn/of de ontwikkelmethode is niet benoemd.",
                                    "De werkzaamheden zijn in de meeste gevallen volledig, correct en overzichtelijk weergegeven.\n\nDe inzet en middelen voor de werkzaamheden zijn in de meeste gevallen duidelijk benoemd.\n\nDe ontwikkelmethode is benoemd.",
                                    "De werkzaamheden zijn volledig en in de meeste gevallen correct en overzichtelijk weergegeven.\n\nDe inzet en middelen voor de werkzaamheden zijn in de meeste gevallen duidelijk benoemd.\n\nDe ontwikkelmethode is benoemd.",
                                    "De werkzaamheden zijn volledig, correct en overzichtelijk weergegeven.\n\nDe inzet en middelen voor de werkzaamheden zijn duidelijk benoemd.\n\nDe ontwikkelmethode is benoemd.",
                                ],
                            ],
                            [
                                'name' => 'T1, T2, T3, T4', 'criteria' => 'Planning',
                                'cells' => [
                                    'De planning, inclusief voortgangsgesprekken, is niet volledig, realistisch, chronologisch en/of overzichtelijk.',
                                    'De planning, inclusief voortgangsgesprekken, is volledig en realistisch, maar niet chronologisch en/of overzichtelijk.',
                                    'De planning, inclusief voortgangsgesprekken, is volledig, realistisch en chronologisch of overzichtelijk.',
                                    'De planning, inclusief voortgangsgesprekken, is volledig, realistisch, chronologisch en overzichtelijk.',
                                ],
                            ],
                            [
                                'name' => 'T5', 'criteria' => 'Afstemmen',
                                'cells' => [
                                    "De bijdrage aan het projectplan wordt niet afgestemd.\n\nEr wordt niet om goedkeuring gevraagd.",
                                    "De bijdrage aan het projectplan wordt afgestemd.\n\nEr wordt om goedkeuring gevraagd.",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W3',
                'description' => 'Levert een bijdrage aan het ontwerp',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 3',
                        'rows' => [
                            [
                                'name' => 'T1, T2', 'criteria' => 'Functionaliteiten*', 'required' => true,
                                'cells' => [
                                    "Te weinig informatie, eisen en wensen uit het PvE zijn opgenomen als functionaliteit.\n\nDe informatie, eisen en wensen uit het PvE zijn niet begrijpelijk, eenduidig en/of niet als functionaliteit beschreven.\n\nDe functionaliteiten zijn in de meeste gevallen niet in overeenstemming met het PvE, zowel qua inhoud als prioritering.",
                                    "Bijna alle informatie, eisen en wensen uit het PvE zijn opgenomen als functionaliteit.\n\nDe informatie, eisen en wensen uit het PvE zijn in de meeste gevallen begrijpelijk, eenduidig en als functionaliteit beschreven.\n\nDe functionaliteiten zijn in de meeste gevallen in overeenstemming met het PvE, zowel qua inhoud als prioritering.",
                                    "Alle informatie, eisen en wensen uit het PvE zijn opgenomen als functionaliteit.\n\nDe informatie, eisen en wensen uit het PvE zijn in de meeste gevallen begrijpelijk, eenduidig en als functionaliteit beschreven.\n\nDe functionaliteiten zijn in overeenstemming met het PvE, zowel qua inhoud als prioritering.",
                                    "Alle informatie, eisen en wensen uit het PvE zijn opgenomen als functionaliteit.\n\nDe informatie, eisen en wensen uit het PvE zijn begrijpelijk, eenduidig en als functionaliteit beschreven.\n\nDe functionaliteiten zijn in overeenstemming met het PvE, zowel qua inhoud als prioritering.",
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Toepassen schema-technieken',
                                'cells' => [
                                    "Er is geen schematechniek gebruikt.\n\nOf\n\nDe verkeerde schematechnieken zijn gebruikt.",
                                    "De schematechnieken zijn in de meeste gevallen juist gekozen.\n\nDe schematechnieken verduidelijken de functionaliteit, maar zijn in de meeste gevallen niet juist uitgevoerd.",
                                    "De juiste schematechnieken zijn gekozen.\n\nDe schematechnieken verduidelijken de functionaliteit en zijn in de meeste gevallen juist uitgevoerd.",
                                    "De juiste schematechnieken zijn gekozen.\n\nDe schematechnieken verduidelijken de functionaliteit en zijn juist uitgevoerd.",
                                ],
                            ],
                            [
                                'name' => 'T4', 'criteria' => 'User interface',
                                'cells' => [
                                    "Het is niet helder hoe het systeem eruit gaat zien qua lay-out.\n\nOf\n\nHet is niet helder welke gegevens nodig zijn.\n\nOf\n\nHet is niet duidelijk welke schermen er komen en de relatie tussen deze schermen is niet helder.",
                                    "Het is helder hoe het systeem eruit gaat zien qua lay-out. Hierbij is niet altijd adequaat gebruikgemaakt van schetsen.\n\nHet is helder welke gegevens nodig zijn, waarbij niet alle formulieren en overzichten zijn geschetst.\n\nHet is duidelijk welke schermen er komen en de relatie tussen deze schermen is in de meeste gevallen helder; hiervoor is een navigatiestructuur opgesteld.",
                                    "Het is helder hoe het systeem eruit gaat zien qua lay-out. Hierbij is adequaat gebruikgemaakt van schetsen.\n\nHet is helder welke gegevens nodig zijn, waarbij niet alle formulieren en overzichten zijn geschetst.\n\nHet is duidelijk welke schermen er komen en de relatie tussen deze schermen is in de meeste gevallen helder; hiervoor is een navigatiestructuur opgesteld.",
                                    "Het is helder hoe het systeem eruit gaat zien qua lay-out. Hierbij is adequaat gebruikgemaakt van schetsen.\n\nHet is helder welke gegevens nodig zijn, waarbij alle formulieren en overzichten zijn geschetst.\n\nHet is duidelijk welke schermen er komen en de relatie tussen deze schermen is helder; hiervoor is een sitemap of navigatiestructuur opgesteld.",
                                ],
                            ],
                            [
                                'name' => 'T5', 'criteria' => 'Communiceren FO*', 'required' => true,
                                'cells' => [
                                    "Het FO wordt niet overgedragen of niet toegelicht.\n\nOf\n\nEr wordt niet om goedkeuring gevraagd.\n\nOf\n\nGebruikt veel vaktaal zonder dit uit te leggen.",
                                    "Het FO wordt overgedragen en toegelicht.\n\nEr wordt om goedkeuring gevraagd.\n\nGebruikt soms vaktaal en/of legt deze in de meeste gevallen niet begrijpelijk uit.",
                                    "Het FO wordt overgedragen en toegelicht.\n\nEr wordt om goedkeuring gevraagd.\n\nGebruikt weinig tot geen vaktaal en/of legt deze in de meeste gevallen begrijpelijk uit.",
                                    "Het FO wordt overgedragen en toegelicht.\n\nEr wordt om goedkeuring gevraagd.\n\nGebruikt weinig tot geen vaktaal en legt deze begrijpelijk uit.",
                                ],
                            ],
                        ],
                    ],
                    [
                        'name' => 'Beoordeling opdracht 4',
                        'rows' => [
                            [
                                'name' => 'T6, T8', 'criteria' => 'Technische specificaties',
                                'cells' => [
                                    "De technische specificaties zijn niet gebaseerd op het FO.\n\nEn/of\n\nDe uitgewerkte technische specificaties zijn niet haalbaar en realistisch voor het te bereiken resultaat.\n\nEn/Of\n\nDe toegepaste schematechnieken verduidelijken de technische specificaties niet.",
                                    "De technische specificaties zijn gebaseerd op het FO.\n\nDe uitgewerkte technische specificaties zijn haalbaar, realistisch, maar niet optimaal voor het te bereiken resultaat.\n\nDe toegepaste schematechnieken verduidelijken in de meeste gevallen de technische specificaties.",
                                    "De technische specificaties zijn gebaseerd op het FO.\n\nDe uitgewerkte technische specificaties zijn haalbaar, realistisch en optimaal voor het te bereiken resultaat.\n\nDe toegepaste schematechnieken verduidelijken in de meeste gevallen de technische specificaties.",
                                    "De technische specificaties zijn gebaseerd op het FO.\n\nDe uitgewerkte technische specificaties zijn haalbaar, realistisch en optimaal voor het te bereiken resultaat.\n\nDe toegepaste schematechnieken verduidelijken de technische specificaties.",
                                ],
                            ],
                            [
                                'name' => 'T7', 'criteria' => 'Relationeel datamodel*', 'required' => true,
                                'cells' => [
                                    "Niet alle entiteiten en weinig eigenschappen / attributen zijn beschreven in het datamodel en daarvan is weinig gebaseerd op het FO.\n\nOf\n\nWeinig informatie uit het FO is verwerkt in het datamodel.\n\nOf\n\nDe relaties zijn niet op de juiste wijze beschreven.",
                                    "Alle entiteiten en sommige eigenschappen / attributen zijn beschreven in het datamodel en daarvan is bijna alles gebaseerd op het FO.\n\nBijna alle informatie uit het FO is verwerkt in het datamodel.\n\nAlle relaties zijn op de juiste wijze beschreven.",
                                    "Alle entiteiten en bijna alle eigenschappen / attributen zijn beschreven in het datamodel en zijn gebaseerd op het FO.\n\nBijna alle informatie uit het FO is verwerkt in het datamodel.\n\nAlle relaties zijn op de juiste wijze beschreven.",
                                    "Alle entiteiten en eigenschappen / attributen zijn beschreven in het datamodel en gebaseerd op het FO.\n\nAlle informatie uit het FO is verwerkt in het datamodel.\n\nAlle relaties zijn op de juiste wijze beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T9', 'criteria' => 'Communiceren TO',
                                'cells' => [
                                    "Het TO wordt niet overgedragen of niet toegelicht.\n\nEr wordt niet om goedkeuring gevraagd.",
                                    "Het TO wordt overgedragen en toegelicht.\n\nEr wordt om goedkeuring gevraagd.",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W4',
                'description' => 'Bereidt de realisatie voor',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 5',
                        'rows' => [
                            [
                                'name' => 'T1', 'criteria' => 'Benodigde onderdelen',
                                'cells' => [
                                    "Weinig van de benodigde software voor de ontwikkelomgeving is beschreven.\n\nWeinig van de benodigde hardware voor de ontwikkelomgeving is beschreven.",
                                    "De benodigde software voor de ontwikkelomgeving is op enkele onderdelen na volledig beschreven.\n\nDe benodigde hardware voor de ontwikkelomgeving is op enkele onderdelen na volledig beschreven.",
                                    "De benodigde software voor de ontwikkelomgeving is op kleinigheden na volledig beschreven.\n\nDe benodigde hardware voor de ontwikkelomgeving is op kleinigheden na volledig beschreven.",
                                    "De benodigde software voor de ontwikkelomgeving is volledig beschreven.\n\nDe benodigde hardware voor de ontwikkelomgeving is volledig beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Installeren en configureren',
                                'cells' => [
                                    'De ontwikkelomgeving is niet juist geïnstalleerd en geconfigureerd.',
                                    'De ontwikkelomgeving is juist geïnstalleerd en geconfigureerd.',
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Testen*', 'required' => true,
                                'cells' => [
                                    'De ontwikkelomgeving is niet getest.',
                                    'De ontwikkelomgeving is getest.',
                                ],
                            ],
                            [
                                'name' => 'T4', 'criteria' => 'Documenteren',
                                'cells' => [
                                    'De documentatie is niet volledig en/of correct en/of duidelijk.',
                                    'De documentatie is volledig, correct en duidelijk.',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'name' => 'B1-K2',
        'code' => 'BF_AMO_AO16-B1-K2_1',
        'description' => 'Realiseert en test (onderdelen van) een product',
        'processes' => [
            [
                'name' => 'B1-K2-W1',
                'description' => 'Realiseert (onderdelen van) een product',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T1', 'criteria' => 'Inventarisatie en planning',
                                'cells' => [
                                    "Niet alle werkzaamheden en activiteiten zijn in de planning opgenomen.\n\nDe werkzaamheden en activiteiten zijn niet gebaseerd op de ontwerp-documentatie.\n\nDe planning is niet chronologisch, realistisch en/of overzichtelijk.",
                                    "Bijna alle werkzaamheden en activiteiten zijn in de planning opgenomen.\n\nDe werkzaamheden en activiteiten zijn voor een klein deel gebaseerd op de ontwerp-documentatie.\n\nDe planning is realistisch en niet chronologisch of overzichtelijk.",
                                    "Alle werkzaamheden en activiteiten zijn in de planning opgenomen.\n\nDe werkzaamheden en activiteiten zijn op enkele na gebaseerd op de ontwerp-documentatie.\n\nDe planning is realistisch en chronologisch of overzichtelijk.",
                                    "Alle werkzaamheden en activiteiten zijn in de planning opgenomen.\n\nDe werkzaamheden en activiteiten zijn volledig gebaseerd op de ontwerp-documentatie.\n\nDe planning is realistisch, chronologisch en overzichtelijk.",
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Gerealiseerde functionaliteiten*', 'required' => true,
                                'cells' => [
                                    'Weinig functionaliteiten zijn gerealiseerd.',
                                    'Het minimum aan functionaliteiten is gerealiseerd.',
                                    'Bijna alle functionaliteiten zijn gerealiseerd.',
                                    'Alle functionaliteiten zijn gerealiseerd.',
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Gerealiseerde eisen*', 'required' => true,
                                'cells' => [
                                    'Weinig gerealiseerde functionaliteiten voldoen aan de eisen uit de ontwerp-documentatie.',
                                    'Het minimum van de gerealiseerde functionaliteiten voldoet aan de eisen uit de ontwerp-documentatie.',
                                    'Bijna alle gerealiseerde functionaliteiten voldoen aan de eisen uit de ontwerp-documentatie.',
                                    'Alle gerealiseerde functionaliteiten voldoen aan de eisen uit de ontwerp-documentatie.',
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Correctheid code*', 'required' => true,
                                'cells' => [
                                    "De code voldoet niet aan de binnen de organisatie geldende naming conventions.\n\nDe code werkt niet efficiënt.\n\nEr wordt niet zinvol gebruikgemaakt van functies en/of methoden.\n\nDe code is niet logisch/structureel en consistent opgebouwd.",
                                    "De code voldoet in enkele gevallen aan de binnen de organisatie geldende naming conventions.\n\nDe code werkt deels efficiënt.\n\nEr wordt bijna overal zinvol gebruikgemaakt van functies en/of methoden.\n\nDe code is deels logisch/structureel en consistent opgebouwd.",
                                    "De code voldoet in de meeste gevallen aan de binnen de organisatie geldende naming conventions.\n\nDe code werkt efficiënt.\n\nEr wordt bijna overal zinvol gebruikgemaakt van functies en/of methoden.\n\nDe code is logisch/structureel en consistent opgebouwd.",
                                    "De code voldoet volledig aan de binnen de organisatie geldende naming conventions.\n\nDe code werkt efficiënt.\n\nEr wordt zinvol gebruikgemaakt van functies en/of methoden.\n\nDe code is logisch/structureel en consistent opgebouwd.",
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Vorm van de code',
                                'cells' => [
                                    'De opmaak van de broncode is niet volgens de binnen de organisatie geldende conventies en/of de code is niet leesbaar.',
                                    'De opmaak van de broncode is volgens de binnen de organisatie geldende conventies en de code is leesbaar.',
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Commentaar',
                                'cells' => [
                                    "Te weinig functies/methodes zijn van functioneel commentaar voorzien volgens de binnen de organisatie geldende conventies.\n\nHet gebruikte commentaar is niet duidelijk voor een ander.",
                                    "Weinig functies/methodes zijn van functioneel commentaar voorzien volgens de binnen de organisatie geldende conventies.\n\nHet gebruikte commentaar is niet overal duidelijk voor een ander.",
                                    "Bijna alle functies/methodes zijn van functioneel commentaar voorzien volgens de binnen de organisatie geldende conventies.\n\nHet gebruikte commentaar is bijna altijd duidelijk voor een ander.",
                                    "Alle functies/methodes zijn van functioneel commentaar voorzien volgens de binnen de organisatie geldende conventies.\n\nHet gebruikte commentaar is duidelijk voor een ander.",
                                ],
                            ],
                            [
                                'name' => 'T4', 'criteria' => 'Documenteren',
                                'cells' => [
                                    'De documentatie is niet volledig en/of correct en/of duidelijk.',
                                    'De documentatie is volledig, correct en duidelijk.',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K2-W2',
                'description' => 'Test het ontwikkelde product',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 2',
                        'rows' => [
                            [
                                'name' => 'T1, T2', 'criteria' => "Opstellen testscenario's",
                                'cells' => [
                                    'Voor weinig van de functionaliteiten uit de ontwerp-documentatie zijn de juiste en benodigde testscenario´s geformuleerd.',
                                    'Voor het minimum van de functionaliteiten uit de ontwerp-documentatie zijn de juiste en benodigde testscenario´s geformuleerd.',
                                    'Voor bijna alle van de functionaliteiten uit de ontwerp-documentatie zijn de juiste en benodigde testscenario´s geformuleerd.',
                                    'Voor alle van de functionaliteiten uit de ontwerp-documentatie zijn de juiste en benodigde testscenario´s geformuleerd.',
                                ],
                            ],
                            [
                                'name' => 'T3, T4', 'criteria' => "Uitvoeren testscenario's*", 'required' => true,
                                'cells' => [
                                    "Voert weinig testscenario´s uit.\n\nResultaten zijn niet duidelijk en volledig beschreven.",
                                    "Voert bijna alle testscenario´s uit.\n\nResultaten zijn niet duidelijk en/of volledig beschreven.",
                                    "Voert alle testscenario´s uit.\n\nResultaten zijn niet duidelijk en/of volledig beschreven.",
                                    "Voert alle testscenario´s uit.\n\nResultaten zijn duidelijk en volledig beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T5, T6', 'criteria' => 'Bevindingen interpreteren',
                                'cells' => [
                                    "De resultaten zijn niet juist geïnterpreteerd, geprioriteerd en gedocumenteerd.\n\nEn/of\n\nEventuele aanpassingen in de applicatie zijn niet of niet juist doorgevoerd en de documentatie is niet aangepast.",
                                    "De resultaten zijn juist geïnterpreteerd, geprioriteerd en gedocumenteerd.\n\nEventuele aanpassingen in de applicatie zijn niet of niet juist doorgevoerd en de documentatie is aangepast.",
                                    "De resultaten zijn juist geïnterpreteerd, geprioriteerd en gedocumenteerd.\n\nEventuele aanpassingen in de applicatie zijn juist doorgevoerd, en de documentatie is niet aangepast.",
                                    "De resultaten zijn juist geïnterpreteerd, geprioriteerd en gedocumenteerd.\n\nEventuele aanpassingen in de applicatie zijn juist doorgevoerd, en de documentatie is aangepast.",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'name' => 'B1-K3',
        'code' => 'BF_AMO_AO16-B1-K3_1',
        'description' => 'Levert een product op',
        'processes' => [
            [
                'name' => 'B1-K3-W1',
                'description' => 'Optimaliseert het product',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T1', 'criteria' => 'Acceptatietest*', 'required' => true,
                                'cells' => [
                                    "Weinig functionele eisen uit het FO komen terug in de acceptatietest en de scenario's. En/of een cruciale eis ontbreekt.\n\nDe testset(s) zijn niet aanwezig.",
                                    "Bijna alle functionele eisen uit het FO komen terug in de acceptatietest en de scenario's. Alle cruciale eisen zijn aanwezig.\n\nDe testset(s) zijn voor een klein deel aanwezig.",
                                    "Alle functionele eisen uit het FO komen terug in de acceptatietest en de scenario's.\n\nDe testset(s) zijn voor een groot deel aanwezig.",
                                    "Alle functionele eisen uit het FO komen terug in de acceptatietest en de scenario's.\n\nAlle testset(s) zijn aanwezig.",
                                ],
                            ],
                            [
                                'name' => 'T1', 'criteria' => 'Uitvoerbaarheid',
                                'cells' => [
                                    "De scenario's in de acceptatietest zijn niet uitvoerbaar in de applicatie die getest wordt.",
                                    "Een klein deel scenario's in de acceptatietest zijn uitvoerbaar in de applicatie die getest wordt.",
                                    "Een groot deel scenario's in de acceptatietest zijn uitvoerbaar in de applicatie die getest wordt.",
                                    "Alle scenario's in de acceptatietest zijn uitvoerbaar in de applicatie die getest wordt.",
                                ],
                            ],
                            [
                                'name' => 'T1', 'criteria' => "Scenario's (volgens functionele eisen)",
                                'cells' => [
                                    "Bijna alle scenario's bevatten een begin- en eindsituatie en een controle-element voor de gebruiker.\n\nBijna alle toegestane situaties worden getest. De niet toegestane situaties worden niet getest.\n\nWeinig scenario's bevatten begrijpelijke instructies voor de gebruiker.",
                                    "Bijna alle scenario's bevatten een begin- en eindsituatie en een controle-element voor de gebruiker.\n\nBijna alle toegestane situaties worden getest. Ook de niet toegestane situaties worden getest.\n\nWeinig scenario's bevatten begrijpelijke instructies voor de gebruiker.",
                                    "Alle scenario's bevatten een begin- en eindsituatie en een controle-element voor de gebruiker.\n\nBijna alle toegestane situaties worden getest. Ook de niet toegestane situaties worden getest.\n\nBijna alle scenario's bevatten begrijpelijke instructies voor de gebruiker.",
                                    "Alle scenario's bevatten een begin- en eindsituatie en een controle-element voor de gebruiker.\n\nAlle toegestane situaties worden getest. Ook de niet toegestane situaties worden getest.\n\nAlle scenario's bevatten begrijpelijke instructies voor de gebruiker.",
                                ],
                            ],
                        ],
                    ],
                    [
                        'name' => 'Beoordeling opdracht 2',
                        'rows' => [
                            [
                                'name' => 'T2', 'criteria' => 'Uitvoeren acceptatietest*', 'required' => true,
                                'cells' => [
                                    "De acceptatietest wordt niet of nauwelijks ingeleid.\n\nRelevante ervaringen en gedrag van gebruikers worden niet of nauwelijks genoteerd.",
                                    "De acceptatietest wordt niet goed ingeleid.\n\nRelevante ervaringen en gedrag van gebruikers worden deels genoteerd.",
                                    "De acceptatietest wordt kort en goed ingeleid.\n\nRelevante ervaringen en gedrag van gebruikers worden deels genoteerd.",
                                    "De acceptatietest wordt kort en goed ingeleid.\n\nRelevante ervaringen en gedrag van gebruikers worden genoteerd.",
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Communiceren met betrokkenen*', 'required' => true,
                                'cells' => [
                                    "Luistert naar opmerkingen van gebruikers en reageert niet of nauwelijks objectief en constructief.\n\nHet is nodig om extra vragen te stellen, maar dit gebeurt niet.",
                                    "Luistert naar opmerkingen en reageert voor een klein deel objectief en constructief.\n\nIndien nodig worden vragen gesteld om extra informatie te achterhalen.",
                                    "Luistert aandachtig en begripvol naar opmerkingen en reageert grotendeels objectief en constructief.\n\nIndien nodig worden vragen gesteld om extra informatie te achterhalen.",
                                    "Luistert aandachtig en begripvol naar opmerkingen en reageert objectief en constructief.\n\nIndien nodig worden vragen gesteld om extra informatie te achterhalen.",
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Verzamelen testresultaten',
                                'cells' => [
                                    'De testresultaten van de gebruikers worden niet of nauwelijks verzameld.',
                                    'De testresultaten van de gebruikers worden voor een klein deel verzameld.',
                                    'De testresultaten van de gebruikers worden grotendeels verzameld.',
                                    'Alle testresultaten van de gebruikers worden verzameld.',
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Uitwerken testresultaten',
                                'cells' => [
                                    "Uit de testresultaten worden niet of nauwelijks de juiste conclusies getrokken, volgens de functionele eisen.\n\nOf\n\nDe benodigde aanpassingen worden niet of nauwelijks correct geprioriteerd.\n\nOf\n\nDe bijbehorende werkzaamheden worden niet of nauwelijks beschreven.",
                                    "Uit de testresultaten worden grotendeels de juiste conclusies getrokken, volgens de functionele eisen.\n\nDe benodigde aanpassingen worden grotendeels correct geprioriteerd en de bijbehorende werkzaamheden worden grotendeels beschreven.",
                                    "Uit de testresultaten worden de juiste conclusies getrokken, volgens de functionele eisen.\n\nDe benodigde aanpassingen worden grotendeels correct geprioriteerd en de bijbehorende werkzaamheden worden grotendeels beschreven.",
                                    "Uit de testresultaten worden de juiste conclusies getrokken, volgens de functionele eisen.\n\nDe benodigde aanpassingen worden correct geprioriteerd en de bijbehorende werkzaamheden worden beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Uitvoeren testresultaten',
                                'cells' => [
                                    "Weinig beschreven werkzaamheden worden uitgevoerd.\n\nOf\n\nEr is niet of nauwelijks volgens voorgeschreven eisen gecodeerd.\n\nOf\n\nWeinig aanpassingen worden getest.",
                                    "Bijna alle beschreven werkzaamheden worden uitgevoerd.\n\nEr is grotendeels volgens voorgeschreven eisen gecodeerd.\n\nBijna alle aanpassingen worden getest.",
                                    "Alle beschreven werkzaamheden worden uitgevoerd.\n\nEr is volgens voorgeschreven eisen gecodeerd.\n\nBijna alle aanpassingen worden getest.",
                                    "Alle beschreven werkzaamheden worden uitgevoerd.\n\nEr is volgens voorgeschreven eisen gecodeerd.\n\nAlle aanpassingen worden getest.",
                                ],
                            ],
                            [
                                'name' => 'T4', 'criteria' => 'Documenteren',
                                'cells' => [
                                    'De documentatie is niet volledig en/of correct en/of duidelijk.',
                                    'De documentatie is volledig, correct en duidelijk.',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K3-W2',
                'description' => 'Levert het product op',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 3',
                        'rows' => [
                            [
                                'name' => 'T1', 'criteria' => 'Product opleveren',
                                'cells' => [
                                    "Het product wordt niet van testomgeving naar productieomgeving gebracht, conform de functionele eisen.\n\nOf\n\nIn de productieomgeving zijn niet dezelfde functionaliteiten beschikbaar als in de testomgeving.",
                                    "Het product wordt van testomgeving naar productie-omgeving gebracht, conform de functionele eisen.\n\nEn\n\nIn de productieomgeving zijn dezelfde functionaliteiten beschikbaar als in de testomgeving.",
                                ],
                            ],
                            [
                                'name' => 'T1', 'criteria' => 'Goedkeuring vragen',
                                'cells' => [
                                    'Er wordt geen goedkeuring gevraagd van de opdrachtgever voor het opgeleverde product.',
                                    'Er wordt goedkeuring gevraagd aan de opdrachtgever voor het opgeleverde product.',
                                ],
                            ],
                            [
                                'name' => 'T1', 'criteria' => 'Inhoud demonstratie',
                                'cells' => [
                                    "Er wordt niet of nauwelijks aangetoond dat het product aansluit op de eisen uit het PvE.\n\nEr wordt niet of nauwelijks adequaat gereageerd op de opdrachtgever.",
                                    "Er wordt minimaal aangetoond dat het product aansluit op de eisen uit het PvE.\n\nEr wordt minimaal adequaat gereageerd op de opdrachtgever.",
                                    "Er wordt grotendeels aangetoond dat het product aansluit op de eisen uit het PvE.\n\nEr wordt grotendeels adequaat gereageerd op de opdrachtgever.",
                                    "Er wordt overtuigend aangetoond dat het product aansluit op de eisen uit het PvE.\n\nEr wordt adequaat gereageerd op de opdrachtgever.",
                                ],
                            ],
                            [
                                'name' => 'T1', 'criteria' => 'Vorm demonstratie',
                                'cells' => [
                                    "Het product wordt op onbegrijpelijke en niet overtuigende wijze gedemonstreerd.\n\nDe demonstratie bevat een onduidelijke opbouw (inleiding, kern en slot).",
                                    "Het product wordt op grotendeels begrijpelijke en grotendeels overtuigende wijze gedemonstreerd.\n\nDe demonstratie bevat een onduidelijke opbouw (inleiding, kern en slot).",
                                    "Het product wordt op grotendeels begrijpelijke en grotendeels overtuigende wijze gedemonstreerd.\n\nDe demonstratie bevat een duidelijke opbouw (inleiding, kern en slot).",
                                    "Het product wordt op begrijpelijke en overtuigende wijze gedemonstreerd. \n\nDe demonstratie bevat een duidelijke opbouw (inleiding, kern en slot).",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K3-W3',
                'description' => 'Evalueert het opgeleverde product',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 4',
                        'rows' => [
                            [
                                'name' => 'T1, T2', 'criteria' => 'Evalueren product',
                                'cells' => [
                                    "Geen van de betrokkenen zijn geraadpleegd over het product.\n\nDe verbeterpunten en wat goed gegaan is, zijn niet of voor een klein deel beschreven.",
                                    "Niet de juiste betrokkenen zijn geraadpleegd over het product.\n\nDe verbeterpunten en wat goed gegaan is, zijn grotendeels volledig beschreven.",
                                    "De betrokkenen zijn geraadpleegd over het product.\n\nDe verbeterpunten en wat goed gegaan is, zijn grotendeels volledig beschreven.",
                                    "De betrokkenen zijn geraadpleegd over het product.\n\nDe verbeterpunten en wat goed gegaan is, zijn volledig beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T1, T2', 'criteria' => 'Evalueren proces',
                                'cells' => [
                                    "Geen van de betrokkenen zijn geraadpleegd over het proces.\n\nDe verbeterpunten en wat goed gegaan is, zijn niet of voor een klein deel beschreven.",
                                    "Niet de juiste betrokkenen zijn geraadpleegd over het proces.\n\nDe verbeterpunten en wat goed gegaan is, zijn grotendeels volledig beschreven.",
                                    "De betrokkenen zijn geraadpleegd over het proces.\n\nDe verbeterpunten en wat goed gegaan is, zijn grotendeels volledig beschreven.",
                                    "Alle betrokkenen zijn geraadpleegd over het proces.\n\nDe verbeterpunten en wat goed gegaan is, zijn beschreven.",
                                ],
                            ],
                            [
                                'name' => 'T1, T2', 'criteria' => 'Schrijven evaluatie',
                                'cells' => [
                                    'Het evaluatieverslag is onnauwkeurig en onvolledig geschreven.',
                                    'Het evaluatieverslag is voor een klein deel nauwkeurig en volledig geschreven.',
                                    'Het evaluatieverslag is grotendeels nauwkeurig en volledig geschreven.',
                                    'Het evaluatieverslag is nauwkeurig en volledig geschreven.',
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Accorderen',
                                'cells' => [
                                    'Het evaluatieverslag is niet geaccordeerd.',
                                    'Het evaluatieverslag is geaccordeerd.',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'name' => 'P1-K1',
        'code' => 'BF_AMO_AO16-P1-K1_1',
        'description' => 'Onderhoudt en beheert de applicatie',
        'processes' => [
            [
                'name' => 'P1-K1-W1',
                'description' => 'Onderhoudt een applicatie',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T1, T2', 'criteria' => 'Incident analyseren',
                                'cells' => [
                                    "Niet alle benodigde informatie is achterhaald\n\nen/of\n\nte weinig incidenten zijn juist gecategoriseerd\n\nen/of\n\nvoor de incidenten is niet gecontroleerd of de mogelijke aanpassingen in lijn zijn met eerder gemaakte afspraken of contracten.",
                                    "Alle benodigde informatie is achterhaald.\n\nBijna alle incidenten zijn juist gecategoriseerd.\n\nVoor bijna alle incidenten is gecontroleerd of de mogelijke aanpassingen in lijn zijn met eerder gemaakte afspraken of contracten.",
                                    "Alle benodigde informatie is achterhaald.\n\nBijna alle incidenten zijn juist gecategoriseerd.\n\nVoor alle incidenten is gecontroleerd of de mogelijke aanpassingen in lijn zijn met eerder gemaakte afspraken of contracten.",
                                    "Alle benodigde informatie is achterhaald.\n\nAlle incidenten zijn juist gecategoriseerd.\n\nVoor alle incidenten is gecontroleerd of de mogelijke aanpassingen in lijn zijn met eerder gemaakte afspraken of contracten.",
                                ],
                            ],
                            [
                                'name' => 'T1, T2, T4', 'criteria' => 'Oplossingen aandragen*', 'required' => true,
                                'cells' => [
                                    "Voor de incidenten zijn in geen of weinig gevallen passende oplossingen aangedragen\n\nen/of\n\nin geen of weinig gevallen is er overleg gepleegd met de betrokkene(n) over de door te voeren oplossingen.",
                                    "Voor alle incidenten zijn oplossingen aangedragen waarvan de meeste of alle passend zijn\n\nen\n\nin de meeste of alle gevallen is er overleg gepleegd met de betrokkene(n) over de door te voeren oplossingen.",
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Oplossing doorvoeren',
                                'cells' => [
                                    "De aanpassingen zijn in geen of weinig gevallen juist doorgevoerd.\n\nIn weinig gevallen zijn de aanpassingen volgens procedures doorgevoerd.",
                                    "De aanpassingen zijn in de meeste gevallen juist doorgevoerd.\n\nIn weinig gevallen zijn de aanpassingen volgens procedures doorgevoerd.",
                                    "De aanpassingen zijn in alle gevallen juist doorgevoerd.\n\nIn de meeste gevallen zijn de aanpassingen volgens procedures doorgevoerd.",
                                    "De aanpassingen zijn in alle gevallen juist doorgevoerd.\n\nIn alle gevallen zijn de aanpassingen volgens procedures doorgevoerd.",
                                ],
                            ],
                            [
                                'name' => 'T3', 'criteria' => 'Documenteren',
                                'cells' => [
                                    'De aanpassingen zijn in geen of weinig gevallen juist gedocumenteerd.',
                                    'De aanpassingen zijn in de meeste of alle gevallen juist gedocumenteerd.',
                                ],
                            ],
                            [
                                'name' => 'T4', 'criteria' => 'Terugkoppelen',
                                'cells' => [
                                    'De aanpassingen zijn in geen of weinig gevallen aan betrokkene(n) teruggekoppeld.',
                                    'De aanpassingen zijn in de meeste of alle gevallen aan betrokkene(n) teruggekoppeld.',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'P1-K1-W2',
                'description' => 'Beheert gegevens',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 2',
                        'rows' => [
                            [
                                'name' => 'T2', 'criteria' => 'Beheren',
                                'cells' => [
                                    "Niet alle benodigde gegevens zijn verzameld\n\nen/of\n\nweinig van de gegevens zijn juist gearchiveerd\n\nen/of\n\nweinig van de gegevens zijn juist geregistreerd.",
                                    "Alle benodigde gegevens zijn verzameld.\n\nDe meeste gegevens zijn juist gearchiveerd.\n\nDe meeste gegevens zijn juist geregistreerd.",
                                    "Alle benodigde gegevens zijn verzameld.\n\nAlle gegevens zijn juist gearchiveerd.\n\nDe meeste gegevens zijn juist geregistreerd.",
                                    "Alle benodigde gegevens zijn verzameld.\n\nAlle gegevens zijn juist gearchiveerd.\n\nAlle gegevens zijn juist geregistreerd.",
                                ],
                            ],
                            [
                                'name' => 'T1', 'criteria' => 'Controleren*', 'required' => true,
                                'cells' => [
                                    "De gegevens zijn niet juist gecontroleerd op volledigheid en actualiteit.\n\nen/of\n\nOntbrekende gegevens zijn niet aangevuld.",
                                    "De gegevens zijn juist gecontroleerd op volledigheid en actualiteit.\n\nOntbrekende gegevens zijn aangevuld.",
                                ],
                            ],
                            [
                                'name' => 'T2', 'criteria' => 'Versiebeheer',
                                'cells' => [
                                    'De verschillende versies van bestanden zijn niet onderscheidend en herkenbaar bewaard en/of niet los opvraagbaar.',
                                    'De verschillende versies van bestanden zijn onderscheidend en herkenbaar bewaard en/of los opvraagbaar.',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
