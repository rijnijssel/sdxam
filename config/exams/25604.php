<?php

$points = ['Niet of nauwelijks', 'Weinig', 'Grotendeels', 'Volledig'];

return [
    [
        'name' => 'B1-K1',
        'code' => 'SD_SD20-PE1_B1-K1-2_1v1',
        'description' => 'Realiseert software',
        'processes' => [
            [
                'name' => 'B1-K1-W1',
                'description' => 'Plant werkzaamheden en bewaakt de voortgang',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T1, T2, T3, T4',
                                'criteria' => 'Eisen/wensen user stories: De uitgangspunten zijn juist verwerkt (Definition of done) en de eisen en wensen zijn verwerkt in de user stories.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T1, T2, T3, T4',
                                'criteria' => 'Criteria user stories: De user stories voldoen aan de criteria (wie, wat, waarom en realistisch).',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T1, T2, T3, T4',
                                'criteria' => 'Planning maken: Op basis van de user stories is een complete en realistische planning gemaakt.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T1, T2, T3, T4',
                                'criteria' => 'Voortgang bewaken: De voortgang is bewaakt en de juiste keuzes/afwegingen zijn gemaakt op basis van prioriteiten.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W2',
                'description' => 'Ontwerpt software',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T4',
                                'required' => true,
                                'criteria' => 'Ontwerp*: De user stories zijn vertaald naar een passend, eenduidig en volledig ontwerp (sluit aan op wensen en eisen).',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T4',
                                'criteria' => 'Schema-technieken: Er is gebruikgemaakt van relevante of toepasselijke schematechnieken (bijv. activiteitendiagram, klassendiagram, ERD, use case diagram).',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T4',
                                'criteria' => 'Onderbouwing: De gemaakte keuzes in het ontwerp zijn onderbouwd met steekhoudende argumenten, waarbij rekening is gehouden met bijv. ethiek, privacy en security.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W3',
                'description' => 'Realiseert (onderdelen van) software',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 2',
                        'rows' => [
                            [
                                'name' => 'T5',
                                'required' => true,
                                'criteria' => 'Gerealiseerde user stories*: Er is voldoende inhoud van de user stories gerealiseerd binnen de gestelde/geplande tijd.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T5',
                                'criteria' => 'Kwaliteit opgeleverde functionaliteiten: De opgeleverde functionaliteiten voldoen aan de eisen en wensen zoals omschreven in de betreffende user story.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T5',
                                'required' => true,
                                'criteria' => 'Kwaliteit code*: De kwaliteit van de code is goed. Dit uit zich onder andere in: OOP, objectstructuur, MVC, validatie, efficiëntie, foutafhandeling en terugkoppeling, security (veilig programmeren).',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T5',
                                'criteria' => 'Code conventions: De code is gestructureerd opgesteld volgens code conventions.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T5',
                                'criteria' => 'Verzorging code: De code is verzorgd, leesbaar, gestructureerd en voorzien van zinvol commentaar.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T5',
                                'criteria' => 'Versiebeheer: Versiebeheer is effectief toegepast.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W4',
                'description' => 'Test software',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 3',
                        'rows' => [
                            [
                                'name' => 'T6',
                                'criteria' => "Testplan: De testcases in het testplan sluiten aan op de user stories en bevatten alle scenario's.",
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T6',
                                'criteria' => 'Testscenario: De stappen, het gewenste resultaat en testdata zijn benoemd. Niet alleen het hoofdscenario, maar ook alternatieve scenario’s.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T6',
                                'criteria' => 'Testrapport: Het testrapport bevat de juiste resultaten en conclusies.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K1-W5',
                'description' => 'Doet verbetervoorstellen voor de software',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 4',
                        'rows' => [
                            [
                                'name' => 'T9',
                                'criteria' => 'Testen: De juiste verbetervoorstellen zijn gedaan vanuit het testen.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T9',
                                'criteria' => 'Oplevering: De juiste verbetervoorstellen zijn gedaan vanuit de oplevering.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T9',
                                'criteria' => 'Reflectie: De juiste verbetervoorstellen zijn gedaan vanuit de reflectie.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                    [
                        'name' => 'Beoordeling opdracht 6',
                        'rows' => [
                            [
                                'name' => 'T10',
                                'required' => true,
                                'criteria' => 'Authenticiteit*: De examinator(en) is/zijn overtuigd van de authenticiteit van het gemaakte werk van de kandidaat.',
                                'cells' => ['Nee', 'Ja'],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    [
        'name' => 'B1-K2',
        'code' => 'SD_SD20-PE1_B1-K1-2_1v1',
        'description' => 'Werkt in een ontwikkelteam',
        'processes' => [
            [
                'name' => 'B1-K2-W1',
                'description' => 'Voert overleg',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 1',
                        'rows' => [
                            [
                                'name' => 'T1, T2, T3',
                                'criteria' => 'Actieve deelname: De kandidaat neemt actief deel waarbij relevante onderwerpen worden ingebracht en de juiste vragen worden gesteld.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T1, T2, T3',
                                'criteria' => 'Afstemmen: De kandidaat stemt regelmatig en tijdig af met projectteamleden en opdrachtgever over de voortgang en eventuele knelpunten.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T1, T2, T3',
                                'criteria' => 'Afspraken vastleggen: De gemaakte afspraken zijn eenduidig vastgelegd.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T1, T2, T3',
                                'criteria' => 'Afspraken nakomen: De kandidaat houdt zich aan gemaakte afspraken.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K2-W2',
                'description' => 'Presenteert het opgeleverde werk',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 2',
                        'rows' => [
                            [
                                'name' => 'T7',
                                'criteria' => 'Presentatie: De kandidaat presenteert een overtuigend, duidelijk, beargumenteerd verhaal, afgestemd op de doelgroep.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T7',
                                'criteria' => 'Informeren betrokkenen: De kandidaat stelt gerichte vragen om te controleren of de betrokkenen goed geïnformeerd zijn over het opgeleverde werk.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T7',
                                'criteria' => 'Reactie op feedback: De kandidaat reageert adequaat op feedback.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'B1-K2-W3',
                'description' => 'Reflecteert op het werk',
                'rubrics' => [
                    [
                        'name' => 'Beoordeling opdracht 3',
                        'rows' => [
                            [
                                'name' => 'T8',
                                'criteria' => 'Feedbackproces: De kandidaat benoemt zowel positieve als verbeterpunten van het proces van zowel eigen als teamprestaties.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T8',
                                'criteria' => 'Reactie op feedback: De kandidaat reageert adequaat op de ontvangen feedback.',
                                'cells' => $points,
                            ],
                            [
                                'name' => 'T8',
                                'criteria' => 'Proactieve houding: De kandidaat heeft een proactieve houding tijdens reflectiemeetings.',
                                'cells' => $points,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
