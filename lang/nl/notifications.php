<?php

return [
    'exam' => [
        'store' => 'Examen aangemaakt',
        'update' => 'Examen bijgewerkt',
        'destroy' => 'Examen verwijderd',
    ],
    'examiner' => [
        'store' => 'Examinator aangemaakt',
        'update' => 'Examinator bijgewerkt',
        'destroy' => 'Examinator verwijderd',
    ],
    'execution' => [
        'store' => 'Uitvoering aangemaakt',
        'update' => 'Uitvoering bijgewerkt',
        'destroy' => 'Uitvoering verwijderd',
    ],
    'group' => [
        'store' => 'Groepsindeling aangemaakt',
        'update' => 'Groepsindeling bijgewerkt',
        'destroy' => 'Groepsindeling verwijderd',
    ],
    'student' => [
        'store' => 'Student aangemaakt',
        'update' => 'Student bijgewerkt',
        'destroy' => 'Student verwijderd',
    ],
    'user' => [
        'store' => 'Gebruiker aangemaakt',
        'update' => 'Gebruiker bijgewerkt',
        'destroy' => 'Gebruiker verwijderd',
    ],
    'profile' => [
        'save' => 'Profiel bijgewerkt',
        'change-password' => 'Wachtwoord gewijzigd',
    ],
];
