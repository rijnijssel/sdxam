<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'nav' => [
        'login' => 'Inloggen',
        'logout' => 'Uitloggen',
        'account' => 'Account',
    ],

    'failed' => 'Deze combinatie van e-mailadres en wachtwoord is niet geldig.',
    'throttle' => 'Te veel mislukte loginpogingen. Probeer het over :seconds seconden nogmaals.',

    'login' => [
        'title' => 'Inloggen',
        'email' => 'E-mailadres',
        'password' => 'Wachtwoord',
        'remember-me' => 'Onthouden',
        'submit' => 'Log in',
        'forgot-password' => 'Wachtwoord vergeten?',
    ],

    'reset' => [
        'title' => 'Wachtwoord herstellen',
        'email' => 'E-mailadres',
        'submit' => 'Verstuur herstellink',
    ],
];
